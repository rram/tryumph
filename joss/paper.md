---
title: 'TryUMph: A triangle mesh generator for planar evolving geometries'
tags:
  - C++
  - meshing
  - finite element method
  - moving boundary problems
  - immersed geometries
authors:
  - name: Ramsharan Rangarajan 
    orcid: 0000-0001-7403-7728
    equal-contrib: false
    affiliation: "1" 
  - name: Adrian Lew 
    orcid: 0000-0001-7403-7728
    affiliation: 2
affiliations:
 - name: Mechanical Engineering, Indian Institute of Science Bangalore, India
   index: 1
 - name: Mechanical Engineering, Stanford University, USA
   index: 2
date: 06 May 2024
bibliography: paper.bib

---

# Summary
<!--TryUMph is a C++ implementation of a novel algorithm for computing
sequences of planar triangle meshes conforming to non-smooth evolving
curvilinear geometries.  TryUMph automatically adapts a persistent
background mesh for each geometry instance and achieves conformity by
computing a connectivity-preserving injective transformation.-->
TryUMph is a C++ implementation of a novel triangulation algorithm for
planar nonsmooth curvilinear geometries based on the paradigm of
universal meshes. TryUMph provides two mesh generation modes.  Both
modes permit geometries composed of piecewise $C^2$-regular curves
intersecting transversally at corners and, therefore, include a
general class of boundaries, interfaces, and cracks of interest in
engineering applications. The first mode computes an adaptively
refined background mesh encompassing a given geometry and a
connectivity-preserving injective map that transforms the background
mesh into a conforming one.  Notably, the library achieves conformity
without seeding the mesh with vertices and edges along the
geometry. The second mode generates a sequence of triangle meshes
conforming to an evolving geometry by automatically adapting a
persistent background (i.e., universal) mesh. The library neither
limits the extent of change nor the topology of the geometry during
its evolution. TryUMph thus provides a robust and automatic tool to
compute meshes in numerical simulations of moving boundary problems
without resorting to ad hoc remeshing operations. In fact, TryUMph can
drastically simplify specialized formulations proposed to accommodate
grids that do not conform to the geometry in such simulations.  In the
case of incrementally evolving geometries typically realized in
simulations, TryUMph attempts to retain the background mesh unchanged
over successive geometry instances, thus providing tangible
algorithmic benefits.


# Statement of need

TryUMph serves two crucial purposes in numerical simulations of
physical models arising in engineering, applied mathematics, and
computer graphics.  Firstly, it is a standalone mesh generator that
admits a significant class of planar geometries. Secondly, it provides
a tool to automatically generate meshes conforming to planar evolving
geometries in simulations of moving boundary problems.


*Unstructured mesh generation.* A broad class of numerical techniques
and finite element methods, in particular, rely on unstructured mesh
generation. All general-purpose finite element software require and count with
meshing capabilities to generate  meshes of triangles [@comsol; @abaqus;
@ansys; @alnaes2015fenics; @arndt2021deal; @hypermesh]. Open-source
libraries to generate meshes of triangles are widely used as well
[@boissonnat2000triangulations;@shewchuk1996triangle;@geuzaine2009gmsh].
TryUMph is a useful addition to the latter. It complements the
existing suite of libraries in numerous ways. It implements a
universal mesh algorithm that is novel both in principle and
implementation. It is hence distinct from ubiquitous Delaunay
triangulation-based libraries. By design, TryUMph does away with the
need to seed meshes with vertices and edges to enforce geometric
conformity. Instead, TryUMph uses intrinsic geometric information
through signed distance functions and is therefore well-suited for
curvilinear geometries.



<!--In fact, *preprocessing* to set up a finite element simulation is
virtually synonymous with mesh generation. In this context, TryUMph
complements the existing suite of libraries available to generate
planar triangle meshes. The *universal mesh* algorithm implemented by
the library is novel in principle and implementation. For instance,
TryUMph does not require Hence, TryUMph provides finite element
practitioners a useful alternative to Delaunay-based triangulation-->


*Simulating moving boundary problems.* While useful as a standalone
mesh generator, TryUMph is primarily intended to benefit finite
element simulations of moving boundary problems that have applications
spanning fluid-structure interactions, shape/topology optimization,
and fracture mechanics.  A distinctive feature of such problems is
that in addition to field variables, geometries are also critical
unknowns. For instance, when simulating crack propagation in a
material, it is necessary to synchronously compute the displacement
field as well as the shape and extent of the crack. In principle, such
simulations require regenerating the mesh at each time increment or
solution iteration and do not afford any scope for user
interactivity. Moving mesh techniques alleviate the difficulty by
advecting the mesh to accommodate small changes in the geometry
[@budd2009adaptivity]. <!--@takashi1994ale; --> In general, though,
remeshing is inevitable. Nevertheless, automatic and robust remeshing for moving
boundary problems remains elusive. This challenge inspired
immersed boundary methods, cut-cell methods, phase field methods,
extended finite element methods, and other related techniques to
propose formulations that accommodate meshes that do *not* conform to
the geometry, see [@mittal2005immersed; @burman2015cutfem;
@stolarska2001modelling; @amiri2014phase] for representative
references.

![To mesh a geometry, TryUMph implements an iterative algorithm to
construct an adaptively refined background mesh and its injective
transformation to a conforming mesh. The geometry in the example shown
is composed of 10 features. The collection of positively cut triangles
and edges highlighted alongside the features play a crucial role in
defining the universal mesh map.  The first try fails because a
necessary condition on the topology of positive edges is violated. The
algorithm refines the quadtree, and in turn, the background mesh,
tries and fails again. The quadtree is further refined in the third
try, and an injective universal mesh map is successfully constructed
to transform the background mesh into a conforming
one.\label{fig1}](smiley.png){width=100%}



The paradigm of universal meshes helps address the challenge of
meshing evolving geometries [@rangarajan2014universal]. <!--The key
idea lies in constructing a sequence of injective transformations over
background meshes to compute meshes conforming to the evolving
geometry.--> The TryUMph library provides a robust implementation of a
recently introduced universal mesh algorithm and offers users a
dedicated meshing tool for simulating moving boundary problems,
*rather than a new numerical method*.

*Progress in universal meshes.* Universal meshes introduced a new
perspective to triangle mesh generation [@rangarajan2014universal]. It
posed the problem as one of constructing a background mesh and
computing an injective transformation of it.  Since its inception
about a decade ago, numerous improvements and applications of
universal meshes have been proposed [@rangarajan2015simulating;
@gawlik2015high; @grossman2019algorithm; @sharma2019shape;
@chiaramonte2020numerical].  TryUMph took shape alongside these
advances. The algorithm implemented in the library represents the most
recent progress on the topic, including automatic mesh sizing and the
capability to handle non-smooth geometries
[@rangarajan2024tryumph]. The library is named after the iterative
nature of the algorithm it implements. A *try* in TryUMph is an
attempt at a solution- to construct a background mesh and compute an
injective *universal mesh map*. If unsuccessful, the algorithm locally
and adaptively refines the background mesh and tries again.  That the
algorithm eventually terminates remains to be proven but is supported
by several partial results [@rangarajan2013analysis;
@rangarajan2017provably].


![A distinctive feature of TryUMph is its capability to reuse the same
background mesh (called a universal mesh) to triangulate successive
instances of an evolving geometry. In the example shown, the evolution
corresponds to a rotation of some of the
features. \label{fig2}](rotating-smiley.png){width=100%}

# Capabilities
TryUMph provides two modes for generating meshes:

- Meshing a given geometry. The first triangulates a given geometry by
		constructing a background quadtree/mesh with commensurate
		refinement, see \autoref{fig1}. Successful execution yields:
	
	i. an adaptively refined quadtree whose tessellation with acute
	triangle is the background mesh,
	ii. a mesh conforming to the geometry, and 
	iii. a detailed set of correspondences between
	triangles/edges/vertices of the conforming mesh and the geometry.
	
	The three outputs are closely related. The conforming mesh differs
    from the background mesh only in the locations of vertices near
    the geometry. In particular, the two meshes have identical
    connectivity. This feature is significant for the second mesh
    generation model discussed next. The mesh-geometry correspondences
    are determined during construction of the transformation mapping
    the background mesh to the conforming one.

- Meshing a sequence of geometries.  The second model uses a
  persistent quadtree/background mesh to triangulate not one but a
  sequence of geometries, see \autoref{fig2} and \autoref{fig3}.  The
  idea herein lies in using the existing quadtree/background mesh as
  *an initial guess* for mesh generation, i.e., for the first
  *try*. The benefit of this functionality becomes apparent when
  computing a sequence of meshes conforming to an *incrementally*
  evolving geometry, which is usually the case in numerical
  simulations of moving boundary problems. When successful in reusing
  the background mesh for successive geometry instances, the mesh
  topology remains unchanged in the simulation over many time steps or
  solution iterations, despite the meshes conforming to different
  geometries. This feature of the algorithm can be gainfully exploited
  in numerical simulations to reduce overheads in allocating and
  distributing data structures.

A critical aspect of TryUMph concerns the class of admissible
geometries and their representation in the library.

- The library accommodates geometries composed of piecewise $C^2$
  curves, termed as *features*. Each feature is finite, open, and
  simple. The endpoints of features are termed *corners*.  At most two
  features may intersect transversally at a corner. TryUMph hence
  permits a large class of geometries (domain boundaries, interfaces,
  cracks) of interest in engineering applications.
  
- TryUMph is agnostic to the method adopted to represent geometries
	(e.g., parametrically with splines or implicitly as level
	sets). Instead, it requires routines to (i) compute signed
	distance functions to features and (ii) sample features. This
	provides considerable freedom to developers interested in
	integrating TryUMph with simulation codes having
	algorithm-specific representations.
	
- TryUMph estimates local mesh sizes by computing distances to the
  medial axes of features. For the purpose of guiding mesh refinement,
  an approximate sampling of the medial axes suffices. To this end,
  the library implements the shrinking ball algorithm [@ma20123d].
  <!--using sample points on features.--> Here again, medial axes
  calculations do not assume a specific representation for geometries.
  
- Signed distance functions to features guide the conformity of
  computed meshes to the geometry.  <!--Specifically, TryUMph
  constructs background mesh transformations using the signed distance
  functions provided.--> In particular, TryUMph does not sample the
  geometry to enforce conformity. Nor does it treat the geometry as a
  constraint by seeding the mesh with vertices/edges along features
  [@bern1995mesh; @chew1987constrained].  <!--We note that sample
  points on the geometry play no role in this context. TryUMph does
  not treat the geometry as a constraint when meshing. For instance,
  the mesh is not pre-seeded with , as is often done in Delaunay-based
  algorithms. -->

- The universal mesh algorithm implemented in TryUMph does not limit
  the extent of change or the topology of the geometry during its
  evolution. In fact, successive geometry instances need not be
  related in any sense. Nevertheless, the benefit of maintaining
  persistent background meshes becomes apparent in the case of
  geometries undergoing incremental changes.

  <!--In a typical numerical simulation of a moving boundary problem,
the geometry undergoes small changes at the end of each timestep or
solution iteration.-->


<!-- 
- Unlike conventional triangulation algorithms (e.g., Delaunay,
advancing front), TryUMph transforms background meshes to conform to
immersed geometries while preserving element connectivities. It does
so without resorting to remeshing techniques (e.g., trimming cells,
edge-flips). achieves conformity by projecting a select collection of
nodes of the background mesh onto the immersed geometry.-->


The next aspect of TryUMph we highlight concerns algorithmic
parameters and user control over mesh generation.

- TryUMph is fully automatic and does not rely on user interactivity.
  It requires specifying a set of algorithmic parameters, none of
  which require meticulous tuning or drastically alter its
  performance. Most parameters relate to mesh relaxation using DVRlib
  [@rangarajan2020dvrlib].
  
- TryUMph provides users control over mesh refinement by providing
  access to quadtree refinement routines. This capability can be
  exploited to guide mesh refinement based not only on geometric
  criteria, as TryUMph does automatically, but also based, for
  instance, on error estimates in numerical simulations.
  
- The library implements a test suite to examine necessary conditions
  required to construct an injective universal mesh map. The results
  of these tests provide detailed diagnostics to analyze mesh
  generation *trys*.
  
Finally, TryUMph  provides:

- Freedom in the choice of the mesh data structure. Recognizing that
  scientific computing codes adopt varied representations for meshes,
  functionalities in the library are templated by the mesh type. For
  convenience, a simple data structure is provided and has been used
  testing and examining performance.

- An elaborate set of utilities for visualizing geometries, meshes,
  and their correspondences computed by TryUMph.
  
- Implementations of linear, quadratic, and cubic spline geometries
  for the purpose of performance tests as well as to demonstrate the
  calculation of signed distance functions to parametric curves.
  

It is essential to note functionalities that the library does not
provide. **TryUMph does not**:

- handle dirty geometry: for example, features with self-intersections, or
  distinct features intersecting at points that are not their common
  corners.

![Examining the extent of background mesh reuse for geometries
undergoing incremental rigid body rotations in steps of $1^\circ$. The
plots show the number of *trys* required to mesh successive geometry
instances. Notice the dominance of green spokes indicating background
mesh reuse.  \label{fig3}](rigid-trys-roseplot.png){width=100%}

- support geometries with cusps, i.e., features with non-transverse intersections.
- support geometries with more than two features intersecting at a corner (e.g., triple-junctions).
- support interactive mesh editing.
- generate non-simplicial meshes (e.g., quadrilaterals).


These observations clarify that TryUMph is not intended to be a
general-purpose meshing library. Instead, it is designed to aid
simulation techniques for moving boundary problems.


# Performance
The documentation pages of the library record several metrics to
examine TryUMph's performance. These include:

- element qualities in computed meshes,
- number of *trys* required for convergence of the meshing algorithm for
  a large collection of geometries,
- runtimes as a function of vertex/element count,
- records of modes of failure in unsuccessful *trys*,
- mesh refinement in the vicinity of the geometry compared to
  geometric estimates,
- consequences of estimating mesh refinement based on medial axes
  computed feature-wise (local refinement) versus geometry-wide (non
  local refinement),
- the extent of background mesh reuse for incrementally evolving
  geometries (see \autoref{fig3}),
- algorithmic efficiency resulting from background mesh reuse (e.g.,
  fewer *trys*, lower runtimes).

Perhaps the single most important performance metric not mentioned
above is the successful termination of the algorithm. Lacking a
mathematical guarantee of termination, TryUMph it is backed up by
extensive testing. The library has been tested on:

- a set of 570 different geometries accessed from the database
provided by [@hu2019triwild],
- sequences of rigid rotations and nonrigid deformations of varied
  geometries, yielding an additional set of 8640 geometry instances.

TryUMph successfully computes a mesh in every case using an identical
set of nominal algorithmic parameters.  Of course, this does not
suffice to claim robustness. Yet, each successful test effectively
verifies a necessary condition.

<!--The possibility of persisting with the same background mesh to compute
triangulations conforming to successive realizations of an evolving
geometry highlight the reason why the background mesh is a critical
output of TryUMph.-->


# Outlook

- TryUMph is an integral part of our ongoing research on numerical
  techniques to simulate hydraulic fracturing and fluid-structure
  interactions. The capabilities and robustness of the universal
  meshes algorithm and its implementation in TryUMph will improve
  alongside these studies.
- Integrating TryUMph with special-purpose modules for simulating
  moving boundary problems in open-source finite element codes will
  make its capabilities accessible to a broader research community.
- A three-dimensional version of the universal meshes algorithm has
  been proposed [@rangarajan2019algorithm], but severely restricts the
  set of admissible geometries. Generalizing the capabilities of
  TryUMph to tetrahedral meshing will be daunting and invaluable in
  equal measure.

# References
