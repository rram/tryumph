# Universal Meshes {#aboutum}

### Overview of the UM algorithm 

### Feature immersed in a background mesh

### Corner perturbations 

###  Positively cut triangles, positive edges and positive vertices

### Parameterizing features over positive edges

### Vertex relaxation with DVR

### Quadtrees for background meshes
