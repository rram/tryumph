# Get Started {#getstarted}

**Install the dependencies**:
- C++17 compiler (>=GCC 8, >= Clang 5)

- [CMake](https://cmake.org) version 3.19 or later (Mac: `brew install
  cmake`, Ubuntu: `sudo apt install cmake`)

- [Boost](https://www.boost.org) (Mac: `brew install boost`, Ubuntu: `sudo apt install libboost-all-dev`)

- [GNU Scientific Library (GSL)](https://www.gnu.org/software/gsl/) required by dvrlib. (Mac: `brew install gsl`, Ubuntu: `sudo apt install libgsl-dev`) 

- [DVRlib](https://bitbucket.org/rram/dvrlib/src):
  Open-source library for mesh relaxation.
  ```sh
  git clone https://rram@bitbucket.org/rram/dvrlib.git
  cd dvrlib && mkdir build && cd build 
  cmake .. && make -j
  sudo make install
  ```

	**Note**: TryUMph dpes not currently support thread-level
	parallelism even though DVRlib does (using OpenMP). OpenMP is not
	requires to use TryUMph.
  
  
- Optional: [Paraview](https://paraview/org) for mesh visualization
- Optional: [Doxygen](https://doxygen.nl) to generate documentation

**Clone the repository**:
```sh
git clone https://rram@bitbucket.org/rram/TryUMph.git
```

**Configure and build**:
```sh
cd TryUMph && mkdir build && cd build && cmake .. && make -j
```

**Install (to a default location)**:
```sh
sudo make install 
```

### Build options

Specify a custom installation path:
```sh
cmake ../ -DCMAKE_INSTALL_PREFIX=destination-folder
```

Build without unit tests (default=ON):
```sh
cmake ../ -DBUILD_TESTS=OFF
```


Run unit tests:
```sh
ctest -j
```

Build documentation (default=OFF, requires  [Doxygen](https://doxygen.nl)):
```sh
cmake ../ -DBUILD_DOCS=ON
make docs
```

### Build tutorial examples

Tutorial-style examples discussed in the @ref tutorial page are
provided in the `tutorial/` folder of the repository. The tutorial is
built as an independent project:

```sh
cd tutorial && mkdir build && cd build && cmake ../ 
make -j
```

Run the examples:
```sh
./hello_tryumph
./rotating_smiley
```


### Quick look at meshing with TryUMph
```cpp

```

### Link TryUMph to a project

Find the TryUMph package:
```
# add to CMakeLists.txt
find_package(TryUMph)
```
- if TryUMph is installed in a custom location, use the `CMAKE_MODULE_PATH` flag to specify the path
- `find_package(TryUMph)` will automatically look for its dependencies (Boost, GSL and DVRlib).

Result variables:  `TRYUMPH_INCLUDE_DIRS, TRYUMPH_LIBRARY_DIRS, TRYUMPH_LIBRARIES`
	
Link TryUMph to the target:
```
# add to CMakeLists.txt
target_include_directories(TARGET PUBLIC ${TRYUMPH_INCLUDE_DIRS})
target_link_directories(TARGET PUBLIC ${TRYUMPH_LIBRARY_DIRS})
target_link_libraries(TARGET PUBLIC ${TRYUMPH_LIBRARIES})
```
