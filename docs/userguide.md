# User guide {#userguide}

In the following, we discuss:
- **Specifying the set of inputs**:
	- the geometry using the um2::FeatureParams struct
	- algorithmic parameters to approximately sample the medial axes of the geometry using the um2::MedialAxisParams struct
	- a square-shaped bounding box for the mesh using the um2::Box struct
	- a set of parameters required by the meshing algorithm using the
  um2::MeshingParams struct.

- **Computing meshes**: <br>
  - create a set of features using the um2::FeatureSet class
  - using the um2::MeshManager class to compute
	  - a mesh conforming to a given geometry
	  - meshes conforming to a sequence of geometries
	    
- **Accessing the output**:
	- the mesh conforming to the geometry using the um2::WorkingMesh
    class. 
	- the background mesh constructed/adapted for a geometry using a
      template mesh type (e.g., using the um2::TriangleMesh class).
	- the quadtree underlying the background mesh using the
      um2::Quadtree class.
	
---

## 1. Namespaces

- All core functionalities of TryUMph are provided under the `um2`
namespace- a reference to *universal meshes in
two-dimensions*.

- Additional utilities related to defining specific geometries and
  mesh visualization utilities are provided under the `um2::utils`
  namespace. These utilities are primarily intended for testing and
  have been used in all the \ref performance tests.

---

## 2. Inputs
### A. Geometry
#### Features
A *feature* in TryUMph is a simple, open and twice-continuously
differentiable (\f$C^2\f$) curve. The geometry in TryUMph is specified
as a collection of features in which pairs of features either do not
intersect at all, or intersect transversely at common endpoints.

![](feature.png)

#### Feature details with the um2::FeatureParams struct
Properties of a feature required by the library are encapsulated by
the um2::FeatureParams struct:
```cpp
struct FeatureParams {
	// unique integer-valued identifier
	int curve_id;

	// Cartesian coordinates of the left corner, the right corner, and an interior point on the curve
	std::array<double,2> leftCnr, rightCnr, intPoint;
	
	// Function to compute the signed distance(sd) and its gradient(dsd) at a point X
	// [in]    X: Cartesian coordinates of point
	// [out]  sd: signed distance
	// [out] dsd: gradient of the signed distance
	std::function<void(const double *X, double &sd, double *dsd)> signed_distance_function; 

	// Function to sample the feature
	// return: (Unordered) List of sample points on the feature
	std::function<std::list<std::array<double,2>>()> sample_feature();
};
``` 

Each feature is specified by an instance of
um2::FeatureParams. Referring to the curve \f$\gamma\f$ in the figure
above for example, the five members of an instance, say `fp`, define its
properties:
```cpp
fp.leftCnr  = {A[0],A[1]};
fp.rightCnr = {B[0],B[1]};
fp.intPoint = {C[0],C[1]};
```

![](curve.png)

The member `fp.signed_distance_function(x, sd, dsd)` is required to
compute the *signed distance to the closest point projection onto*
\f$\gamma\f$ , and its gradient.  Specifically, \f${\rm sd} =
\phi({\bf x})\f$ and \f${\rm dsd} = {\bf n}(\pi({\bf x}))\f$, where
- \f$ \phi({\bf x}) =({\bf x}-\pi({\bf x}))\cdot {\bf n}({\pi}({\bf
x})),\f$
- \f$ \pi({\bf x}) = \arg \min_{{\bf
y}\in\gamma}\|{\bf x}-{\bf y}\|\f$ is the closest point projection
onto \f$\gamma\f$, and 
- \f${\bf n}\f$ the unit outward normal to \f$\gamma\f$.
 

The member `fp.sample_feature()` is required to return a list of
distinct sample points on the feature, i.e., a list of the form \f$\{{\bf y}_i\in
\gamma\}\f$. The list need not be ordered.
  
#### Examples
Definitions of feature parameters for a representative collection of
curves is provided in the library under the `um2::utils`
namespace. The classes um2::utils::LineSegment,
um2::utils::QuadraticRationalBezier and um2::utils::CubicBezier
initialize instances of um2::FeatureParams for features that are line
segments, quadratic rational Bezier curves, and cubic Bezier curves,
respectively. These classes implement methods to return sample points
and to compute signed distances.  These classes are used in all the
unit tests and in the performance tests discussed in @ref performance.

Additionally, the @ref tutorial page discusses strategies to define
instances of the struct using the example of a line segment and a
circular arc.

---

### B. Medial axis sampling
TryUMph estimates the local refinement required for the background
mesh using distances to medial axes of features.  To this end, it
approximately samples of medial axes of features, as well as of the
collection of features using the shrinking ball algorithm \cite @Peters16. 
The figure below shows a representative calculation. The feature
\f$\gamma\f$ depicted in blue, is a cubic spline with end points
\f$A\f$ and \f$B\f$. The computed sample points on the medial axis of
\f$\gamma\f$ are indicated by the sets \f$\Upsilon^-\f$ and
\f$\Upsilon^+\f$. Points in \f$\Upsilon^-\f$ and \f$\Upsilon^+\f$ lie
on different sides of the curve, as distinguished by the orientation
of the curve.

![An approximate sampling of the medial axis of a feature](farm-medial.pdf)

The algorithmic parameters required to compute the medial axis
sampling are specified using an instance of the um2::MedialAxisParams
struct. By requiring only non dimensional parameters, the same
instance is used to compute medial axis samples for all features.

```cpp
struct MedialAxisParams {
  // non dimensional distance for subsampling points on the feature
  double exclusion_radius; 

  // non dimensional upper bound for the distance to the medial axis
  double maximum_radius;    

  // non dimensional tolerance for convergence of medial ball radii in the shrinking ball algorithm
  double eps_radius;
};
```

To compute medial axis samples for the feature \f$\gamma\f$, TryUMph:
- invokes the `sample_feature()` method, which returns a set of points, say \f$S_\gamma\f$, sampling \f$\gamma\f$.
- computes an axis-aligned rectangular bounding box for \f$S_\gamma\f$
  to find the length of its longer edge \f$\ell_\gamma\f$.
- determines a well-separated subset of points \f$S_\gamma^\delta\subseteq S_\gamma\f$ such that 
\f[ {\bf x},{\bf y}\in S_\gamma^\delta ~\Rightarrow~ \|{\bf x}-{\bf y}\|\geq \delta \ell_\gamma, \f]
where \f$\delta\f$ is the `exclusion_radius`.
- implements the shrinking ball algorithm to sample the medial axis of
\f$\gamma\f$ using the set of points \f$S_\gamma^\delta\f$,  with the
convergence tolerance for iterations to compute radii of medial axis balls set to \f$\ell_\gamma\f$ x `eps_radius`.
- caps the maximum medial ball radius by \f$\ell_\gamma\f$ x
  `maximum_radius`. The bound is required because the distance to the medial axis can become
  unbounded (e.g., along a  straight section of \f$\gamma\f$).
  
The rationale for the parameter `exclusion_radius` is that it helps avoid undefined behavior in the shrinking ball algorithm when sample points in \f$S_\gamma\f$ get arbitrarily close.

The parameter choices are expected to be such that:
- \f$\ell_\gamma\f$ x `eps_radius` is small compared to the radius of curvature of \f$\gamma\f$.
- \f$\ell_\gamma\f$ x `maximum_radius` is comparable to diameter of \f$\gamma\f$.
- \f$\ell_\gamma\f$ x `exclusion_radius` is small compared to ?

---

### C. Bounding box for the mesh

The um2::Box struct defines the extent of the square-shaped bounding
box over which background and conforming meshes are computed:

```cpp
  struct Box {
    // center of the square
	std::array<double,2> center;
    
	// size of the square
	double size;
  };

```

--- 
### D. Algorithmic parameters
The set of meshing-related parameters is specified using the
um2::MeshingParams struct:

```cpp
 struct MeshingParams {
    bool   nonlocal_refinement;        // whether to size the mesh using the medial axis of the entire geometry
    double refinement_factor;          // mesh refinement factor relative to geometry-based estimates
    int    num_proj_steps;             // #steps over which to project positive vertices onto the geometry
    int    num_dvr_int_relax_iters;    // #DVR iterations for unconstrained mesh relaxation near the geometry
    int    num_dvr_int_relax_dist;     // neighbor distance defining the mesh relaxation radius around the geometry
    int    num_dvr_bd_relax_iters;     // #DVR relaxation iterations for vertices lying on the geometry
    int    num_dvr_bd_relax_samples;   // #sample points to use for geometry-constrained relaxation in DVR
    double min_quality;                // positive lower bound for element quality
    int    max_num_trys;               // max number of trys
  };
```

The first two parameters relate to initial refinement of the background mesh:
- `nonlocal_refinement`: If *false*, the background mesh is refined in
  the vicinity of the geometry based on the medial axis computed
  *feature-wise*. If *true*, TryUMph additionally refines the mesh
  using the medial axis computed for the entire geometry (excluding
  small regions around corners). Turning off nonlocal refinement
  usually results in coarser meshes, but at the expense of additional
  iterations of the algorithm.
  
- `refinement_factor`: provides coarse control over the mesh size in
the vicinity of the geometry.  The parameter defines the mesh
refinement relative to the local mesh size estimated for the geometry
based on medial axis calculations. A larger value yields more
refinement.  Though not enforced, use a factor greater than or equal
to 1 so that the initial refinement of the background mesh is
commensurate with distances to medial axes. Choosing a factor smaller
than 1 will likely cause difficulties in computing signed distances to
features.
  
  
  
TryUMPh projects a subset of background mesh vertices, called
`positive vertices` onto features to achieve conformity. These
projections can result in triangles becoming poorly shaped, or even a
tangled mesh. Hence, positive vertices are projected towards the
geometry in small increments, while relaxing the mesh in the vicinity
at the end of each increment.


- `num_proj_steps` defines the number of steps over positive vertices
in the background mesh are incrementally projected towards the
geometry. Choosing too few steps can result in failed iterations in
the algorithm because of poor element qualities caused by vertex
projections. Choosing too many steps results in unnecessary mesh
smoothing iterations, and in turn, higher execution times.


*DVR-related parameters*: TryUMph uses the directional vertex
relaxation (dvr) algorithm implemented by
[DVRlib](https://bitbucket.org/rram/dvrlib/src/master/) for relaxing
the mesh in the vicinity of the geometry. TryUMph uses two versions of the
algorithms implemented by DVRlib:
- unconstrained vertex relaxation along horizontal and vertical
directions for vertices in the vicinity of the geometry, excluding the
set of positive vertices.
- constrained vertex relaxation for positive vertices once they are
  projected onto the geometry.

Four mesh relaxation-related parameters required by DVRlib are:
- `num_dvr_int_relax_iters` specifies the number of  unconstrained relaxation iterations executed after each incremental
  perturbation of positive vertices towards the geometry.
- `num_dvr_int_relax_dist` indicates the neighborhood size around the
  geometry in which vertices are relaxed. Roughly speaking, the
  parameter indicates the number of *layers* of vertices around the
  geometry that are relaxed.
- `num_dvr_bd_relax_iters` specifies the number of relaxation
iterations for positive vertices.
- `num_dvr_bd_relax_samples` indicates the number of locations on the
geometry to examine when optimizing the location of each positive
vertex.


The parameter `min_quality` in the range (0,1) specifies a lower bound
for element qualities measured using the mean ratio metric: \f[Q(K) =
4\sqrt{3} \frac{\Delta(K)}{a^2+b^2+c^2},\f] where \f$\Delta(K)\f$ is
the signed area measure of the triangle \f$K\f$ and \f$a,b,c\f$ are
its edge lengths. If an element's quality falls below this threshold,
the algorithm triggers a failed iteration, leading to local mesh
refinement. *The parameter is not a means to improving mesh
quality*. Rather, it serves as a tolerance that ensures the integrity
of the computed mesh during interim iterations.  Using a large value
can even result in non termination of the algorithm. Specifying a
small value *does not* necessarily yield a mesh of poor quality.  

The performance of TryUMph is supported by a number of partial
mathematical results. At this time, however, TryUMph lacks a proof of
termination.  Hence, the parameter `max_num_trys` limits the number of
meshing interations in the algorithm.


The table below lists parameter choices used in the \ref performance
tests:
<center>
|Parameter | Value |
:-----------:|:-----:|
`nonlocal_refinement` | false |
`refinement_factor` | 1.0 |
`num_proj_steps` | 5 |
`num_dvr_int_relax_iters` | 5 |
`num_dvr_int_relax_dist` | 3 |
`num_dvr_bd_relax_iters` | 3 |
`num_dvr_bd_relax_samples` | 10 |
`min_quality` | 0.01 |
`max_num_trys` | 10 |
</center>

---
### Remarks

- <b>Relation of \f$\phi\f$ to the signed distance function</b>: The
	function \f$\phi\f$ discussed above coincides with the
	conventional [signed distance
	function](https://en.wikipedia.org/wiki/Signed_distance_function)
	to \f$\gamma\f$ over the [tubular
	neighborhood](https://en.wikipedia.org/wiki/Tubular_neighborhood). Within
	this region, the definition of `dsd` coincides with
	\f$\nabla\phi\f$ as well. In this sense, the function \f$\phi\f$
	represents an extension of the signed distance function beyond the
	tubular neighborhood.  <!--In fact, \f$\phi\f$ can be interpreted
	to be the signed distance to an extension of \f$\gamma\f$ by its
	tangents in the vicinity of the endpoints \f${\bf A}\f$ and
	\f${\bf B}\f$. -->


- **Closest point projection**: The definition of \f$\phi\f$ presumes
  that the closest point projection is single-valued. This may not be
  the case at points far away from \f$\gamma\f$. Such scenarios can
  result in unpredictable behavior of in TryUMph, including
  non-termination of the algorithm. It is for this reason that TryUMph
  estimates distances to the medial axis of a feature, refines the
  background mesh commesurately, and evaluates signed distances only
  at vertices in the vicinity of the geometry.
 
- **Purpose of sample points:** The points returned by
  `um2::FeatureParams::sample_feature()` are used for medial axis
  computations.  *They are not used to seed mesh vertices*.

- **Accuracy of medial axis computations**: TryUMph uses medial axes
	computations only to adaptively refine the background mesh. Hence,
	an approximate computation of medial axes suffices and the
	shrinking ball algo is well suited for this purpose.
	
- **Prefer dense sampling:** Nevertheless,
	`um2::FeatureParams::sample_feature()` should return a dense
	sampling of the curve to ensure that the medial axis of each
	feature is well approximated. This has two consequences.
	  - Since TryUMph evaluates signed distances at background mesh
	  vertices in the vicinity of a feature, adequate background mesh
	  refinement ensures that the closest point projection and in
	  turn the signed distance function are well defined at these
	  vertices.
	  - Background mesh refinement based on an accurate medial axis
	  computation ensures that the triangulation algorithm in TryUMph
	  converges in fewer attempts, implying quicker run times.
	
	
 
---

## 3. Computing meshes

### A. Set of features
    
The geometry consisting of a collection of features is
defined by an instance of the class `um2::FeatureSet`
```cpp
std::vector<um2::FeatureParams> feature_params(num_features);

// initialize feature parameters
// ...

um2::FeatureSet fset(feature_params, corner_tol, medial_axis_params);
```

In addition to the vector of feature parameters, the constructor of
the class requires:
- `corner_tol`: the tolerance used to identify distinct
corners. Feature endpoints closer to each other than this tolerance
are deemed to be coincident. The rest are assumed to be distinct.

- `medial_axis_params`: an instance of the struct
`um2::MedialAxisParams` containing parameters required for medial axis
computations, see the discussion above Section 2B.

At construction, the class `um2::FeatureSet` performs geometric
computations required subsequently by the meshing algorithms. These
include estimating medial axis distances for features, enumerating
distinct corners and identifying closed components.

In addition, the class *splits* features and assigns *polarities* to
features, as required in the TryUMph algorithm. More details about
polarities and feature splitting can be found in \cite
rangarajan2024TryUMph.

---

### B. Meshing 
The um2::MeshManager class implements the main functionality of
the library- generating meshes conforming to geometries. 
<!--One instance of the class suffices for meshing an evolving geometry, and is-->
An instance of the class is initialized by providing a bounding box
over which to generate meshes:
```cpp
// bounding box 
um2::Box box(center, size);

// instance of the manager
um2::MeshManager<MeshType> manager(box);
```

In order to provide flexibility in the choice of the mesh data
structure, um2::MeshManager is templated by the mesh type (denoted as
`MeshType` in the snippet). This feature is expected to help use
TryUMph in simulation codes having elaborate application-specific mesh
data structures. The assumptions (traits) on the mesh type are
discussed below in Section 3C.


#### Meshing a geometry

Two overloaded `triangulate()` methods in the class can be invoked for
meshing. The first:

```cpp
// [in] tag: identifier for the geometry-mesh pair
// [in] fset: the geometry as an instance of um2::FeatureSet 
// [in] meshing_params: meshing-related algorithmic parameters
// return: information about execution
RunInfo triangulate (const int num, const FeatureSet &fset, const MeshingParams &meshing_params);
```
implements an algorithm that begins by adapting a persistent quadtree
based on the medial axes of features. The background mesh computed by
tessellating the quadtree serves as the initial guess for computing a
mesh that conforms to the geometry.

This `triangulate()` method is expected to be used to mesh a given
geometry, or the first instance of a sequence of geometries. The
latter is the rationale for requiring an integer-valued identifier
`tag`. The tag serves to label the state of the quadtree and the
background mesh, and is therefore useful in tracking mesh updates. For
instance, if the geometry evolves with time, `tag` can simply be set
to equal the step count. In a shape optimization calculation, the
parameter could be associated with the iteration count tracking the
evolution of the geometry. The tag additionally helps enumerate files
for mesh visualization.

The `triangulate()` method returns an instance of the `um2::RunInfo` struct containing
granular information about the execution of the algorithm:
```cpp
 struct RunInfo {
    bool success;                   // whether triangulation was successful
    int nTrys;                      // total number of iterations of the algorithm
    double runtime;                 // execution time
    std::vector<TryInfo> try_info;  // diagnostics of each iteration
    RunInfo();                      // constructor initializing members
    friend std::ostream& operator << (std::ostream& out, const RunInfo& runinfo); // convenient output to stream
  };

```

- `success`: If true, the `triangulate()` call was
successful. Otherwise, mesh generation failed. Inspect this result
before accessing any output. Meshes accessed from the um2::MeshManager
class are meaningful only if mesh generation was successful.

- `nTrys`: Each iteration in the meshing algorithm is termed a
`try`. The parameter `nTrys` counts the number of iterations of the
algorithm executed by the `triangulate()` method. Note that `nTrys`
cannot exceed the user-specified option `max_num_trys` limiting the
maximum number of trys..

- `runtime`: TryUMph keeps track of the execution time and reports it
through the parameter `runtime`. It is useful when profiling numerical
simulations to evaluate the contribution of mesh generation to the
overall runtime.

- `try_info`: The vector `try_info` of length `nTrys` provides
detailed diagnostics about the execution of the
algorithm. `try_info[i]` reports details of the i-th try, and can be
queried to examine the nature and number of failures encountered.  It
is particularly useful in case mesh generation fails.  Even in case of
a success, it reveals, for instance, the reasons behind the refinement
of the mesh. See the documentation of the struct `um2::TryInfo` for
more details.

#### Meshing a sequence of geometries
The second `triangulate()` method:

```cpp
// [in] prev_tag: Identifier for the previous instance of the geometry-mesh pair
// [in] curr_tag: Identifier for the geometry-mesh pair
// [in] fset: Instance of um2::FeatureSet representing the geometry
// [in] meshing_params: Meshing-related algorithmic parameters
// return: information about execution
RunInfo triangulate (const int prev_tag, const int curr_tag, const FeatureSet &fset, const MeshingParams &meshing_params);

```

generates a sequence of meshes conforming to an evolving
geometry. The method computes a mesh conforming to `fset` by first
attempting to use the existing background mesh associated with state
`prev_tag` as the initial guess in the meshing algorithm. This *0-th*
try, if successful, yields a mesh conforming to `fset` virtually for
free.  The quadtree and background mesh are retained unchanged from
state `prev_tag` to `curr_tag`. The two meshes computed, though
conforming to distinct geometries, have identical connectivity and can
differ only in the locations of their vertices.

It is important to note that the 0-th try just described is *not
equivalent* to invoking the first `triangulate()` method with
`max_num_trys=1`. The distinction lies in the fact that the 0-th try
uses the background mesh of the state `prev_tag` as is, without
attempting to adapt it to the given geometry `fset`. Essentially, the
0-th try is an attempt by the algorithm to *get lucky!*

If, on the otherhand, the 0-th try fails, then the algorithm resorts
to calling the first `triangulate()` method to compute a mesh
conforming to `fset`. However, it does so after pruning the terminal
branches of the quadtree. This step enables the possibility of mesh
coarsening in the algorithm.

#### Assumptions
- Both `triangulate()` require that the geometry provided lies
completely within the bounding box provided. This assumption is not
explicitly checked and violating it can lead to unexpected behavior.
- `triangulate(prev_tag, curr_tag, fset, mesh_params)` requires that the current state
  have tag equal to`prev_tag`.  

#### Example usage
Consider an evolving geometry represented by `fsets` of type
`std::vector<um2::FeatureSet>`, with `fsets[i]` representing the i-th
instance of the geometry in its (discretized) evolution. For
simplicity, we use the index `i` to tag states as well.

A mesh conforming to the geometry `fsets[0]` is computed by invoking
the first `triangulate()` method as 
```cpp
auto result_0 = manager.triangulate(0, fsets[0], meshing_params);
```

Next, a mesh conforming to the geometry `fsets[1]` is computed by
invoking the second `triangulate()` method as 
```cpp
auto result_1 = manager.triangulate(0, 1, fsets[1], meshing_params);
```
which uses the background mesh previously computed for `fsets[0]` to
initiate mesh generation for the new geometry `fsets[1]`. 
Similarly, a mesh conforming to `fsets[n+1]` is computed using the
previous calculation for `fsets[n]` as: 
```cpp
auto result = manager.triangulate(n, n+1, fsets[n+1], meshing_params);
```


The class assumes that `prev_tag` coincides with the state assigned
in the last `triangulate()` call.  For instance, 
```cpp
manager.triangulate(n-1, n, fsets[n], meshing_params);     // ok if the current state is tagged as n-1
manager.triangulate(n-1, n+1, fsets[n+1], meshing_params); // error: prev_tag should be n
```
is not permitted. The rationale for this assumption is the expectation
that the geometry `fsets[n+1]` is *close* to `fset[n]`, i.e.,
the (n+1)-th instance of the geometry is a perturbation of that at the
n-th instance. Hence, the background mesh computed for `fsets[n]`
is likely to be a good initial guess for that required
 for `fsets[n+1]`.  Of course, this need not always be the
 case and *TryUMph does not assume any notion of similarity/closeness
 between successive instances of the geometry*. 
 However, the memory overhead to save the entire history of
background mesh states makes it impractical to consider more general scenarios.

A second point to note is that either `triangulate()` method can be
used to compute meshes conforming to an evolving geometry. For
instance, using the first method as: 
```cpp
manager.triangulate(n+1 fsets[n+1], meshing_params);
```
refines the existing background mesh (whatever its tag) to initialize mesh
generation iterations for `fsets[n+1]`. 
<!--Consequently, the quadtree, and in turn the
background mesh computed for `fset[n+1]` is necessarily only refined
further. -->

On the otherhand, invoking the second method as:
```cpp
manager.triangulate(n, n+1, fsets[n+1], meshing_params);
```
attempts a 0-th try, and if that fails, prunes the quadtree before
initializing meshing iterations for `fsets[n+1]`.  In particular, invoking the first
method to mesh an evolving geometry will only progressively refine
meshes, while the second method attempts to reuse or coarsen meshes
whenever possible.


#### Mesh revision
By maintaining persistent data structures, TryUMph offers the
possibility of *mesh revision* as well. Simply put, the purpose of
successive `triangulate()` calls is not be limited to accommodating
evolving geometries. It also serves to revise the mesh computed for a
given geometry:
```cpp
manager.triangulate(1, fset, mesh_params_1);
manager.triangulate(2, fset, mesh_params_2);    // same geometry, possibly revised mesh
```
In the snippet above, the second `triangulate()` call can be used to
increase the level of mesh refinement, say, by setting
`mesh_params_2.refinement_factor` to be larger.

The um2::MeshManager class provides mutable access to the underlying
quadtree. This feature can be used, for instance, to tailor mesh
refinement based on error estimates in a numerical simulation:
```cpp
manager.triangulate(1, fset, mesh_params_1);
auto& QT = manager.getQuadtree(1);
// ..
// refine/coarsen tree
// ..
manager.triangulate(2, fset, mesh_params_1);  // same geometry, possibly revised mesh
```

When invoking the `triangulate()` method repeatedly:
```cpp
manager.triangulate(1, fset, mesh_params_1);
manager.triangulate(2, fset, mesh_params_1);    // tag updated but meshes remain unchanged
manager.triangulate(2, 3, fset, mesh_params_1); // tag updated but meshes remain unchanged
```
the second and third invocations effectively only serve to update the tags associated with
the state of the persistent data structures. The quadtree, background
mesh and the conforming mesh remain unchanged.

#### Mesh validity
The um2::MeshManager::validiate method examines a suite of necessary
conditions to validate a generated mesh:
```cpp
// [in] tag: Tag associated with the current state
// [in] fset: geometry corresponding to the tag, with which the mesh is expected to conform to
// [in] min_quality: lower bound for element qualities
// [in] distTOL: upper bound on the distances of positive vertices from the geometry
// Return: nothing if all checks pass, asserts otherwise.
void MeshManager::validate(const int num,  const FeatureSet& fset, const double min_quality, const double distTOL) const;
```

The threshold on the element quality used for the validatity test
should not exceed the `meshing_params.min_quality` parameter provided
at the time of the `triangulate()` call. The tolerance `distTOL`
specified on the distances of positive vertices to the geometry should
be commensurate with the accuracy of signed distance function
computations provided by the `signed_distance_function`in the
um2::FeatureParams structs. For simplicity, the test uses the same
tolerance for all features.

### C. User-controlled mesh refinement


---

### D. The mesh data structure

The um2::MeshManager class, as well as a number of other classes and
functions in TryUMph are templated by the mesh type (henceforth
`MeshType`). Instantiating/invoking these requires a definition for
`MeshType`. The library requires `MeshType` to be a class with
**0-based vertex and triangle indexing** and having the following
public methods:

```cpp

// Constructible from a vector of nodal coordinates and triangle connectivities:
// coords[in]: nodal coordinates. (coords[2n], coords[2n+1]) = Cartesian coordinates of node #n
// conn[in]  : triangle connectivities. (conn[3t], conn[3t+1], conn[3t+2]) = vertex indices of triangle #t
MeshType(const std::vector<double>& coords, const std::vector<int>& conn);

// Access to nodal coordinates.
// node_num[in]: 0-based index of vertex
// return: coords such that (coords[0],coord[1]) -> Cartesian coordinates of vertex
const double* coordinates(const int node_num) const;

// Access to triangle connectivities:
// tri_num[in]: 0-based index of triangle
// return: conn such that (conn[0],conn[1],conn[2]) -> vertices of triangle 
const int* connectivity(const int tri_num) const;

// Access to the number of nodes in the mesh
// return: #vertices in the mesh
int n_nodes() const;

// Access to the number of triangles in the mesh
// return: #triangles in the mesh
int n_elements() const;

// Embedding dimension of the mesh (should equal 2)
inline int spatial_dimension() const { return 2;}

```


These requirements are common to DVRlib as well and the requirement
for the method `spatial_dimension()` in fact stems from the dependence
on DVRlib. Although TryUMph is limited to planar triangles, DVRlib
handles three-dimensional tetrahedral meshes as well.

TryUMph provides the function um2::CheckMeshTraits to
verify the traits assumed on  `MeshType`. When defining a new mesh class or adapting an
existing one, use the compile-time check
```cpp
static_assert(um2::CheckMeshType<MeshType>()==true);
```
to ensure that `MeshType` is
compatible with TryUMph. A caveat of the check, however, is that it does
not recognize inherited methods- a limitation from using the [Boost
type
traits](https://www.boost.org/doc/libs/1_83_0/libs/tti/doc/html/the_type_traits_introspection_library/tti_detail_has_member_function.html)
library.

It is possible to impose the requirements on `MeshType` by defining an
interface class rather than by using templates . However, TryUMph
refrains from doing so mainly to ensure efficient access to nodal
coordinates and triangle connectivities using the `coordinates()` and
`connectivity()` methods. These methods are extensively called by
TryUMph and DVRlib.  Virtual tables created when inheriting from an
interface critically slows down execution.


**um2::utils::TriangleMesh**: A minimal mesh data structure
um2::utils::TrianglMesh provided in the library satisfies all the
assumptions. Besides furnishing a simple example, the class is used in
all unit tests and performance evaluations of the library. More
details about the class can be found in the \ref @utilities page.

---

## 4. Output

### A. Meshes
Given the `tag` associated with the current state of the
um2::MeshManager object, and a successful `triangulate()` call, 
the class provides *immutable* access to:
-  the persistent quadtree data structure of type um2::Quadtree:
```cpp
auto& QT = manager.getQudatree(tag);
```
- the background mesh (a tessellation of the quadtree) of type `MeshType`:
```cpp
auto& UM = manager.getUniversalMesh(tag);
```
-  the mesh conforming to the geometry of type um2::WorkingMesh<MeshType>:
```cpp
auto& WM = manager.getWorkingMesh(tag);
```
- correspondences between `WM` and the geometry:
```cpp
auto& correspondence_map = manager.getFeatureMeshCorrespondence(tag);
```
that maps feature identifiers (um2::FeatureParams::curve_id) to
instances of the  struct um2::FeatureMeshCorrespondence
encapsulating detailed information about triangles/edges/vertices
conforming to the geometry.


Both `UM` and `WM` satisfy the mesh traits assumed of the templated
class `MeshType`, and therefore, provide simple access to element
connectivities and nodal coordinates. The two meshes are closely
related- they differ only in the coordinates of vertices in the
vicinity of the geometry.


The correspondence between elements/edges/vertices of `WM` and each
feature is provided an instance of the `FeatureMeshCorrespondence` struct:
```cpp
struct FeatureMeshCorrespondence {
MeshingReason reason;       // enumeration of success or type of failure for the feature
std::vector<int> PosVerts;  // ordered list of positive vertices guaranteed to lie on the feature
std::vector<std::pair<int,int>> PosCutElmFaces; // ordered list of positively cut elements and their positive edges
int terminalVertices[2];    // vertex indices coinciding with the corners of the feature
int splittingVertex;        // either -1 or the vertex coinciding with intPoint of the feature
};
```
- The member `reason` is used to track failures encountered when
processing features in the algorithm and can be ignored.

- The vector `PosVerts` is a sequential list of indices of *positive
vertices* lying on the feature. The `validate()` method checks that
the signed distances of each vertex in this list has magnitude smaller
than `distTOL`.

- `terminalVertices` contains the indices of the pair of vertices
coinciding with the corners of the feature. In particular,
`terminalVertices` are necessarily positive vertices. Furthermore,
`PosVertices.front()=terminalVertices[0]` and
`PosVertices.back()=terminalVertices[1]`.

- The vector `PosCutElmFaces` is an ordered list of *positively cut
  triangles and positive edges*.  Precise definitions of these
  terminologies can be found in \cite rangarajan2014universal. It suffices to
  note that:
	  - both vertices of a positive edge lie on the feature, 
	  - each positive edge belongs to a positively cut triangle, and 
	  - positive vertices contained in the list `PosVerts` are
		vertices of positive edges. 
  In particular, positive edges interpolate the geometry. The
  element index of the i-th positively cut triangle is accessed as
  `std::get<0>(PosCutElmFaces[i])`.  Enumerating triangle edges as
  shown in the figure above, the index of the positive edge of the
  i-th positively cut triangle is given by
  `std::get<1>(PosCutElmFaces[i])`.
  
- the `splittingVertex`, if not -1, equals the index of a vertex
  coinciding with um2::FeatureParams::intPoint of the feature. In the
  latter case, the `splittingVertex` is necessarily a positive vertex
  as well.

### B. Visualization
