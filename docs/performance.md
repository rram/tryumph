# Performance {#performance}

- run time as a function of num of nodes, elements, num of segments

- run time required for remeshing, with different refinement factors

- run time when the background mesh remains unchanged
  
- mesh size as a function of feature sizes

- record of the number of trys for all the examples


Failure mode | Count w/ local refinement  | Count w/ nonlocal refinement|
:-----------|:-----:|:---------:|
Conformity at corners | 345 | 4 |
Injective projection of positive edges | 575 | 36 |
Positive vertex on boundary | 13 | 1 |
Fetaure-wise topology of positive edges | 114 | 6 |
Global topology of positive edges | 7710 | 782 |
Element quality | 161 | 16 |


![](perf-1.png)

![](perf-2.png)

![](perf-3.png)

![](perf-4.png)

![](perf-5.png)



