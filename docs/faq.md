failure

quadtree

more refinement
only near the geometry

mesh size control

medial axis

only square domains?

**Successive** `triangulate()` **calls**: As the discussion above
highlights, reinvoking the `triangulate()` method with a modified
geometry yields a new mesh.

The `triangulate()` method can be invoked again for the same geometry,
while specifying a modified set of meshing parameters.

```cpp
auto run_info     = mesh_manager.triangulate(fset, mesh_params);
auto mod_run_info = mesh_manager.triangulate(fset, mod_mesh_params);
```

Whether a modified mesh results from the second invocation depends on
the choice of 


yields a new mesh if:
- the geometry changes, 
- the set of meshing parameters are revised, or
- if the first `triangulate()` call is unsuccessful.

The first possibility is highlighted by the use case for evolving
geometries discussed above. The second possibility refers to  
invoking the `triangulate()` method again with
the geometry unchanged, but with a modified set of meshing parameters:

For instance, increasing `um2::MeshingParams::refinement_factor`, or the mesh
relaxation parameters `Here, 
The persistence of the background mesh during the second invocation of
the `triangulate()` method implies that a new mesh is computed only if
the background mesh needs to be r

`triangulate()` calls is useful in other contexts too. Evidently, such
a call is

It may be desirable to refine the mesh computed for a
given geometry, say while using a larger value of the parameter
`um2::MeshingParams::refinement_factor`. 


- *refining a conforming mesh* using a larger refinement factor
. This will likely change the
	connectivity of the mesh.
- *improving the quality of a mesh* by performing additional mesh
	relaxation itertations using `um2::MeshingParams::num_dvr_int_relax_iters. 
- *handling failures* by altering meshing parameters
  `um2::MeshingParams::min_quality` and/or
  `um2::MeshingParams::num_proj_steps`.


um2 considers mesh
  refinement only based on geometric criteria, and that too only on
  the vic, by alterning the set of
  meshing parameters. The 
  
irst usees
the background mesh computed previously

method are
presumed to serve two purposes:
- reattempt the triangulation for the 

The first argument to the method is an identifier associated with the
geometry and the computed mesh (equal to 1 in the snippet). It is
intended to track the evolution of the geometry and its purpose
becomes apparent when updating the mesh to accommodate the evolution
of the geometry:


The overloaded `triangulate()` method provides a way to compute a mesh
for the new geometry specified by the `um2::FeatureSet` object
`next_fset` and assigned the identifier `2`. Crucially, the method
1 to
compute a mesh for geometry 2. IOtherwise,
the method revises the background mesh and proceeds to compute a
conforming mesh.
