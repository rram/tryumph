# About TryUMph {#introduction}

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

**TryUMph** is an open-source C++ library to generate
geometrically-accurate planar triangle meshes.  The algorithm it
implements is a specific realization of the idea of **Universal
Meshes(UM)** introduced in @cite rangarajan2014universal.

**Authors** <br />
__[Ramsharan Rangarajan](https://mecheng.iisc.ac.in/~rram)__,  Indian
Institute of Science Bangalore, India.  <br /> 
   __[Adrian Lew](https://stanford.edu/~lewa)__, Stanford University,
USA. 

## Why a new meshing library?
TryUMph is more than an alternative to existing mesh generation
libraries (eg., [Gmsh](http://gmsh.info),
[CGAL](https://doc.cgal.org/latest/Mesh_2/index.html#Chapter_2D_Conforming_Triangulations_and_Meshes),
[Triangle](https://www.google.com/search?client=safari&rls=en&q=quake+triangle&ie=UTF-8&oe=UTF-8))

**TryUMph**:

- implements the UM algorithm. Unlike conventional triangulation
  algorithms (e.g., Delaunay, advancing front), TryUMph transforms
  background meshes to conform to immersed geometries while
  preserving element connectivities. It does so without resorting to
  remeshing techniques (e.g., trimming cells, edge-flips).

- is designed to mesh not one, but a *sequence of geometries*. By
  maintaining persistent data structures (background meshes and
  quadtrees), it computes meshes conforming to successive geometries
  efficiently and robustly.

- enables efficient simulations of moving boundary problems involving
  large changes in geometry. Whenever possible, TryUMph retains the
  background mesh unchanged when meshing successive geometry iterates.
  Simulation algorithms can exploit this feature to reduce overheads
  in allocating and distributing data structures.

- achieves conformity by projecting a select collection of nodes of
  the background mesh onto the immersed geometry. In particular, mesh
  conformity is *not limited to a predetermined set of points
  sampling the geometry (boundaries, interfaces)*.

- generates and adapts background meshes automatically, ensuring mesh
  refinement commensurate with geometric features.

- is for all practical purposes, automatic. It requires a few 
  algorithmic parameters that do not strongly affect its performance.
  
- provides users considerable freedom in reperesentations and data
  structures for geometries and meshes that can enable its integration
  with numerical simulation codes.


## Capabilities
**TryUMph**:
- generates triangle meshes conforming to non-smooth geometries
- adapts background meshes to conform to evolving geometries through constructive
  connectivity-preserving vertex perturbations 
- handles large geometry changes
- automatically sizes and refines meshes

<br />

![capability1](capability-1.png "Fig 1. Meshing with TryUMph") 

The image on the left in Fig 1 shows a collection of 10 features
(f1-f10). Each feature is a simple open curve, and pairs of features
intersect at common endpoints. Given this input, TryUMph returns:

- a background mesh refined commensurate to the geometry
- a mesh conforming to the features that differs form the bakground
mesh only in the posititions of vertices in the vicinity of the
features
- detailed correspondences between vertices/edges/triangles of the
conforming mesh and the set of features.

A snippet of the code used to generate the mesh shown above:
```cpp
#include <TryUMph>
// ...

int main() {
  // create arcs and segments appearing in the smiley face
  std::vector<std::unique_ptr<demo::CircularArc>> arcs{};
  std::vector<demo::LineSegmentData> segs{};
  createSmileyFace(arcs, segs);
  
  // feature parameters in a manner required by TryUMph
  std::vector<um2::FeatureParams> feature_params{};
  for(auto& arc:arcs) {
    feature_params.emplace_back(arc->generateFeatureParams());
  }
  for(auto& seg:segs) {
    feature_params.emplace_back(generateLineSegmentFeatureParams(seg));
  }

  // algorithm parameters to sample the medial axis of the geometry
  um2::MedialAxisParams medial_axis_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // a collection of features defines a feature set
  const double corner_tol = 1.e-3; // tolerance to identify distinct corners in the geometry
  um2::FeatureSet feature_set(feature_params, corner_tol, medial_axis_params);
  
  // algorithmic parameters for meshing
  um2::MeshingParams meshing_params{.nonlocal_refinement=false,
      .refinement_factor=1., 
	  .num_proj_steps = 5,
      .num_dvr_int_relax_iters = 10, // mesh relaxation parameters for DVRlib
      .num_dvr_int_relax_dist = 2,
      .num_dvr_bd_relax_iters = 3,
      .num_dvr_bd_relax_samples = 10,
      .min_quality = 0.01,          // lower bound on element quality
      .max_num_trys = 10};          // limit on number of 'trys'

  // create the mesh manager
  um2::Box box{.center={0.,0.},.size=5.25}; // square-shaped bounding box for the extent of the mesh
  um2::MeshManager<um2::TriangleMesh> mesh_manager(box); 
  
  // compute the mesh
  const int tag = 0;   // identifier for this geometry-mesh pairing
  auto run_info = mesh_manager.triangulate(tag, feature_set, meshing_params);
  assert(run_info.success==true);
  mesh_manager.validate(case_id, feature_set, meshing_params, 1.e-3); // run checks on mesh guarantees
  const auto& WM = mesh_manager.getWorkingMesh(tag);                  // mesh conforming to the geometry
}
```

<br />

![capability-2](capability-2.png "Fig 2. Meshing a moving geometry with TryUMph ('conn' = mesh connectivity)")

<br />

A distinctive capability of TryUMph is its approach to handling
evolving geometries. Moving boundary problems are routinely
encountered in engineering applications related to shape/topology
optimization, fluid-structure interation, phase transformations or
crack propagation in solids. In numerical simulations of such
problems, it may even be necessary to determine the geometry as part
of the solution scheme. Domain boundaries/interfaces/cracks generally
evolve with each solution iteration or with each time step. To
determine meshes conforming to evolving geometries in such problems,
TryUMph implements a novel strategy of adapting the *background mesh*- a
key departure from conventional *remeshing*-based methods.

Fig 2 illustrates how TryUMph handles an evolving geometry. The
inner triangle rotates by \f$180^\circ\f$ in increments of
\f$22.5^\circ\f$. At each step, TryUMph first attempts to reuse the existing
background mesh to compute a conforming triangulation. If successful,
the resulting mesh differs from the previous one in just the locations
of a few vertices in the vicinity of the geometry. The number of
vertices, the number of triangles, as well as the connectivity of the
conforming mesh remains unchanged. Otherwise, TryUMph adapts the
background mesh to compute a conforming triangulation to the new
geometry.  In the example shown, over the course of rotating the
triangle by \f$180^\circ\f$, the background mesh is recomputed *just once*.

A snippet showing modified sections of the code to generate meshes for this example:
```cpp
 for(int riter=0; riter<20; ++ riter) { // incremental rotation of the geometry
      
	  const double angle = static_cast<double>(riter)*M_PI/20.; // rotation angle
	  // define rotated arcs and segments and populate feature parameters
      std::vector<um2::FeatureParams> feature_params{};
	  // ...
	  
	  // Feature set
      um2::FeatureSet feature_set(feature_params, corner_tol, medial_axis_params);
      
      // Mesh
      um2::RunInfo run_info;
      if(tag==0)
		  run_info = mesh_manager.triangulate(riter, feature_set, meshing_params); // mesh for the first instance of the geometry
      else
		  run_info = mesh_manager.triangulate(riter-1, riter, feature_set, meshing_params); // compute a mesh for this angle using the previous backgorund mesh to initialize trys
      }
      assert(run_info.success==true);
```

A crucial advantage of TryUMph's approach therefore is that the mesh
topology can be retained unchanged over many solution iterations or
time steps in numerical simulations of moving boundary problems.  This
feature of TryUMph can be gainfully exploited to design algorithms
that preserve matrix data structures and to implement efficient
parallelization strategies.

The possibility of persisting with the same background mesh to compute
triangulations conforming to successive realizations of an evolving
geometry highlight the reason why the background mesh is a critical
output of TryUMph.

## Incapabilities
**TryUMph does not**:
- handle *dirty* geometry- e.g., features with self-intersections, or
  distinct features intersecting at points that are not their common
  corners
- handle geometries with *cusps*, i.e., features with non-transverse intersections
- handle geometries with more than two features intersecting at a
  corner (e.g., triple-junctions)
- support interactive mesh editing
- generate non-simplicial meshes (e.g., quads)

## Acknowledge
If you use TryUMph in your research, please <a
 href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=TryUMph"
 target="_top">let us know</a> and acknowledge its use as 

\verbatim
 @article{rangarajan2024tryumph,
  title={TryUMph: A C++ library for triangulating planar evolving non-smooth geometries with Universal Meshes},
  author={Rangarajan, Ramsharan and Lew, Adrian},
  year={2024}
}
\endverbatim
<!--  journal={Journal of Open Source Software},
  volume={X},
  number={X},
  pages={X},
-->

## Contributing to TryUMph
Please <a
 href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=Universal Meshes"
 target="_top">get in touch</a> if you would like to contribute to the
development of TryUMph.
<br />
Found a bug? Please raise an issue on the [bitbucket](https://bitbucket.org/rram/um/src/) page.

## More on Universal Meshes

Bibtex entries of research articles on Universal Meshes is
[here](https://rram.bitbucket.io/um2-docs/tryump-bibtex.bib).

### Articles on UM

- [Rangarajan et al., "An algorithm for triangulating smooth three‐dimensional domains immersed in universal meshes." IJNME 117.1
  (2019): 84-117.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.5949)  

- [Rangarajan et al., "Universal meshes: A method for triangulating planar curved domains immersed in nonconforming meshes." IJNME 98.4
  (2014): 236-264.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4624)

- [Rangarajan et al., "Analysis of a method to parameterize planar
  curves immersed in triangulations." SINUM 51.3 (2013): 1392-1420.](https://epubs.siam.org/doi/abs/10.1137/110831805)

### Representative applications 

#### Fracture mechanics
- [Chiaramonte et al., "Numerical analyses of crack path instabilities in quenched plates." Extreme Mech.Letters 40 (2020):100878.](https://www.sciencedirect.com/science/article/pii/S2352431620301577)
- [Grossman‐Ponemon et al., "An algorithm for the simulation of curvilinear plane‐strain and axisymmetric hydraulic fractures with lag using the universal meshes." Int. J. Num. Analytical Meth. Geomechanics 43.6 (2019): 1251-1278.](https://onlinelibrary.wiley.com/doi/full/10.1002/nag.2896)
- [Rangarajan et al., "Simulating curvilinear crack propagation in two dimensions with universal meshes." IJNME 102.3-4 (2015): 632-670.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4731)

#### Fluid-structure interaction
- [Gawlik et al., "High‐order methods for low Reynolds number flows around moving obstacles based on universal meshes." IJNME 104.7 (2015): 513-538.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4891)

#### Shape optimization
- [Sharma et al., "A shape optimization approach for simulating contact of elastic membranes with rigid obstacles." IJNME 117.4 (2019): 371-404.](https://onlinelibrary.wiley.com/doi/abs/10.1002/nme.5960)

## License

MIT License

Copyright (c) [2024] [Ramsharan Rangarajan]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


@page getstarted Get Started
@page userguide User guide
@page utilities Utilities
@page tutorial Tutorial
@page performance Performance
@page gallery Mesh gallery
