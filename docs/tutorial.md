# Tutorial {#tutorial}

The `tutorial/` folder of the repository contains two examples. The
first, `hello_um2.cpp` demonstrates the use of um2 for meshing a
geometry resembling a smiley face. The second example in
`rotating_smiley.cpp` showcases using the library to mesh a sequence
of geometries realized by incrementally rotating the smiley face. The
examples are self-contained in the sense that they only use core
functionalities provided by the library, and in particular, none of
the testing-related utilties (under the `um2::utils` namespace)
discussed in @ref utilities.

## A first example: hello_um2

```cpp
#include <TryUMph>

#include "source/demo_Mesh.h"
#include "source/demo_CircularArc.h"
#include "source/demo_LineSegment.h"
#include "source/demo_Visualization.h"

#include <memory>

// generate geometry
void create_smiley_face(std::vector<std::unique_ptr<demo::CircularArc>>& arcs,
	                    std::vector<demo::LineSegmentData>& segs);

int main() {

  // create features
  std::vector<std::unique_ptr<demo::CircularArc>> arcs{};
  std::vector<demo::LineSegmentData> segs{};
  create_smiley_face(arcs, segs);
  const int ngeoms = static_cast<int>(arcs.size()+segs.size());
  
  // populate feature parameters
  std::vector<um2::FeatureParams> feature_params{};
  for(auto& arc:arcs) {
    feature_params.emplace_back(arc->generate_feature_params());
  }
  for(auto& seg:segs) {
    feature_params.emplace_back(generate_line_segment_feature_params(seg));
  }

  // visualize features
  demo::plot_features_vtk(feature_params, "smiley.vtk");

  // Medial axis calc parameters
  um2::MedialAxisParams medial_axis_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // corner tolerance
  const double corner_tol = 1.e-3;
  
  // Feature set
  um2::FeatureSet feature_set(feature_params, corner_tol, medial_axis_params);
  
  // Meshing parameters
  um2::MeshingParams meshing_params{
    .refinement_factor=1.,
    .num_proj_steps = 5,
      .num_dvr_int_relax_iters = 10,
      .num_dvr_int_relax_dist = 2,
      .num_dvr_bd_relax_iters = 3,
      .num_dvr_bd_relax_samples = 10,
      .min_quality = 0.01,
      .max_num_trys = 10};

  // Create the mesh manager
  um2::Box box{.center={0.,0.}, .size=5.25};
  um2::MeshManager<demo::SimpleMesh> mesh_manager(box);
  
  // Generate mesh
  const int mesh_id = 0;
  auto run_info = mesh_manager.triangulate(case_id, feature_set, meshing_params);
  assert(run_info.success==true);

  // correctness checks
  mesh_manager.validate(case_id, meshing_params, 1.e-3);
  
  // visualize the background mesh and the working mesh
  const auto& BG = mesh_manager.get_UniversalMesh(case_id);
  const auto& WM = mesh_manager.get_WorkingMesh(case_id);
  demo::plot_mesh_vtk(BG, "bgmesh.vtk");
  demo::plot_mesh_vtk(WM, "conforming.vtk");
}

```


We examine the example in detail.

- All core functionalities of the um2 library are included with the header `TryUMph`
```cpp
#include <TryUMph>
```

- The mesh data structure, the geometries of circular arcs and line
  segments, and functions related to visualization required in the
  example are provided in the `tutorial/source/` folder under the namespace
  `demo`.
  
  ```cpp
  #include "source/demo_Mesh.h"
  #include "source/demo_CircularArc.h"
  #include "source/demo_LineSegment.h"
  #include "source/demo_Visualization.h"
  ``` 
	These headers declare the mesh data structure `demo::SimpleMesh`, the
	class `demo::CircularArc` defining the geometry of a circular arc and the
	struct `demo::LineSegmentData` encapsulating the parameters defining a
	line segment. The functions `demo::plot_features_vtk` and 
	`demo::plot_mesh_vtk` help visualize features and meshes, respectively.

- The circular arcs and line segments defining the smiley face are
  created as instances of `demo::CircularArc` and
  `demo::LineSegmentData`. We discuss the implementation of these
  classes in the next section. 
  
  ```cpp
  std::vector<std::unique_ptr<demo::CircularArc>> arcs{};
  std::vector<demo::LineSegmentData> segs{};
  create_smiley_face(arcs, segs);
  const int ngeoms = static_cast<int>(arcs.size()+segs.size());
  ```

- The function `create_smiley_face`   specifies parameters of arcs and
  line segments defining the outline of the face, the eyes, the nose and the
  lip to create the geometry shown below:
  ![](smiley.png)
  ```cpp
void create_smiley_face(std::vector<std::unique_ptr<demo::CircularArc>>& arcs,
			std::vector<demo::LineSegmentData>& segs) {
  arcs.clear();
  segs.clear();
  
  const int    nSamples = 25;
  const double R_face   = 1.25;
  const double R_eye    = 0.75;
  const double R_smile  = 0.8625;
  const double r_eye    = 0.3;
  const double y_eye    = 0.15;
  const double alpha    = std::atan(y_eye/r_eye);
  const double rhat     = std::sqrt(r_eye*r_eye + y_eye*y_eye);
  
  // eyes & nose coordinates
  const double C_right_eye[]     = {R_eye*std::cos(M_PI/3.)+0.5*r_eye, R_eye*std::sin(M_PI/3.)-r_eye};
  const double C_right_eye_low[] = {R_eye*std::cos(M_PI/3.)+0.5*r_eye, R_eye*std::sin(M_PI/3.)-r_eye+y_eye};

  const double C_left_eye[]      = {R_eye*std::cos(2.*M_PI/3.)-0.5*r_eye, R_eye*std::sin(2.*M_PI/3.)-r_eye};
  const double C_left_eye_low[]  = {R_eye*std::cos(2.*M_PI/3.)-0.5*r_eye, R_eye*std::sin(2.*M_PI/3.)-r_eye+y_eye};

  const std::vector<std::array<double,2>> nose_verts = {{0,0.15},
							{0.65*std::cos(M_PI+0.75*M_PI/3.),    0.65*std::sin(M_PI+0.75*M_PI/3.)},
							{0.65*std::cos(2.*M_PI-0.75*M_PI/3.), 0.65*std::sin(2.*M_PI-0.75*M_PI/3.)}};
  
  // face 
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(0, nSamples, {0.,0.}, R_face, {0.,M_PI})));
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(1, nSamples, {0.,0.}, R_face, {M_PI,2.*M_PI})));
    
  // right eye
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(2, nSamples, {C_right_eye[0],    C_right_eye[1]},     r_eye, {0.,M_PI})));   
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(3, nSamples, {C_right_eye_low[0],C_right_eye_low[1]}, rhat,  {M_PI+alpha,2.*M_PI-alpha})));

  // left eye
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(4, nSamples, {C_left_eye[0],    C_left_eye[1]},     r_eye, {0.,M_PI})));  
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(5, nSamples, {C_left_eye_low[0],C_left_eye_low[1]}, rhat,  {M_PI+alpha,2.*M_PI-alpha})));
  
  // smile
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(6, nSamples, {0.,0.}, R_smile, {M_PI+M_PI/4.,2.*M_PI-M_PI/4.})));

  // triangular nose
  segs.emplace_back(demo::LineSegmentData{.curve_id=7,.nSamples=nSamples,.A=nose_verts[0],.B=nose_verts[1]});
  segs.emplace_back(demo::LineSegmentData{.curve_id=8,.nSamples=nSamples,.A=nose_verts[1],.B=nose_verts[2]});
  segs.emplace_back(demo::LineSegmentData{.curve_id=9,.nSamples=nSamples,.A=nose_verts[2],.B=nose_verts[0]});
  return;
}
  ```


- Details of the geometry are specified using instances of the the
   `um2::FeatureParams` struct. There is one instance of the struct
   per feature in the geometry:
   ```cpp 
   // populate feature parameters
   std::vector<um2::FeatureParams> feature_params{};
   for(auto& arc:arcs) {
     feature_params.emplace_back(arc->generate_feature_params());
   }
   for(auto& seg:segs) {
     feature_params.emplace_back(generate_line_segment_feature_params(seg));
   }
  ```
  The example uses different strategies to implement signed distance
   computations and to populate details of
   circular arcs and line segments. The details are explained in the
   next section.

- Optional: The function `demo::plot_features_vtk` helps visualize features. It
  is declared in `source/demo_Visualization.h` and is defined in
  `source/demo/demo_Visualization.cpp`. It visuzlies each feature as a
  polygonal chain defined by the list of sample points 
  provided by the `um2::FeatureParams::sample()` method:
 ```cpp
 demo::plot_features_vtk(feature_params, "smiley.vtk");
 ```
 The file `smiley.vtk`, in [legacy VTK format](https://examples.vtk.org/site/VTKFileFormats/),
  lists polygonal chains of features in the order in which features appear in the `feature_params`
  vector. Furthermore, features are assigned integer-valued colors coinciding with
  their respective identifiers (`um2::FeatureParams::curve_id`).

- With the vector of parameters of all features  defined, an instance of the class `um2::FeatureSet` defines the
  geometry:
  ```cpp
  // Medial axis calc parameters
  um2::MedialAxisParams medial_axis_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // corner tolerance
  const double corner_tol = 1.e-3;
  
  // Feature set
  um2::FeatureSet feature_set(feature_params, corner_tol,
  medial_axis_params);
  ```
  Recall that `medial_axis_params` specifies parameters for estimating
  the medial axes of features using the shrinking-ball algorithm. For
  line segments and circular arcs that compose the geometry in this
  example, the exact medial axes are straightforward to
  compute. Nevertheless, the library implements a general strategy
  without assumptions on the types of features.
  
   
- The set of algorithmic parameters required for meshing are specified
  using an instance of the `um2::MeshingParams` struct:
  ```cpp
  um2::MeshingParams meshing_params{
    .refinement_factor=1.,
    .num_proj_steps = 5,
      .num_dvr_int_relax_iters = 10,
      .num_dvr_int_relax_dist = 2,
      .num_dvr_bd_relax_iters = 3,
      .num_dvr_bd_relax_samples = 10,
      .min_quality = 0.01,
      .max_num_trys = 10};
  ```
  Notice that all values have been initialized with the defaults
  specified in the @ref userguide.
  
- The extent of the mesh is defined by an instance of the `um2::Box`
  struct:
   ```cpp
   um2::Box box{.center={0.,0.}, .size=5.25};
   ```
   
- An instance of the mesh manager class is instantiated by providing
  the extent of the mesh:
  ```cpp
   um2::MeshManager<demo::SimpleMesh> mesh_manager(box);
  ```
  Notice that a definition of the mesh data structure is required
  here. The class `demo::SimpleMesh` is defined in
  `source/demo_Mesh.h` and satisfies the traits required by the
  library. We discuss more details of its definition in a subsequent
  section.  At construction, the `MeshManager` class creates a quadtree over
  the specified `box`.
  
- The `mesh_manager` object can now be used to compute a mesh for the
  geometry specified by `fset`:
  ```cpp
  // Generate mesh
  const int mesh_id = 0;
  auto run_info = mesh_manager.triangulate(mesh_id, feature_set, meshing_params);
  ```
  The mesh identifier `mesh_id` is purposeful in this example; its use
  will become apparent when meshing a sequence of geometries (see the
  next example discussed below). The `run_info` object returned by the
  `um2::MeshManager::triangulate()` method contains details about
  the execution- whether it succeeded (`run_info.success`), the
  execution time (`run_info.runtime`) and about execution details of
  each try (`run_info.try_info`). Additionally, the object provides
  information about failure modes encountered during failed iterations
  of the meshing algorithm (`run_info.failures`). Examining it
  helps infer reasons behind refinement of the computed conforming
  mesh, or even the reason why a triangulation call was unsuccessful
  (i.e., when `run_info.success==false`).
  
- The background and conforming meshes computed by
  for the geometry `fset` can be accessed:
  ```cpp
  const auto& BG = mesh_manager.get_UniversalMesh(case_id);
  const auto& WM = mesh_manager.get_WorkingMesh(case_id);
  ```
  The background mesh `BG` is of type `demo::SimpleMesh` while the
  conforming mesh `WM` is of type
  `um2::WorkingMesh<demo::SimpleMesh>`. 
  
- Optional: The meshes `BG` and `WM` are written to file in legacy VTK
  format using the function `demo::plot_vtk_mesh` that is defined in
  `source/demo_Visualization.h`:
  ```cpp
  demo::plot_mesh_vtk(BG, "bgmesh.vtk");
  demo::plot_mesh_vtk(WM, "conforming.vtk");
  ```

- The features and meshes (`smiley.vtk`, `bgmesh.vtk` and
  `conforming.vtk`) can all be visualized with
  [Paraview](paraview.org). It is instructive to compare the
  background and conforming meshes to visualize the vertex
  perturbations performed to ensure conforming with the geometry while
  retaining elements with good quality.

## Triangle mesh data structure

## Specifying geometry: Circular arcs

## Specifying geometry: Line segments

## Moving boundaries
