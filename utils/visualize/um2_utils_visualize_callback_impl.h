// Sriramajayam

namespace um2 {
  namespace utils {
    
    // plot the feature set
    template<typename MeshType>
      void  Try_Visualization_Callback<MeshType>::operator()(const FeatureSet& fset)  {
	const std::string filename = outdir + "features.vtk";
	saveFeatureSetVTK(filename, fset);
      }
	
    // plot the quadtree
    template<typename MeshType>
      void Try_Visualization_Callback<MeshType>::operator()(const Quadtree& QT) {
	const int trynum = this->getTryNumber();
	std::string filename = outdir + "QT-t" + std::to_string(trynum) + ".tec";
	QT.plotTec(filename);
      }

    // plot the
    // background mesh
    // complete working mesh
    // positive edges
    // positive vertices
    // positive elements
    template<typename MeshType>
      void Try_Visualization_Callback<MeshType>::operator()(const WorkingMesh<MeshType>& WM) {
	const int trynum = this->getTryNumber();
	std::string filename;
	const auto status = WM.getMeshStatus();

	if(status==WorkingMeshStatus::UNPROCESSED)
	  {
	    filename = outdir + "BG-t" + std::to_string(trynum) + ".vtk";
	    VTK_FeatureMesh mesh(WM.getParentMesh());
	    mesh.save(filename);
	  }
	else
	  {
	    filename = outdir + "WM-t" + std::to_string(trynum);
	    
	    switch(status)
	      {
	      case WorkingMeshStatus::CORNERS_SNAPPED: filename += "-cnr.vtk"; break;
		
	      case WorkingMeshStatus::POSITIVE_EDGES_IDENTIFIED: filename += "-pos.vtk"; break;
		
	      case WorkingMeshStatus::NONCONFORMING:
	      case WorkingMeshStatus::CONFORMING: filename += ".vtk"; break;

	      default:
		assert(false && "Unexpected working mesh status");
	      }
	    
	    VTK_FeatureMesh mesh(WM, WM.getSplitFeatureCorrespondenceMap());
	    mesh.save(filename); 
	  }
	
	// done
	return;
      }

    template<typename MeshType>
      void Try_Visualization_Callback<MeshType>::operator()(const WorkingMesh<MeshType>& WM,
							const int proj_num,
							const int relax_num)  {
      const auto status = WM.getMeshStatus();
	assert(status==WorkingMeshStatus::POSITIVE_EDGE_TOPOLOGY_INSPECTED);
	const int trynum = this->getTryNumber();
	const std::string filename = outdir
	  + "WM-t" + std::to_string(trynum)
	  + "-p" + std::to_string(proj_num)
	  + "-r" + std::to_string(relax_num)
	  + ".vtk";
	
	// save the mesh
	VTK_FeatureMesh mesh(WM, WM.getSplitFeatureCorrespondenceMap());
	mesh.save(filename);
	
	// done
	return;
      }

    // visualize the result of a successful try
    template<typename MeshType>
      void Try_Visualization_Callback<MeshType>::operator()(const Quadtree& QT, const WorkingMesh<MeshType>& WM,
							    const std::map<int,FeatureMeshCorrespondence>& fm_correspondences) {

      assert(WM.getMeshStatus()==WorkingMeshStatus::CONFORMING);

	QT.plotTec(outdir+"QT-final.tec");
	
	VTK_FeatureMesh bg_mesh(WM.getParentMesh());
	bg_mesh.save(outdir + "BG-final.vtk");

	VTK_FeatureMesh wm_mesh(WM, fm_correspondences);
	wm_mesh.save(outdir + "WM-final.vtk");
      }



    // plot the feature set
    template<typename MeshType>
      void Final_Try_Visualization_Callback<MeshType>::operator()(const FeatureSet& fset) {
      const std::string filename = outdir + "geom-" + std::to_string(this->getMeshID()) + ".vtk";
      saveFeatureSetVTK(filename, fset);
    }

    // visualize the result of a successful try
    template<typename MeshType>
      void Final_Try_Visualization_Callback<MeshType>::operator()(const Quadtree& QT, const WorkingMesh<MeshType>& WM,
								  const std::map<int,FeatureMeshCorrespondence>& fm_correspondences) {

      assert(WM.getMeshStatus()==WorkingMeshStatus::CONFORMING);
      const int gid = this->getMeshID();
	std::string filename;

	// quadtree
	filename = outdir + "QT-" + std::to_string(gid) + ".tec";
	QT.plotTec(filename);

	// background mesh
	filename = outdir + "BG-" + std::to_string(gid) + ".vtk";
	VTK_FeatureMesh bg_mesh(WM.getParentMesh());
	bg_mesh.save(filename);

	// working mesh
	filename = outdir + "WM-" + std::to_string(gid) + ".vtk";
	VTK_FeatureMesh wm_mesh(WM, fm_correspondences);
	wm_mesh.save(filename);
      }
    
  } // namespace um2::utils
} // namespace um2
