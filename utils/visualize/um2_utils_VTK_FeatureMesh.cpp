// Sriramajayam

#include <um2_utils_VTK_FeatureMesh.h>
#include <fstream>
#include <filesystem>
#include <cassert>
#include <iostream>

namespace um2
{
  namespace utils
  {

    // function to position the cursor at a given word in a file
    bool position_cursor_after_word(std::ifstream& file, const std::string word)
    {
      bool found = false;
      std::string line;
      while (std::getline(file, line))
	{
	  auto position = line.find(word);
	  if ( position != std::string::npos)
	    {
	      file.seekg(-line.length()-1+position+word.length(), std::ios::cur);
	      found = true;
	      break;
	    }
	}
      // done
      return found;
    }

    // Create from a file
    std::unique_ptr<VTK_FeatureMesh> VTK_FeatureMesh::create(const std::string filename)
    {
      VTK_FeatureMesh mesh;

      // aliases
      auto& nNodes        = mesh.nNodes;
      auto& nCells        = mesh.nCells;
      auto& coordinates   = mesh.coordinates;
      auto& connectivity  = mesh.connectivity;
      auto& cell_types    = mesh.cell_types;
      auto& part_id       = mesh.part_id;
      auto& color         = mesh.color;
      auto& start_PosCutCells       = mesh.start_PosCutCells;
      auto& start_PosEdgeCells      = mesh.start_PosEdgeCells;
      auto& start_PosVertCells      = mesh.start_PosVertCells;
      auto& start_TerminalVertCells = mesh.start_TerminalVertCells;
      auto& pos_cut_cells           = mesh.pos_cut_cells;
      
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
      std::ifstream file;
      file.open(filename);
      assert(file.good() && file.is_open());

      // read # nodes
      bool flag = position_cursor_after_word(file, "POINTS");
      assert(flag==true);
      file >> nNodes;
      
      // read coordinates
      coordinates.resize(3*nNodes);
      std::string line;
      std::getline(file, line);
      for(int n=0; n<nNodes; ++n)
	{
	  file >> coordinates[3*n] >> coordinates[3*n+1] >> coordinates[3*n+2];
	}

      // read cells
      flag = position_cursor_after_word(file, "CELLS");
      assert(flag==true);
      file >> nCells;
      std::getline(file, line);
      connectivity.clear();
      for(int e=0; e<nCells; ++e)
	{
	  int num_nodes;
	  file >> num_nodes;
	  std::vector<int> conn(num_nodes);
	  for(int a=0; a<num_nodes; ++a)
	    file >> conn[a];
	  connectivity.push_back(conn);
	}

      // read cell types
      cell_types.resize(nCells);
      flag = position_cursor_after_word(file, "CELL_TYPES");
      assert(flag==true);
      std::getline(file, line);
      for(int e=0; e<nCells; ++e)
	{
	  int val;
	  file >> val;
	  cell_types[e] = static_cast<VTK_CELL_TYPES>(val);
	}

      // read part ids and initialize start indices
      flag = position_cursor_after_word(file, "part_id");
      assert(flag==true);
      std::getline(file, line);
      std::getline(file, line);
      part_id.resize(nCells);
      start_PosCutCells       = -1;
      start_PosEdgeCells      = -1;
      start_PosVertCells      = -1;
      start_TerminalVertCells = -1;
      for(int e=0; e<nCells; ++e)
	{
	  int val;
	  file >> val;
	  part_id[e] = static_cast<PartID>(val);
	  if(start_PosCutCells==-1 && part_id[e]==PartID::Positively_Cut_Triangle)
	    {
	      start_PosCutCells = e;
	    }
	  else if(start_PosEdgeCells==-1 && part_id[e]==PartID::Positive_Edge)
	    {
	      start_PosEdgeCells = e;
	    }
	  else if(start_PosVertCells==-1 && part_id[e]==PartID::Positive_Vertex)
	    {
	      start_PosVertCells = e;
	    }
	  else if(start_TerminalVertCells==-1 && part_id[e]==PartID::Terminal_Vertex)
	    {
	      start_TerminalVertCells = e;
	    }
	}

      // read colors
      flag = position_cursor_after_word(file, "color");
      assert(flag==true);
      std::getline(file, line);
      std::getline(file, line);
      color.resize(nCells);
      for(int e=0; e<nCells; ++e)
	{
	  file >> color[e];
	}

      // partition positively cut cells in a feature-wise manner
      const int nFeatures = start_PosVertCells-start_PosEdgeCells;
      pos_cut_cells.clear();
      int indx = start_PosCutCells;
      for(int i=0; i<nFeatures; ++i)
	{
	  // #pos-cut-elms for this features
	  int npos_cut_cells = static_cast<int>(connectivity[start_PosEdgeCells+i].size())-1;
					  
	  // pos cut cells for this feature
	  std::vector<int> my_pos_cut_cells{};
	  for(int j=0; j<npos_cut_cells; ++j, ++indx)
	    {
	      my_pos_cut_cells.push_back(indx);
	    }

	  pos_cut_cells.push_back(my_pos_cut_cells);
	}
      
      // done
      return std::make_unique<VTK_FeatureMesh>(mesh);
    }


    // Save to a vtk file
    void VTK_FeatureMesh::save(const std::string filename) const
    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
      std::fstream out;
      out.open(filename, std::ios::out);
      assert(out.good());

      // Headers
      out << "# vtk DataFile Version 3.0" << std::endl;
      out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
      out << "ASCII" << std::endl;
      out << "DATASET UNSTRUCTURED_GRID" << std::endl;

      // nodes
      out << "POINTS " << nNodes << " double" << std::endl;
      for(int i=0; i<nNodes; ++i)
	{
	  const double* X = &coordinates[3*i];
	  out << X[0] << " " << X[1] << " " << X[2] << std::endl;
	}

      // cells
      int cell_size = 0;
      for(auto& conn:connectivity)
	{
	  cell_size += 1+static_cast<int>(conn.size());
	}
      out << "CELLS " << nCells << " " << cell_size << std::endl;
      for(auto& conn:connectivity)
	{
	  out << static_cast<int>(conn.size()) << " ";
	  for(auto& v:conn)
	    {
	      out << v << " ";
	    }
	  out << std::endl;
	}

      // cell types	
      out << "CELL_TYPES " << nCells << std::endl;
      for(auto& x:cell_types)
	{
	  out << static_cast<int>(x) << std::endl;
	}

      // part ids
      out << "CELL_DATA " << nCells << std::endl
	  << "SCALARS part_id int 1" << std::endl
	  << "LOOKUP_TABLE default" << std::endl;
      for(auto& x:part_id)
	{
	  out << static_cast<int>(x) << std::endl;
	}

      // colors
      out << "SCALARS color int 1" << std::endl
	  << "LOOKUP_TABLE default" << std::endl;
      for(auto& x:color)
	{
	  out << static_cast<int>(x) << std::endl;
	}

      // done
      out.close();
      return;

    }


    // animate mesh motion
    void VTK_FeatureMesh::animate_mesh_motion(const std::string filename1,
					      const std::string filename2,
					      const int num_frames,
					      const std::string stem)
    {
      // read the two meshes
      auto ptr_mesh1 = VTK_FeatureMesh::create(filename1); 
      auto ptr_mesh2 = VTK_FeatureMesh::create(filename2);
      auto& mesh1 = *ptr_mesh1;
      auto& mesh2 = *ptr_mesh2;

      // sanity checks
      assert(mesh1.nNodes==mesh2.nNodes);
      // displacement 1 -> 2
      std::vector<double> disp(mesh1.nNodes*3);
      for(int a=0; a<mesh1.nNodes; ++a)
	{
	  for(int k=0; k<3; ++k)
	    {
	      disp[3*a+k] = mesh2.coordinates[3*a+k]-mesh1.coordinates[3*a+k];
	    }
	}
      

      // animate
      for(int f=0; f<=num_frames; ++f)
	{
	  std::cout << "Animating frame " << f << std::endl;
	  const double lambda = static_cast<double>(f)/static_cast<double>(num_frames);
	  for(int a=0; a<mesh1.nNodes; ++a)
	    {
	      const double *X  = &mesh1.coordinates[3*a];
	      double       *Y  = &mesh2.coordinates[3*a];
	      const double *u = &disp[3*a];

	      for(int k=0; k<3; ++k)
		{
		  Y[k] = X[k] + lambda*u[k];
		}
	    }

	  // save
	  const std::string filename = stem+"-f"+std::to_string(f)+".vtk";
	  mesh2.save(filename);

	  // append displacement for visualization as a glyph
	  std::fstream file;
	  file.open(filename, std::ios::app);
	  file << std::endl;
	  file << "POINT_DATA " << mesh1.nNodes << std::endl;
	  file << "SCALARS disp double 3" << std::endl
	      << "LOOKUP_TABLE default" << std::endl;
	  for(int a=0; a<mesh1.nNodes; ++a)
	    {
	      file << (1.-lambda)*disp[3*a] << " " << (1.-lambda)*disp[3*a+1] << " " << (1.-lambda)*disp[3*a+2] << std::endl;
	    }
	  file.close();
	}

      // done
      return;
    }
    

    // animate crawling of positive edges/elements/vertices
    void VTK_FeatureMesh::animate_crawling(const std::string stem)
    {
      // copy of this mesh
      VTK_FeatureMesh mesh(*this);

      // sequentially activate pos elms, pos edges and pos vertices
      const int nFeatures = start_PosVertCells-start_PosEdgeCells;
      assert(nFeatures>0);
      int pos_count = 0;
      bool flag = true;
      while(flag==true)
	{
	  flag = false;
	  mesh.connectivity = connectivity;
	  
	  // activate the first "pos_count" pos elements in each feature
	  for(int f=0; f<nFeatures; ++f)
	    {
	      const int num_pos = static_cast<int>(pos_cut_cells[f].size());
	      if(pos_count<num_pos)
		{
		  flag = true;

		  // erase trailing pos elements of this feature
		  for(int i=pos_count; i<num_pos; ++i)
		    {
		      mesh.connectivity[pos_cut_cells[f][i]].clear();
		    }

		  // truncate connectivity of pos edges
		  mesh.connectivity[start_PosEdgeCells+f].resize(pos_count+1);

		  // truncate connectivity of pos vertices
		  mesh.connectivity[start_PosVertCells+f].resize(pos_count+1);
		}
	    }
	  
	  // save this mesh
	  mesh.save(stem+std::to_string(pos_count)+".vtk");

	  // next
	  ++pos_count;
	}
    }
    
  } // namespace um2::test
} // namespace um2
