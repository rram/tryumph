// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    template<typename MeshType>
      VTK_FeatureMesh::VTK_FeatureMesh(const MeshType& MD)
      {
	nNodes = MD.n_nodes();
	nCells = MD.n_elements();

	// coordinates
	coordinates.resize(3*nNodes);
	for(int a=0; a<nNodes; ++a)
	  {
	    const double* X    = MD.coordinates(a);
	    coordinates[3*a+0] = X[0];
	    coordinates[3*a+1] = X[1];
	    coordinates[3*a+2] = 0.;
	  }

	// cells
	connectivity.resize(nCells, std::vector<int>(3));
	for(int e=0; e<nCells; ++e)
	  {
	    const int* conn = MD.connectivity(e);
	    connectivity[e] = {conn[0], conn[1], conn[2]};
	  }

	// cell types
	cell_types.resize(nCells);
	std::fill(cell_types.begin(), cell_types.end(), VTK_CELL_TYPES::TRIANGLE);

	// part id
	part_id.resize(nCells);
	std::fill(part_id.begin(), part_id.end(), PartID::Triangle);

	// color
	color.resize(nCells);
	std::fill(color.begin(), color.end(), -1);

	// no features
	start_PosCutCells       = -1;
	start_PosEdgeCells      = -1;
	start_PosVertCells      = -1;
	start_TerminalVertCells = -1;
	pos_cut_cells.clear();

	// done
      }

    template<typename MeshType>
      VTK_FeatureMesh::VTK_FeatureMesh(const WorkingMesh<MeshType>& WM,
				       const std::map<int,FeatureMeshCorrespondence>& fm_correspondences)
      {
	nNodes = WM.n_nodes();

	// coordinates
	coordinates.resize(3*nNodes);
	for(int a=0; a<nNodes; ++a)
	  {
	    const double* X = WM.coordinates(a);
	    coordinates[3*a+0] = X[0];
	    coordinates[3*a+1] = X[1];
	    coordinates[3*a+2] = 0.;
	  }

	// cells in order: triangles, pos-cut-triangles, pos-edges, pos-verts, terminal-verts
	connectivity.clear();
	pos_cut_cells.clear();
	const int nTrias = WM.n_elements();
	for(int e=0; e<nTrias; ++e)
	  {
	    const int* conn = WM.connectivity(e);
	    connectivity.push_back({conn[0], conn[1], conn[2]});
	  }
	start_PosCutCells = static_cast<int>(connectivity.size());

	for(auto& it:fm_correspondences)
	  {
	    const auto& fm = it.second;
	    std::vector<int> my_pos_cut_cells{};
	    for(auto& jt:fm.PosCutElmFaces)
	      {
		const int& e = std::get<0>(jt);
		const int* conn = WM.connectivity(e);
		connectivity.push_back({conn[0], conn[1], conn[2]});
		my_pos_cut_cells.push_back(static_cast<int>(connectivity.size())-1);
	      }
	    pos_cut_cells.push_back(my_pos_cut_cells);
	  }
	start_PosEdgeCells = static_cast<int>(connectivity.size());
	for(auto& it:fm_correspondences)
	  {
	    const auto& fm = it.second;
	    connectivity.push_back(fm.PosVerts);
	  }
	start_PosVertCells = static_cast<int>(connectivity.size());
	for(auto& it:fm_correspondences)
	  {
	    const auto& fm = it.second;
	    connectivity.push_back(fm.PosVerts);
	  }
	start_TerminalVertCells = static_cast<int>(connectivity.size());
	for(auto& it:fm_correspondences)
	  {
	    const auto& fm = it.second;
	    connectivity.push_back({fm.terminalVertices[0], fm.terminalVertices[1]});
	  }

	// total cells
	nCells = static_cast<int>(connectivity.size());
	
	// cell types in order: triangles, pos-triangles, pos-edges, pos-verts, terminal-verts
	// part ids in order  : triangles, pos-triangles, pos-edges, pos-verts, terminal-verts
	cell_types.resize(nCells);
	part_id.resize(nCells);
	for(int e=0; e<nTrias; ++e)
	  {
	    cell_types[e] = VTK_CELL_TYPES::TRIANGLE;
	    part_id[e]    = PartID::Triangle;
	  }
	for(int e=start_PosCutCells; e<start_PosEdgeCells; ++e)
	  {
	    cell_types[e] = VTK_CELL_TYPES::TRIANGLE;
	    part_id[e]    = PartID::Positively_Cut_Triangle;
	  }
	for(int e=start_PosEdgeCells; e<start_PosVertCells; ++e)
	  {
	    cell_types[e] = VTK_CELL_TYPES::POLY_LINE;
	    part_id[e]    = PartID::Positive_Edge;
	  }
	for(int e=start_PosVertCells; e<start_TerminalVertCells; ++e)
	  {
	    cell_types[e] = VTK_CELL_TYPES::POLY_VERTEX;
	    part_id[e]    = PartID::Positive_Vertex;
	  }
	for(int e=start_TerminalVertCells; e<nCells; ++e)
	  {
	    cell_types[e] = VTK_CELL_TYPES::POLY_VERTEX;
	    part_id[e]    = PartID::Terminal_Vertex;
	  }
	
	// cell colors in order: pos cut triangles, pos-edges, pos-verts, terminal-verts
	color.resize(nCells, -1);
	int indx = 0;
	for(auto& it:fm_correspondences)
	  {
	    const int& mycolor = it.first; // curve_id
	    for(auto& e:pos_cut_cells[indx])
	      {
		color[e] = mycolor;
	      }

	    color[start_PosEdgeCells+indx]      = mycolor;
	    color[start_PosVertCells+indx]      = mycolor;
	    color[start_TerminalVertCells+indx] = mycolor;
	    ++indx;
	  }
	// done
      }

  } // um::utils::
} // um::
