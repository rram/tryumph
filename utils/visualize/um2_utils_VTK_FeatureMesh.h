// Sriramajayam

#pragma once

#include <um2_WorkingMesh.h>
#include <um2_Output.h>

#include <vector>
#include <string>

namespace um2
{
  namespace utils
  {
    //! Used to label cells in vtk files
    enum PartID
    {
      Triangle                = 0,
      Positively_Cut_Triangle = 1,
      Positive_Edge           = 2,
      Positive_Vertex         = 3,
      Terminal_Vertex         = 4
    };
    
    //! VTK cell Types
    // Cell types: https://kitware.github.io/vtk-js/api/Common_DataModel_CellTypes.html
    enum VTK_CELL_TYPES
    {
      TRIANGLE    = 5,
      POLY_LINE   = 4,
      POLY_VERTEX = 2
    };
    
    class VTK_FeatureMesh
    {
    public:
      //! Create from a vtk file
      static std::unique_ptr<VTK_FeatureMesh> create(const std::string filename);
      
      int nNodes;
      int nCells;
      std::vector<double>           coordinates;
      std::vector<std::vector<int>> connectivity;
      std::vector<VTK_CELL_TYPES>   cell_types;
      std::vector<PartID>           part_id;
      std::vector<int>              color;

      int start_PosCutCells;
      int start_PosEdgeCells;
      int start_PosVertCells;
      int start_TerminalVertCells;
      
      std::vector<std::vector<int>> pos_cut_cells;
      
      //! Create from a mesh
      template<typename MeshType>
	VTK_FeatureMesh(const MeshType& MD);

      //! Create from a working mesh and feature mesh output
      template<typename MeshType>
	VTK_FeatureMesh(const WorkingMesh<MeshType>& WM, const std::map<int,FeatureMeshCorrespondence>& fm_correspondences);
      
      //! Save to a vtk file
      void save(const std::string filename) const;

      //! animate crawling of positive edges/elements/vertices
      void animate_crawling(const std::string stem);
      
      //! animate mesh motion
      static void animate_mesh_motion(const std::string filename1, const std::string filename2,
				      const int num_frames, const std::string stem);

      //private:
      VTK_FeatureMesh() = default;
      VTK_FeatureMesh(const VTK_FeatureMesh&) = default;
    };

  } // um::utils::
} // um::


// implementation of templated constructor
#include <um2_utils_VTK_FeatureMesh_impl.h>
