// Sriramajayam

#pragma once

#include <um2_FeatureSet.h>
#include <um2_utils_VTK_FeatureMesh.h>
#include <filesystem>
#include <cassert>
#include <string>
#include <fstream>

// Paraview bug in rendering lines
// https://discourse.paraview.org/t/cell-values-partly-disappear-when-editing-width-on-mac/11666

// Paraview bug in transferring colors to glyphs
// cell-wise data is not transferred to glyphs for coloring because of a bug in paraview
// https://gitlab.kitware.com/paraview/paraview/-/issues/18662
// 3 scalars are required to choose the vector as an orientation in paraview

namespace um2
{
  namespace utils
  {
    // write a feature set to file
    void saveFeatureSetVTK(const std::string filename, const FeatureSet& fset);


    // save a mesh to file
    template<typename MeshType>
      inline void saveMeshVTK(const std::string filename, const MeshType& mesh)
      {
	VTK_FeatureMesh vtk_mesh(mesh);
	vtk_mesh.save(filename);
      }

  }
}

