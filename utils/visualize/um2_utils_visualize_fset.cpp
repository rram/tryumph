// Sriramajayam

#include <um2_utils_visualize.h>

namespace um2
{
  namespace utils
  {
    // write a feature set to file
    void saveFeatureSetVTK(const std::string filename, const FeatureSet& fset)

    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");

      std::fstream file;
      file.open(filename, std::ios::out);
      assert(file.good() && file.is_open());

      // Headers
      file << "# vtk DataFile Version 3.0" << std::endl;
      file << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
      file << "ASCII" << std::endl;
      file << "DATASET POLYDATA" << std::endl;

      // feature samples
      const auto& fparams = fset.getFeatureParams();
      const int nFeatures = static_cast<int>(fparams.size());
      std::vector<std::list<std::array<double,2>>> sample_points{};
      for(auto& f:fparams)
	{
	  sample_points.push_back(f.sample_feature());
	}

      // number of points
      int nPoints = 0;
      for(auto& s:sample_points)
	{
	  nPoints += static_cast<int>(s.size());
	}
      
      // nodes
      file << "POINTS " << nPoints << " double" << std::endl;
      for(auto& s:sample_points)
	for(auto& p:s)
	  {
	    file << p[0] << " " << p[1] << " " << 0. << std::endl;
	  }

      // each line = samples on a feature
      // cell size = total number of integers in the list
      const int cell_size = nPoints + nFeatures;
      file << "LINES " << nFeatures << " " << cell_size << std::endl;
      int pcount = 0;
      for(auto& s:sample_points)
	{
	  const int nsamples = static_cast<int>(s.size());
	  file << nsamples << " ";
	  for(int i=0; i<nsamples; ++i, pcount++)
	    {
	      file << pcount << " ";
	    }
	  file << std::endl;
	}

      // colors for each feature
      file << "CELL_DATA " << nFeatures << std::endl
	  << "SCALARS color int 1" << std::endl
	  << "LOOKUP_TABLE default" << std::endl;
      for(int i=0; i<nFeatures; ++i)
	{
	  file << fparams[i].curve_id << std::endl;
	}
      
      // RGB scheme
      //file << "CELL_DATA " << nFeatures << std::endl
      //   << "COLOR_SCALARS feat_colors 4" << std::endl;
      //for(auto& f:fparams)
      //{
      //file << f.rgba[0] << " " << f.rgba[1] << " " << f.rgba[2] << " " << f.rgba[3] << std::endl;
      //}
      
      // done
      file.close();
    }
    
  } // namespace um2::test
} // namespace um2

