// Sriramajayam

#pragma once

#include <um2_CallbackInterface.h>
#include <um2_utils_visualize.h>
#include <um2_utils_VTK_FeatureMesh.h>

namespace um2 {
  namespace utils  {

    /** Callback interface for detailed geometry and mesh visualization.
	Derived class of CallbackInterface with overloaded operators that all
	trigger context-dependent geometry and mesh output.

	The class is expected to be used for testing/debugging purposes and can trigger
	a larger number of outputs. To discourage its use with multiple geometries, the file naming
	convention in the class does not use the geometry identifier.
	
	@tparam MeshType Mesh type satisfying um::CheckMeshTraits.
	
	@sa Final_Try_Visualization_Callback
	
	@author Ramsharan Rangarajan
    */
    
    template<typename MeshType>
      class Try_Visualization_Callback: public CallbackInterface<MeshType>  {
    public:
      //! @brief Constructor
      //! \param[in] out output directory. Copied. Assumed to have been created.
      inline Try_Visualization_Callback(const std::string out)
	:outdir(out+"/") {}

      /** @brief Visualize the feature set as a sequence of segments defined by the sample points. 

	  Saves the file features.vtk in VTK file format.
	  @param[in] fset Feature set
	  @sa  save_featureset_vtk
      */
      void operator()(const FeatureSet& fset) override;
	
      /** @brief Visualizes the quadtree in VTK file format.

	  Saves the file QT-try_num.vtk in the output directory
	  @param[in] QT quadtee
      */
      void operator()(const Quadtree& QT) override;

      /** @brief Visualize the working mesh at different stages of a try
	  
	  Accesses the status of the working mesh using the WorkingMesh::get_mesh_status method.
	  - for status = WorkingMeshStatus::UNPROCESSED, saves the background mesh as BG-trynum.vtk
	  - for status = WorkingMeshStatus::CORNERS_SNAPPED, saves the working mesh and corners as WM-trynum-cnr.vtk 
	  - for status = WorkingMeshStatus::POSITIVE_EDGES_IDENTIFIED, saves the working mesh, 
	    positive edges and positively cut triangles as WM-trynum-pos.vtk
	  - for status = WorkingMeshStatus::NONCONFORMING orWorkingMeshStatus::CONFORMING, saves the working mesh, 
	    positive edges and positively cut triangles as WM-trynum.vtk
	  
	  @param[in] WM working mesh
      */
      void operator()(const WorkingMesh<MeshType>& WM) override;

      /** @brief Visualize the quadtree, background mesh and working mesh at the end of a successful try.

	  Requires that the status of the working mesh be WorkingMeshStatus::CONFORMING.
	  Saves the quadtree as "QT-final.tec", the working mesh as "WM-final.vtk" 
	  and the background mesh as "BG-final.vtk"
	  @param[in] QT quadtree
	  @param[in] WM working mesh with status equal to WorkingMeshStatus::CONFORMING.
      */
      void operator()(const Quadtree& QT, const WorkingMesh<MeshType>& WM,
		      const std::map<int,FeatureMeshCorrespondence>& fm_correspondences) override;
      
      /** @brief Visualize the working mesh during projection-relaxation iterations of a try

	  @param[in] WM working mesh with status equal to WorkingMeshStatus::POSITIVE_EDGE_TOPOLOGY_INSPECTED
	  @param[in] proj_num iteration count for projection of positive vertices towards the immersed feature.
	  @param[in] relax_num DVR relaxation iteration counter
      */
      void operator()(const WorkingMesh<MeshType>& WM, const int proj_num, const int relax_num) override;

    private:
      const std::string outdir; //!< output directory. 
    };


    /** 
	Callback interface for visualizing geometries and conforming meshes computed at the end of successful trys.
	
	Derived class of CallbackInterface with overloaded operators in which just one of the methods triggers a mesh output.
	
	The class does not produce detailed outputs but is helpful in visualizing the result of a 
	successful exection of the UM algorithm. 

	It can be used with multiple geometries, and explicitly uses the geometry identifier in filenames to 
	label geometries and meshes.
	
	@tparam MeshType Mesh type satisfying um::CheckMeshTraits.
	
	@sa Try_Visualization_Callback
	
	@author Ramsharan Rangarajan
     */
    
    template<typename MeshType>
      class Final_Try_Visualization_Callback: public CallbackInterface<MeshType> {
    public:
      //! @brief Constructor
      //! \param[in] out output directory. Copied. Assumed to have been created.
      inline Final_Try_Visualization_Callback(const std::string out)
	:outdir(out+"/") {}

      /** @brief Visualize the feature set as a sequence of segments defined by the sample points. 

	  Saves the file geom-gid.vtk in VTK file format.
	  @param[in] fset Feature set
	  @sa save_featureset_vtk
      */
      void operator()(const FeatureSet& fset) override;
	
      //! overloaded to do nothing.
      void operator()(const Quadtree& QT) override {
	return;
      }

      //! overloaded to do nothing.
      void operator()(const WorkingMesh<MeshType>& WM) override {
	return;
      }

      //! overloaded to do nothing.
      void operator()(const WorkingMesh<MeshType>& WM, const int proj_num, const int relax_num) override {
	return;
      }

      /** @brief Visualize the quadtree, background mesh and working mesh at the end of a successful try.

	  Requires that the status of the working mesh be WorkingMeshStatus::CONFORMING.
	  Saves the quadtree as "QT-geom-gid.tec", the working mesh as "WM-geom-gid.vtk" 
	  and the background mesh as "BG-geom-gid.vtk"
	  @param[in] QT quadtree
	  @param[in] WM working mesh with status equal to WorkingMeshStatus::CONFORMING.
      */
      void operator()(const Quadtree& QT, const WorkingMesh<MeshType>& WM,
		      const std::map<int,FeatureMeshCorrespondence>& fm_correspondences) override;
      
    private:
      const std::string outdir;
    };
    
  } // namespace um2::utils
} // namespace um2

// Implementation
#include <um2_utils_visualize_callback_impl.h>
