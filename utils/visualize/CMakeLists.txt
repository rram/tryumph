# Sriramajayam

add_library(um2_utils_visualize STATIC
                 um2_utils_visualize_fset.cpp
		 um2_utils_VTK_FeatureMesh.cpp)

# include headers in the current directory
target_include_directories(um2_utils_visualize PUBLIC
				      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# Link
target_link_libraries(um2_utils_visualize PUBLIC um2_featureset um2_mesh)

# Add required flags
target_compile_features(um2_utils_visualize PUBLIC ${UM2_COMPILE_FEATURES})

# install headers
install(FILES
	um2_utils_visualize.h
	um2_utils_VTK_FeatureMesh.h
	um2_utils_VTK_FeatureMesh_impl.h
	um2_utils_visualize_callback.h
	um2_utils_visualize_callback_impl.h
	DESTINATION ${PROJECT_NAME}/include)
