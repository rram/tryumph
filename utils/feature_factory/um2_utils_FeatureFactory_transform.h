// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    template<typename T>
      std::unique_ptr<FeatureFactory> FeatureFactory::transform(T transform_func) const
      {
	// transformed factory and its features
	std::unique_ptr<FeatureFactory> tr_factory = std::unique_ptr<FeatureFactory>(new FeatureFactory());
	auto& tr_p1curves = tr_factory->p1curves;
	auto& tr_p2curves = tr_factory->p2curves;
	auto& tr_p3curves = tr_factory->p3curves;

	// create tranformations
	for(auto& geom:p1curves)
	  tr_p1curves.push_back(geom->create_transformation(transform_func));

	for(auto& geom:p2curves)
	  tr_p2curves.push_back(geom->create_transformation(transform_func));

	for(auto& geom:p3curves)
	  tr_p3curves.push_back(geom->create_transformation(transform_func));

	// create feature set
	tr_factory->createFeatureSet(fset->getCornerTolerance(), fset->getMedialAxisParams());
	
	// done
	return std::move(tr_factory);
      }
    
  } // namespace um2::test
} // namespace um2
