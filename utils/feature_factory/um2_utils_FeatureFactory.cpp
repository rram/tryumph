// Sriramajayam

#include <um2_utils_FeatureFactory.h>
#include <limits>
#include <iostream>
#include <random>

namespace um2
{
  namespace utils
  {
    FeatureFactory::FeatureFactory(const std::string filename,
				   int nsamples,
				   const double corner_tol,
				   const MedialAxisParams& mparams,
				   const RootFindingParams& nlparams)
      :p1curves{}, p2curves{}, p3curves{},
       fset(nullptr)
    {
      validate_input(mparams);
      
      // create geometries from the file
      createGeometries(filename, nsamples, nlparams);

      // create feature set from the geometries
      createFeatureSet(corner_tol, mparams);

      // done
    }


    // Read geometry data from a .fset file
    void FeatureFactory::createGeometries(const std::string filename,
					   const int nSamplesPerFeature,
					   const RootFindingParams &NLparams)
    {
      assert(std::string(std::filesystem::path(filename).extension())==".fset");
      std::fstream in;
      in.open(filename, std::ios::in);
      assert(in.good() && in.is_open());
      
      // Header contains the file format
      // #Format: curve_id, npoles, poles, nweights, weights";
      std::string line1;
      std::getline(in, line1);

      // Read the number of features
      int nFeatures;
      in >> nFeatures;

      // Read each feature
      int curve_id, npoles, nweights;
      std::vector<double> poles{}, weights{};
      for(int f=0; f<nFeatures; ++f)
	{
	  // Read this feature's parameters
	  in >> curve_id;
	  in >> npoles;
	  poles.resize(2*npoles);
	  for(int p=0; p<2*npoles; ++p)
	    {
	      in >> poles[p];
	    }
	  in >> nweights;
	  weights.resize(nweights);
	  for(int p=0; p<nweights; ++p)
	    {
	      in >> weights[p];
	    }

	  // Create this feature
	  switch(npoles)
	    {
	    case 2:
	      {
		assert(nweights==0);
		p1curves.push_back( std::make_unique<LineSegment>(curve_id, &poles[0], &poles[2], nSamplesPerFeature, NLparams) );
		break;
	      }

	    case 3:
	      {
	      assert(nweights==npoles);
	      p2curves.push_back( std::make_unique<QuadraticRationalBezier>(curve_id, poles, weights, nSamplesPerFeature, NLparams) );
		break;
	    }

	    case 4:
	      { assert(nweights==0);
		p3curves.push_back( std::make_unique<CubicBezier>(curve_id, poles, nSamplesPerFeature, NLparams) );
		break;
	      }

	    default: assert(false && "CreateFeatures: Unknown feature type");
	    }
	}

      
      in.close();
      std::cout << std::endl
		<< "Created: "
		<< p1curves.size()<< " line segments" << std::endl
		<< p2curves.size()<< " quadratic rational bezier segments" << std::endl
		<< p3curves.size()<< " cubic bezier segments" << std::endl;
      // done
      return;
    }
    
    
    // Create a feature set
    void FeatureFactory::createFeatureSet(const double cornerTol,
					    const MedialAxisParams &mAxisParams)
    {
      const int np1curves = static_cast<int>(p1curves.size());
      const int np2curves = static_cast<int>(p2curves.size());
      const int np3curves = static_cast<int>(p3curves.size());
      assert(np1curves+np2curves+np3curves>0);

      // collect the feature set params
      std::vector<FeatureParams> gparams{};
      
      // P1
      for(int n=0; n<np1curves; ++n)
	{
	  gparams.push_back( p1curves[n]->GetFeatureParams() );
	}
      
      // P2
      for(int n=0; n<np2curves; ++n)
	{
	  gparams.push_back( p2curves[n]->GetFeatureParams() );
	}
      
      // P3
      for(int n=0; n<np3curves; ++n)
	{
	  gparams.push_back( p3curves[n]->GetFeatureParams() );
	}

      // create the feature set
      fset = std::unique_ptr<FeatureSet>(new FeatureSet(gparams, cornerTol, mAxisParams));
      
      // done
      return;
    }

    
    // Determine bounds for a curve using its control points
    template<class T>
    void CurveBounds(const T& geom, double* xlim, double* ylim)
    {
      std::vector<double> points{};
      geom.GetControlPoints(points);
      const int nPoints = static_cast<int>(points.size()/2);
      for(int p=0; p<nPoints; ++p)
	{
	  const double& x = points[2*p];
	  const double& y = points[2*p+1];

	  if(x<xlim[0]) xlim[0] = x;
	  if(x>xlim[1]) xlim[1] = x;

	  if(y<ylim[0]) ylim[0] = y;
	  if(y>ylim[1]) ylim[1] = y;
	}

      // done
      return;
    }


    // Compute the bounding box
    Box FeatureFactory::getBoundingBox() const
    {
      double xlim[] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest()};
      double ylim[] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest()};

      for(auto& geom:p1curves)
	CurveBounds(*geom, xlim, ylim);
      for(auto& geom:p2curves)
	CurveBounds(*geom, xlim, ylim);
      for(auto& geom:p3curves)
	CurveBounds(*geom, xlim, ylim);

      std::array<double,2> center;
      center[0] = 0.5*(xlim[0]+xlim[1]);
      center[1] = 0.5*(ylim[0]+ylim[1]);

      double hx = xlim[1]-xlim[0];
      double hy = ylim[1]-ylim[0];
      double size = (hx>hy) ? hx : hy;

      // done
      return Box({center, size});
    }
    
    
    // Print a .fset file
    void FeatureFactory::save(const std::string filename) const
    {
      assert(std::string(std::filesystem::path(filename).extension())==".fset");
      
      std::fstream pfile;
      pfile.open(filename, std::ios::out);
      assert(pfile.good());
      pfile << "#Format: curve_id, npoles, poles, nweights, weights" << std::endl
	    << p1curves.size() + p2curves.size() + p3curves.size();
		       
      // line segments
      for(auto& geom:p1curves)
	{
	  pfile << std::endl;
	  pfile << geom->GetCurveID()<<" "<<2<<" ";
	  std::vector<double> pts(4);
	  geom->GetControlPoints(pts);
	  for(auto& p:pts)
	    {
	      pfile << p <<" ";
	    }
	  pfile << 0 ;
	}

      // quadratic rational beziers segments
      for(auto& geom:p2curves)
	{
	  pfile << std::endl;
	  pfile << geom->GetCurveID()<<" "<<3<<" ";
	  std::vector<double> pts(6);
	  geom->GetControlPoints(pts);
	  for(auto& p:pts)
	    {
	      pfile << p <<" ";
	    }
	  pfile << 3 ;
	  const double* wts = geom->GetWeights();
	  pfile << " "<<wts[0]<<" "<<wts[1]<<" "<<wts[2];
	}

      // cubic beziers
      for(auto& geom:p3curves)
	{
	  pfile << std::endl;
	  pfile << geom->GetCurveID()<<" "<<4<<" ";
	  std::vector<double> pts(8);
	  geom->GetControlPoints(pts);
	  for(auto& p:pts)
	    {
	      pfile << p << " ";
	    }
	  pfile << 0 ;
	}
      pfile.close();
      return;
    }


  } // namespace um2::test
} // namespace um2
