# Sriramajayam

add_library(um2_utils_feature_factory STATIC
	         um2_utils_FeatureFactory.cpp)

# include headers in the current directory
target_include_directories(um2_utils_feature_factory  PUBLIC
  				      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# Add required flags
target_link_libraries(um2_utils_feature_factory PUBLIC um2_featureset um2_utils_geometry)
target_compile_features(um2_utils_feature_factory PUBLIC ${UM2_COMPILE_FEATURES})

# install headers
install(FILES
	  um2_utils_FeatureFactory.h
          um2_utils_FeatureFactory_transform.h
          DESTINATION ${PROJECT_NAME}/include)
