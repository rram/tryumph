// Sriramajayam

#pragma once

#include <um2_FeatureSet.h>

#include <um2_utils_LineSegment.h>
#include <um2_utils_QuadraticRationalBezier.h>
#include <um2_utils_CubicBezier.h>

#include <fstream>
#include <string>
#include <memory>
#include <filesystem>

namespace um2
{
  namespace utils
  {
    struct FeatureFactory
    { 
    FeatureFactory(const std::string filename,
		   int nsamples,
		   const double corner_tol,
		   const MedialAxisParams& mparams,
		   const RootFindingParams& nlparams);
      
      ~FeatureFactory() = default;

      //! get the feature set
      inline const FeatureSet& getFeatureSet() const
      { return *fset; }

      //! transform control points of geometries and get a new feature factory
      template<typename T>
      std::unique_ptr<FeatureFactory> transform(T transform_func) const;
      
      // Compute the bounding box
      Box getBoundingBox() const;

      // Print a .fset file
      void save(const std::string filename) const;
      
    private:
      inline FeatureFactory()
	:p1curves{},
	p2curves{},
	p3curves{},
	fset(nullptr) {}
      
      // read an fset file and create geometries
      void createGeometries(const std::string filename,
			     const int nSamplesPerFeature,
			     const RootFindingParams &NLparams);
      
      // Create a feature set
      void createFeatureSet(const double cornerTol, const MedialAxisParams &mAxisParams);

      std::vector<std::unique_ptr<LineSegment>>             p1curves;
      std::vector<std::unique_ptr<QuadraticRationalBezier>> p2curves;
      std::vector<std::unique_ptr<CubicBezier>>             p3curves;
      std::unique_ptr<FeatureSet>                           fset;
    };
    
  } // namespace um2::test
} // namespace um2


#include <um2_utils_FeatureFactory_transform.h>

