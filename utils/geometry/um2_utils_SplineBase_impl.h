// Sriramajayam

#pragma once

#include <cassert>
#include <um2_utils_Spline_RootFinding_Boost.h>

namespace um2
{
  namespace utils
  {
    // Constructor
    template<class T>
      SplineBase<T>::SplineBase(int id, double t0, double t1, const int nsamples,
				const RootFindingParams& nlparams)
      :curve_id(id),
      nSamples(nsamples),
      NLParams(nlparams),
      tbounds{t0, t1}
	{
	  assert(tbounds[0]<tbounds[1]);
	  assert(nSamples>2);
	}


      // Initialize members: should be called from the constructor of the derived class
      template<class T>
	void SplineBase<T>::Initialize()
	{
	  auto* obj = static_cast<T*>(this);

	  // terminal points
	  obj->Evaluate(tbounds[0], terminalPoints, nullptr, nullptr);
	  obj->Evaluate(tbounds[1], terminalPoints+2, nullptr, nullptr);

	  // mid point
	  obj->Evaluate(0.5*(tbounds[0]+tbounds[1]), midPoint, nullptr, nullptr);
	  

	  // Populate the rtree
	  rtree.clear();
	  double t, X[2];
	  pointParam pp;
	  int indx = 0;
	  const double ds = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples-1);
	  for(int s=0; s<nSamples; ++s)
	    {
	      t = tbounds[0] + static_cast<double>(s)*ds;
	      obj->Evaluate(t, X, nullptr, nullptr);
	      pp.first = boost_point(X[0], X[1]);
	      pp.second = std::pair<int,double>(indx++,t);
	      rtree.insert(pp);
	    }

	  // done
	  return;
	}

  
      // Get the signed distance
      template<class T>
	void SplineBase<T>::GetSignedDistance(const double* X, double& sd, double* dsd)
	{
	  auto* obj = static_cast<T*>(this);

	  // spacing between sample points
	  const double dt = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples);
	  const double tEPS = dt*1.e-3;

	  // Initial guess for the closest point
	  boost_point pt(X[0], X[1]);
	  std::vector<pointParam> result{};
	  rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
	  assert(static_cast<int>(result.size())==1);
	  const double t0 = result[0].second.second;
	  assert(t0>=tbounds[0]-tEPS && t0<=tbounds[1]+tEPS);

	  // determine the root
	  const double tcpt = Spline_FindRoot_Boost(X, obj, t0, dt, tbounds, NLParams);
	  assert(tcpt>=tbounds[0]-tEPS && tcpt<=tbounds[1]+tEPS);
	
	  // Evaluate the spline and its derivatives here.

	  // Y is the closest point projection
	  double Y[2], dY[2];
	  obj->Evaluate(tcpt, Y, dY, nullptr);
	  double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
	  assert(norm>NLParams.normTol); 
	  double nvec[] = {-dY[1]/norm, dY[0]/norm};
	
	  // signed distance
	  sd = (X[0]-Y[0])*nvec[0] + (X[1]-Y[1])*nvec[1];
	
	  // Gradient
	  if(dsd!=nullptr)
	    { dsd[0] = nvec[0];
	      dsd[1] = nvec[1]; }
	
	  // done
	  return;
	}

        
      // Samples the feature
      template<class T>
	std::list<std::array<double,2>> SplineBase<T>::GetSampling() const
      {
	assert(!rtree.empty());

	// ordered list of samples
	std::map<int, std::array<double,2>> samples{};
	for(auto& it:rtree)
	  {
	    const auto& X = it.first;
	    const auto& tID = it.second;
	    const int& indx = tID.first;
	    samples.insert({indx, {X.template get<0>(), X.template get<1>()}});
	  }

	// map -> list
	std::list<std::array<double,2>> sample_list{};
	for(auto& it:samples)
	  {
	    sample_list.push_back(it.second);
	  }
	
	return std::move(sample_list);
      }


      // Register feature params
      template<class T>
	FeatureParams SplineBase<T>::GetFeatureParams()
	{
	  FeatureParams params;
      
	  // curve id
	  params.curve_id = curve_id;

	  // signed distance -> std::function<void(const double* X, double& sd, double* dsd)> 
	  params.signed_distance_function = std::bind(&SplineBase<T>::GetSignedDistance, this,
						      std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);             
      
	  // sampling -> std::function<std::list<std::array<double,2>> ()>
	  params.sample_feature = std::bind(&SplineBase<T>::GetSampling, this);
	  
	  // terminal points and mid point
	  for(int k=0; k<2; ++k)
	    {
	      params.leftCnr[k]  = terminalPoints[k];
	      params.rightCnr[k] = terminalPoints[2+k];
	      params.intPoint[k] = midPoint[k];
	    }
		
	  // done
	  return params;
	}
      
  } // um::utils::
  
} // um::

    
