// Sriramajayam

#pragma once

#include <um2_utils_SplineBase.h>

namespace um2
{
  namespace utils
  {
    //! Class defining a Cubic Bezier curve. Not thread safe.
    class CubicBezier: public SplineBase<CubicBezier>
    {
    public:
      //! Constructor
      //! \param[in] id Curve id
      //! \param[in] pts Set of 4 control points in order.
      //! \param[in] nsamples Number of sampling points to use
      CubicBezier(int id, std::vector<double>& pts,
		  const int nsamples,
		  const RootFindingParams& nlparams);
  
      //! Destructor
      inline ~CubicBezier() {}

      // Disable copy and assignment
      CubicBezier(const CubicBezier&) = delete;
      CubicBezier operator=(const CubicBezier&) = delete;

      //! Evaluate the coordinates of the spline at the requested point
      //! \param[in] t Parameter value
      //! \param[out] X evaluate coordinate
      //! \param[out] dX evaluated derivative
      //! \param[out] d2X evaluated 2nd derivative
      void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;
	  
      //! Return the set of control points
      void GetControlPoints(std::vector<double>& control_pts) const;

      //! transform
      template<typename T>
	std::unique_ptr<CubicBezier> create_transformation(T transform) const;
      
    private:
      const double P0[2], P1[2], P2[2], P3[2];
    };
    
  } // um::utils::
} // um::


#include <um2_utils_CubicBezier_transform.h>
