// Sriramajayam

#pragma once

#include <um2_utils_SplineBase.h>

namespace um2
{
  namespace utils
  {
    class LineSegment: public SplineBase<LineSegment>
    {
    public:
      //! Constructor
      //! \param[in] id Curve id
      //! \param[in] A Starting point
      //! \param[out] B End point
      //! \param[in] nsamples Number of sampling points
      LineSegment(const int id,
		  const double* A, const double* B,
		  const int nsamples,
		  const RootFindingParams& nlparams);
  
      //! Destructor
      inline ~LineSegment() {}
	
      //! Disable copy and assignment
      LineSegment(const LineSegment&) = delete;
      LineSegment operator=(const LineSegment&) = delete;
    
      //! Evaluate the coordinates of the spline at the requested point
      //! \param[in] t Parameter value
      //! \param[out] X evaluate coordinate
      //! \param[out] dX evaluated derivative
      //! \param[out] d2X evaluated 2nd derivative
      void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;
    
      //! Return the set of control points
      void GetControlPoints(std::vector<double>& control_pts) const;

      //! Transform the feature
      template<typename T>
	std::unique_ptr<LineSegment> create_transformation(T transform) const;
      
    private:
      const double left_pt[2], right_pt[2]; //!< Left and right end points
    };
  
  } // um::utils::
} // um::


#include <um2_utils_LineSegment_transform.h>
