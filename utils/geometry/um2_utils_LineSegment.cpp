// Sriramajayam

#include <um2_utils_LineSegment.h>

namespace um2
{
  namespace utils
  {
    // Constructor
    LineSegment::LineSegment(const int id,
			     const double* A, const double* B,
			     const int nsamples,
			     const RootFindingParams& nlparams)
      :SplineBase<LineSegment>(id, 0., 1., nsamples, nlparams),
      left_pt{A[0],A[1]},
      right_pt{B[0],B[1]}
      {
	// invoke base class initialization method
	Initialize();
      }
  
  
    // Evaluate the coordinates of the spline at the requested point
    void LineSegment::Evaluate(const double& t, double* X, double* dX, double* d2X) const
    {
      X[0] = left_pt[0] + t*(right_pt[0]-left_pt[0]);
      X[1] = left_pt[1] + t*(right_pt[1]-left_pt[1]);
      if(dX!=nullptr)
	{
	  dX[0] = right_pt[0]-left_pt[0];
	  dX[1] = right_pt[1]-left_pt[1];
	}
      if(d2X!=nullptr)
	{ d2X[0] = d2X[1] = 0.; }
      return;
    }

    // Return the set of control points
    void LineSegment::GetControlPoints(std::vector<double>& control_pts) const
    {
      control_pts.resize(4);
      for(int k=0; k<2; ++k)
	{
	  control_pts[k]   = left_pt[k];
	  control_pts[2+k] = right_pt[k];
	}
      return;
    }

  }
}
