// Sriramajayam

#include <um2_utils_CubicSpline.h>
#include <iostream>
#include <cassert>

namespace um2
{
  namespace utils
  {
    // Constructor
    CubicSpline::CubicSpline(int id,
			     const std::vector<double>& tcoords,
			     const std::vector<double>& pts,
			     const int nsamples,
			     const RootFindingParams& nlparams)
      :SplineBase<CubicSpline>(id, tcoords.front(), tcoords.back(), nsamples, nlparams),
      tvec(tcoords),
      interp_pts(pts),
      tmin(tcoords.front()),
      tmax(tcoords.back())
    {
      const int nPoints = static_cast<int>(tvec.size());
      
      // Sanity checks on the data
      assert(nPoints>=2);
      assert(nSamples>=1);
      for(int i=0; i<nPoints-1; ++i)
	{ assert(tvec[i]<tvec[i+1]); }

      // Accelerators
      xacc = gsl_interp_accel_alloc();
      yacc = gsl_interp_accel_alloc();

      // X spline
      xspline = gsl_spline_alloc(gsl_interp_cspline, nPoints);

      // Y spline
      yspline = gsl_spline_alloc(gsl_interp_cspline, nPoints);

      // Extensions
      tfirst[0] = tvec[0];
      tfirst[1] = tvec[1];
      tlast[0] = tvec[nPoints-2];
      tlast[1] = tvec[nPoints-1];

      // X & Y data
      std::vector<double> xvec(nPoints), yvec(nPoints);
      for(int p=0; p<nPoints; ++p)
	{ xvec[p] = pts[2*p];
	  yvec[p] = pts[2*p+1]; }
      gsl_spline_init(xspline, &tvec[0], &xvec[0], nPoints);
      gsl_spline_init(yspline, &tvec[0], &yvec[0], nPoints);

      // Extend the spline at the left end
      ExtendSpline(tfirst[0], tfirst[1], xspline, xacc, yspline, yacc, first_coefs);

      // Extend the spline at the right end
      ExtendSpline(tlast[0], tlast[1], xspline, xacc, yspline, yacc, last_coefs);
    
      // invoke base class initialization method
      Initialize();

      // done
    }


    // Helper function to extend a spline
    void CubicSpline::ExtendSpline(const double& t0, const double& t1,
				   gsl_spline* xspline, gsl_interp_accel* xacc,
				   gsl_spline* yspline, gsl_interp_accel* yacc,
				   double coefs[2][4])
    {
      // Sample the interval [t0,t1] at 4 points 
      const double lambda[] = {1.,2./3.,1./3.,0.};

      // Inverse of the matrix with rows
      // T^3 T^2 T 1, where T = 1-lambda.
      const double MatInv[4][4] = {{-4.5,   13.5,   -13.5,  4.5},
				   {9.,     -22.5,  18.,    -4.5},
				   {-5.5,   9.,     -4.5,   1.},
				   {1.,     0.,     0.,      0.}};

      double xy[2][4];
      for(int i=0; i<4; i++)
	{
	  double tval = lambda[i]*t0 + (1.-lambda[i])*t1;
	  xy[0][i] = gsl_spline_eval(xspline, tval, xacc);
	  xy[1][i] = gsl_spline_eval(yspline, tval, yacc);
	}

      // Solve for coefficients for x and y parameterizations in terms of T
      for(int k=0; k<2; ++k)
	for(int i=0; i<4; i++)
	  { coefs[k][i] = 0.;
	    for(int j=0; j<4; ++j)
	      coefs[k][i] += MatInv[i][j]*xy[k][j];  }
      
      // done
      return;
    }
  
  
    // Destructor: clean up
    CubicSpline::~CubicSpline()
    {
      // delete splines
      gsl_spline_free(xspline);
      gsl_spline_free(yspline);

      // delete accelerators
      gsl_interp_accel_free(xacc);
      gsl_interp_accel_free(yacc);

    }

  
    // Evaluate the coordinates of the spline at the requested point
    void CubicSpline::Evaluate(const double& t, double* X, double* dX, double* d2X) const
    {
      // Need to evaluate the extension?
      if(t<tmin)
	Evaluate_Extension(t, tfirst[0], tfirst[1], first_coefs, X, dX, d2X);
      else if(t>tmax)
	Evaluate_Extension(t, tlast[0], tlast[1], last_coefs, X, dX, d2X);
      else // extension not required
	{
	  // Coordinates
	  assert(X!=nullptr);
	  X[0] = gsl_spline_eval(xspline, t, xacc);
	  X[1] = gsl_spline_eval(yspline, t, yacc);

	  // Derivatives
	  if(dX!=nullptr)
	    { dX[0] = gsl_spline_eval_deriv(xspline, t, xacc);
	      dX[1] = gsl_spline_eval_deriv(yspline, t, yacc); }

	  if(d2X!=nullptr)
	    { d2X[0] = gsl_spline_eval_deriv2(xspline, t, xacc);
	      d2X[1] = gsl_spline_eval_deriv2(yspline, t, yacc); }
	}
      // done
      return;
    }


    // Evaluate spline extension for t<tmin or t>tmax
    void CubicSpline::Evaluate_Extension(const double& t, const double& t0, const double& t1,
					 const double coefs[2][4],
					 double* X, double* dX, double* d2X)
    {
      // Scale coordinate
      const double dt = (t1-t0);
      const double T = (t-t0)/dt;
    
      if(X!=nullptr)
	for(int k=0; k<2; ++k)
	  X[k] = coefs[k][0]*T*T*T + coefs[k][1]*T*T + coefs[k][2]*T + coefs[k][3];
    
      if(dX!=nullptr)
	for(int k=0; k<2; ++k)
	  dX[k] = (3.*coefs[k][0]*T*T + 2.*coefs[k][1]*T + coefs[k][2])/dt;

      if(d2X!=nullptr)
	for(int k=0; k<2; ++k)
	  d2X[k] = (6.*coefs[k][0]*T + 2.*coefs[k][1])/(dt*dt);

      // done
      return;
    }

  
    // Return the set of control points
    void CubicSpline::GetControlPoints(std::vector<double>& control_pts) const
    {
      control_pts = interp_pts;
      return;
    }


  } // um::utils::  
} // um::

