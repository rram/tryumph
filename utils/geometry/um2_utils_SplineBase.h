// Sriramajayam

#pragma once

#include <um2_Input.h>
#include <um2_BoostAliases.h>
#include <um2_utils_RootFindingParams.h>
#include <list>
#include <memory>

namespace um2
{
  namespace utils
  {
    // Thanks to: https://isocpp.org/wiki/faq/templates#template-friends
    template<class T> class SplineBase;
    template<class T> std::ostream& operator<< (std::ostream&, const SplineBase<T>&);

    //! Base class for parametric curves
    template<class T>
      class SplineBase
      {
      public:

	//! Constructor
	SplineBase(int id, double t0, double t1, const int nsamples,
		   const RootFindingParams& nlparams);
		 
	//! Destructor
	~SplineBase() = default;

	// Disable copy and assignment
	SplineBase(const SplineBase<T>&) = delete;
	SplineBase<T> operator=(const SplineBase<T>&) = delete;

	//! Get the signed distance
	//! \param[in] X Point at which to evaluate
	//! \param[out] sd Computed signed distance
	//! \param[out] dsd Computed derivative of the signed distance. Defaulted to nullptr.
	void GetSignedDistance(const double* X, double& sd, double* dsd);

	//! Register feature params
	FeatureParams GetFeatureParams();

	//! Samples the feature
	std::list<std::array<double,2>> GetSampling() const;
      
	// Get the curve id
	inline int GetCurveID() const
	{ return curve_id; }

	// Overload extraction operator
	inline friend std::ostream& operator << (std::ostream &out, const SplineBase<T>& geom)
	{
	  const int& nSamples = geom.nSamples;
	  auto* tbounds = geom.tbounds;
	  const double dt = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples-1);
	  double tval, X[2];
	  auto* obj = static_cast<const T*>(&geom);
	  for(int i=0; i<nSamples; ++i)
	    {
	      tval = tbounds[0] + static_cast<double>(i)*dt;
	      obj->Evaluate(tval, X);
	      out << X[0] <<",\t" << X[1] << std::endl;
	    }
	  return out;
	}
  
      
      protected:
	void Initialize();
	const int curve_id;
	const int nSamples;
	const RootFindingParams& NLParams;

	const double tbounds[2];
	double terminalPoints[4];
	double midPoint[2];
	
	using tID        = std::pair<int,double>;        //!< Pairing of point index and its parametric coordinate
	using pointParam = std::pair<boost_point,tID>;
	boost::geometry::index::rtree<pointParam, boost::geometry::index::quadratic<8>> rtree; //!< boost r-tree for closest point searches
      };
  }
}


#include <um2_utils_SplineBase_impl.h>
