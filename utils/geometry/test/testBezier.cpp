// Sriramajayam

#include <um2_utils_CubicBezier.h>
#include <cmath>
#include <random>
#include <iostream>
#include <fstream>

int main()
{
  // Randomly generate 4 well spaced points
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> pdis(-1.,1.);
  std::uniform_real_distribution<> tdis(-0.9,0.9);
  std::vector<double> points{};
  for(int p=0; p<4; ++p)
    {
      double X[] = {pdis(gen), pdis(gen)};

      // Check distances to previously generated points
      bool flag = true;
      for(int q=0; q<p && flag==true; ++q)
	{
	  const double* Y = &points[2*q];
	  double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	  if(dist<0.2) flag = false;
	}

      // Good ?
      if(flag==true)
	{ points.push_back(X[0]);
	  points.push_back(X[1]);
	}
      // Not good?
      else
	--p;
    }
  
  // Create spline
  const int id = 0;
  um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=30, .normTol=1.e-3};
  um2::utils::CubicBezier spline(id, points, 100, nlparams);

  // Get the feature's parameters
  um2::FeatureParams fparams = spline.GetFeatureParams();
  
  // Plot the spline
  std::fstream pfile;
  pfile.open((char*)"bezier.dat", std::ios::out);
  for(int n=0; n<101; ++n)
    {
      double t = static_cast<double>(n)/100.;
      double Y[2], dY[2], d2Y[2];
      spline.Evaluate(t, Y, dY, d2Y);
      pfile<<t<<" "
	   <<Y[0]<<" "<<dY[1]<<" "
	   <<dY[0]<<" "<<dY[1]<<" "
	   <<d2Y[0]<<" "<<d2Y[1]<<"\n";
    }
  pfile.close();

  // Consistency checks
  int ndfails = 0, nd2fails = 0;
  for(int trial=0; trial<100; ++trial)
    {
      const double EPS = 1.e-5;
      double t = tdis(gen);
      double Y[2], dY[2], d2Y[2];
      spline.Evaluate(t, Y, dY, d2Y);

      // Numerical derivatives
      double tplus = t + EPS;
      double Yplus[2], dYplus[2];
      spline.Evaluate(tplus, Yplus, dYplus);

      double tminus = t - EPS;
      double Yminus[2], dYminus[2];
      spline.Evaluate(tminus, Yminus, dYminus);

      double dYnum[2], d2Ynum[2];
      for(int k=0; k<2; ++k)
	{ dYnum[k] = (Yplus[k]-Yminus[k])/(2.*EPS);
	  d2Ynum[k] = (dYplus[k]-dYminus[k])/(2.*EPS); }

      // Consistency
      if(std::abs(dY[0]-dYnum[0])+std::abs(dY[1]-dYnum[1])>10.*EPS ) ++ndfails;
      if(std::abs(d2Y[0]-d2Ynum[0])+std::abs(d2Y[1]-d2Ynum[1])>100.*EPS ) ++nd2fails;
    }
  assert(ndfails<10 && nd2fails<10);

  // Closest point searches
  int ncptfails = 0;
  for(int trial=0; trial<100; ++trial)
    {
      // Generate a random point
      const double X[] = {pdis(gen), pdis(gen)};
      double sd, dsd[2];
      spline.GetSignedDistance(X, sd, dsd);

      // Check that the closest point has zero distance from the curve
      double cpt[] = {X[0]-sd*dsd[0], X[1]-sd*dsd[1]};
      spline.GetSignedDistance(cpt, sd, nullptr);
      if(std::abs(sd)>1.e-2) ++ncptfails;
    }
  assert(ncptfails<10);


  auto samples = spline.GetSampling();
  pfile.open("samples.dat", std::ios::out);
  for(auto& x:samples)
    pfile << x[0] <<" " << x[1] << std::endl;
  pfile.close();
}
