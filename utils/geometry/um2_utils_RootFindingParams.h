// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    //! \brief Struct encapsulating parameters to be used when computing the signed distance function
    struct RootFindingParams
    {
      int digits;      //!< Number of digits required to converge
      int max_iter;    //!< Maximum number of permitted iterations
      double normTol;  //!< Tolerance for the norms of vectors. Vector norms will be checked to be larger than this.
    };

  } // um::utils::
} // um::
