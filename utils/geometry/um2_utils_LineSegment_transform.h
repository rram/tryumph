// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    // Transform the feature
    template<typename T>
      std::unique_ptr<LineSegment> LineSegment::create_transformation(T transform) const
      {
	// transform the poles
	double X[] = {left_pt[0],  left_pt[1]};
	double Y[] = {right_pt[0], right_pt[1]};
	transform(X);
	transform(Y);

	// new geometry
	std::unique_ptr<LineSegment> tr_geom =
	  std::make_unique<LineSegment>(curve_id, X, Y, nSamples, NLParams);

	// done
	return std::move(tr_geom);
      }
  }
}
