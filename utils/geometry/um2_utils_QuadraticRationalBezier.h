// Sriramajayam

#pragma once

#include <um2_utils_SplineBase.h>

namespace um2
{
  namespace utils
  {
    //! Class defining a quadratic rational bezier curve. Not thread safe
    class QuadraticRationalBezier: public SplineBase<QuadraticRationalBezier>
    {
    public:
      //! Constructor
      //! \param[in] id Curve id
      //! \param[in] pts Set of 3 control points in order
      //! \param[in] wts Set of 3 weights in order
      //! \param[in] nsamples Number of sampling points to use
      QuadraticRationalBezier(const int id,
			      const std::vector<double>& pts,
			      const std::vector<double>& wts,
			      const int nsamples,
			      const RootFindingParams& nlparams);

      //! Disable copy and assignment
      QuadraticRationalBezier(const QuadraticRationalBezier&) = delete;
      QuadraticRationalBezier operator=(const QuadraticRationalBezier&) = delete;

      //! Destructor
      inline virtual ~QuadraticRationalBezier() {}

      //! Evaluate the coordinates of the spline at the requested point
      //! \param[in] t Parameter value
      //! \param[out] X evaluate coordinate
      //! \param[out] dX evaluated derivative
      //! \param[out] d2X evaluated 2nd derivative
      void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;
    
      //! Return the set of control points
      void GetControlPoints(std::vector<double>& control_pts) const;

      //! Return the weights
      const double* GetWeights() const;

      //! transform the feature
      template<typename T>
	std::unique_ptr<QuadraticRationalBezier> create_transformation(T transform) const;

    private:
      double P0[2], P1[2], P2[2]; //!< Coordinates of control points
      const std::vector<double> weights;    //!< Weights for control points
    };

    
  } // utils::
} // um::


#include <um2_utils_QuadraticRationalBezier_transform.h>
