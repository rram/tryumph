// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    // transform the feature
    template<typename T>
      std::unique_ptr<QuadraticRationalBezier> QuadraticRationalBezier::create_transformation(T transform) const
      {
	// transform the poles
	std::vector<double> control_pts{P0[0],P0[1], P1[0],P1[1], P2[0], P2[1]};
	transform(&control_pts[0]);
	transform(&control_pts[2]);
	transform(&control_pts[4]);

	// new geometry
	std::unique_ptr<QuadraticRationalBezier> tr_geom =
	  std::make_unique<QuadraticRationalBezier>(curve_id, control_pts, weights, nSamples, NLParams);

	// done
	return std::move(tr_geom);
      }

  } // um::utils::
} // um::
