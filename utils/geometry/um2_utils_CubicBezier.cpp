// Sriramajayam

#include <um2_utils_CubicBezier.h>

namespace um2
{
  namespace utils
  {
    // Constructor
    CubicBezier::CubicBezier(int id, std::vector<double>& pts, const int nsamples,
			     const RootFindingParams& nlparams)
      :SplineBase<CubicBezier>(id, 0., 1., nsamples, nlparams),
      P0{pts[0], pts[1]},
      P1{pts[2], pts[3]},
      P2{pts[4], pts[5]},
      P3{pts[6], pts[7]}
	  {
	    // invoke base class initialization method
	    Initialize();
	  }

  
    
    // Return the set of control points
    void CubicBezier::GetControlPoints(std::vector<double>& control_pts) const
    {
      control_pts.resize(8);
      for(int k=0; k<2; ++k)
	{
	  control_pts[0+k] = P0[k];
	  control_pts[2+k] = P1[k];
	  control_pts[4+k] = P2[k];
	  control_pts[6+k] = P3[k];
	}
      return;
    }


    // Evaluate
    void CubicBezier::Evaluate(const double& t, double* X, double* dX, double* d2X) const
    {
      // Coordinates
      assert(X!=nullptr);
    
      // Evaluate

      // X
      for(int k=0; k<2; ++k)
	X[k] =
	  (1.-t)*(1.-t)*(1.-t)*P0[k] +
	  3.*(1.-t)*(1.-t)*t*  P1[k] +
	  3.*(1.-t)*t*t*       P2[k] +
	  t*t*t*               P3[k];

      // dX
      if(dX!=nullptr)
	{
	  for(int k=0; k<2; ++k)
	    dX[k] =
	      3.*(1.-t)*(1.-t)*(P1[k]-P0[k]) +
	      6.*(1.-t)*t*     (P2[k]-P1[k]) +
	      3.*t*t*          (P3[k]-P2[k]);
	}

      // d2X
      if(d2X!=nullptr)
	{
	  for(int k=0; k<2; ++k)
	    d2X[k] =
	      6.*(1.-t)*(P2[k]-2.*P1[k]+P0[k]) +
	      6.*t*     (P3[k]-2.*P2[k]+P1[k]);
	}

      // done
      return;
    }


  }   
}
