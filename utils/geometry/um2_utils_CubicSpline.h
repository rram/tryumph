// Sriramajayam

#pragma once

#include <um2_utils_SplineBase.h>
#include <gsl/gsl_spline.h>

namespace um2
{
  namespace utils
  {
    //! Class defining a natural cubic spline
    //! NOT THREAD_SAFE YET.
    //! Tolerance for root finding is not non-dimensional
    //! Tolerance for root finding is not set by the user
    class CubicSpline: public SplineBase<CubicSpline>
    {
    public:
      //! Constructor
      //! \param[in] id Curve id
      //! \param[in] tvec Parametric points
      //! \param[in] pts Interpolation points
      //! \param[in] nsamples Number of sampling points per interval.
      //! Useful during closest point searches
      CubicSpline(const int id,
		  const std::vector<double>& tcoords,
		  const std::vector<double>& pts,
		  const int nsamples_per_interval,
		  const RootFindingParams& nlparams);

      //! Disable copy and assignment
      CubicSpline(const CubicSpline&) = delete;
      CubicSpline& operator=(const CubicSpline) = delete;

      //! Destructor
      //! Clean up
      virtual ~CubicSpline();
    
      //! Evaluate the coordinates of the spline at the requested point
      //! \param[in] t Parameter value
      //! \param[out] X evaluate coordinate
      //! \param[out] dX evaluated derivative
      //! \param[out] d2X evaluated 2nd derivative
      void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;

      //! Return the set of control points
      void GetControlPoints(std::vector<double>& control_pts) const;
    
    private:
    
      //! Helper function to evaluate spline when t exceeds bounds
      static void ExtendSpline(const double& t0, const double& t1,
			       gsl_spline* xspline, gsl_interp_accel* xacc,
			       gsl_spline* yspline, gsl_interp_accel* yacc,
			       double coefs[2][4]);
      
      //! Evaluate spline extension for t<tmin or t>tmax
      static void Evaluate_Extension(const double& t, const double& t0, const double& t1,
				     const double coefs[2][4],
				     double* X, double* dX, double* d2X);

      const std::vector<double> tvec;       //!< knot vector
      const std::vector<double> interp_pts; //!< interpolating points
      const double tmin, tmax;        //!< Extreme values of the parameteric coordinate
      gsl_spline* xspline;            //!< Spline function for the x-coordinate
      gsl_spline* yspline;            //!< Spline function for the y-coordinate
      gsl_interp_accel* xacc;         //!< Accelerator for x-spline
      gsl_interp_accel* yacc;         //!< Accelerator for x-spline

      double tfirst[2];               //!< First interval
      double first_coefs[2][4];       //!< Special evaluation for the first interval
      double tlast[2];                //!< Last interval
      double last_coefs[2][4];        //!< Special evaluation for the last interval      
    };
    
  } // um::utils::
} // um::
