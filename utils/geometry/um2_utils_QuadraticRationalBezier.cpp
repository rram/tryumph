// Sriramajayam

#include <um2_utils_QuadraticRationalBezier.h>

namespace um2
{
  namespace utils
  {
    // Constructor
    QuadraticRationalBezier::QuadraticRationalBezier(int id,
						     const std::vector<double>& pts,
						     const std::vector<double>& wts,
						     const int nsamples,
						     const RootFindingParams& nlparams)
      :SplineBase<QuadraticRationalBezier>(id, 0., 1., nsamples, nlparams),
      P0{pts[0], pts[1]},
      P1{pts[2], pts[3]},
      P2{pts[4], pts[5]},
      weights({wts[0], wts[1], wts[2]})
      {
	// invoke base class initialization method
	Initialize();
      }

 
    // Return the set of control points
    void QuadraticRationalBezier::GetControlPoints(std::vector<double>& control_pts) const
    {
      control_pts.resize(6);
      for(int k=0; k<2; ++k)
	{
	  control_pts[0+k] = P0[k];
	  control_pts[2+k] = P1[k];
	  control_pts[4+k] = P2[k];
	}
      return;
    }


  
    // Return the weights
    const double* QuadraticRationalBezier::GetWeights() const
    { return weights.data(); }
  
  
    // Evaluate
    void QuadraticRationalBezier::Evaluate(const double& t, double* X, double* dX, double* d2X) const
    {
      // Coordinates
      assert(X!=nullptr);
    
      // Evaluate
    
      // Basis functions and derivatives
      const double b0 = (1.-t)*(1.-t)*weights[0];
      const double b1 = 2.*t*(1.-t)*weights[1];
      const double b2 = t*t*weights[2];
      const double denom = b0+b1+b2;

      // Evaluate
      for(int k=0; k<2; ++k)
	X[k] = (b0*P0[k] + b1*P1[k] + b2*P2[k])/denom;

      // First derivatives
      if(dX!=nullptr || d2X!=nullptr)
	{
	  const double db0 = weights[0]*2.*(t-1.);
	  const double db1 = weights[1]*(2.-4.*t);
	  const double db2 = weights[2]*2.*t;
	  const double ddenom = db0+db1+db2;

	  if(dX!=nullptr)
	    for(int k=0; k<2; ++k)
	      dX[k] = (db0*P0[k] + db1*P1[k] + db2*P2[k])/denom - X[k]*ddenom/denom;

	  if(d2X!=nullptr)
	    {
	      const double d2b0 = weights[0]*2.;
	      const double d2b1 = -4.*weights[1];
	      const double d2b2 = 2.*weights[2];
	      const double d2denom = d2b0+d2b1+d2b2;
	      for(int k=0; k<2; ++k)
		d2X[k] =
		  (d2b0*P0[k] + d2b1*P1[k] + d2b2*P2[k])/denom
		  -(db0*P0[k] + db1*P1[k] + db2*P2[k])*ddenom/(denom*denom)
		  -(dX[k]*ddenom+X[k]*d2denom)/denom + X[k]*ddenom*ddenom/(denom*denom);
	    }
	}

      // done
      return;
    }

  }
}
