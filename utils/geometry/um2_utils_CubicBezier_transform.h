// Sriramajayam

#pragma once

namespace um2
{
  namespace utils
  {
    template<typename T>
      std::unique_ptr<CubicBezier> CubicBezier::create_transformation(T transform) const
      {
	// transform control points
	std::vector<double> control_pts{P0[0],P0[1], P1[0],P1[1], P2[0],P2[1], P3[0],P3[1]};
	transform(&control_pts[0]);
	transform(&control_pts[2]);
	transform(&control_pts[4]);
	transform(&control_pts[6]);

	// transformed geomerty
	std::unique_ptr<CubicBezier> tr_geom = std::make_unique<CubicBezier>(curve_id, control_pts, nSamples, NLParams);

	// done
	return std::move(tr_geom);
      }
      
  }
}
