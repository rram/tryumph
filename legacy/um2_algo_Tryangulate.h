// Sriramajayam

#pragma once

#include <um2_SplitFeatureSet.h>
#include <um2_algo_Project_Relax.h>
#include <um2_algo_PosEdgeTopology.h>
#include <um2_algo_PositiveEdges.h>
#include <um2_algo_PosEdgeBoundaryIntersection.h>
#include <um2_algo_SnapMeshToFeaturePoints.h>
#include <um2_algo_CallbackInterface.h>
#include <fstream>

namespace um2
{
  namespace algo
  {
    //! \ingroup algo
    template<typename MeshType>
      std::pair<std::set<int>, TryInfo>
      Tryangulate(MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
		  const MeshingParams& mparams,
		  CallbackInterface<MeshType> *callback)
      
      {
	// Info about this try
	TryInfo try_info;
	
	// Start the clock
	std::chrono::steady_clock::time_point timer_begin = std::chrono::steady_clock::now();
	
	// Access
	using pointID       = std::pair<boost_point2D, int>;
	auto& WM            = mesh_wrapper.GetMesh();
	const auto& BG      = WM.getParentMesh();
	const auto& fset    = WM.getSplitFeatureSet();
	const int nFeatures = fset.getNumFeatures();
	const auto& gparams = fset.getFeatureParams();
	auto& mout          = WM.getSplitFeatureCorrespondences();

	// check the status of the working mesh
	assert(WM.getMeshStatus()==WorkingMeshStatus::UNPROCESSED);
	
	// Reinitialize outputs
	for(auto& m:mout) {
	     m.reason = MeshingReason::Unassigned;
	     m.PosVerts.clear();
	     m.PosCutElmFaces.clear();
	     m.terminalVertices[0] = -1;
	     m.terminalVertices[1] = -1;
	     m.splittingVertex     = -1;
	  }
	
	// vertices of the background mesh around which to refine in case of a failure
	std::set<int> refine_Verts{}; 

	// vertices of the background mesh that cannot be perturbed
	const std::set<int> frozen_nodes = mesh_wrapper.getBoundaryNodes(); 
	  
	// 1. Snap vertices in WM to terminals and splitting points of features
	SnapMeshToFeaturePoints(mesh_wrapper, frozen_nodes, mparams, refine_Verts);
	WM.setMeshStatus(WorkingMeshStatus::CORNERS_SNAPPED);
	
	// Visualize corner-adjusted mesh
	if(callback!=nullptr)
	  (*callback)(WM);
	
	// 2. Attempt to find positive edges for features which can be inspected further
	IdentifyPositiveEdges(mesh_wrapper, mparams.min_quality, refine_Verts);
	WM.setMeshStatus(WorkingMeshStatus::POSITIVE_EDGES_IDENTIFIED);
	
	// Visualize positive edges and positive elements
	if(callback!=nullptr)
	  (*callback)(WM);

	// Inspect if any positive vertices intersect the boundary
	InspectPositiveEdgeBoundaryIntersection(frozen_nodes, WM, refine_Verts);
	
	// 3. Inspect pair-wise intersections of sets of all computed positive edges
	InspectPositiveEdgesTopology(WM, refine_Verts);
	WM.setMeshStatus(WorkingMeshStatus::POSITIVE_EDGE_TOPOLOGY_INSPECTED);
      			            
	// Count the # successfully processed features
	int fcount = 0;
	for(int f=0; f<nFeatures; ++f)
	  {
	    if(mout[f].reason==MeshingReason::Success)
	      ++fcount;
	  }

	// 4. If successful in identifying positive vertices, proceed to projecting & relaxing vertices
	if(fcount!=nFeatures)
	  {
	    WM.setMeshStatus(WorkingMeshStatus::NONCONFORMING);
	    try_info.success = false;
	  }
	else
	  {
	    // tuple (result, defective elements, #relax iters, #relax verts)
	    auto PR_result = Project_Relax(mesh_wrapper, frozen_nodes, mparams, callback);
	    WM.setMeshStatus(WorkingMeshStatus::VERTICES_PROJECTED_RELAXED);
	    
	    const auto& success          = std::get<0>(PR_result);
	    const auto& defectiveElmList = std::get<1>(PR_result);
	    try_info.nRelaxIters         += std::get<2>(PR_result);
	    try_info.nRelaxVerts         += std::get<3>(PR_result);

	    if(success==MeshingReason::Success)
	      {
		WM.setMeshStatus(WorkingMeshStatus::CONFORMING);
		try_info.success = true;
	      }
	    else
	      {
		WM.setMeshStatus(WorkingMeshStatus::NONCONFORMING);
		assert(defectiveElmList.empty()==false);
		try_info.success = false;
	      }
	  
	    // Some elements may need to be refined.
	    // Append a vertex from each defective element to the list of vertices around which to refine
	    for(auto& feat_elm_pair:defectiveElmList)
	      {
		mout[feat_elm_pair.first].reason = MeshingReason::Fail_ElementQuality;
		refine_Verts.insert(BG.connectivity(feat_elm_pair.second)[0]);  // Choice of 0th vertex is arbitrary
	      }
	  }
	
	// try completed
	if(callback!=nullptr)
	  {
	    (*callback)(WM);
	  }
	
	// stop the clock
	std::chrono::steady_clock::time_point timer_end = std::chrono::steady_clock::now();

	// Track the run time
	try_info.runtime = std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_begin).count();

	// Track failure record
	for(int c=0; c<nFeatures; ++c)
	  {
	    auto& reason = mout[c].reason;
	    assert(reason!=MeshingReason::Unassigned);
	    auto jt = try_info.failures.find(reason);
	    assert(jt!=try_info.failures.end());
	    ++(jt->second);
	  }
	
	// done
	return {refine_Verts, try_info}; // refine_Verts is empty if successful in meshing  
      }

  } // algo::
} // um::

