// Sriramajayam

#ifndef UM_SIGNED_DISTANCE_H
#define UM_SIGNED_DISTANCE_H

#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_CubicSpline.h>

namespace um
{
  //! Signed distance function for a geometry
  //! params is assumed to be of a pointer to GeomType object
  template<class GeomType>
    inline void Geometry_SignedDistanceFunction(const double* X, double& sd, double* dsd, void* params)
    {
      assert(params!=nullptr);
      auto geom = static_cast<GeomType*>(params);
      geom->GetSignedDistance(X, sd, dsd);
      return;
    }
  
  //! Explicit instantiations of templated signed distance function
  const auto p1segment_sdfunc = Geometry_SignedDistanceFunction<LineSegment>;
  const auto p2rationalbezier_sdfunc = Geometry_SignedDistanceFunction<QuadraticRationalBezier>;
  const auto p3bezier_sdfunc = Geometry_SignedDistanceFunction<CubicBezier>;
  const auto p3spline_sdfunc = Geometry_SignedDistanceFunction<CubicSpline>;

  //! Useful aliases
  using P1Segment_FuncParams        = LineSegment*;
  using P2RationalBezier_FuncParams = QuadraticRationalBezier*;
  using P3Bezier_FuncParams         = CubicBezier*;
  using P3Spline_FuncParams         = CubicSpline*;
  
}

#endif
