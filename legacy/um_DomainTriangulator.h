// Sriramajayam

/** \file um_DomainTriangulator.h
 * \brief Defines the class um::DomainTriangulator
 * \author Ramsharan Rangarajan
 * Last modified: May 28, 2020.
 */

#ifndef UM_DOMAIN_TRIANGULATOR_H
#define UM_DOMAIN_TRIANGULATOR_H

#include <dvr_Module>
#include <um_WorkingMesh.h>
#include <um_Types.h>
#include <list>
#include <map>

namespace um
{
  /** \brief Class to encapsulate data and utilities to compute a um::WorkingMesh
   * from a given universal mesh  that conforms to a specified geometry.
   * The class is templated by the type of the universal mesh.
   * Information about the geometry is specified through three function pointers.
   * 
   * \tparam UMeshType Type of the universal mesh
   */
  template<class UMeshType>
    class DomainTriangulator
    {
    public:
      /** \brief Constructor
       * \param[in] bgmesh Universal mesh. Referenced.
       * \param[in] ifunc Indicator function
       * \param[in] sdfunc Signed distance function
       */
      DomainTriangulator(const UMeshType& bgmesh,
			 const IndicatorFunction ifunc,
			 const dvr::SignedDistanceFunction sdfunc);

      //! Destructor
      inline virtual ~DomainTriangulator() {}

      // Disable copy
      DomainTriangulator(const DomainTriangulator<UMeshType>&) = delete;

      // Disable assignment
      DomainTriangulator& operator=(const DomainTriangulator&) = delete;

      //! Returns reference to the background mesh
      inline const UMeshType& GetUniversalMesh() const
      { return BG; }
    
      //! Returns reference to the working mesh
      inline WorkingMesh<UMeshType>& GetWorkingMesh()
      { return WM; }
    
      //! Returns the set of positive vertices
      inline const std::vector<int>&  GetPositiveVertices() const
      { return PosVerts; }
    
      //! Returns the list of positive element-face pairs
      inline const std::list<std::pair<int,int>>& GetPositiveElmFacePairs() const
      { return PosElmsFaces; }
    
      /** \brief Perturb/relax a working mesh to conform to the boundary.
       *
       * \param[in] Nproject Number of project steps
       * \param[in] Nrelax Number of relaxation steps
       * \param[in] Ndist Graph-distance from the boundary defining the relaxation radius
       */
      void GenerateMesh(const int Nproject, const int Nrelax, const int Ndist);
      
      /** Relax the working mesh using DVR
       * \param[in] Nrelax Number of relaxation steps
       * \param[in] Ndist Graph-distance from the boundary defining the relaxation radius
       * \param[in] bdflag If true, relax positive vertices along tangential directions. Defaulted to true.
       * If true, checks that the Mesh() method has been invoked.
       */
      void OptimizeMesh(const int Nrelax, const int Ndist, const bool bdFlag=true);
  
    private:
      const UMeshType& BG; //!< Reference to the background mesh
      dvr::SignedDistanceFunction SDFunc; //!< Function pointer to evaluate the signed distance function
      WorkingMesh<UMeshType> WM; //!< Working mesh
      std::list<std::pair<int, int>> PosElmsFaces; //!< List of positive (element, face) pairs
      std::vector<int> PosVerts; //!< List of positive vertices
      bool isConforming; //!< Flag to track whether the working mesh conforms to the boundary
    
      //! Helper function to identify the set of positively cut elements
      //! \param[in] XiVals Values of the indicator function
      void IdentifyPositivelyCutElements(const std::map<int, Indicator>& XiVals);

      //! Helper function to identify the set of working elements
      //! Elements in WM are set. No coordinate perturbations are applied
      void SetWorkingElements();

      //! Helper method to identify vertices lying with a specified number of 1-rings
      //! of the set of positive vertices in the working mesh
      //! \param[in] nLayers Number of rings to consider
      //! \return Vertices lying a graph-distance of "nLayers" from the set of +ve vertices, not including +ve vertices
      std::vector<int> GetVertexLayers(const int nLayers) const;

      //! Helper method for meshing
      template<typename QType, int SPD>
	void MeshWithDVR(const int Nproject, const int Nrelax, const int Ndist);

      //! Helper method for relaxation
      template<typename QType, int SPD>
	void RelaxWithDVR(const int Nrelax, const int Ndist, const bool bdFlag);
    };
}


// Implementation of main functionalities
#include <um_DomainTriangulator_Impl.h>

#endif
