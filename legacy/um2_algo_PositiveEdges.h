// Sriramajayam

#pragma once

#include <um2_FeatureSet.h>
#include <um2_algo_TriangleFreeChain.h>
#include <um2_Output.h>

namespace um2
{
  namespace algo
  {
    // Helper function to identify the set of positive vertices for a feature
    template<typename MeshType>
      MeshingReason IdentifyPositiveVerticesForFeature(const MeshWrapper<WorkingMesh<MeshType>> &mesh_wrapper,
						       const FeatureParams& segParams,
						       const FeatureOrientation& orientation,
						       const int* terminalVerts,
						       const std::vector<int>& onFeatureVerts,
						       const double qthreshold,
						       std::vector<int>& PosVerts,
						       std::vector<std::tuple<int,int,bool>>& PosCutElmFaces);

    template<typename MeshType>
      void IdentifyPositiveEdges(MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
				 const double qthreshold,
				 std::set<int>& refine_Verts)
      {
	auto& WM                 = mesh_wrapper.GetMesh();
	const auto& fset         = WM.getSplitFeatureSet();
	const int nFeatures      = fset.getNumFeatures();
	const auto& gparams      = fset.getFeatureParams();
	const auto& orientations = fset.getFeatureOrientations();
	auto& mout               = WM.getSplitFeatureCorrespondences();

	// Check status of the working mesh
	assert(WM.getMeshStatus()==WorkingMeshStatus::CORNERS_SNAPPED);
	
	// Attempt to find positive edges for features which can be inspected further
	for(int c=0; c<nFeatures; ++c)
	  {
	    if(mout[c].reason==MeshingReason::Success)
	      {
		// Vertices snapped onto the feature already
		const std::vector<int> onFeatureVerts{mout[c].terminalVertices[0], mout[c].terminalVertices[1]};

		mout[c].reason = IdentifyPositiveVerticesForFeature(mesh_wrapper, gparams[c], orientations[c],
								    mout[c].terminalVertices, onFeatureVerts,
								    qthreshold, mout[c].PosVerts, mout[c].PosCutElmFaces);

		if(mout[c].reason!=MeshingReason::Success) // Failure? refine around the last inspected positive vertex
		  refine_Verts.insert(mout[c].PosVerts.back());
		  
	      } // Finished processing feature #c
	  }

	// done
	return;
      }
				 
    
    
    // Identify the set of positive vertices for a feature
    template<typename MeshType>
      MeshingReason IdentifyPositiveVerticesForFeature(const MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
						       const FeatureParams& segParams,
						       const FeatureOrientation& orientation,
						       const int* terminalVerts,
						       const std::vector<int>& onFeatureVerts,
						       const double qthreshold,
						       std::vector<int>& PosVerts,
						       std::vector<std::tuple<int,int,bool>>& PosCutElmFaces)
      {
	auto& WM = mesh_wrapper.GetConstMesh();
	const auto& BG = WM.getParentMesh();
	PosVerts.clear();
	PosCutElmFaces.clear();
	const bool PosCutElmFlag = (orientation==FeatureOrientation::Positive) ? true : false;
	
	// Aliases
	auto& sdfunc = segParams.signed_distance_function;
      
	// helpers
	const int nodes_element = 3;
	const int nfaces_element = 3;
	const int nodes_face = 2;
	int posedges[2][2];
	double SD = 0.;
	double dSD[2];
	
	// Enumeration of local face nodes (BG and WM have identical orderings)
	const int* local_facenodes[] = {mesh_wrapper.getLocalFaceNodes(0),
					mesh_wrapper.getLocalFaceNodes(1),
					mesh_wrapper.getLocalFaceNodes(2)};

	// Map: local node number ==> local opposite face
	int local_node_to_oppface_map[3] = {-1,-1,-1};
	{
	  std::set<int> all_elm_nodes{};
	  for(int f=0; f<nfaces_element; ++f)
	    for(int i=0; i<nodes_face; ++i)
	      all_elm_nodes.insert(local_facenodes[f][i]);
	
	  for(int f=0; f<nfaces_element; ++f)
	    { std::set<int> thisfacenodes(local_facenodes[f], local_facenodes[f]+nodes_face);
	      std::vector<int> oppnode{};
	      std::set_difference(all_elm_nodes.begin(), all_elm_nodes.end(),
				  thisfacenodes.begin(), thisfacenodes.end(),
				  std::inserter(oppnode, oppnode.begin()));
	      assert(static_cast<int>(oppnode.size())==1);
	      local_node_to_oppface_map[oppnode[0]] = f;
	    }
	  for(int f=0; f<nfaces_element; ++f)
	    { assert(local_node_to_oppface_map[f]!=-1); }
	}
     
      
	// Sign of the signed distance function at vertices, and whether these vertices project onto the feature?
	// Maintain a map to avoid redundant signed distance calculations at vertices of 1-rings
	std::map<int, bool> sdvals{}; // vertex -> sign
	for(auto& vert:onFeatureVerts)
	  sdvals[vert] = true;

	// Maintain a map of closest point projections. needed only for positive vertices
	// note that vertices already on the feature project onto themselves.
	std::map<int, std::pair<double,double>> cptmap{};
	for(auto& vert:onFeatureVerts)
	  {
	    const double* X = WM.coordinates(vert);
	    cptmap.insert({vert, {X[0],X[1]}});
	  }
	
	// Assign the orientation of positive edges
	double sdsign = 1.;
	if(orientation==FeatureOrientation::Negative)
	  sdsign = -1.;
      
	// Start crawling from the first terminal vertex towards the second
	int curr_pos_vertex = terminalVerts[0];
	const int& stopping_vertex = terminalVerts[1];
      
	// Evaluate the tangent at the first terminal point
	double tgtvec[2] = {0.,0.};
	{
	  const double* X = WM.coordinates(terminalVerts[0]);
	  sdfunc(X, SD, dSD);
	  tgtvec[0] = dSD[1];
	  tgtvec[1] = -dSD[0];
	}
      
	// Relative orientation between edges and the curve
	// localPosVert = 0 -> the next positive edge emanates from the current positive node
	// localPosVert = 1 -> the next positive edge is incident at the current positive node
	int localPosVert = -1;

	// Topology check: ensure that no vertex is repeated
	std::set<int> PosVertSet{};

	do
	  {
	    // Note that this is a positive vertex
	    sdvals[curr_pos_vertex] = true;   // +ve sign
	  
	    // Append this vertex to the list of positive vertices
	    PosVerts.push_back(curr_pos_vertex);
	    auto insertion_check = PosVertSet.insert(curr_pos_vertex);
	    if(insertion_check.second==false) // repeated positive vertex
	      return MeshingReason::Fail_PositiveEdge_FeatureTopology;
	    
	    // Vertices in the 1-ring
	    const auto& oneRingVerts = mesh_wrapper.Get1RingVertices(curr_pos_vertex);
	  
	  
	    // Signed distance function at vertices around  "curr_pos_vertex"
	    for(auto& vert:oneRingVerts)
	      {
		// Compute signed distance only if it not already available
		auto it = sdvals.find(vert);
		bool vertexSign;
		if(it==sdvals.end())
		  {
		    const double* X = WM.coordinates(vert);
		    sdfunc(X, SD, dSD);
		    vertexSign = (sdsign*SD>0.) ? true : false;
		    sdvals[vert] = vertexSign;
		  }

		// save the closest point projection
		if(vertexSign==true && cptmap.find(vert)==cptmap.end())
		  {
		    const double* X = WM.coordinates(vert);
		    cptmap.insert({vert, {X[0]-SD*dSD[0], X[1]-SD*dSD[1]}});
		  }
	      }

	    // Elements in the 1-ring
	    const auto& oneRingElms = mesh_wrapper.Get1RingElements(curr_pos_vertex);
	    
	    // which elements in the 1-ring are positively cut?
	    std::vector<std::pair<int,int>> onering_posElmsFaces{};
	    for(auto& elm:oneRingElms)
	      {
		// This element & its connectivity
		const int* conn = BG.connectivity(elm);
	      
		// Signed distances at the nodes of this element
		std::vector<int> posNodes{}, negNodes{};
		for(int a=0; a<nodes_element; ++a)
		  { const int& vert = conn[a];
		    auto it = sdvals.find(vert);
		    assert(it!=sdvals.end());
		    if(it->second==true)
		      posNodes.push_back(a);
		    else
		      negNodes.push_back(a);
		  }
	      
		// Is this element positively cut?
		if(static_cast<int>(posNodes.size())==nodes_element-1)
		  // yes. set the positive element -> face pair
		  onering_posElmsFaces.push_back( std::make_pair(elm, local_node_to_oppface_map[negNodes[0]]) );
	      }

	    // Precisely 2 elements in the 1-ring should be positively cut
	    if(static_cast<int>(onering_posElmsFaces.size())!=2)
	      return MeshingReason::Fail_PositiveEdge_FeatureTopology;

	    // The two positive edges at the current positive vertex
	    for(int i=0; i<2; ++i)
	      {
		const int& elm = onering_posElmsFaces[i].first;
		const int& face = onering_posElmsFaces[i].second;		
		const int* conn = BG.connectivity(elm);
	      
		// vertices of this face
		posedges[i][0] = conn[local_facenodes[face][0]];
		posedges[i][1] = conn[local_facenodes[face][1]];
	      }

	    // sanity checks: curr_pos_vert should be a node of both edges
	    assert((posedges[0][0]==curr_pos_vertex || posedges[0][1]==curr_pos_vertex) &&
		   (posedges[1][0]==curr_pos_vertex || posedges[1][1]==curr_pos_vertex) );
	  
	    // Set the orientation of the chain of positive vertices
	    // This is executed only at the first terminal point
	    if(localPosVert==-1)
	      {
		// Edge emanating from the current positive vertex
		const int edgeNum = (posedges[0][0]==curr_pos_vertex) ? 0 : 1;
		assert(posedges[edgeNum][0]==curr_pos_vertex);

		const double* A = WM.coordinates(posedges[edgeNum][0]);
		const double* B = WM.coordinates(posedges[edgeNum][1]);
		double dot = (B[0]-A[0])*tgtvec[0] + (B[1]-A[1])*tgtvec[1];
	      
		// Pick the orientation having positive sign
		// i.e., Is this edge emanating or incident at the current positive vertex
		localPosVert = (dot>0.) ? 0 : 1;
	      }
	    // Relative orientation of edges & segment is now known
	    
	    // Advance to the nuly found positive vertex
	    int prev_pos_vertex = curr_pos_vertex;
	    if(posedges[0][localPosVert]==curr_pos_vertex)
	      {
		curr_pos_vertex = posedges[0][(localPosVert+1)%2];
		PosCutElmFaces.push_back( std::make_tuple(onering_posElmsFaces[0].first, onering_posElmsFaces[0].second, PosCutElmFlag) );
	      }
	    else
	      {
		curr_pos_vertex = posedges[1][(localPosVert+1)%2];
		PosCutElmFaces.push_back( std::make_tuple(onering_posElmsFaces[1].first, onering_posElmsFaces[1].second, PosCutElmFlag) );
	      }

	    // Check that the positive edge added has
	    // (i)  positive dot product with the local tangent
	    // (ii) is not projected to too small a length
	    {
	      const double* A = WM.coordinates(prev_pos_vertex);
	      sdfunc(A, SD, dSD);
	      tgtvec[0] = dSD[1];
	      tgtvec[1] = -dSD[0];
	      const double* B = WM.coordinates(curr_pos_vertex);
	      double dot = (B[0]-A[0])*tgtvec[0] + (B[1]-A[1])*tgtvec[1];

	      if(dot<=0.)
		return MeshingReason::Fail_PositiveEdge_Obtuse;

	      // unprojected edge length
	      const double lenAB = std::sqrt((A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]));

	      // projected edge length
	      auto it_A = cptmap.find(prev_pos_vertex);
	      auto it_B = cptmap.find(curr_pos_vertex);
	      assert(it_A!=cptmap.end() && it_B!=cptmap.end());
	      const double piA[] = {it_A->second.first, it_A->second.second};
	      const double piB[] = {it_B->second.first, it_B->second.second};
	      const double proj_lenAB = std::sqrt((piA[0]-piB[0])*(piA[0]-piB[0]) + (piA[1]-piB[1])*(piA[1]-piB[1]));
	      if(proj_lenAB/lenAB<qthreshold)
		return MeshingReason::Fail_ElementQuality;
	    }
	  
	    // Proceed to the next positive vertex
	  } while(curr_pos_vertex!=stopping_vertex);
      
	// Append the stopping vertex
	PosVerts.push_back( stopping_vertex );
      
	// All positive vertices for this segment have been identified

	// Make this chain triangle-free
	MakeOpenChainTriangleFree(mesh_wrapper, PosVerts, PosCutElmFaces);
	
	// Sanity check
	assert(PosVerts.front()==terminalVerts[0] && PosVerts.back()==terminalVerts[1]);
	
	// done
	return MeshingReason::Success;
      }
  }
}

