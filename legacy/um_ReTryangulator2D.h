// Sriramajayam

#ifndef UM_RETRYANGULATOR2D_H
#define UM_RETRYANGULATOR2D_H

#include <um_WorkingMesh.h>
#include <um_Triangulator2D_Utils.h>

namespace um
{
  template<typename UMeshType>
    class ReTryangulator2D
    {
    public:
      ReTryangulator2D(const WorkingMesh<UMeshType>& wm,
		       const std::vector<FeatureMeshOutput>& fmo);

      inline virtual ~ReTryangulator2D() {}

      // Disable copy and assignment
      ReTryangulator2D(const ReTryangulator2D&) = delete;
      ReTryangulator2D& operator=(const ReTryangulator2D&) = delete;

      // Access the background mesh
      inline UMeshType& GetUniversalMesh() const
      { return *BG; }

      //! Access the working mesh
      inline const WorkingMesh<UMeshType>& GetWorkingMesh() const
      { return WM; }

      // Main functionality
      void GenerateMesh(const MeshingParams& mparams,
			const FeatureSet& fset,
			RunInfo& run_info);

    private:
      const UMeshType& BG;
      const WorkingMesh<UMeshType>& parentWM;
      const std::vector<FeatureMeshOutput>& feature_mesh_details;
      WorkingMesh<UMeshType> WM;
    };
}

#endif

// Implementation
#include <um_ReTryangulator2D_generate.h>
