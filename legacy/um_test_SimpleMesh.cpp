
/** \file um_test_SimpleMesh_Impl.h
 * \brief Implementation of the class SimpleMesh.
 * \author Ramsharan Rangarajan
 */

#include <fstream>
#include <unordered_set>
#include <algorithm>
#include <array>
#include <cmath>
#include <um_test_SimpleMesh.h>

namespace um
{
  namespace test
  {
    // Constructor
    SimpleMesh:: SimpleMesh(const std::vector<double>& coord, const std::vector<int>& conn)
      :dvr::test::SimpleMesh()
    {
      Coordinates = coord;
      Connectivity = conn;
      spatial_dimension = 2;
      nodes_element = 3;
      nodes = static_cast<int>(coord.size()/2);
      elements = static_cast<int>(conn.size()/3);
      SetupMesh();
    }
  
    // Method to finalize the mesh data
    void SimpleMesh::SetupMesh()
    {
      assert(spatial_dimension==2 && nodes_element==3);
    
      // Compute 1-rings and valencies
      dvr::test::SimpleMesh::SetupMesh();

      // Compute element neighbor lists
      ComputeElementNeighbors();

      // -- done --
      return;
    }


    // Computes the element neighbor list
    void SimpleMesh::ComputeElementNeighbors()
    {
      // Helper struct
      struct facestructure
      {
	int element;
	int face;
	std::vector<int> conn;
	bool operator<(const facestructure &other) const
	{ return conn<other.conn; }
      };
  
      const int nFaces = nodes_element;
      const int nFaceNodes = nodes_element-1;
  
      // Enumerate all faces in the mesh
      facestructure FS;
      FS.conn.resize(nFaceNodes);
      std::vector<facestructure> ElmFacePairs(elements*nFaces);
  
      int pcount = 0;
      for(int e=0; e<elements; ++e)
	{
	  const auto* elmconn = connectivity(e);
	  for(int f=0; f<nFaces; ++f)
	    {
	      // This face
	      FS.element = e;
	      FS.face = f;
	  
	      // local nodes on this face
	      const auto* locnodes = GetLocalFaceNodes(f);
	      for(int i=0; i<nFaceNodes; ++i)
		FS.conn[i] = elmconn[locnodes[i]];
	      std::sort(FS.conn.begin(), FS.conn.end());
	      ElmFacePairs[pcount++] = FS;
	    }
	}
  
      // Sort the pairs of element-face pairs
      std::sort(ElmFacePairs.begin(), ElmFacePairs.end());
  
      // Resize
      ElmNbs.resize( elements*nFaces*2 );
      std::fill(ElmNbs.begin(), ElmNbs.end(), -1);
  
      // Faces that have exactly the same connectivity should appear consecutively
      // If cannot find a pair, the face has no neighbor.
      int e, f, nbelm, nbface;
      for(auto it=ElmFacePairs.begin(); it!=ElmFacePairs.end(); ++it)
	{
	  e = it->element;
	  f = it->face;
	  if((it+1)!=ElmFacePairs.end())
	    {
	      nbelm = (it+1)->element;
	      nbface = (it+1)->face;
	      if(it->conn==(it+1)->conn)
		{
		  ElmNbs[2*e*nFaces+2*f] = nbelm;
		  ElmNbs[2*e*nFaces+2*f+1] = nbface;
		  ElmNbs[2*nbelm*nFaces+2*nbface] = e;
		  ElmNbs[2*nbelm*nFaces+2*nbface+1] = f;
	      
		  // Skip next element
		  ++it;
		}
	    }
	}  
    }


    // Print the mesh to a file in tecplot format
    void SimpleMesh::PlotTecSubMesh(const std::string filename,
				    const std::set<int>& ElmSet) const
    {
      // Open file to plot
      std::fstream outfile;
      outfile.open(filename, std::ios::out);
      outfile.precision(16);
      outfile.setf( std::ios::scientific );
  
      // Line 1:
      outfile<<"VARIABLES = \"X\", \"Y\" \n";
    
      // Line 2:
      outfile<<"ZONE t=\"t:0\", N="<<nodes
	     <<", E="<<static_cast<int>(ElmSet.size())
	     <<", F=FEPOINT, ET=TRIANGLE";
  
      // Nodal coordinates
      for(int a=0; a<nodes; ++a)
	{
	  const auto* X = coordinates(a);
	  outfile<<"\n";
	  for(int k=0; k<spatial_dimension; ++k)
	    outfile<<X[k]<<" ";
	}
  
      // Element connectivity
      for(auto& e:ElmSet)
	{
	  outfile<<"\n";
	  const auto* conn = connectivity(e);
	  for(int a=0; a<nodes_element; ++a)
	    outfile<<conn[a]+1<<" ";
	}
      outfile.flush();
      outfile.close();
    }


    // Self-similar subdivision of a triangle mesh
    void SimpleMesh::SubdivideTriangles()
    { assert(spatial_dimension==2 && nodes_element==3);

      isFinalized = false;
    
      // Helper struct
      struct facestructure
      {
	int element, face;
	std::array<int,2> conn;
	facestructure(int e, int f, const int* co)
	  :element(e), face(f), conn({co[0], co[1]})
	{ std::sort(conn.begin(), conn.end()); }
    
	bool operator<(const facestructure &other) const
	{ return conn<other.conn; }
      };

      // Accummulate edges of triangles
      std::vector<facestructure> Faces({});
      Faces.reserve(2*elements); // Just an estimate, makes push_back less inefficient
      int edgeconn[2];
      for(int e=0; e<elements; ++e)
	for(int f=0; f<3; ++f) // 3 faces/element
	  {
	    edgeconn[0] = Connectivity[3*e+f];
	    edgeconn[1] = Connectivity[3*e+(f+1)%3];
	    Faces.push_back( facestructure(e,f,edgeconn) );
	  }
      Faces.shrink_to_fit();
      std::sort(Faces.begin(), Faces.end());

      // Record midside node numbers for each face in each element
      std::vector<int> ElmToNodeMap(3*elements);
      int  middlenodenum = nodes;
      Coordinates.reserve(Coordinates.size()+spatial_dimension*Faces.size());
	
      for(auto it=Faces.begin(); it!=Faces.end(); ++it)
	{
	  // Append mid-point
	  for(int k=0; k<spatial_dimension; ++k)
	    Coordinates.push_back
	      (0.5*(Coordinates[spatial_dimension*it->conn[0]+k] + Coordinates[spatial_dimension*it->conn[1]+k]) );

	  // Record midsize node number
	  ElmToNodeMap[3*it->element + it->face] = middlenodenum;

	  // Examine the next face
	  if(it+1!=Faces.end())
	    if(it->conn[0] == (it+1)->conn[0] && it->conn[1] == (it+1)->conn[1])
	      {
		// Identical to the previous face. Assign same mid-side node number
		it++;
		ElmToNodeMap[it->element*3 + it->face] = middlenodenum;
	      }

	  // Update midsize node number
	  ++middlenodenum;
	}

      // Update connectivity
      std::vector<int> copyconn(Connectivity);
      Connectivity.resize(elements*3*4);
      for(int e=0; e<elements; ++e)
	{
	  // triangle 1
	  Connectivity[e*4*3+ 0*3 + 0] = copyconn[e*3];
	  Connectivity[e*4*3+ 0*3 + 1] = ElmToNodeMap[e*3 + 0];
	  Connectivity[e*4*3+ 0*3 + 2] = ElmToNodeMap[e*3 + 2];

	  // triangle 2
	  Connectivity[e*4*3+ 1*3 + 0] = copyconn[e*3+1];
	  Connectivity[e*4*3+ 1*3 + 1] = ElmToNodeMap[e*3 + 1];
	  Connectivity[e*4*3+ 1*3 + 2] = ElmToNodeMap[e*3 + 0];

	  // triangle 3
	  Connectivity[e*4*3+ 2*3 + 0] = copyconn[e*3+2];
	  Connectivity[e*4*3+ 2*3 + 1] = ElmToNodeMap[e*3 + 2];
	  Connectivity[e*4*3+ 2*3 + 2] = ElmToNodeMap[e*3 + 1];

	  // triangle 4
	  Connectivity[e*4*3+ 3*3 + 0] = ElmToNodeMap[e*3 + 0];
	  Connectivity[e*4*3+ 3*3 + 1] = ElmToNodeMap[e*3 + 1];
	  Connectivity[e*4*3+ 3*3 + 2] = ElmToNodeMap[e*3 + 2];
	}

      // Update the number of elements and nodes
      nodes = middlenodenum;
      elements *= 4;

      SetupMesh();
      isFinalized = true;
    
      // done
      return;
    }


    // Create an equilateral mesh
    void CreateEquilateralMesh(SimpleMesh& MD,
			       const double* center, const double size, int ndiv)
    {
      MD.nodes_element = 3;
      MD.spatial_dimension = 2;
      MD.Connectivity = std::vector<int>({0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1});
      MD.elements = 6;
      MD.nodes = 7;
      MD.Coordinates.reserve(2*7);
      for(int k=0; k<2; ++k)
	MD.Coordinates.push_back( 0. );
      for(int i=0; i<6; ++i)
	{
	  double theta = static_cast<double>(i)*(M_PI/3.);
	  MD.Coordinates.push_back( center[0]+size*std::cos(theta) );
	  MD.Coordinates.push_back( center[1]+size*std::sin(theta) );
	}
      MD.SetupMesh();
	
      // Subdivide
      for(int i=0; i<ndiv; ++i)
	MD.SubdivideTriangles();

      // Finalize
      MD.SetupMesh();

      // done
      return;
    }
  
    // Create a square mesh
    void CreateSquareMesh(SimpleMesh& MD,
			  const double* center, const double size, int ndiv)
    {
      MD.nodes_element = 3;
      MD.spatial_dimension = 2;
    
      MD.Coordinates = std::vector<double>
	{-5.000000e-01, -5.000000e-01, 
	 -1.666667e-01, -5.000000e-01, 
	 1.666667e-01, -5.000000e-01, 
	 5.000000e-01, -5.000000e-01, 
	 5.000000e-01, -1.666667e-01, 
	 5.000000e-01, 1.666667e-01, 
	 5.000000e-01, 5.000000e-01, 
	 1.666667e-01, 5.000000e-01, 
	 -1.666667e-01, 5.000000e-01, 
	 -5.000000e-01, 5.000000e-01, 
	 -5.000000e-01, 1.666667e-01, 
	 -5.000000e-01, -1.666667e-01, 
	 -3.000000e-01, -2.500000e-01, 
	 0.000000e+00, -1.666667e-01, 
	 3.000000e-01, -2.500000e-01, 
	 2.500000e-01, 0.000000e+00, 
	 3.000000e-01, 2.500000e-01, 
	 0.000000e+00, 1.666667e-01, 
	 -3.000000e-01, 2.500000e-01, 
	 -2.500000e-01, 0.000000e+00};
      MD.nodes = static_cast<int>(MD.Coordinates.size())/2;
      for(int n=0; n<MD.nodes; ++n)
	{ MD.Coordinates[2*n]   += center[0];
	  MD.Coordinates[2*n+1] += center[1];
	  MD.Coordinates[2*n]   *= size;
	  MD.Coordinates[2*n+1] *= size; }
    
      MD.Connectivity = std::vector<int>
	{14, 16, 18, 
	 14, 18, 20, 
	 14, 15, 16, 
	 16, 17, 18, 
	 18, 19, 20, 
	 20, 13, 14, 
	 16, 5, 6, 
	 16, 6, 17, 
	 17, 6, 7, 
	 17, 7, 8, 
	 18, 17, 8, 
	 18, 8, 9, 
	 18, 9, 19, 
	 19, 9, 10, 
	 19, 10, 11, 
	 20, 19, 11, 
	 20, 11, 12, 
	 20, 12, 13, 
	 13, 12, 1, 
	 13, 1, 2, 
	 13, 2, 14, 
	 14, 2, 3, 
	 14, 3, 15, 
	 15, 3, 4, 
	 15, 4, 5, 
	 15, 5, 16};
      for(auto& n:MD.Connectivity) --n;
      MD.elements = static_cast<int>(MD.Connectivity.size())/3;
      MD.SetupMesh();
    
      // Subdivide
      for(int i=0; i<ndiv; ++i)
	MD.SubdivideTriangles();
    
      // Finalize
      MD.SetupMesh();
    }
  }
}
