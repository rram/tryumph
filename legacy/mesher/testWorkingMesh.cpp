// Sriramajayam

#include <umesh_SimpleMesh.h>
#include "um_WorkingMesh.h"
#include <cassert>
#include <random>
#include <iostream>
#include <cmath>
#include <map>


int main()
{
  // Generate a random set of coordinates and connectivity
  umesh::SimpleMesh MD;
  MD.nodes_element = 3;
  MD.elements = 20;
  MD.nodes = 15;
  MD.spatial_dimension = 2;
  std::random_device rand_dev;
  std::mt19937 generator(rand_dev());
  std::uniform_int_distribution<int> int_dist(0, 14);
  MD.Connectivity.resize(MD.nodes_element*MD.elements);
  for(int e=0; e<MD.elements; ++e)
    for(int a=0; a<MD.nodes_element; ++a)
      MD.Connectivity[MD.nodes_element*e+a] = int_dist(generator);
  
  std::uniform_real_distribution<double> real_dist(-2.,2.);
  MD.Coordinates.resize(MD.nodes*MD.spatial_dimension);
  for(int n=0; n<MD.nodes; ++n)
    for(int k=0; k<MD.spatial_dimension; ++k)
      MD.Coordinates[MD.spatial_dimension*n+k] = real_dist(generator);
  MD.SetupMesh();
  
  // Create working mesh
  um::WorkingMesh<decltype(MD)> WM(MD);

  // Insert some modified nodes (all even numbered)
  assert(static_cast<int>(WM.GetPerturbedNodes().size())==0);
  std::vector<double> newcoord(MD.spatial_dimension);
  int nNewNodes = 0;
  for(auto a=0; a<MD.nodes; a+=2)
    {
      newcoord[0] = real_dist(generator);
      newcoord[1] = real_dist(generator);
      WM.update(a, &newcoord[0]);
      ++nNewNodes;
      const double* X = WM.coordinates(a);
      assert(std::abs(X[0]-newcoord[0])+std::abs(X[1]-newcoord[1])<1e-8);
    }
  assert(static_cast<int>(WM.GetPerturbedNodes().size())==nNewNodes);
  
  // Randomly insert 10 elements
  std::set<int> wmelms{};
  while(static_cast<int>(wmelms.size())<10)
    wmelms.insert(int_dist(generator));
  for(auto& e:wmelms)
    WM.addElement(e);
  WM.SetupMesh();

  // List of removed elements
  std::set<int> removed_elms{};
  std::set<int> allelms(MD.GetElements().begin(), MD.GetElements().end());
  std::set_difference(allelms.begin(), allelms.end(), wmelms.begin(), wmelms.end(),
		      std::inserter(removed_elms, removed_elms.begin()));
  assert(static_cast<int>(removed_elms.size())!=0);
  assert(removed_elms.size()+wmelms.size()==allelms.size());
  
  // Check that the element list ranges is correct
  const auto& ElmList = WM.GetElements();
  assert(ElmList.size()==wmelms.size());
  assert(WM.GetSpatialDimension()==2);
  assert(WM.GetNumNodesPerElement()==MD.GetNumNodesPerElement());
  for(auto& e:wmelms)
    { assert(ElmList.find(e)!=ElmList.end());
      assert(WM.connectivity(e)==MD.connectivity(e)); }

  // Access tests
  assert(WM.GetNumNodesPerElement()==MD.GetNumNodesPerElement());
  assert(WM.GetMaxElementValency()==MD.GetMaxElementValency());
  assert(WM.GetMaxVertexValency()==MD.GetMaxVertexValency());

  // Test element neighbors
  for(auto& e:ElmList)
    {
      const int* wm_nblist = WM.GetElementNeighbors(e);
      const int* md_nblist = MD.GetElementNeighbors(e);

      // wm_nblist should not contain removed elements, should only contain elements in WM
      for(int f=0; f<3; ++f)
	if(wm_nblist[2*f]!=-1)
	  { assert(removed_elms.find(wm_nblist[2*f])==removed_elms.end());
	    assert(ElmList.find(wm_nblist[2*f])!=ElmList.end()); }

      // wm_nblist and md_nblist should differ only if a neighbor is a removed element
      for(int f=0; f<3; ++f)
	if(wm_nblist[2*f]!=md_nblist[2*f])
	  { const int& nbelm = md_nblist[2*f];
	    assert(nbelm!=-1);
	    assert(removed_elms.find(nbelm)!=removed_elms.end()); }
      
      // All removed elements should be reflected in the working mesh list
      for(int f=0; f<3; ++f)
	{
	  const int& nbelm = md_nblist[2*f];
	  if(nbelm!=-1)
	    if(removed_elms.find(nbelm)!=removed_elms.end())
	      assert(wm_nblist[2*f]==-1);
	}
    }

  // test 1-ring elements of vertices in WM
  std::set<int> wmverts{};
  for(auto& e:ElmList)
    { const int* conn = WM.connectivity(e);
      for(int a=0; a<3; ++a)
	wmverts.insert(conn[a]); }
  
  int wm_n1RingElms = -1;
  const int* wm_oneRingElms;
  int md_n1RingElms = -1;
  const int* md_oneRingElms;
  for(auto& vert:wmverts)
    {
      WM.Get1RingElements(vert, &wm_oneRingElms, wm_n1RingElms);
      std::set<int> wm_set(wm_oneRingElms, wm_oneRingElms+wm_n1RingElms);
      MD.Get1RingElements(vert, &md_oneRingElms, md_n1RingElms);
      std::set<int> md_set(md_oneRingElms, md_oneRingElms+md_n1RingElms);
      assert(md_n1RingElms>=wm_n1RingElms);
      
      // wm_oneRingElms should be a subset of md_oneRingElms
      // missing elements can only be ones removed.

      // WM\MD should be empty
      std::set<int> wm_minus_md{};
      std::set_difference(wm_set.begin(), wm_set.end(), md_set.begin(), md_set.end(),
			  std::inserter(wm_minus_md, wm_minus_md.begin()));
      assert(wm_minus_md.empty()==true);
      
      // MD\WM should contain only removed elements
      std::set<int> md_minus_wm{};
      std::set_difference(md_set.begin(), md_set.end(), wm_set.begin(), wm_set.end(),
			  std::inserter(md_minus_wm, md_minus_wm.begin()));
      for(auto& e:md_minus_wm)
	assert(removed_elms.find(e)!=removed_elms.end());

      // wm_1RingElements should only contain elements from WM
      for(auto& e:wm_set)
	assert(ElmList.find(e)!=ElmList.end());
    }
  
  // Brute-force test of element 1-rings
  std::map<int, std::set<int>> wm_1rings{};
  for(auto& e:ElmList)
    {
      const int* conn = WM.connectivity(e);
      for(int a=0; a<3; ++a)
	{
	  auto it = wm_1rings.find(conn[a]);
	  if(it==wm_1rings.end())
	    wm_1rings[conn[a]] = std::set<int>{e};
	  else
	    it->second.insert(e);
	}
    }
  for(auto& v:wmverts)
    {
      const int* onering;
      int n1Ring;
      WM.Get1RingElements(v, &onering, n1Ring);
      std::set<int> testring(onering, onering+n1Ring);
      auto it = wm_1rings.find(v);
      assert(it!=wm_1rings.end());
      assert(testring==it->second);
    }
  
  // test 1-ring vertices of all vertices in WM
  const std::set<int> allverts(MD.Connectivity.begin(), MD.Connectivity.end());
  
  // Make a list of removed vertices
  std::set<int> removed_verts{};
  std::set_difference(allverts.begin(), allverts.end(), wmverts.begin(), wmverts.end(),
		      std::inserter(removed_verts, removed_verts.begin()));
  
  const int* wm_oneRingVerts;
  const int* md_oneRingVerts;
  int wm_n1RingVerts = -1;
  int md_n1RingVerts = -1;
  for(auto& vert:wmverts)
    {
      WM.Get1RingVertices(vert, &wm_oneRingVerts, wm_n1RingVerts);
      std::set<int> wm_set(wm_oneRingVerts, wm_oneRingVerts+wm_n1RingVerts);
      MD.Get1RingVertices(vert, &md_oneRingVerts, md_n1RingVerts);
      std::set<int> md_set(md_oneRingVerts, md_oneRingVerts+md_n1RingVerts);

      // wm's 1-ring should be a subset of md's 1-ring
      std::set<int> wm_minus_md{};
      std::set_difference(wm_set.begin(), wm_set.end(), md_set.begin(), md_set.end(),
			  std::inserter(wm_minus_md, wm_minus_md.begin()));
      assert(wm_minus_md.empty()==true);

      // none of the removed vertices should appear in the wm-rings
      for(int i=0; i<wm_n1RingVerts; ++i)
	assert(removed_verts.find(wm_oneRingVerts[i])==removed_verts.end());
    }
}
