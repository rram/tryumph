// Sriramajayam

#include <um_DomainTriangulator.h>
#include <umesh_SimpleMesh.h>
#include <cmath>
#include <iostream>

// Create a background mesh of equilateral triangles
void GetEquilateralMesh(umesh::SimpleMesh& MD);

void func_signed_distance(const double* X, double& sd, double* dsd)
{ const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
  sd = r-0.45;
  if(dsd!=nullptr)
    { dsd[0] = X[0]/r;
      dsd[1] = X[1]/r; }
}

um::Indicator func_characteristic(const double* X)
{ double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
  if(r>=0.45) return um::Indicator::Plus;
  else return um::Indicator::Minus; }

int main()
{
  umesh::SimpleMesh BG;
  GetEquilateralMesh(BG);
  BG.PlotTecMesh("BG.tec");

  // Sanity check on neighbor list
  for(int e=0; e<BG.elements; ++e)
    {
      const int* elm_nbs = BG.GetElementNeighbors(e);
      for(int f=0; f<3; ++f)
	if(elm_nbs[2*f]>=0)
	  {
	    const int& nb_elm = elm_nbs[2*f];
	    const int& nb_face = elm_nbs[2*f+1];
	    const int* nbelm_nbs = BG.GetElementNeighbors(nb_elm);
	    assert(nbelm_nbs[2*nb_face]==e);
	    assert(nbelm_nbs[2*nb_face+1]==f);
	  }
    }
  
  // Domain triangulator
  um::IndicatorFunction XiFunc(func_characteristic);
  dvr::SignedDistanceFunction SDFunc(func_signed_distance);
  um::DomainTriangulator<decltype(BG)> dt(BG, XiFunc, SDFunc);

  // Plot positively cut elements
  const auto& PosElmFacePairs = dt.GetPositiveElmFacePairs();
  std::set<int> PosElms{};
  for(auto& it:PosElmFacePairs)
    PosElms.insert(it.first);
  BG.PlotTecSubMesh("pos.tec", PosElms);

  // Plot the working mesh
  const auto& WM = dt.GetWorkingMesh();
  const auto& ElmList = WM.GetElements();
  std::set<int> wmelms(ElmList.begin(), ElmList.end());
  BG.PlotTecSubMesh("wm.tec", wmelms);

  // Mesh
  const int Nproject = 4;
  const int Nrelax = 5;
  const int Ndist = 3;
  dt.GenerateMesh(Nproject, Nrelax, Ndist);
  WM.PlotTecMesh("circ.tec");
  dt.OptimizeMesh(Nrelax,Ndist,false);
}


// Create a background mesh of equilateral triangles
void GetEquilateralMesh(umesh::SimpleMesh& MD)
{
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.Connectivity = std::vector<int>({0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1});
  MD.elements = 6;
  MD.nodes = 7;
  MD.Coordinates.reserve(2*7);
  for(int k=0; k<2; ++k)
    MD.Coordinates.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = static_cast<double>(i)*(M_PI/3.);
      MD.Coordinates.push_back( std::cos(theta) );
      MD.Coordinates.push_back( std::sin(theta) );
    }
  
  // Subdivide
  for(int i=0; i<3; ++i)
    MD.SubdivideTriangles();

  MD.SetupMesh();

  return;
}
