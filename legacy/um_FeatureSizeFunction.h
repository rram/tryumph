// Sriramajayam

#ifndef UM_FEATURE_SIZE_H
#define UM_FEATURE_SIZE_H

#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_CubicSpline.h>

namespace um
{
  //! Distance to the medial axis for a geometry
  template<class GeomType>
    inline double Geometry_FeatureSizeFunction(const double* X, void* params)
    {
      assert(params!=nullptr);
      auto& geom = *static_cast<const GeomType*>(params);

      double in_dist, out_dist;
      double in_X[2], out_X[2];
      geom.GetMedialAxis().GetDistanceToMedialAxis(X, out_dist, out_X, in_dist, in_X);

      if(out_dist<in_dist) return out_dist;
      else return in_dist;
    }

  //! Explicit instantiations of templated feature size functions;
  const auto p1segment_fsfunc        = Geometry_FeatureSizeFunction<LineSegment>;
  const auto p2rationalbezier_fsfunc = Geometry_FeatureSizeFunction<QuadraticRationalBezier>;
  const auto p3bezier_fsfunc         = Geometry_FeatureSizeFunction<CubicBezier>;
  const auto p3spline_fsfunc         = Geometry_FeatureSizeFunction<CubicSpline>;
  
}

#endif
