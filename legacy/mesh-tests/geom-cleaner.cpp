// Sriramajayam

#include <iostream>
#include <string>
#include <um_CreateFeatureSet.h>

// Determine a bounding box 
void GetBoundingBox(const um::test::GeomData& gdata, double* center, double& size);

int main()
{
  std::fstream pfile;
  pfile.open((char*)"filenames.txt", std::ios::in);
  std::string fname;
  pfile >> fname;
  std::vector<std::string> filenames{};
  while(pfile.good())
    {
      filenames.push_back(fname);
      pfile >> fname;
    }
  pfile.close();

  for(auto& gfile:filenames)
    {
      std::cout<<"\n\nProcessing geometry: "<<gfile<<std::flush;
	
      // output path for this case
      std::string output_path = "output/";

      // Nonlinear solver parameters
      um::NLSolverParams nlparams{.ftol=1.e-4, .ttol=1.e-3, .max_iter=25, .normTol=1.e-4};
	
      // Read features
      um::test::GeomData gdata;
      um::test::ReadFeatures("geom-data/"+gfile, 50, nlparams, gdata);

      // Sample each feature
      const auto& p1curves = gdata.p1curves;
      const auto& p2curves = gdata.p2curves;
      const auto& p3curves = gdata.p3curves;

      bool flag = true;
      for(auto& geom:p1curves)
	{
	  double endpts[4];
	  geom->GetTerminalPoints(endpts);
	  double len = std::sqrt((endpts[0]-endpts[2])*(endpts[0]-endpts[2])+(endpts[1]-endpts[3])*(endpts[1]-endpts[3]));
	  if(len<1.e-3)
	    {
	      std::cout<<"\nDetected a problem in linear feature "<<geom->GetCurveID()<<"\n"<<std::flush;
	      flag = false;
	    }
	}

      const int N = 1000;
      for(auto& geom:p2curves)
	for(int i=0; i<=N; ++i)
	  {
	    const double t = static_cast<double>(i)/static_cast<double>(N);
	    double X[2], dX[2];
	    geom->Evaluate(t, X, dX);
	    double len = std::sqrt(dX[0]*dX[0]+dX[1]*dX[1]);
	    if(len<1.e-3)
	      {
		std::cout<<"\nDetected a problem in quadratic feature "<<geom->GetCurveID()<<"\n"<<std::flush;
		flag = false;
	      }
	  }

      for(auto& geom:p3curves)
	for(int i=0; i<=N; ++i)
	  {
	    const double t = static_cast<double>(i)/static_cast<double>(N);
	    double X[2], dX[2];
	    geom->Evaluate(t, X, dX);
	    double len = std::sqrt(dX[0]*dX[0]+dX[1]*dX[1]);
	    if(len<1.e-3)
	      {
		std::cout<<"\nDetected a problem in cubic feature "<<geom->GetCurveID()<<"\n"<<std::flush;
		flag = false;
	      }
	  }

      if(flag==false)
	exit(1);
    }
  
}
