// Sriramajayam

#include <iostream>
#include <string>
#include <um_UMeshManager.h>
#include <umesh_SimpleMesh.h>
#include <um_GeomData.h>

int main()
{
  const std::string gfile = "geom-1.fset";
  
  // output path for this case
  std::string output_path = "./";

  // Non dimensional medial axis parameters
  um::MedialAxisParams mas_params{.exclusion_radius=1.e-3, .maximum_radius=2., .eps_radius=1.e-3};

  // Nonlinear solver parameters
  um::NLSolverParams nlparams{.digits=5, .max_iter=40, .normTol=1.e-4};
	
  // Read features
  um::test::GeomData gdata(50, mas_params, nlparams);
  std::string fname = gfile;
  std::fstream pfile;
  pfile.open(fname.c_str(), std::ios::in);
  assert(pfile.good());
  pfile >> gdata;
  pfile.close();

  // Visualize
  fname = output_path+"geom.csv";
  pfile.open(fname.c_str(), std::ios::out);
  pfile << gdata;
  pfile.close();

  
  // Create feature set
  um::FeatureSet fset(1.e-4);
  gdata.CreateFeatureSet(fset);

  // Read the background mesh
  umesh::SimpleMesh UM;
  UM.ReadTecplotFile("bg.msh");
  UM.SetupMesh();
  
  // Working mesh
  um::WorkingMesh<decltype(UM)> WM(UM);

  // Meshing parameters
  um::MeshingParams mparams{.qfactor=4., .Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10, .quality=0.01};
  
  // Output
  std::vector<um::FeatureMeshOutput> mout{};
  um::RunInfo run_info;
  run_info.output_path = "./";

  // Triangulate
  um::algo::Triangulate(WM, mparams, fset, mout, run_info);
  
  // Output
  WM.PlotTecMesh(run_info.output_path+"wm.tec");

  std::vector<int> coloring{};
  um::UMeshManager<decltype(UM)>::ComputeWMColoring(WM, fset, mout, coloring);

  // Coloring
  const int nNodes = UM.GetNumNodes();
  const int nElements = UM.GetNumElements();
  std::vector<double> coordinates(2*nNodes);
  for(int n=0; n<nNodes; ++n)
    {
      const double* X = WM.coordinates(n);
      coordinates[2*n] = X[0];
      coordinates[2*n+1] = X[1];
    }
  std::vector<int> connectivity{};
  for(int e=0; e<nElements; ++e)
    if(coloring[e]==1)
      {
	const int* conn = UM.connectivity(e);
	for(int a=0; a<3; ++a)
	  connectivity.push_back(conn[a]);
      }
  umesh::SimpleMesh colorBG(coordinates, connectivity);
  colorBG.PlotTecMesh("airfoil.tec");
}
