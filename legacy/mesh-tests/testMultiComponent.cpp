// Sriramajayam

#include <um_Triangulator2D.h>
#include <umesh_SimpleMesh.h>
#include <cmath>
#include <iostream>

struct CircParams
{
  double center[2];
  double radius;
  double orientation;
};
  
void circle_signed_distance(const double* X, bool &onFeature, double& sd, double* dsd, void* params)
{
  assert(params!=nullptr);
  const auto& circ = *static_cast<const CircParams*>(params);
  const double* center = circ.center;
  const double& R = circ.radius;
  const double& orientation = circ.orientation;
  const double r = std::sqrt((X[0]-center[0])*(X[0]-center[0])+(X[1]-center[1])*(X[1]-center[1]));
  sd = orientation*(r-R);
  if(dsd!=nullptr)
    { dsd[0] = orientation*(X[0]-center[0])/r;
      dsd[1] = orientation*(X[1]-center[1])/r; }
  onFeature = true;
  return;
}

template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureMeshOutput& mout,
			   const WMType& wm);

int main()
{
  // Background mesh
  umesh::SimpleMesh BG;
  const double origin[] = {0.,0.};
  umesh::CreateEquilateralMesh(BG, origin, 1., 4);
  BG.SetupMesh();
  BG.PlotTecMesh("BG.tec");

  // Meshing parameters
  um::MeshingParams mparams{.Nproject=5, .Nrelax=5, .Ndist=3, .Nsamples=10, .quality=0.01}; //, .dnd_vertices=BG.GetBoundaryNodes()};

  // Geometry of the two segments of the first component
  CircParams circ1{.center={-0.2,0.}, .radius=0.2, .orientation=-1.};
  CircParams circ2{.center={0.2,0.}, .radius=0.2, .orientation=1.};
  
  // Geometry of the two segments of the second component
  CircParams circ3{.center={0.,0.}, .radius=0.55, .orientation=1.};
  
  // Feature parameters
  um::FeatureSet fset(1.e-4);
  fset.AppendFeature(um::FeatureParams{.curve_id=0, .sdfunc=circle_signed_distance, .sdparams=&circ1, .terminalPoints={-0.4,0., 0.,0.}});
  fset.AppendFeature(um::FeatureParams{.curve_id=1, .sdfunc=circle_signed_distance, .sdparams=&circ2, .terminalPoints={0.,0., 0.4,0.}});
  fset.AppendFeature(um::FeatureParams{.curve_id=2, .sdfunc=circle_signed_distance, .sdparams=&circ3, .terminalPoints={0.55,0., -0.55,0.}});
  fset.AppendFeature(um::FeatureParams{.curve_id=3, .sdfunc=circle_signed_distance, .sdparams=&circ3, .terminalPoints={-0.55,0., 0.55,0.}});
  fset.Finalize();

  // Check for no split features
  const auto& orientations = fset.GetFeatureOrientations();
  for(auto& orient:orientations)
    { assert(orient==um::FeatureOrientation::Positive || orient==um::FeatureOrientation::Negative); }

  // Mesh output
  std::vector<um::FeatureMeshOutput> mout{};
  
  // Triangulate
  um::RunInfo run_info;
  run_info.output_path = "";
  um::Triangulator2D<decltype(BG)> tri(BG);
  tri.GenerateMesh(mparams, fset, mout, run_info);
  auto& WM = tri.GetWorkingMesh();
  
  // Checks
  const int nFeatures = fset.GetNumFeatures();
  assert(mout.size()==nFeatures);
  const auto& gparams = fset.GetFeatureParams();
  for(int c=0; c<nFeatures; ++c)
    TestFeatureMeshOutput(gparams[c], mparams, mout[c], WM);

  // Plot the working mesh
  WM.PlotTecMesh("wm.tec");
}

template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureMeshOutput& mout,
			   const WMType& WM)
{
  // curve id
  assert(gparam.curve_id==mout.curve_id);

  // end points
  for(int p=0; p<2; ++p)
    { const int& n = mout.terminalVertices[p];
      assert(n>=0);
      const double* X = WM.coordinates(n);
      const double* Y = &gparam.terminalPoints[2*p];
      double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
      assert(dist<1.e-6);
    }

  // positive vertices should start and terminate at the end points
  assert(static_cast<int>(mout.PosVerts.size())>=2);
  assert(mout.terminalVertices[0]==mout.PosVerts.front());
  assert(mout.terminalVertices[1]==mout.PosVerts.back());
  
  // positive vertices should lie on the feature
  for(auto& p:mout.PosVerts)
    {
      const double* X = WM.coordinates(p);
      double sd;
      bool onFeature;
      gparam.sdfunc(X, onFeature, sd, nullptr, gparam.sdparams);
      assert(std::abs(sd)<1.e-3);
    }

  // no repeated positive vertex
  std::set<int> pset(mout.PosVerts.begin(), mout.PosVerts.end());
  assert(pset.size()==mout.PosVerts.size());
  
  // Check element qualities in WM
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);
  const auto& ElmSet = WM.GetElements();
  for(auto& e:ElmSet)
    { assert(Quality.Compute(e)>0.); }

  // Qualities of elements in the 1-rings of positive vertices should be better than the threshold value
  const int* oneRingElms;
  int n1RingElms;
  for(auto& p:mout.PosVerts)
    {
      WM.Get1RingElements(p, &oneRingElms, n1RingElms);
      for(int indx=0; indx<n1RingElms; ++indx)
	{ assert(Quality.Compute(oneRingElms[indx])>=mparams.quality); }
    }

  return;
}
