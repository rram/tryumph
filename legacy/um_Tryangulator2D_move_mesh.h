// Sriramajayam

#ifndef UM_TRYANGULATOR2D_MOVE_MESH_H
#define UM_TRYANGULATOR2D_MOVE_MESH_H

namespace um
{
  // Main functionality
  template<typename UMeshType>
    void Tryangulator2D_MoveMesh(WorkingMesh<UMeshType> *& pert_WM,
				 const WorkingMesh<UMeshType>& WM,
				 const MeshingParams& mparams,
				 const FeatureSet& fset, 
				 const PerturbationAlgorithm algo,
				 const std::vector<FeatureMeshOutput>& mout,
				 RunInfo& run_info)
    {
      // Initialize profiling info
      run_info.nRelaxVerts = 0;
      run_info.nRelaxIters = 0;
      run_info.runtime = 0.;
      const bool print_output = !run_info.output_path.empty();
      
      // Start the clock
      std::chrono::steady_clock::time_point timer_begin = std::chrono::steady_clock::now();
      run_info.runtime = 0.;
      
      // Check consistency of features provided
      const int nFeatures = static_cast<int>(mout.size());
      assert(fset.GetNumFeatures()==nFeatures);
      const auto& fparams = fset.GetFeatureParams();
      for(int f=0; f<nFeatures; ++f)
	assert(fparams[f].curve_id==mout[f].curve_id);

      // Daughter mesh
      if(pert_WM!=nullptr) delete pert_WM;
      pert_WM = new WorkingMesh<UMeshType>(WM);
      
      // Snap terminal vertices to end points of features
      for(int f=0; f<nFeatures; ++f)
	{
	  const double* terminalPoints = fparams[f].terminalPoints;
	  const int* terminalVertices = mout[f].terminalVertices;
	  for(int k=0; k<2; ++k)
	    pert_WM->update(terminalVertices[k], terminalPoints+2*k);
	}

      // Attempt to project and relax
      std::set<std::pair<int,int>> defElmSet{};
      auto success = algo::FrontalProjectAndRelax_Adaptive(*pert_WM, mparams, defElmSet, fparams, mout, run_info.nRelaxIters, run_info.nRelaxVerts);
      assert(success==algo::MeshingReason::Success);

      // Stop the clock
      std::chrono::steady_clock::time_point timer_end = std::chrono::steady_clock::now();
      
      // Track the run time
      run_info.runtime = std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_begin).count();
      
      // done
      return;
    }
}


#endif
