// Sriramajayam

#pragma once

#include <um2_MeshWrapper.h>
#include <um2_WorkingMesh.h>
#include <vector>
#include <map>
#include <cassert>
#include <tuple>

namespace um2
{
  namespace algo
  {
    // Make an open chain triangle-free
    template<typename MeshType>
      void MakeOpenChainTriangleFree(const MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
				     std::vector<int>& PosVerts,
				     std::vector<std::tuple<int,int,bool>>& PosCutElms)
      {
	assert(PosVerts.front()!=PosVerts.back());

	// Convert vectors to lists
	std::list<int> PosVertList(PosVerts.begin(), PosVerts.end());
	std::list<std::tuple<int,int,bool>> PosCutElmsList(PosCutElms.begin(), PosCutElms.end());
	bool completedFlag = false;

	while(completedFlag==false) // recursively check set of positively cut triangles
	  {
	    completedFlag = true;
	    int eindx = 0;
	    for(auto triplet=PosCutElmsList.begin(); triplet!=PosCutElmsList.end(); ++triplet, ++eindx)
	      {
		const int& elm = std::get<0>(*triplet);
		const int& face = std::get<1>(*triplet);

		// The next triplet
		auto next_triplet = std::next(triplet);
		if(next_triplet==PosCutElmsList.end())
		  break; // done
	    
		const int& next_elm = std::get<0>(*next_triplet);
		const int& next_face = std::get<1>(*next_triplet);

		// Check if the neighbor along the positive faces of the two elements coincide
		const int* elm_nbs      = mesh_wrapper.getElementNeighbors(elm);
		const int* next_elm_nbs = mesh_wrapper.getElementNeighbors(next_elm);
		if(elm_nbs[2*face]!=-1)
		  if(elm_nbs[2*face]==next_elm_nbs[2*next_face])
		    {
		      const int& common_elm = elm_nbs[2*face];
		      const int& left_face = elm_nbs[2*face+1];
		      const int& right_face = next_elm_nbs[2*next_face+1];
		      assert(left_face!=right_face);

		      // Identify the non-positive face of "common_elm"
		      bool faces[] = {true, true, true};
		      faces[left_face] = false;
		      faces[right_face] = false;
		      int nu_pos_face = -1;
		      for(int f=0; f<3 && nu_pos_face==-1; ++f)
			if(faces[f]==true)
			  nu_pos_face = f;

		      // Over-write (elm, face) -> (common_elm, nu_pos_face)
		      std::get<0>(*triplet) = common_elm;
		      std::get<1>(*triplet) = nu_pos_face;

		      // Erase next_triplet (invalidated after erase)
		      PosCutElmsList.erase(next_triplet);		 
		      		      
		      // Remove the (eindx+1)-th positive vertex
		      PosVertList.erase(std::next(PosVertList.begin(), eindx+1));

		      // Note that there has been an alteration in the chain
		      completedFlag = true;
		    }
	      } // end for
	  } // end while

	// Have there been any alterations?
	if(PosVerts.size()!=PosVertList.size())
	  {
	    PosVerts.clear();
	    PosVerts.assign(PosVertList.begin(), PosVertList.end());
	    PosCutElms.clear();
	    PosCutElms.assign(PosCutElmsList.begin(), PosCutElmsList.end());
	  }
	
	// done
	return;
      }
    
  } // um::algo::
} // um::

