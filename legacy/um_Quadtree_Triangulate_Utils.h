// Sriramajayam

#ifndef UM_QUADTREE_TRIANGULATE_UTILS_H
#define UM_QUADTREE_TRIANGULATE_UTILS_H 

#include <um_Utils.h>

namespace um
{
  namespace detail
  {
    // Identify boundary ndoes in a mesh
    std::set<int> GetBoundaryNodes(const std::vector<int>& connectivity)
      {
	// Create a list of all edges
	// Save the number of elements that each edge belongs to.
	// A free edge belongs to just one element.
  
	std::map<std::array<int,2>,int> EdgeList{};
	const int nElements = static_cast<int>(connectivity.size()/3);
	std::array<int,2> edgeconn;
	for(int e=0; e<nElements; ++e)
	  for(int f=0; f<3; f++)
	    {
	      edgeconn[0] = connectivity[3*e+f];
	      edgeconn[1] = connectivity[3*e+(f+1)%3];
	      if( edgeconn[1]<edgeconn[0] )
		{ int temp = edgeconn[0];
		  edgeconn[0] = edgeconn[1];
		  edgeconn[1] = temp; }
	
	      // Is this a new edge
	      if( EdgeList.find(edgeconn)==EdgeList.end() )
		EdgeList[edgeconn] = 1;
	      else // second count
		EdgeList[edgeconn] += 1;
	    }
	
	std::set<int> BdNodes{};
	for(auto& it:EdgeList)
	  if(it.second==1)
	    for(int k=0; k<2; ++k)
	      BdNodes.insert( it.first[k] );

	// done
	return BdNodes;
      }
  }
}

#endif
