
/** \file um_WorkingMesh.h
 * \brief File defining the class um::WorkingMesh
 * \author Ramsharan Rangarajan
 * Last modified: May 29, 2020
 */

#ifndef UM_WORKING_MESH_H
#define UM_WORKING_MESH_H

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <cassert>
#include <algorithm>

namespace um
{
  /** \brief Class defining a working mesh.
   * A working mesh is, roughly speaking, a perturbed subtriangulation 
   * of a given parent triangulation \f${\cal T}\f$. 
   *
   * Specifically, given a triangulation \f${\cal T}\f$ in \f${\mathbb R}^2\f$, say the
   * parent mesh, a working mesh \f${\cal S}\f$ of \f${\cal T}\f$ is one that:
   * <ul>
   * <li> contains a subset of the elements in \f${\cal T}\f$,  and </li>
   * <li> whose vertices are possibly perturbed from their corresponding locations in \f${\cal T}\f$.</li>
   * </ul>
   *
   * Identifying the parent triangulation \f${\cal T}\f$ with the 3-tuple
   * \f$({\rm V},{\rm I},{\rm C})\f$ consisting of the list of nodal coordinates \f${\rm V}\f$,
   * vertex indices \f${\rm I}\f$ and the list of element connectivities \f${\rm C}\f$, the 3-tuple
   * defining a working mesh \f${\cal S}\f$ of \f${\cal T}\f$ has the form 
   * \f$({\rm W},{\rm I}_{\rm sub}, {\rm C}_{\rm sub})\f$, where \f${\rm I}_{\rm sub}\subseteq {\rm I}\f$,
   * and \f${\rm C}_{\rm sub}\subseteq {\rm C}\f$. 
   *
   * The um::WorkingMesh class is provided
   * to take advantage of the facts that\f${\rm I}_{\rm sub}\subseteq {\rm I}\f$,
   * and \f${\rm C}_{\rm sub}\subseteq {\rm C}\f$. The class defines
   * the triangulation \f${\cal S}\f$ by only storing the following information:
   * <ul>
   * <li> The indices of elements in \f${\cal S}\f$. In particular, the connectivities
   *      are accessed from the parent mesh \f${\cal T}\f$. 
   *      <br>
   *      The of elements in \f${\cal S}\f$ as an unordered set. 
   * </li>
   * <li> The indices \f${\cal I}_{\rm sub}^{\rm p}\subset {\rm I}_{sub}\f$ of
   *      nodes in \f${\cal S}\f$ that are perturbed from their corresponding locations
   *      in \f${\cal T}\f$ 
   * <li>
   * <li> The coordinates of nodes in \f${\rm I}_{\rm sub}^{\rm p}\f$.
   *      <br>
   *      The class internally uses an unordered map between vertex indices in 
   *      \f${\rm I}_{\rm sub}^{\rm p}\f$ and their perturbed coordinates.
   * </li>
   * </ul>
   *
   * Note: 
   * <ul>
   * <li> Coordinate access through the working mesh class
   * is efficient only if \f${\rm I}_{\rm sub}^{\rm p}\f$ is a small subset of \f${\rm I}\f$. </li>
   * <li> The class is not thread-safe. The methods addElement(), update() and SetupMesh() in particular, do not 
   * protect from data races. </li>
   * </ul>
   *
   * The class is templated by the type of the parent mesh \f${\cal T}\f$.
   * \tparam UMeshType typename of the parent mesh
   *
   */   
  template<class UMeshType>
    class WorkingMesh
    {
    public:
      //! Constructor
      //! \param[in] umesh Parent triangulation, referenced.
      inline WorkingMesh(const UMeshType& umesh)
	:RefMesh(umesh),
	SPD(umesh.GetSpatialDimension()),
	isSetup(true), // empty mesh
	ElmList{},
	CoordList(RefMesh.GetNumNodes()*RefMesh.GetSpatialDimension())
	  { RefMesh.CopyNodalCoordinates(&CoordList[0]); }
	
      //! Disable copy and assignment
      WorkingMesh(const WorkingMesh& obj) = delete;
      WorkingMesh& operator=(const WorkingMesh&) = delete;
      
      //! Destructor, does nothing
      inline virtual ~WorkingMesh() {}

      //! Clears the mesh
      inline void Clear()
      { ElmList.clear();
	CoordList.clear();
	ElmNbs.clear();
	Vert1Rings.clear();
	Elm1Rings.clear();
	isSetup = false; }
      
      //! Returns the reference mesh
      //! \return Const reference to the parent mesh
      inline virtual const UMeshType& GetParentMesh() const
      { return RefMesh; }
      
      /** \brief Access the coordinates of a node.
       * The implementation first checks if the node belonds to \f${\rm I}_{\rm sub}^{\rm p}\f$.
       * If yes, it returns the perturbed coordinate. 
       * Otherwise, it returns the coordinates of the node from the parent mesh.
       * Mesh need not have been setup for this call.
       * \param[in] vert_indx Vertex number, should belong to the node set \f${\rm I}\f$ of the parent mesh.
       * \return Const pointer to the coordinates of the requested node
       * \warning Does not require or check of the vertex requested belongs to \f${\rm I}_{\rm sub}\f$.
       */
      inline const double* coordinates(const int vert_indx) const
      { return &CoordList[SPD*vert_indx]; }
      
      /** \brief Update the coordinates of a node
       * Only indices and coordinates of nodes perturbed through this
       * method are deemed to belong to \f${\rm I}_{\rm sub}^{rm p}\f$, and 
       * are stored within this class.
       * Mesh need not have been setup for this call.
       * \param[in] vert_indx Node number. Expected to belong to \f${\rm I}\f$.
       * \param[in] X Pointer to the new nodal coordinates for node "vert_indx"
       * \warning This method is NOT thread-safe
       */
      inline void update(const int vert_indx, const double* X)
      {
	for(int k=0; k<SPD; ++k)
	  CoordList[SPD*vert_indx+k] = X[k];
	return;
      }

      /** Add element to the working mesh
       * Invalidates the mesh setup. 
       * \param[in] elm_indx Index of an element in the parent mesh
       */
      inline void addElement(const int elm)
      { ElmList.insert(elm);
	isSetup = false; }

      /** \brief Returns the connectivity of a specified element.
       * This directly accesses the connectivity from the parent mesh without checking of the 
       * the element exists in the working mesh.
       *
       * Does not requure the mesh to be setup.
       * \param[in] elm_indx Index of the element
       * \return const pointer to the connectivity array of the requested element
       */
      inline const int* connectivity(const int elm) const
      { return RefMesh.connectivity(elm); }

      /** \brief Setup the mesh: computes element neighbors, 1-ring elements, 1-ring vertices
       * The mesh setup state should be invalid for this call.
       */
      void SetupMesh();
      
      /** \brief Returns the number of nodes per element.
       * 3 for a planar triangle and 4 for a tet.
       */
      inline  int GetNumNodesPerElement() const
      { return RefMesh.GetNumNodesPerElement(); }

      /** \brief Returns local node numbers on a specified face
       * \param[in] locface Local face number. Should range from 0 to GetNumNodesPerElement()-1.
       */
      inline const int* GetLocalFaceNodes(const int locface) const
      { return RefMesh.GetLocalFaceNodes(locface); }
    
      /** \brief Returns the embedding dimension.
       * 2 for a planar triangle and 3 for a tet.
       */
      inline int GetSpatialDimension() const
      { return SPD; }

      /** \brief Returns a bound for the #elements in the 1-ring of a vertex
       * Used for memory allocation. Does not have to be precise.
       */
      inline int GetMaxElementValency() const
      { return RefMesh.GetMaxElementValency(); }

      /** \brief Returns a bound for the #vertices in the 1-ring of a vertex
       * Used for memory allocation. Does not have to be precise.
       */
      inline int GetMaxVertexValency() const
      { return RefMesh.GetMaxVertexValency(); }

      /** \brief Method that returns the list of vertices in the 1-ring of a vertex.
       * Mesh should have been setup
       * \param[in] vert_index Node for which to return 1-ring
       * \param[out] oneRingVerts Pointer to the list of vertices in the 1-ring
       * \param[out] nVerts Number of vertices in the 1-ring
       * \warning Vertices in 1-rings may belong to elements not in the working mesh
       */
      inline void Get1RingVertices(const int vert_index, const int** oneRingVerts, int& nVerts) const
      {
	assert(isSetup==true);
	auto it = Vert1Rings.find(vert_index);
	if(it!=Vert1Rings.end())
	  {
	    (*oneRingVerts) = &(it->second[0]);
	    nVerts = static_cast<int>(it->second.size());
	  }
	else
	  RefMesh.Get1RingVertices(vert_index, oneRingVerts, nVerts);
      }

      /** \brief Method that returns the list of elements in the 1-ring of a vertex.
       * Mesh should have been setup
       * \param[in] node_index Node for which to return 1-ring
       * \param[out] oneRingElms Pointer to the list of elements in the 1-ring
       * \param[out] nElms Number of elements in the 1-ring
       */
      inline void Get1RingElements(const int vert_index, const int** oneRingElms, int& nElms) const
      {
	assert(isSetup==true);
	
	// Check if vertex has a pruned 1-ring list
	const auto it = Elm1Rings.find(vert_index);
	if(it!=Elm1Rings.end())
	  { nElms = static_cast<int>(it->second.size());
	    *oneRingElms = &(it->second[0]); }
	else
	  RefMesh.Get1RingElements(vert_index, oneRingElms, nElms);
	return;
      }
    
 
      /** \brief Returns the list of elements in the working mesh
       * Mesh state should have been setup
       */
      const std::unordered_set<int>& GetElements() const
      { assert(isSetup==true);
	return ElmList; }

      /** \brief Returns the element neighbor list for the working mesh.
       * Mesh state should have been setup
       * \param[in] elm Element number
       * \return Integer array, elm_nbs of length 6. 
       * elm_nbs[2*f] = index of the element in the working mesh adjacent to face 'f' of elm_indx.
       * Set to -1 if f lies on the boundary.
       * elm_nbs[2*f+1] = face of elm_nbs[2*f] that is adjacent to face 'f' of elm_index.
       * Set to -1 if f lies on the boundary of the working mesh.
       */
      inline const int* GetElementNeighbors(const int elm_indx) const
	{
	assert(isSetup==true);
	//assert(ElmList.find(elm_indx)!=ElmList.end());
	
	auto it = ElmNbs.find(elm_indx);
	if(it!=ElmNbs.end())
	  return &(it->second[0]);
	else
	  return RefMesh.GetElementNeighbors(elm_indx);
      }
       
      /** \brief Helper method to plot the working mesh to a file in tecplot format
       * Mesh state need not be setup.
       * Retains all nodes from the parent mesh and mere plots a submesh containing elements in the working mesh.
       * \param[in] filename File name 
       */
      void PlotTecMesh(const std::string filename) const;
      
    private:
      const UMeshType& RefMesh; //!< Reference mesh
      const int SPD; //!< Spatial dimension
      bool isSetup; //!< Flag to note if the mesh has been finalized
      std::unordered_set<int> ElmList; //!< List of elements in this mesh
      std::vector<double> CoordList; //!< List of nodal coordinates
      std::unordered_map<int, std::vector<int>> ElmNbs; //!< Element neighbors
      std::unordered_map<int, std::vector<int>> Vert1Rings; //!< 1-ring vertices. Only vertices along the boundary of the mesh
      std::unordered_map<int, std::vector<int>> Elm1Rings; //!< 1-ring elements. Only vertices along the boundary of the mesh

      //! Helper function to compute the element neighbor list.
      //! Only neighbor lists differing from the reference mesh are save
      void ComputeElementNeighborList();

      //! Helper function to compute 1-ring elements.
      //! Should be called after "ComputeElementNeighborList"
      void Compute1RingElements();

      //! Helper function to compute 1-ring vertices
      //! Should be called after "Compute1RingElements
      void Compute1RingVertices();
      
    };
}

#endif


// Implementation of class
#include <um_WorkingMesh_Impl.h>
