// Sriramajayam

#include <um_test_animate.h>
#include <um_test_visualize.h>
#include <um_test_TriangleMesh.h>
#include <filesystem>
#include <fstream>
#include <cassert>
#include <iostream>

namespace um
{
  namespace test
  {
    // read a mesh in vtk format
    void read_vtk_triangle_mesh(const std::string filename,
				std::vector<int>& connectivity,
				std::vector<double>& coordinates)
    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
      std::ifstream file;
      file.open(filename);
      assert(file.good() && file.is_open());

      connectivity.clear();
      coordinates.clear();
      
      // read the number of nodes
      position_cursor_after_word(file, "POINTS");
      int nNodes;
      file >> nNodes;

      // read coordinates
      coordinates.resize(2*nNodes);
      std::string line;
      std::getline(file, line);
      double zcoord;
      for(int n=0; n<nNodes; ++n)
	{
	  file >> coordinates[2*n] >> coordinates[2*n+1] >> zcoord;
	}

      // read the number of triangles
      position_cursor_after_word(file, "POLYGONS");
      int nTriangles;
      file >> nTriangles;

      // connectivity
      connectivity.resize(3*nTriangles);
      std::getline(file, line);
      int nodes_per_elm;
      for(int e=0; e<nTriangles; ++e)
	{
	  file >> nodes_per_elm >> connectivity[3*e] >> connectivity[3*e+1] >> connectivity[3*e+2];
	  assert(nodes_per_elm==3);
	}

      // done
      file.close();
    }
    
    // Animate vertex motion from one mesh to another
    void animate_mesh_motion(const std::string file1, const std::string file2,
			       const int num_frames, const std::string stem)
    {
      assert(num_frames>1);

      assert(std::string(std::filesystem::path(file2).extension())==".vtk");

      // Read triangle meshes in vtk format
      std::vector<int> conn1{}, conn2{};
      std::vector<double> coord1{}, coord2{};
      read_vtk_triangle_mesh(file1, conn1, coord1);
      read_vtk_triangle_mesh(file2, conn2, coord2);

      // sanity checks
      assert(coord1.size()==coord2.size());
      assert(conn1==conn2);

      // Animate mesh motion
      um::test::TriangleMesh mesh(coord1, conn1);
      const int nNodes = static_cast<int>(coord1.size()/2);
      for(int f=0; f<=num_frames; ++f)
	{
	  const double lambda = static_cast<double>(f)/static_cast<double>(num_frames);
	  for(int a=0; a<nNodes; ++a)
	    {
	      const double* X = &coord1[2*a];
	      const double* Y = &coord2[2*a];
	      double Z[2];
	      for(int k=0; k<2; ++k)
		{
		  Z[k] = (1.-lambda)*X[k] + lambda*Y[k];
		}
	      mesh.update(a, Z);
	    }

	  // save this mesh
	  std::string filename = stem + "-f" + std::to_string(f) + ".vtk";
	  save_mesh_vtk(filename, mesh);
	}

      // done
      return;
    }
    
  }
}
