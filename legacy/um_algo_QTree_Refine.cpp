// Sriramajayam

#include <um_algo_QTree_Refine.h>

namespace um
{
  namespace algo
  {
    void RefineQuadtreeAtTerminals(Quadtree& QT, const std::vector<FeatureParams>& gparams)
    {
      // Ensure that pairs of terminals of each feature lie in distinct quadtree nodes
      for(auto& f:gparams)
	{
	  const double* P = &f.terminalPoints[0];
	  const double* Q = &f.terminalPoints[2];
	
	  int iter = 0;
	  while(true)
	    {
	      auto pQN = QT.Query(P);
	      auto qQN = QT.Query(Q);
	      if(pQN!=qQN) break;
	      else
		{
		  if(iter%2==0)
		    pQN->AddChildren();
		  else
		    qQN->AddChildren();
		}
	    }
	  assert(QT.Query(P)!=QT.Query(Q));
	}

      // done
      return;
    }

    
    void RefineQuadtreeAlongFeature(Quadtree& QT, const FeatureParams& FP, const FeatureSet& fset, const double qfactor)
    {
      // Get samples along the feature
      auto samples = FP.samplefunc();
      
      // loop over sample points
      // find the mesh size to be set at the sample point
      // set the mesh size
      double X[2];
      for(auto& pt:samples)
	{
	  X[0] = pt.first;
	  X[1] = pt.second;
	  const double hreq = qfactor*fset.GetDistanceToMedialAxis(X, FP.curve_id);
	  QT.Refine(X, hreq);
	}

      // done
      return;
    }
    
  } // um::algo::
} // um::
