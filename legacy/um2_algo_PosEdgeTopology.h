// Sriramajayam

#pragma once

#include <um2_WorkingMesh.h>
#include <set>

namespace um2
{
  namespace algo
  {
    template<typename MeshType>
      void InspectPositiveEdgesTopology(WorkingMesh<MeshType>& WM, 
					std::set<int>& refine_Verts)
      {
	// check status of the working mesh
	assert(WM.getMeshStatus()==WorkingMeshStatus::POSITIVE_EDGES_IDENTIFIED);

	// feature correspondences
	auto& mout    = WM.getSplitFeatureCorrespondences();
	const int nFeatures = static_cast<int>(mout.size());

	// lambda for set intersection
	auto VertexSetIntersection = [](const std::vector<int>& A, const std::vector<int>& B)
	  {
	    std::set<int> Aset(A.begin(), A.end());
	    std::set<int> Bset(B.begin(), B.end());
	    std::set<int> AintB{};
	    std::set_intersection(Aset.begin(), Aset.end(), Bset.begin(), Bset.end(),
				   std::inserter(AintB, AintB.begin()));
	    return std::move(AintB);
	  };
	
	// Examine positive edges in a pairwise manner
	for(int f=0; f<nFeatures; ++f)
	  {
	    const auto& Fset = mout[f].PosVerts;
	    if(!Fset.empty())
	      for(int g=f+1; g<nFeatures; ++g)
		{
		  const auto& Gset = mout[g].PosVerts;
		  if(!Gset.empty())
		    {
		      std::set<int> FintG = VertexSetIntersection(Fset, Gset);
		      
		      if(!FintG.empty()) // Positive edges of features f & g intersect
			{
			  // Remove common terminal vertices of F & G from FintG
			  std::set<int> terminals_FintG{};
			  for(int p=0; p<2; ++p)
			    {
			      const int& tf = mout[f].terminalVertices[p];
			      if(tf!=-1 && (tf==mout[g].terminalVertices[0] || tf==mout[g].terminalVertices[1]))
				{
				  int nremoved = FintG.erase(tf);
				  //assert(nremoved==1); // IS THIS REQUIRED OR NOT? HACK HACK HACK
				}
			    }

			  // Refine around remaining vertices in FintG
			  if(!FintG.empty()) // This is a failure
			    {
			      if(mout[f].reason==MeshingReason::Success)
				{
				  mout[f].reason = MeshingReason::Fail_PositiveEdge_GlobalTopology;
				}

			      if(mout[g].reason==MeshingReason::Success)
				{
				  mout[g].reason = MeshingReason::Fail_PositiveEdge_GlobalTopology;
				}

			      for(auto& n:FintG)
				{
				  refine_Verts.insert(n);
				}
			    }
			}
		    }
		}
	  }

	// done
	return;
      }

  } // um::algo::
} // um::


