// Sriramajayam

#pragma once

#include <um2_Quadtree.h>
#include <um2_FeatureSet.h>
#include <um2_WorkingMesh.h>

namespace um2
{
  namespace algo
  {
    //! \ingroup utils
    void RefineQuadtreeAlongFeature_Local(Quadtree& QT, const FeatureParams& gparam,
					  const SplitFeatureSet& fset, const double qfactor);

    void RefineQuadtreeAlongFeature_NonLocal(Quadtree& QT, const FeatureParams& gparam,
					     const FeatureSet& fset, const double qfactor);

    void RefineQuadtreeAroundCorners(Quadtree& QT, const SplitFeatureSet& fset, const double qfactor);
    
    template<typename MeshType>
      void RefineQuadtreeAtVertices(const WorkingMesh<MeshType>& WM,
				    const std::set<int>& refine_Verts,
				    Quadtree& QT)
      {
	// Avoid refining the same quadtree node twice.
	// To this end, identify a subset of vertices falling in distinct quadtree nodes
	std::set<int> nodeSet{};
	std::set<const std::shared_ptr<QuadtreeNode>> qnSet{};
	for(auto& v:refine_Verts)
	  {
	    const double* X = WM.coordinates(v);
	    const auto QN = QT.query(X);
	    assert(QN!=nullptr);
	    if(qnSet.find(QN)==qnSet.end())
	      {
		nodeSet.insert(v);
		qnSet.insert(QN);
	      }
	  }
	qnSet.clear();

	// Refine quadtree nodes
	for(auto& v:nodeSet)
	  {
	    const double* X = WM.coordinates(v);
	    auto QN = QT.query(X);
	    QN->addChildren();
	  }

	// done
	return;
      }

  } // um::algo::
} // um::
