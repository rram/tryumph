// Sriramajayam

#ifndef UM_ALGORITHM_POSEDGE_QUALITY_H
#define UM_ALGORITHM_POSEDGE_QUALITY_H

#include <um_FeatureSet.h>
#include <um_Algorithm_MeshingReason.h>

namespace um
{
  namespace algo
  {
    // Check qualities of positive edges
    template<typename MeshType>
      MeshingReason InspectPositiveEdgeQuality(const MeshType& MD,
					       const FeatureParams& gparams,
					       const std::vector<int>& PosVerts,
      					       const double qthreshold, 
					       std::set<int>& defVerts)
    {
      // Loop over positive edges and determine the ratio of final to initial edge length
      double cpt_left[2], cpt_right[2];
      double sd, dsd[2];
      double ref_len, def_len, quality;
      
      const int nPosVerts = static_cast<int>(PosVerts.size());

      // precompute closest point projections of positive vertices
      std::vector<double> cpt(nPosVerts*2);
      auto sdfunc = gparams.sdfunc;
      for(int i=0; i<nPosVerts; ++i)
	{
	  const double* X = MD.coordinates(PosVerts[i]);
	  sdfunc(X, sd, dsd);
	  for(int k=0; k<2; ++k)
	    cpt[2*i+k] = X[k]-sd*dsd[k];
	}

      // examine edge qualities
      bool flag = true;
      for(int e=0; e<nPosVerts-1; ++e)
	{
	  // un-projected
	  const double* Xleft  = MD.coordinates(PosVerts[e]);
	  const double* Xright = MD.coordinates(PosVerts[e+1]);
	  ref_len = (Xright[0]-Xleft[0])*(Xright[0]-Xleft[0]) + (Xright[1]-Xleft[1])*(Xright[1]-Xleft[1]);

	  // projected
	  const double* cpt_left = &cpt[2*e];
	  const double* cpt_right = &cpt[2*(e+1)];
	  def_len = (cpt_right[0]-cpt_left[0])*(cpt_right[0]-cpt_left[0]) + (cpt_right[1]-cpt_left[1])*(cpt_right[1]-cpt_left[1]);

	  // Check quality
	  quality = std::sqrt(def_len/ref_len);
	  if(quality<qthreshold)
	    {
	      defVerts.insert(PosVerts[e]);
	      defVerts.insert(PosVerts[e+1]);
	      flag = false;
	    }
	}

      // report status
      if(flag==true)
	return MeshingReason::Success;
      else
	return MeshingReason::Fail_ElementQuality;
    }

    
  } // um::algo::
} // um::

#endif
