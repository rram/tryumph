// Sriramajayam

#ifndef UM_RETRYANGULATOR2D_GENERATE_H
#define UM_RETRYANGULATOR2D_GENERATE_H

namespace um
{
  // Constructor
  template<typename UMeshType>
    ReTryangulator2D<UMeshType>::ReTryangulator2D(const WorkingMesh<UMeshType>& wm,
						  const std::vector<FeatureMeshOutput>& fmo)
    :BG(wm.GetParentMesh()),
    parentWM(wm),
    feature_mesh_details(fmo),
    WM(wm)
    {}
  
  
  // Main functionality
  template<typename UMeshType>
    void ReTryangulator2D<UMeshType>::GenerateMesh(const MeshingParams& mparams,
						   const FeatureSet& fset,
						   RunInfo& run_info)
    {
      // Initialize profiling info
      run_info.nRelaxVerts = 0;
      run_info.nRelaxIters = 0;
      run_info.runtime = 0.;
      const bool print_output = !run_info.output_path.empty();

      // Start the clock
      std::chrono::steady_clock::time_point timer_begin = std::chrono::steady_clock::now();
      run_info.runtime = 0.;
      
      // Check consistency of features provided
      const int nFeatures = static_cast<int>(feature_mesh_details.size());
      assert(fset.GetNumFeatures()==nFeatures);
      const auto& fparams = fset.GetFeatureParams();
      for(int f=0; f<nFeatures; ++f)
	assert(fparams[f].curve_id==feature_mesh_details[f].curve_id);

      // Snap terminal vertices to end points of features
      for(int f=0; f<nFeatures; ++f)
	{
	  const double* terminalPoints = fparams[f].terminalPoints;
	  const int* terminalVertices = feature_mesh_details[f].terminalVertices;
	  for(int k=0; k<2; ++k)
	    WM.update(terminalVertices[k], terminalPoints+2*k);
	}

      // Attempt to project and relax
      std::set<std::pair<int,int>> defElmSet{};
      auto success = algo::FrontalProjectAndRelax_Adaptive(WM, mparams, defElmSet, fparams, feature_mesh_details, run_info.nRelaxIters, run_info.nRelaxVerts);
      assert(success==algo::MeshingReason::Success);

      // Stop the clock
      std::chrono::steady_clock::time_point timer_end = std::chrono::steady_clock::now();
      
      // Track the run time
      run_info.runtime = std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_begin).count();

      // done
      return;
    }
}

#endif
