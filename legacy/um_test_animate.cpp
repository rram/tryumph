// Sriramajayam

#include <um_test_animate.h>
#include <fstream>

namespace um
{
  namespace test
  {
    // function to position the cursor at a given word in a file
    void position_cursor_after_word(std::ifstream& file, const std::string word)
    {
      std::string line;
      while (std::getline(file, line))
	{
	  auto position = line.find(word);
	  if ( position != std::string::npos)
	    {
	      file.seekg(-line.length()-1+position+word.length(), std::ios::cur);
	      break;
	    }
	}
      // done
      return;
    }

  } // um::test::
} // um::
