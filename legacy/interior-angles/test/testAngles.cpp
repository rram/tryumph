// Sriramajayam

#include <um_Utils.h>
#include <cassert>

int main()
{
  double angles[3];
  const double A1[] = {1.,0.};
  const double B1[] = {0.,1.};
  const double C1[] = {0.,0.};
  const double* coords1[] = {A1, B1, C1};
  um::ComputeInteriorAnglesInTriangle(coords1, angles);
  assert(std::abs(angles[0]-M_PI/4.) + std::abs(angles[1]-M_PI/4.) + std::abs(angles[2]-M_PI/2.)<1.e-4);

  const double B2[] = {-1./std::sqrt(2.), 1./std::sqrt(2.)};
  const double* coords2[] = {A1,B2,C1};
  um::ComputeInteriorAnglesInTriangle(coords2, angles);
  assert(std::abs(angles[0]-M_PI/8.) + std::abs(angles[1]-M_PI/8.) + std::abs(angles[2]-3.*M_PI/4.)<1.e-4);
}
