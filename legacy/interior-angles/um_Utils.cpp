// Sriramajayam

#include <cassert>
#include <vector>
#include <map>
#include <array>
#include <cmath>

namespace um
{
  // Compute neighbor lists for triangle and tetrahedral elements
  void DetectFreeFaces(const std::vector<int>& connectivity,
		       std::vector<int>& FreeFaces)
  {
    const int SPD = 2;
    const int nodes_element = SPD+1;
    const int nElements = static_cast<int>(connectivity.size()/nodes_element);

    // Enumeration of faces for triangle and tetrahedral elements
    std::vector<int> facenodes{};
    if(SPD==2)
      facenodes = std::vector<int>{0,1, 1,2, 2,0};
    else
      facenodes = std::vector<int>{0,2,1, 0,3,2, 0,1,3, 1,2,3};
	
    // Create a list of all faces
    // Save the number of elements that each face belongs to
    // A free face belongs to just one element
    std::map<std::array<int,SPD>, std::vector<int>> FaceList{}; // #nodes/face = SPD
    std::array<int,SPD> fconn;
    for(int e=0; e<nElements; ++e)
      {
	const int* conn = &connectivity[nodes_element*e];

	// Loop over faces
	for(int f=0; f<SPD+1; ++f) // Number of faces = SPD+1
	  {
	    // Connectivity of this face
	    for(int a=0; a<SPD; ++a)
	      fconn[a] = conn[facenodes[SPD*f+a]];

	    // Sorted connectivity
	    std::sort(fconn.begin(), fconn.end());

	    // Is this a new face?
	    auto it = FaceList.find(fconn);
	    if(it==FaceList.end())
	      FaceList.insert({fconn, std::vector<int>{e,f}}); // This is a new face
	    else
	      { it->second.push_back(e);
		it->second.push_back(f); } // Add the count for this face
	  }
      }


    // Make a list of free faces
    FreeFaces.clear();
    for(auto& it:FaceList)
      if(it.second.size()==2)
	{ FreeFaces.push_back( it.second[0] );
	  FreeFaces.push_back( it.second[1] ); }

    // done
    return;
  }
  
  // Compute the interior angles of a triangle
  void ComputeInteriorAnglesInTriangle(const double* vertCoords[], double* angles)
  {
    for(int a=0; a<3; ++a)
      {
	const double* A = vertCoords[a];
	const double* B = vertCoords[(a+1)%3];
	const double* C = vertCoords[(a+2)%3];
	const double AB[] = {B[0]-A[0], B[1]-A[1]};
	const double AC[] = {C[0]-A[0], C[1]-A[1]};
	double costheta = (AB[0]*AC[0]+AB[1]*AC[1])/
	  (std::sqrt(AB[0]*AB[0]+AB[1]*AB[1])*std::sqrt(AC[0]*AC[0]+AC[1]*AC[1]));
	assert(std::abs(costheta)<=1.);
	angles[a] = std::acos(costheta);
      }

    // Sanity checks
    assert(std::abs(angles[0]+angles[1]+angles[2]-M_PI)<1.e-4);

    return;
  }


  
} // end namespace um
