// Sriramajayam

#include <iostream>
#include <string>
#include <um_UMeshManager.h>
#include <umesh_SimpleMesh.h>
#include <um_GeomData.h>
#include <um_SignedDistanceFunction.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
inline fs::path operator+(fs::path left, fs::path right){return fs::path(left)+=right;}

int main()
{
  fs::path currdir = fs::current_path();
  assert(fs::exists(currdir));
  assert(fs::is_directory(currdir));

  fs::path geomdir = currdir + "/geom-data";
  assert(fs::exists(geomdir));
  assert(fs::is_directory(geomdir));

  std::fstream tfile;
  tfile.open((char*)"s0-ma-times.csv", std::ios::out);
  assert(tfile.good());

  std::fstream ifile;
  ifile.open((char*)"s0-ma-qtref.csv", std::ios::out);
  assert(ifile.good());
  
  for(auto& entry:fs::recursive_directory_iterator(geomdir))
    if(fs::is_regular_file(entry) && entry.path().extension()==".fset")
      if(entry.path().filename().stem().string()!="249421")
	{
	  const std::string gfile = entry.path().filename().stem().string();
	  std::cout<<"\n\nProcessing geometry: "<<gfile<<std::flush;
	
	  // output path for this case
	  std::string output_path = currdir.string()+"/output-"+gfile+"/";

	  // Nonlinear solver parameters
	  um::NLSolverParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
	
	  // Read features
	  um::test::GeomData gdata(50, nlparams);
	  std::string fname = "geom-data/" + gfile + ".fset";
	  std::fstream pfile;
	  pfile.open(fname.c_str(), std::ios::in);
	  assert(pfile.good());
	  pfile >> gdata;
	  pfile.close();

	  // Bounding box for the initial geometry
	  double bb_center[2], bb_size;
	  gdata.BoundingBox(bb_center, bb_size);

	  tfile << "\n" << gfile <<", ";
	  ifile << "\n" << gfile <<", ";

	  // Info for transforming geometry
	  um::test::RotationStruct rotStruct{.center={bb_center[0], bb_center[1]}, .angle=5.*M_PI/180.};
	  
	  // Perturb
	  for(int pnum=0; pnum<20; ++pnum)
	    {
	      const std::string pert_label = gfile + "-" + std::to_string(pnum);

	      // Rotate geometry
	      gdata.Transform(um::test::RotationFunc, &rotStruct);

	      // Visualize
	      fname = output_path+"geom-"+std::to_string(pnum)+".csv";
	      pfile.open(fname.c_str(), std::ios::out);
	      pfile << gdata;
	      pfile.close();
	      
	      // Create a rotated feature set
	      um::FeatureSet fset(1.e-4);
	      gdata.CreateFeatureSet(fset);

	      // Feature samples
	      um::MedialAxisSamplesVec MASvec{};
	      gdata.GetSamples(MASvec);
	      
	      // Create the mesh manager
	      um::UMeshManager<umesh::SimpleMesh> manager(bb_center, 2.*bb_size);

	      // start clock
	      std::chrono::steady_clock::time_point timer_begin = std::chrono::steady_clock::now();
	      
	      // Compute the medial axes
	      um::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=bb_size, .eps_radius=1.e-3};
	      um::MedialAxisSampler MAS(MASvec, mas_params);
	    
	      // Meshing parameters
	      um::MeshingParams mparams{.algo=um::PerturbationAlgorithm::Frontal_NonAdaptive,
		  .qfactor=4., .Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10, .quality=0.01};
	      manager.SetMeshingParameters(mparams);
	      
	      // Set refinement along features
	      manager.SetRefinement(MAS, MASvec);
	      
	      // Tryangulate
	      um::RunInfo run_info;
	      run_info.output_path = output_path;
	      manager.Tryangulate(pert_label, fset, run_info);

	      // stop clock
	      std::chrono::steady_clock::time_point timer_end = std::chrono::steady_clock::now();

	      // Track the run time
	      double time = std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_begin).count();
	      tfile << time << ", ";
	      tfile.flush();

	      // Track the number of quadtree refinement iterations
	      ifile << run_info.nQTRefinements <<", ";
	      ifile.flush();
	      
	      // Run correctness checks on the computed mesh
	      manager.RunCheck(pert_label, fset, 1.e-3);

	      // Output
	      fname = output_path+"geom-"+std::to_string(pnum)+".csv";
	      manager.GetUniversalMesh(pert_label).PlotTecMesh(run_info.output_path+"bg-"+std::to_string(pnum)+".tec");
	      manager.GetWorkingMesh(pert_label).PlotTecMesh(run_info.output_path+"wm-"+std::to_string(pnum)+".tec");
	    }
	}
  tfile.close();
}
