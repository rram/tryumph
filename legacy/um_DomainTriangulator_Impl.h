// Sriramajayam

/** \file um_DomainTriangulator_Impl.h
 * \brief Implementation of the class um::DomainTriangulator
 * \author Ramsharan Rangarajan
 * Last modified: May 28, 2020.
 */

#ifndef UM_DOMAIN_TRIANGULATOR_IMPL_H
#define UM_DOMAIN_TRIANGULATOR_IMPL_H

#include <unordered_map>
#include <set>
#include <algorithm>
#include <src/Optimizer/dvr_RelaxationDirGenerators.h>
#include <dvr_Module>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace um
{
  // Constructor
  template<class UMeshType> DomainTriangulator<UMeshType>::
    DomainTriangulator(const UMeshType& bgmesh,
		       const IndicatorFunction ifunc,
		       const dvr::SignedDistanceFunction sdfunc)
    :BG(bgmesh), 
    SDFunc(sdfunc),
    WM(BG),
    PosElmsFaces({}),
    PosVerts({}),
    isConforming(false)
      {
	// Evaluate the indicator function at the nodes of the background mesh
	std::map<int, Indicator> XiVals({});
	const int nBGnodes = BG.GetNumNodes();
	for(int n=0; n<nBGnodes; ++n)
	  XiVals[n] = ifunc(BG.coordinates(n));
	
	// Identify the set of positively cut elements and positive vertices
	IdentifyPositivelyCutElements(XiVals);
	assert(PosElmsFaces.size()>0 && "um::DomainTriangulator: no positively cut elements found");
      
	// Identify elements belonging to the working mesh
	SetWorkingElements();
      }


  // Identify positively cut elements
  template<class UMeshType> void DomainTriangulator<UMeshType>::
    IdentifyPositivelyCutElements(const std::map<int, Indicator>& XiVals) 
    {
      PosElmsFaces.clear();
      
      // Map from local node number to the local face number opposite. Assume simplices
      const int nodes_per_element = BG.GetNumNodesPerElement();
      const int num_faces = nodes_per_element;
      const int nodes_per_face = nodes_per_element-1;
      std::vector<int> node2oppFaceMap(nodes_per_element);
      {
	std::vector<bool> flag(nodes_per_element);
	for(int f=0; f<num_faces; ++f) 
	  {
	    const int* loc_fconn = BG.GetLocalFaceNodes(f);
	    // Find the local node missing from fconn. It is the local vertex opposite face 'f'
	    std::fill(flag.begin(), flag.end(), false);
	    for(int b=0; b<nodes_per_face; ++b)
	      flag[loc_fconn[b]] = true;
	    for(int a=0; a<nodes_per_element; ++a)
	      if(flag[a]==false)
		node2oppFaceMap[a] = f;
	  }
      }
      
      // Identify simplices in which characteristic function is +ve on a face and -ve at the opposing vertex
      const int nElements = BG.GetNumElements();
#pragma omp parallel default(shared)
      {
	// Thread-local data
	std::list<std::pair<int,int>> MyPosElmFaces({});
	bool xiFlag; // Is characteristic function available at all vertices in an element?
	int nPos; // Number of positive vertices
	int aNeg; // Local number of negative vertex
#pragma omp for
	for(int e=0; e<nElements; ++e)
	  {
	    const int* econn = BG.connectivity(e);
	    xiFlag = true;
	    nPos = 0;
	    aNeg = -1;
	    for(int a=0; a<nodes_per_element && xiFlag==true; ++a)
	      {
		const auto& it = XiVals.find(econn[a]);
		if(it==XiVals.end()) // Insufficient data. this element is assumed not to be +vely cut
		  xiFlag = false;
		else // Check the sign at this vertex
		  {
		    if(it->second==Indicator::Plus || it->second==Indicator::Zero)
		      ++nPos;
		    else if(it->second==Indicator::Minus)
		      aNeg = a;
		    else // The sign here is invalid
		      xiFlag = false;
		  }
	      }

	    // Check if this element is positively cut
	    if(xiFlag==true && nPos==nodes_per_element-1)
	      MyPosElmFaces.push_back( std::make_pair(e, node2oppFaceMap[aNeg]) );
	  }

	// Collate all element face pairs into a single list
#pragma omp critical
	for(auto& ef:MyPosElmFaces)
	  PosElmsFaces.push_back(ef);
      }

      // Compile the list of positive vertices
      std::set<int> pvset;
      for(auto& efpairs:PosElmsFaces)
	{
	  const int* econn = BG.connectivity(efpairs.first);
	  const int* fconn = BG.GetLocalFaceNodes(efpairs.second);
	  for(int i=0; i<nodes_per_face; ++i)
	    pvset.insert(econn[fconn[i]]);
	}
      PosVerts.clear();
      std::move(pvset.begin(), pvset.end(), std::back_inserter(PosVerts)); 
      return;
    }


  // Initialize elements in the working mesh
  template<class UMeshType> void DomainTriangulator<UMeshType>::
    SetWorkingElements()
    {
      // Number of faces
      const int num_faces = BG.GetNumNodesPerElement();

      // Accummulate a map of elements and their faces to check
      // ElmFaces2Check[e] = [face0, face1, face2, ..]
      // value: true -> face has been inspected. false -> face needs to be inspected
      std::unordered_map<int, std::vector<bool>> ElmFaces2Check({});

      // Start with the list of positively cut elements. Add all faces excluding the positive face.
      std::vector<bool> ElmFaceFlags(num_faces);
      for(auto& ef:PosElmsFaces)
	{
	  const int& elm = ef.first; // positive element
	  const int& locface = ef.second; // positive face

	  // Add this to the list of elements to be checked. Mark the +ve face as inspected
	  std::fill(ElmFaceFlags.begin(), ElmFaceFlags.end(), false);
	  ElmFaceFlags[locface] = true;
	  ElmFaces2Check.insert({elm,ElmFaceFlags});
	}

      // Regularize faces requiring inspection:
      // neighboring faces of +vely cut elements that are also +vely cut
      // should be marked as not requiring inspection
      for(auto& it:ElmFaces2Check)
	{
	  const int& elm = it.first;
	  const int* elmnbs = BG.GetElementNeighbors(elm);
	  auto& finfo = it.second;
	  for(int f=0; f<num_faces; ++f)
	    {
	      if(elmnbs[2*f]<0) // Free face. Mark as inspected.
		finfo[f] = true;
	      else if(finfo[f]==false) // This face appears to need inspection. It does not, if the neighbor is +vely cut.
		{
		  const int& nb_elm = elmnbs[2*f];
		  const int& nb_face = elmnbs[2*f+1];
		  auto jt = ElmFaces2Check.find(nb_elm);
		  if(jt!=ElmFaces2Check.end())
		    {
		      // This face of elm, and the corresponding face of its neighbor should be marked as inspected
		      finfo[f] = true;
		      jt->second[nb_face] = true;
		    }
		}
	    }
	}

      // Accummulate elements in the working mesh
      while(!ElmFaces2Check.empty())
	{
	  // Get the first element to check. 
	  auto it = ElmFaces2Check.begin();
	  const int& elm = it->first;        
	  const auto& faceFlags = it->second;

	  // Get the neighbors
	  const int* elm_nbs = BG.GetElementNeighbors(elm);
	  
	  // Inspect face-by-face
	  for(int f=0; f<num_faces; ++f)
	    if(elm_nbs[2*f]>=0 && faceFlags[f]==false) // non-boundary faces that need inspection
	      {
		// Neighboring element and its mating face
		const int& nb_elm = elm_nbs[2*f];
		const int& nb_face = elm_nbs[2*f+1];
		
		// If the neighbor is new to the queue, add it.
		// Mark all faces except nb_face as needing inspection.
		// Otherwise, just mark nb_face as checked
		auto jt = ElmFaces2Check.find(nb_elm);
		if(jt==ElmFaces2Check.end())
		  {
		    // add this element's info the list
		    std::fill(ElmFaceFlags.begin(), ElmFaceFlags.end(), false);
		    ElmFaceFlags[nb_face] = true;
		    ElmFaces2Check.insert({nb_elm,ElmFaceFlags}); // Invalidates 'it' and 'jt'
		  }
		else
		  { // update this element's info in the list
		    jt->second[nb_face] = true; }
	      }
	  
	  // Add this element to the working mesh, then remove it from the list of elements to check
	  WM.addElement(elm);
	  ElmFaces2Check.erase( ElmFaces2Check.find(elm) );
	}

      // Add all positive vertices to the working mesh through a dummy update
      for(auto& n:PosVerts)
	{ const double* X = BG.coordinates(n);
	  WM.update(n, X); }

      // Prune element 1-rings of positive vertices
      WM.Prune1Rings(PosVerts);
      
      // done
      return;
    }



  // Identify nodes in the working mesh that lie
  // within a prescribed "graph-distance" from the
  // set of positive vertices
  template<typename UMeshType> std::vector<int>
    DomainTriangulator<UMeshType>::GetVertexLayers(const int nLayers) const
    {
      // Create a map from a local face# to the local node# opposite the face
      /// face2oppNode[f] = local node opposite face 'f'
      const int num_faces = BG.nodes_element;
      const int num_face_nodes = BG.nodes_element-1;
      std::vector<int> face2oppNode(num_faces);
      std::vector<bool> isfaceNode(BG.nodes_element);
      for(int f=0; f<num_faces; ++f)
	{
	  std::fill(isfaceNode.begin(), isfaceNode.end(), false);
	  const int* locfacenodes = BG.GetLocalFaceNodes(f);
	  for(int i=0; i<num_face_nodes; ++i)
	    isfaceNode[locfacenodes[i]] = true;
	  for(int i=0; i<BG.nodes_element; ++i)
	    if(isfaceNode[i]==false)
	      { face2oppNode[f] = i;
		break; }
	}


      // Vertex layers to compute
      std::unordered_set<int> VertSet({});
      
      // Work with 2 layers of nodes
      std::unordered_set<int> curr_layer({});
      std::unordered_set<int> next_cluster({});
      
      // Initial condition:
      // VertSet = +ve nodes to direct the growth into the domain. Remove at the end of the calculation
      // curr_layer = nodes opposite +ve faces
      for(auto& n:PosVerts)
	VertSet.insert(n);

      for(auto& ef:PosElmsFaces)
	{
	  // Add the node opposite the +ve face
	  const int& elm = ef.first;
	  const int& f = ef.second;
	  const int* conn = BG.connectivity(elm);
	  curr_layer.insert(conn[face2oppNode[f]]);
	}


      // Append curr_layer into the vertex bucket.
      // Accrue the next_cluster = union of 1-rings of vertices in (curr_layer\prev_layer)
      // Update curr_layer = next_cluster\VertSet
      const int* oneRingVerts;
      int n1RingVerts;
      for(int layer=0; layer<nLayers; ++layer)
	{
	  // Append the curr_layer into the vertex bucket
	  next_cluster.clear();
	  for(auto& n:curr_layer)
	    {
	      // Append this to the vertex bucket
	      auto it = VertSet.insert(n);
	      assert(it.second==true);  // do not expect duplication.
	      
	      // Accrue the 1-ring into the next_layer
	      BG.Get1RingVertices(n, &oneRingVerts, n1RingVerts);
	      for(int i=0; i<n1RingVerts; ++i)
		next_cluster.insert(oneRingVerts[i]);
	    }
	  
	  // Prune: curr_layer = next_cluster\VertSet
	  curr_layer.clear();
	  for(auto& n:next_cluster)
	    if(VertSet.find(n)==VertSet.end())
	      curr_layer.insert(n);
	}

      // Remove +ve vertices from Verts
      for(auto& n:PosVerts)
	VertSet.erase(n);

      // Convert set -> vector and return
      std::vector<int> Verts(VertSet.begin(), VertSet.end());
      return std::move(Verts);
    }


  // Main functionality: make the mesh conform to the boundary
  // by iterative projection and relaxation
  template<typename UMeshType> void
    DomainTriangulator<UMeshType>::GenerateMesh(const int Nproject, const int Nrelax, const int Ndist)
    {
      // Cannot mesh again
      dvr_assert(isConforming==false && "um::DomainTriangulator: Cannot invoke GenerateMesh twice");
      
      if(BG.GetSpatialDimension()==2) // 2D triangles
	MeshWithDVR<dvr::GeomTri2DQuality<decltype(WM)>, 2>(Nproject, Nrelax, Ndist); 
      else //3D tets
	MeshWithDVR<dvr::GeomTet3DQuality<decltype(WM)>, 3>(Nproject, Nrelax, Ndist);
      
      // -- done --
      isConforming = true;
      return;
    }


  // Relax the working mesh using DVR
  template<typename UMeshType> void
    DomainTriangulator<UMeshType>::OptimizeMesh(const int Nrelax, const int Ndist, const bool bdFlag)
    {
      if(BG.GetSpatialDimension()==2)
	RelaxWithDVR<dvr::GeomTri2DQuality<decltype(WM)>, 2>(Nrelax, Ndist, bdFlag); // relax triangles
      else
	RelaxWithDVR<dvr::GeomTet3DQuality<decltype(WM)>, 3>(Nrelax, Ndist, bdFlag); // relax tets

      // -- done --
      return;
    }


  // Main functionality
  template<typename UMeshType>
    template<typename QType, int SPD>
    void DomainTriangulator<UMeshType>::MeshWithDVR(const int Nproject, const int Nrelax, const int Ndist)
  {
    // Vertices of the working mesh within the prescribed graph-distance from positive faces
    std::vector<int> Ir = GetVertexLayers(Ndist);
        
    // Prepare for vertex relaxation
    // Element qualities
    QType Quality(WM, 1);

    // Max-min solver & relaxation directions for interior nodes
    dvr::ReconstructiveMaxMinSolver<QType> intSolver(Quality, 1);
    dvr::CartesianDirGenerator<SPD> CartGen;

    // Tangential relaxation directions along the boundary
    dvr::TangentialDirGenerator<SPD> TgtGen(SDFunc);
      
    // For thread-safe update of coordinates in the working mesh,
    // perform a dummy update of all vertices in Ir
    // Positive vertices have already been added
    for(auto& n:Ir)
      { const double* X = WM.coordinates(n);
	WM.update(n, X); }
      
    // (i)  Partial projection of a positive vertex
    // (ii) Relax interior vertices
    // (iii) Relax positive vertices along tangential directions (unconstrained)
    const int nPosVerts = static_cast<int>(PosVerts.size());
    for(int piter=0; piter<Nproject; ++piter)
      {
	// Projection fraction: lambda*X + (1-lambda)*pi(X)
	const double lambda = 1.-static_cast<double>(piter+1)/static_cast<double>(Nproject);

	// Project positive vertices
	// THIS LOOP SHOULD BE PARALLELIZED

	// thread-local variables
	double Y[SPD];
	double cpt[SPD];
	double sd;
	double dsd[SPD];
	for(int i=0; i<nPosVerts; ++i)
	  {
	    // This vertex
	    const int& n = PosVerts[i];
	      
	    // Its current location in the working mesh
	    const double* X = WM.coordinates(n);
	      
	    // Closest point projection of X
	    SDFunc(X, sd, dsd);
	    for(int k=0; k<SPD; ++k)
	      cpt[k] = X[k]-sd*dsd[k];
	      
	    // New coordinates of this node
	    for(int k=0; k<SPD; ++k)
	      Y[k] = lambda*X[k] + (1.-lambda)*cpt[k];

	    // Update coordinates in the working mesh
	    WM.update(n, Y);
	  }

	// Relax interior vertices
	for(int riter=0; riter<Nrelax; ++riter)
	  { CartGen.iteration = riter;
	    dvr::Optimize(Ir, WM, intSolver, CartGen, nullptr); }
	
	// Relax positive vertices in the tangential direction WITHOUT constraints
	if(piter!=Nproject-1)
	  for(int riter=0; riter<Nrelax; ++riter)
	    dvr::Optimize(PosVerts, WM, intSolver, TgtGen, nullptr);
      }

    // Max-min solver to relax positive vertices CONSTRAINED to the boundary
    const int nSamples = 15;
    dvr::SamplingMaxMinSolver<QType> bdSolver(Quality, nSamples, 1);
    
    // Projector for sampling the boundary
    dvr::ClosestPointStruct<SPD> cpt(SDFunc);

    // Alternately relax boundary and interior nodes
    for(int riter=0; riter<Nrelax; ++riter)
      {
	// Boundary nodes
	dvr::Optimize(PosVerts, WM, bdSolver, TgtGen, &cpt);
	
	// Interior nodes
	CartGen.iteration = riter;
	dvr::Optimize(Ir, WM, intSolver, CartGen, nullptr);
      }
      
    // done
    return;
  }

  
  // Relax a mesh using DVR
  template<typename UMeshType>
    template<typename QType, int SPD>
    void DomainTriangulator<UMeshType>::RelaxWithDVR(const int Nrelax, const int Ndist, const bool bdFlag)
  {
    // Vertices of the working mesh within the prescribed graph-distance
    std::vector<int> Ir = GetVertexLayers(Ndist);

    // For thread-safe update of coordinates in the working mesh,
    // perform a dummy update of all vertices that can be perturbed
    // Positive vertices have already been added
    for(auto& n:Ir)
      { const double* X = WM.coordinates(n);
	WM.update(n, X); }
      
    // Prepare for vertex relaxation
      
    // Element qualities
    QType Quality(WM, 1);
      
    // Max-min solver & relaxation directions for interior nodes
    dvr::ReconstructiveMaxMinSolver<QType> intSolver(Quality, 1);
    dvr::CartesianDirGenerator<SPD> CartGen;
      
    // Max-min solver & relaxation direction for boundary nodes
    const int nSamples = 15;
    dvr::SamplingMaxMinSolver<QType> bdSolver(Quality, nSamples, 1);

    // Projector for sampling the boundary
    dvr::ClosestPointStruct<SPD> cpt(SDFunc);
      
    // Tangential relaxation directions along the boundary
    dvr::TangentialDirGenerator<SPD> TgtGen(SDFunc);
      
    // Alternate between relaxing interior and boundary vertices
    for(int riter=0; riter<Nrelax; ++riter)
      {
	// Interior
	CartGen.iteration = riter;
	dvr::Optimize(Ir, WM, intSolver, CartGen, nullptr);
	  
	// Boundary
	if(bdFlag==true)
	  { dvr::Optimize(PosVerts, WM, bdSolver, TgtGen, &cpt); }
      }

    // -- done --
    return;
  }
}

#endif

/*
// Loop over positive vertices. Project them onto the boundary
const int nPosVerts = static_cast<int>(PosVerts.size());
#pragma omp parallel
{
int vert, k;
double cpt[3], newcoord[3]; // More than sufficient space
#pragma omp for
for(int i=0; i<nPosVerts; ++i)
{
// This vertex
vert = PosVerts[i];
const auto* coord = WM.coordinates(vert);
	    
// Its closest point projection
BdGeom.GetClosestPoint(coord, cpt);

// New coordinates
for(k=0; k<SPD; ++k)
newcoord[k] = coord[k] + alpha*(cpt[k]-coord[k]);

// Update the new coordinates
WM.Insert(vert, newcoord);
}
}
return;
}
*/


