// Sriramajayam

#include <um_test_visualize.h>

namespace um
{
  namespace test
  {
    // save a vertex set with a vector field to a file
    void save_vertex_displacement_field_vtk(const std::string filename, 
					      const std::vector<std::map<int, std::pair<XY, XY>>>& vertex_coords,
					      const std::vector<int>& colorMap)
      {
	assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	
	std::fstream out;
	out.open(filename, std::ios::out);
	assert(out.good() && out.is_open());

	// # features
	const int nFeatures = static_cast<int>(colorMap.size());
	assert(colorMap.size()==vertex_coords.size());
	  
	// Headers
	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
	out << "ASCII" << std::endl;
	out << "DATASET POLYDATA" << std::endl;

	// nodes
	int nNodes = 0;
	for(auto& it:vertex_coords)
	  nNodes += static_cast<int>(it.size());
	out << "POINTS " << nNodes << " double" << std::endl;
	for(auto& it:vertex_coords)
	  for(auto& jt:it)
	    {
	      auto& X = jt.second.first;
	      out << X.first << " " << X.second << " " << 0. << std::endl;
	  }
	
	out << "VERTICES " << nFeatures << " " << nNodes+nFeatures << std::endl;
	for(int f=0, vcount=0; f<nFeatures; ++f)
	  {
	    out << static_cast<int>(vertex_coords[f].size()) << " ";
	    for(auto& jt:vertex_coords[f])
	      {
		out << vcount++ << " ";
	      }
	    out << std::endl;
	  }
	out << std::endl;
	
	// displacement vector field as pointwise data
	out << "POINT_DATA " << nNodes << std::endl;
	out << "SCALARS disp_field double 3" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(auto& it:vertex_coords)
	  {
	    for(auto& jt:it)
	      {
		const auto& X = jt.second.first;
		const auto& Y = jt.second.second;
		out << Y.first-X.first << " " << Y.second-X.second << " " << 0. << std::endl;
	      }
	  }

	// colors as pointwise data
	// note: cell-wise data is not transferred to glyphs for coloring because of a bug in paraview
	// https://gitlab.kitware.com/paraview/paraview/-/issues/18662
	// 3 scalars are required to choose the vector as an orientation in paraview
	out << "SCALARS color int 1" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(int f=0; f<nFeatures; ++f)
	  {
	    const auto& color = colorMap[f];
	    const int nverts = static_cast<int>(vertex_coords[f].size());
	    for(int i=0; i<nverts; ++i)
	      {
		out << color << std::endl;
	      }
	  }
	
	// done
	out.close();
      }

  } // test::
} // um::
