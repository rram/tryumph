// Sriramajayam

#ifndef UM_SPLITTING_POINT_FUNCTION_H
#define UM_SPLITTING_POINT_FUNCTION_H

#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_CubicSpline.h>

namespace um
{
  template<class GeomType>
    inline void Geometry_SplittingPointFunction(double* split_point, void* params)
    {
      assert(params!=nullptr);
      auto& geom = *static_cast<GeomType*>(params);
      geom.GetSplittingPoint(split_point);
      return;
    }

  //! Explicit instantiations of templated feature size functions;
  const auto p1segment_splitpointfunc        = Geometry_SplittingPointFunction<LineSegment>;
  const auto p2rationalbezier_splitpointfunc = Geometry_SplittingPointFunction<QuadraticRationalBezier>;
  const auto p3bezier_splitpointfunc         = Geometry_SplittingPointFunction<CubicBezier>;
  const auto p3spline_splitpointfunc         = Geometry_SplittingPointFunction<CubicSpline>;

}

#endif
