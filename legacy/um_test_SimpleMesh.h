
/** \file um_test_SimpleMesh.h
 * \brief Definition of the class um::SimpleMesh.
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <cassert>
#include <dvr_test_SimpleMesh.h>

namespace um
{
  namespace test
  {
    /** \brief Class implementing a rudimentary mesh data structure for 2D triangles.
     * The class inherits from dvr::test::SimpleMesh. 
     *
     * The class provides the following basic functionalities, some of
     * which are inherited from dvr::test::SimpleMesh. The list below is
     * not exhaustive, but is expected to be sufficient for common usage:
     *
     * \code{.cpp}
     * 
     * //  Access nodal coordinates
     *  const double* coordinates(const int& node_index) const; 
     *
     * // Update nodal coordinates
     * void update(const int node_index, const double* X); 
     *
     *  // Access element connectivities
     *  const int* connectivity(const int elm_index) const; 
     * 
     * // Access the total number of nodes
     * int GetNumNodes() const;
     *
     * // Access the total number of elements
     * int GetNumElements() const;
     *
     * // Access the element neighbor list
     * const int* GetElementNeighbors(const int elm_indx) const;
     *
     * \endcode
     *
     * The class is very rudimentary:
     * <ul>
     * <li> Nodes are numbered sequentially, starting from 0 to nodes-1. </li>
     * <li> Elements are numbered sequentially, starting from 0 to elements-1. </li>
     * <li> Nodal coordinates are stored sequentially in the public vector Coordinates.
     *  Coordinates of the node <tt>n</tt> is located at Coordinates[2*<tt>n</tt>] </li>
     * <li> Element connectivities are stored sequentially in the public vector Connectivity.
     * Connectivity of the element <tt>e</tt> is located at Connectivity[3*e]. </li>
     * </ul>
     *
     * In addition to the methods mentioned above, the class
     * provides some additional utilities:
     * <ul>
     * <li> To read a mesh from a tecplot file, through the ReadTecplotFile method. </li>
     * <li> To print a mesh in tecplot file, through the PlotTecFile method. </li>
     * <li> To identify nodes lying on the boundary of the mesh, through the GetBoundaryNodes method. </li>
     * <li> To subdivide a triangle mesh in a self-similar manner, through the SubdivideTriangles method.
     * </ul>
     *
     * <b> Code sample: simple use case</b>: 
     * \code{.cpp}
     *
     * // Create a mesh
     * umesh::SimpleMesh SM;
     * SM.nodes = #nodes;
     * SM.elements = #elements;
     * SM.Coordinates.resize(2*SM.nodes);
     * // Set nodal coordinates
     * for(int n=0; n<SM.nodes; ++n)
     * { SM.Coordinates[2*n+0] = x-coordinate;
     *   SM.Coordinates[2*n+1] = y-coordinate; }
     * // Set element connectivities
     * SM.Connectivity.resize(3*SM.elements);
     * for(int e=0; e<SM.elements; ++e)
     * { SM.connectivity[3*e+0] = index of node in triangle #e;
     *   SM.connectivity[3*e+1] = index of node in triangle #e;
     *   SM.connectivity[3*e+2] = index of node in triangle #e; }
     * // Compute mesh data structures (1-rings, element neighbors, etc)
     * SM.SetupMesh(); 
     * // Plot the mesh in tecplot format
     * SM.PlotTecMesh("name.tec"); 
     *
     * \endcode
     *
     * <b>Caveats</b>: This class is provided solely for the purpose of testing and examples.
     * <br>
     * It is neither general purpose, nor efficient.
     * <br>
     * The class can be easily abused-- for example, it is possible to
     * add/delete nodes/elements through public access to the list of nodal coordinates and element connectivities,
     * which will invalidate all 1-ring vertex/element lists.
     * While useful for prototyping, SimpleMesh is not expected to be an integral part of production codes.
     *
     * The class can be easily abused-- for example, it is possible to
     * add/delete nodes/elements through public access to the list of nodal coordinates and element connectivities,
     * which will invalidate all 1-ring vertex/element lists.
     * While useful for prototyping, SimpleMesh is not expected to be an integral part of production codes.
     *
     */
    class SimpleMesh: public dvr::test::SimpleMesh
    {
    public:
      /**! \brief Default constructor.
       * Sets the spatial dimension to 2 and nodes per element to 3.
       */
    SimpleMesh()
      :dvr::test::SimpleMesh(),
	ElmNbs({})
	  { spatial_dimension = 2;
	    nodes_element = 3; }

      //! Constructor
      //! \param[in] coords Vertex coordinates, indexed from 0
      //! \param[in] conn Element connectivities
      SimpleMesh(const std::vector<double>& coord, const std::vector<int>& conn);

      //! Destructor. Nothing to destroy
      inline virtual ~SimpleMesh() {}

      //! Disable assignment and copy
      SimpleMesh(const SimpleMesh& obj) = delete;
      SimpleMesh& operator=(const SimpleMesh&) = delete;

      /** \brief Method to finalize the mesh data structure.
       * Sets up 1-ring data structures by invoking dvrmesh::SetupMesh().
       * Computes the element neighbor list.
       *
       * Should be called prior to accessing vertex 1-rings, element 1-rings
       * and element neighbor lists.
       */
      void SetupMesh();

      //! Returns the number of nodes
      //! Node indices range from 0 to GetNumNodes()-1.
      //! \return Total number of nodes in the mesh.
      inline int GetNumNodes() const
      { return nodes; }

      //! Copy the set of nodal coordinates
      inline void CopyNodalCoordinates(double* copy) const
      { assert(isFinalized==true && "SimpleMesh:: call SetupMesh()");
	for(int n=0; n<nodes; ++n)
	  { const double* X = coordinates(n);
	    for(int k=0; k<spatial_dimension; ++k)
	      copy[spatial_dimension*n+k] = X[k]; }
	return;
      }
    
      //! Returns the number of elements
      //! Element indices range from 0 to GetNumElements()-1.
      //! \return Total number of elements in the mesh
      inline int GetNumElements() const 
      { return elements; }
  
      //! Returns the local node numbers on a local face
      //! Assumes that SetupMesh() has been called.
      //! \param[in] locface Lacal face number. Should equal 0, 1 or 2.
      //! \return locfacenodes array of length 2.
      //! locfacenodes[k] = k-th node on face locface, k=0,1.
      inline const int* GetLocalFaceNodes(const int locface) const
      { assert(isFinalized==true && "SimpleMesh:: Call SetupMesh(). ");
	return &LocFaceNodes[locface][0]; }
  
      /** \brief Access the element neighbor list
       * Assumes that SetupMesh() has been called.
       * \param[in] elm_indx Index of the element. 0<=elm_indx<=GetNumElements()-1.
       * \return elm_nbs Integer array of length 6.
       * elm_nbs[2*f] = index of the element adjacent to face 'f' of elm_indx.
       * Set to -1 if f lies on the boundary.
       * elm_nbs[2*f+1] = face of elm_nbs[2*f] that is adjacent to face 'f' of elm_index.
       * Set to -1 if f lies on the boundary.
       */
      inline const int* GetElementNeighbors(const int elm_indx) const
      { // Check that the neighbor list has been computed
	assert(isFinalized==true && "SimpleMesh:: call SetupMesh()");
	return &ElmNbs[2*nodes_element*elm_indx]; }
  
      //! \brief Write the mesh to file in tecplot format
      //! \param[in] filename Filename
      //! \param[in] ElmSet Subset of elements to plot
      void PlotTecSubMesh(const std::string filename, const std::set<int>& ElmSet) const;
  
      /** \brief Self-similar subdivision of a triangle mesh
       * The resulting mesh has 4 times as many elements.
       * Invokes SetupMesh.
       */
      void SubdivideTriangles();

      // Members
    protected:
      const int LocFaceNodes[3][3] = {{0,1},{1,2},{2,0}}; //!< Enumeration of nodes on faces
      std::vector<int> ElmNbs; //!< Element neighbor list
    
      //! Helper method to compute element neighbors
      void ComputeElementNeighbors();
    };

    /** \brief Helper function: create an equilateral mesh over a regular hexagon.
     * \param[out] mesh Created SimpleMesh object. The boundary of the mesh is a hexagon.
     * \param[in] domain_center Cartesian coordinates of the center of the hexagonal domain. Should have length 2.
     * \param[in] domain_size Length of the side of the hexagon.
     * \param[in] ndiv Number of subdivisions. On return, each edge in the mesh has length domain_size/pow(2,ndiv).
     *
     * Note that mesh.SetupMesh() is called before return.
     *
     * Sample usage:
     * \code{.cpp}
     * double center[] = {0,0};
     * SimpleMesh SM;
     * CreateEquilateralMesh(SM, center, 2., 5);
     * SM.PlotTecMesh("hex.tec");
     * \endcode
     */
    void CreateEquilateralMesh(SimpleMesh& mesh, const double* domain_center, const double domain_size, int ndiv);

    /** \brief Helper function to create an acute triangulation over a square.
     * Specifically, the square-shaped stencil labelled "BBBB" in  in the paper 
     * <i>Bern, Eppstein & Gilbert, Provably good mesh generation (1994)<\i> is used.
     *
     * \param[out] mesh Created SimpleMesh object. The boundary of the mesh is a hexagon.
     * \param[in] domain_center Cartesian coordinates of the center of the square domain. Should have length 2.
     * \param[in] domain_size Length of the side of the square.
     * \param[in] ndiv Number of subdivisions. On return, each edge in the mesh has length domain_size/pow(2,ndiv).
     *
     * Note that mesh.SetupMesh() is called before return.
     *
     * Sample usage:
     * \code{.cpp}
     * double center[] = {0,0};
     * SimpleMesh SM;
     * CreateSquareMesh(SM, center, 2., 5);
     * SM.PlotTecMesh("square.tec");
     * \endcode
     */

    void CreateSquareMesh(SimpleMesh& mesh, const double* domain_center, const double domain_size, int ndiv);
  }
}
