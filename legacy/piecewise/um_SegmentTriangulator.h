// Sriramajayam

/** \file um_SegmentTriangulator.h
 * \brief Defines the class um::SegmentTriangulator
 * \author Ramsharan Rangarajan
 * Last modified: October 31, 2020
 */

#ifndef UM_SEGMENT_TRIANGULATOR_H
#define UM_SEGMENT_TRIANGULATOR_H

#include <dvr_Module>
#include <um_WorkingMesh.h>
#include <um_Types.h>
#include <um_LineSegment.h>
#include <list>
#include <map>

namespace um
{
  /** \brief Class to encapsulate data and utilities to compute a um::WorkingMesh
   * from a given universal mesh that conforms to a closed segment.
   * Information about the geometry is passed through the signed distance function
   * 
   * \tparam UMeshType Type of the universal mesh
   */
  template<class UMeshType>
    class SegmentTriangulator
    {
    public:
      /** \brief Constructor
       * \param[in] bgmesh Universal mesh, Referenced.
       * \param[in] geom Segment geometry
       */
      SegmentTriangulator(const UMeshType& bgmesh,
			  const LineSegment& geom);

      //! Destructor
      inline virtual ~SegmentTriangulator() {}

      //! Disable copy and assignment
      SegmentTriangulator(const SegmentTriangulator<UMeshType>&) = delete;
      SegmentTriangulator& operator=(const SegmentTriangulator&) = delete;

      //! Returns a reference to the working mesh
      inline WorkingMesh<UMeshType>& GetWorkingMesh()
      { return WM;}

      //! Returns the set of positive vertices
      inline const std::vector<int>&  GetPositiveVertices() const
      { return PosVerts; }
    
      //! Returns the list of positive element-face pairs
      inline const std::list<std::pair<int,int>>& GetPositiveElmFacePairs() const
      { return PosElmsFaces; }

      /** \brief Perturb/relax a working mesh to conform to the boundary.
       *
       * \param[in] Nproject Number of project steps
       * \param[in] Nrelax Number of relaxation steps
       * \param[in] Ndist Graph-distance from teh boundary defining the relaxation radius
       */
      void GenerateMesh(const int Nproject, const int Nrelax, const int Ndist);

    private:
      const UMeshType& BG; //!< Reference to the background mesh
      const LineSegment& Geom; //!< Reference to the geometry
      WorkingMesh<UMeshType> WM; //!< Working mesh
      std::list<std::pair<int, int>> PosElmsFaces; //!< List of positive (element, face) pairs
      int coincidentVertices[2]; //!< Vertices coincident with endpoints of the segment
      std::vector<int> PosVerts; //!< List of positive vertices
      bool isConforming; //!< Flag to track whether the working mesh conforms to the boundary

      //! Helper method to identify vertices lying with a specified number of 1-rings
      //! of the set of positive vertices in the working mesh
      //! \param[in] nLayers Number of rings to consider
      //! \return Vertices lying a graph-distance of "nLayers" from the set of +ve vertices, not including +ve vertices
      std::set<int> GetVertexLayers(const int vertNum, const int nLayers) const;
      
      //! Helper function to identify the set of positively cut elements
      void IdentifyPositivelyCutElements();

      //! Helper function to make the sequence of positive edges polygon-free
      void MakePolygonFree();

      //! Helper function that makes the end points of the segment coincide with
      //! a vertex in the mesh
      void AdaptForEndPoints(const int Nproject, const int Nrelax, const int Ndist);

      //! Helper method for meshing
      void MeshWithDVR(const int Nproject, const int Nrelax, const int Ndist);
    };
}

#endif

// Implementation of main functionalities
#include <um_SegmentTriangulator_Impl.h>
