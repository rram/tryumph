// Sriramajayam

/** \file um_SegmentTriangulator_Impl.h
 * \brief Implementation of the class um::SegmentTriangulator
 * \author Ramsharan Rangarajan
 * Last modified: October 31, 2020
 */

#ifndef UM_SEGMENT_TRIANGULATOR_IMPL_H
#define UM_SEGMENT_TRIANGULATOR_IMPL_H

#include <um_Utils.h>

namespace um
{
  // Constructor
  template<class UMeshType>
    SegmentTriangulator<UMeshType>::
    SegmentTriangulator(const UMeshType& bgmesh,
			const LineSegment& geom)
    :BG(bgmesh),
    Geom(geom),
    WM(BG),
    PosElmsFaces({}),
    coincidentVertices{-1,-1},
    PosVerts({}),
    isConforming(false)   {}


   // Generate a mesh
  template<class UMeshType>
    void SegmentTriangulator<UMeshType>::
    GenerateMesh(const int Nproject, const int Nrelax, const int Ndist)
    { 
      // Cannot mesh twice
      dvr_assert(isConforming==false && "um::SegmentTriangulator: Cannot invoke GenerateMesh twice");

      //=========
      // HACK:
      //==========
      // Insert all elements into the working mesh
      const int nElements = BG.GetNumElements();
      for(int e=0; e<nElements; ++e)
	WM.addElement(e);
      
      // Adapt the mesh at the corners
      AdaptForEndPoints(Nproject, Nrelax, Ndist);

      // Identify the set of positively cut elements
      IdentifyPositivelyCutElements();

      // Mesh using DVR
      MeshWithDVR(Nproject, Nrelax, Ndist);
    }


  // Perturb a mesh to make vertices conform to the end points
  template<class UMeshType>
    void SegmentTriangulator<UMeshType>::
    AdaptForEndPoints(const int Nproject, const int Nrelax, const int Ndist)
    {
      // End points of the segment
      const double* EndPoints = Geom.GetEndPoints();
      
      // For each end point of the segment:
      // Identify the closest vertex in the background mesh
      // Accummulate these vertices in the prescribed neighborhood to be perturbed
      // Add these vertices to the working mesh
      std::set<int> cnrneighbors{};
      for(int cnrNum=0; cnrNum<2; ++cnrNum)
	{
	  coincidentVertices[cnrNum] = BG.GetClosestVertex(EndPoints+cnrNum*2);
	  std::set<int> vertexNeighbors = GetVertexLayers(coincidentVertices[cnrNum], Ndist);
	  for(auto& n:vertexNeighbors)
	    cnrneighbors.insert(n);
	}
      
      // Sanity check
      assert(coincidentVertices[0]!=coincidentVertices[1] &&
	     "um::SegmentTriangulator::AdaptForEndPoints()- Endpoints too close");
      
      // Remove coincident vertices from cnrneighbors
      for(int cnrNum=0; cnrNum<2; ++cnrNum)
	{
	  auto it = cnrneighbors.find(coincidentVertices[cnrNum]);
	  if(it!=cnrneighbors.end())
	    cnrneighbors.erase(it);
	}


      // Mesh relaxation
      const int nthreads = 1;
      
      // Quality metric
      dvr::GeomTri2DQuality<decltype(WM)> Quality(WM, nthreads);

      // Max-min solver for relaxation, Assume interior nodes
      dvr::ReconstructiveMaxMinSolver<decltype(Quality)> dvrSolver(Quality, nthreads);

      // Relaxation directions
      dvr::CartesianDirGenerator<2> CartGen;

      // Relaxed vertices
      std::vector<int> Ir(cnrneighbors.begin(), cnrneighbors.end());
      
      // Insert all perturbed nodes into the working mesh- ensures thread-safe updates later.
      for(int cnrNum=0; cnrNum<2; ++cnrNum)
	{ const double* X = WM.coordinates(coincidentVertices[cnrNum]);
	  WM.update(coincidentVertices[cnrNum], X); }
      for(auto& n:cnrneighbors)
	{ const double* X = WM.coordinates(n);
	  WM.update(n, X); }
      
      // Project the coincident vertices towards the end points
      // and relax the neighborhood vertices
      for(int pstep=0; pstep<Nproject; ++pstep)
	{
	  // Project vertices towards the end points
	  // lambda*X + (1-lambda)*pi(X)
	  const double lambda = 1.-static_cast<double>(pstep+1)/static_cast<double>(Nproject);
	  for(int cnrNum=0; cnrNum<2; ++cnrNum)
	    {
	      const double* X = WM.coordinates(coincidentVertices[cnrNum]);
	      double Y[2];
	      for(int k=0; k<2; ++k)
		Y[k] = lambda*X[k] + (1.-lambda)*EndPoints[2*cnrNum+k];
	      WM.update(coincidentVertices[cnrNum], Y);
	    }

	  // Relax vertices in the neighborhood using DVR
	  for(int riter=0; riter<Nrelax; ++riter)
	    { CartGen.iteration = riter;
	      dvr::Optimize(Ir, WM, dvrSolver, CartGen, nullptr); }
	}

      // done
      return;
    }

  // Helper function to identify the set of positively cut elements
  template<class UMeshType>
    void SegmentTriangulator<UMeshType>::IdentifyPositivelyCutElements()
    {
      // elements in the 1-ring of a vertex
      const int* oneRingElms;//std::vector<int> oneRingElms(maxElmValency);
      int n1RingElms = 0;

      // vertices in the 1-ring of a vertex
      const int* oneRingVerts;
      //std::vector<int> oneRingVerts(maxVertValency);
      int n1RingVerts = 0;

      const int nodes_element = BG.GetNumNodesPerElement();
      dvr_assert(nodes_element==3);
      
      // Enumeration of face nodes
      const int* locfacenodes[] = {BG.GetLocalFaceNodes(0),
				   BG.GetLocalFaceNodes(1),
				   BG.GetLocalFaceNodes(2)};

      // node number -> opposite  face (local numberings)
      int loc_node_to_oppface_map[3] = {-1,-1,-1};
      {
	std::set<int> all_elm_nodes{};
	for(int f=0; f<3; ++f)
	  for(int i=0; i<nodes_element-1; ++i)
	    all_elm_nodes.insert(locfacenodes[f][i]);

       	for(int f=0; f<3; ++f)
	  { std::set<int> thisfacenodes(locfacenodes[f], locfacenodes[f]+nodes_element-1);
	    std::vector<int> oppnode{};
	    std::set_difference(all_elm_nodes.begin(), all_elm_nodes.end(),
				thisfacenodes.begin(), thisfacenodes.end(),
				std::inserter(oppnode, oppnode.begin()));
	    dvr_assert(static_cast<int>(oppnode.size())==1);
	    loc_node_to_oppface_map[oppnode[0]] = f;
	  }
	for(int f=0; f<3; ++f)
	  { dvr_assert(loc_node_to_oppface_map[f]!=-1); }
      }

      
      // Start from the vertex coincident with the left end point
      // March towards the vertex coincident with the right end point

      int curr_pos_vert = coincidentVertices[0];
      while(curr_pos_vert!=coincidentVertices[1])
	{
	  // Append this vertex to the list of positive vertices
	  PosVerts.push_back(curr_pos_vert);
	  
	  // Vertices in the 1-ring
	  BG.Get1RingVertices(curr_pos_vert, &oneRingVerts, n1RingVerts);

	  // Elements in the 1-ring
	  BG.Get1RingElements(curr_pos_vert, &oneRingElms, n1RingElms);

	  // Signed distance function at vertices
	  std::map<int, bool> sdvals{};
	  for(int vnum=0; vnum<n1RingVerts; ++vnum)
	    {
	      const int& vert = oneRingVerts[vnum];
	      const double* X = WM.coordinates(vert);
	      double SD = 0.;
	      Geom.GetSignedDistance(X, SD, nullptr);
	      sdvals[vert] = (SD>0.) ? true : false;
	    }

	  // Vertices whose signs cannot be ambiguous
	  sdvals[curr_pos_vert] = true;
	  sdvals[coincidentVertices[0]] = true;
	  sdvals[coincidentVertices[1]] = true;

	  // Identify positively cut elements and their positive faces
	  std::vector<std::pair<int,int>> loc_posElmsFaces{};
	  std::vector<int> posNodes{}, negNodes{};
	  for(int ecount=0; ecount<n1RingElms; ++ecount)
	    {
	      // This element
	      const int elm = oneRingElms[ecount];

	      // Its connectivity
	      const int* conn = BG.connectivity(elm);

	      // Signed distances at the nodes
	      posNodes.clear();
	      negNodes.clear();
	      for(int a=0; a<nodes_element; ++a)
		{ const int& vert = conn[a];
		  auto it = sdvals.find(vert);
		  dvr_assert(it!=sdvals.end());
		  if(it->second==true)
		    posNodes.push_back(a);
		  else
		    negNodes.push_back(a);
		}
	      
	      // Positively cut?
	      if(static_cast<int>(posNodes.size())==nodes_element-1)
		// yes. set the positive element -> face pair
		loc_posElmsFaces.push_back( std::make_pair(elm, loc_node_to_oppface_map[negNodes[0]]) );
	    }

	  // Should have precisely 2 positively cut elements
	  dvr_assert(static_cast<int>(loc_posElmsFaces.size())==2);

	  // Pick the edge emanating from curr_pos_vert
	  int posedges[2][2];
	  for(int i=0; i<2; ++i)
	    {
	      const int& elm = loc_posElmsFaces[i].first;
	      const int& face = loc_posElmsFaces[i].second;		
	      const int* conn = BG.connectivity(elm);

	      // vertices of this face
	      posedges[i][0] = conn[locfacenodes[face][0]];
	      posedges[i][1] = conn[locfacenodes[face][1]];
	    }

	  // sanity checks: curr_pos_vert should be a node on both edges
	  dvr_assert((posedges[0][0]==curr_pos_vert || posedges[0][1]==curr_pos_vert) &&
		     (posedges[1][0]==curr_pos_vert || posedges[1][1]==curr_pos_vert) );

	  // pick the edge originating from curr_pos_vert
	  // update to the next step
	  if(posedges[0][0]==curr_pos_vert)
	    { PosElmsFaces.push_back( loc_posElmsFaces[0] );
	      curr_pos_vert = posedges[0][1]; }
	  else
	    { PosElmsFaces.push_back( loc_posElmsFaces[1] );
	      curr_pos_vert = posedges[1][1]; }
	  
	  // proceed to the next step
	}

      // Append the terminal vertex
      PosVerts.push_back( coincidentVertices[1] );
      
      // ===================================
      // TODO: prune to a polygon free path
      // ===================================

      // done
      return;
    }


  // Helper method to identify vertices lying with a specified number of 1-rings
  // of the set of positive vertices in the working mesh
template<class UMeshType> std::set<int>
  SegmentTriangulator<UMeshType>::GetVertexLayers(const int vertNum, const int nLayers) const
  {    
    // Vertex layers to compute
    std::set<int> VertSet({});
    
    // Work with 2 layers of nodes
    std::unordered_set<int> curr_layer({});
    std::unordered_set<int> next_cluster({});

    // Initial condition:
    // curr_layer = provided node. Remove at the end of the calculation
    // next_cluster: aggregrate of 1-ring neighbors
    curr_layer.insert(vertNum);
    const int* oneRingVerts;
    int n1RingVerts = 0;
    for(int layer=0; layer<nLayers; ++layer)
      {
	// Append all vertices from curr_layer into VertSet
	for(auto& vert:curr_layer)
	  VertSet.insert(vert);
	
	// Accummulate 1-ring neighbors of vertices in the current layer into the next cluster
	next_cluster.clear();
	for(auto& vert:curr_layer)
	  {
	    BG.Get1RingVertices(vert, &oneRingVerts, n1RingVerts);
	    for(int i=0; i<n1RingVerts; ++i)
	      next_cluster.insert(oneRingVerts[i]);
	  }

	// Prune: curr_layer  = next_cluster/VertSet
	curr_layer.clear();
	for(auto& vert:next_cluster)
	  if(VertSet.find(vert)==VertSet.end())
	    curr_layer.insert(vert);
      }

    // Remove "vertNum" from VertSet
    VertSet.erase(vertNum);

    // return
    return VertSet;
  }


// Helper function to make the sequence of positive edges polygon-free
template<class UMeshType> void
  SegmentTriangulator<UMeshType>::MakePolygonFree()
  {
    
  }

      
// Helper method for meshing
 template<class UMeshType> void
   SegmentTriangulator<UMeshType>::MeshWithDVR(const int Nproject, const int Nrelax, const int Ndist)
   {
     // Identify vertices to relax
     std::set<int> IrSet{};
     for(auto& vert:PosVerts)
       {
	 auto vertnbs = GetVertexLayers(vert, Ndist);
	 for(auto& v:vertnbs)
	   IrSet.insert(v);
       }
     // Remove positive vertices
     for(auto& vert:PosVerts)
       {
	 auto it = IrSet.find(vert);
	 if(it!=IrSet.end())
	   IrSet.erase(it);
       }
     std::vector<int> Ir(IrSet.begin(), IrSet.end());
     std::cout<<"\nNumber of relaxed vertices: "<<Ir.size()<<std::flush;
     const int nthreads = 1;
     
     // Element qualities
     dvr::GeomTri2DQuality<decltype(WM)> Quality(WM, nthreads);

     // Max-min solver
     dvr::ReconstructiveMaxMinSolver<decltype(Quality)> dvrSolver(Quality, nthreads);

     // Relaxation directions
     dvr::CartesianDirGenerator<2> CartGen;

     // Insert all perturbed nodes into the working mesh from thread-safe updates
     for(auto& vert:PosVerts)
       { const double* X = WM.coordinates(vert);
	 WM.update(vert, X); }
     for(auto& vert:Ir)
       { const double* X = WM.coordinates(vert);
	 WM.update(vert, X); }
     
     // Project positive vertices to their closest point in steps.
     // Relax vertices in Ir
     const int nPosVerts = static_cast<int>(PosVerts.size());
     for(int piter=0; piter<Nproject; ++piter)
       {
	 // Projection fraction
	 const double lambda = 1.-static_cast<double>(piter+1)/static_cast<double>(Nproject);

	 // Project positive vertices
	 for(int i=0; i<nPosVerts; ++i)
	   {
	     // This vertex
	     const int& vert = PosVerts[i];

	     // Current location
	     const double* X = WM.coordinates(vert);

	     // Closest point projection
	     double cpt[2];
	     Geom.GetClosestPointProjection(X, cpt);

	     // New coordinates
	     double Y[2];
	     for(int k=0; k<2; ++k)
	       Y[k]  =lambda*X[k] + (1.-lambda)*cpt[k];

	     // update
	     WM.update(vert, Y);
	   }

	 // Relax neighboring vertices
	 for(int riter=0; riter<Nrelax; ++riter)
	   { CartGen.iteration = riter;
	     dvr::Optimize(Ir, WM, dvrSolver, CartGen, nullptr); }
       }

     // done
     return;
   }
  
}
#endif
