// Sriramajayam

#ifndef UM_LINE_SEGMENT_IMPL_H
#define UM_LINE_SEGMENT_IMPL_H

namespace um
{
  // Compute the closest point projection.
  void LineSegment::GetClosestPointProjection(const double* X,
					      double* cpt) const
  {
    const double* A = &EndPts[0];
    const double* B = &EndPts[2];
    double AB2 = (A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]);
  
    // cpt = A + lamda(B-A)
    // Compute lambda using the condition: (X-cpt).(B-A) = 0
    double lambda = ((X[0]-A[0])*(B[0]-A[0]) + (X[1]-A[1])*(B[1]-A[1]))/AB2;
    cpt[0] = A[0]+lambda*(B[0]-A[0]);
    cpt[1] = A[1]+lambda*(B[1]-A[1]);
    return;
  }

  // Returns the signed distance function and its derivative
  void LineSegment::GetSignedDistance(const double* X,
				      double& SD, double* dSD) const
  {
    // get the closest point projection
    double cpt[2] = {0.,0.};
    GetClosestPointProjection(X, cpt);

    const double* A = &EndPts[0];
    const double* B = &EndPts[2];
    const double AB = std::sqrt((B[0]-A[0])*(B[0]-A[0])+(B[1]-A[1])*(B[1]-A[1]));

    // normal to the segment
    const double nvec[] = {(B[1]-A[1])/AB, (A[0]-B[0])/AB};
    SD = (X[0]-cpt[0])*nvec[0] + (X[1]-cpt[1])*nvec[1];

    if(dSD!=nullptr)
      { dSD[0] = nvec[0];
	dSD[1] = nvec[1]; }

    // done
    return;
  }

}
#endif
