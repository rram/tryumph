// Sriramajayam

/** \file um_LineSegment.h
 * Definition of the class LineSegment
 * \author Ramsharan Rangarajan
 * \data October 31, 2020
 */

#ifndef UM_LINE_SEGMENT_H
#define UM_LINE_SEGMENT_H

#include <vector>
#include <cassert>
#include <cmath>

namespace um
{
  //! Class defining a planar line segment
  class LineSegment
    {
    public:
      //! Constructor
      //! \param[in] pts End points. Copied. Assumed to be in order
      LineSegment(const double* pts)
	:EndPts{pts[0], pts[1], pts[2], pts[3]}
      {}

      //! Disable copy and assignment
      LineSegment(const LineSegment&) = delete;
      LineSegment& operator=(const LineSegment&) = delete;

      //! Destructor. Nothing to do.
      inline virtual ~LineSegment() {}
      //! Returns the requested control point

      inline const double* GetEndPoints() const 
      { return &EndPts[0]; }

      //! Returns the closest point projection.
      //! \param[in] X Point in the plane
      //! \param[out] cpt Computed closest point projection.
      //! May lie outside the bounds of the segment
      void GetClosestPointProjection(const double* X, double* cpt) const;

      //! Returns the signed distance function and its derivative
      //! Implemented by template specializations
      //! \param[in] X Point in the plane
      //! \param[out] SD Computed signed distance
      //! \param[out] dSD Computed derivative of the signed distance
      //! The signed distance is computed irrespective of the location
      //! of the closest point projection
      void GetSignedDistance(const double* X, double& SD, double* dSD) const;

    private:
      const std::vector<double> EndPts;
    };
}

#endif

#include <um_LineSegment_impl.h>

