// Sriramajayam

#include <iostream>
#include <um_LineSegment.h>
#include <um_Utils.h>
#include <um_SegmentTriangulator.h>
#include <umesh_SimpleMesh.h>

int main()
{
  umesh::SimpleMesh BG;
  BG.ReadTecplotFile("BBBB.mesh");
  for(int i=0; i<3; ++i)
    BG.SubdivideTriangles();
  BG.SetupMesh();
  BG.PlotTecMesh("BG.tec");
  
  // Line segment
  const double EndPts[] = {-0.13345, -0.3456, 0.3535, 0.22345};
  um::LineSegment Geom(EndPts);

  // Triangulator
  um::SegmentTriangulator<decltype(BG)> tri(BG, Geom);

  // Mesh
  const int Nproject = 4;
  const int Nrelax = 5;
  const int Ndist = 4;
  tri.GenerateMesh(Nproject, Nrelax, Ndist);
  
  // Visualize
  auto& WM = tri.GetWorkingMesh();
  WM.PlotTecMesh("WM.tec");
}
