// Sriramajayam

//! \file um_Algorithm_Project_Relax.h
//! \brief Implements function that incrementally projects positive vertices and relaxes the neighborhood
//! \author Ramsharan Rangarajan

#pragma once

#include <um2_Output.h>
#include <um2_WorkingMesh.h>
#include <um2_MeshWrapper.h>
#include <um2_FeatureSet.h>
#include <um2_algo_CallbackInterface.h>

#include <dvrlib>
#include <tuple>
#include <memory>

namespace um2
{
  namespace algo
  {
    //! \brief Iteratively project positive vertices towards features and relax nearby vertices using DVR
    //! \tparam WMMeshType Type of the working mesh
    //! \ingroup algo
    //! \param[in,out] WM          Working mesh to modify
    //! \param[in]     mparams     Meshing parameters/options
    //! \param[in]     gparams     List of feature parameters
    //! \param[in]     mout        Vertex information about features
    //! \return Element 0 of the tuple: The result of the algorithm 
    //! \return Element 1 of the tuple: The set of defective elements in case of a failure 
    //! \return Element 2 of the tuple: Number of relaxation iterations performed. This can be different from that specified in mparams in case of a failure
    //! \return Element 3 of the tuple: Number of relaxed nodes, counted once per relaxation iteration
    //! \return Updated working mesh WM
    //! \remark A failure corresponds to an element having a quality smaller than the threshold specified in mparams
    //! \todo Add facility to let some vertices be undisturbed during relaxation. This may be required in the vicinity of the mesh boundary
    template<typename MeshType>
      std::tuple<MeshingReason, std::set<std::pair<int,int>>, int, int>
      Project_Relax(MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
		    const std::set<int>& frozen_nodes,
		    const MeshingParams& mparams,
		    CallbackInterface<MeshType> *callback)
    {
      // outputs
      std::set<std::pair<int,int>> defElmSet{};
      int nRelaxIters = 0;
      int nRelaxVerts = 0;

      // Access
      auto& WM            = mesh_wrapper.GetMesh();
      const auto& BG      = WM.getParentMesh();
      const auto& fset    = WM.getSplitFeatureSet();
      const int nFeatures = fset.getNumFeatures();
      const auto& gparams = fset.getFeatureParams();
      const auto& mout    = WM.getSplitFeatureCorrespondences();

      // Aliases
      const int& Ndist         = mparams.num_dvr_int_relax_dist;
      const int& N_int_relax   = mparams.num_dvr_int_relax_iters;
      const int& Nproject      = mparams.num_proj_steps;
      const int& Nsamples      = mparams.num_dvr_bd_relax_samples;
      const int& N_bd_relax    = mparams.num_dvr_bd_relax_iters;
      const double& qThreshold = mparams.min_quality;

      // check the status of the mesh
      assert(WM.getMeshStatus()==WorkingMeshStatus::POSITIVE_EDGE_TOPOLOGY_INSPECTED);
      
      // Prepare for relaxing interior nodes
      
      // # threads
      const int nthreads = 1;

      // Quality metric
      dvr::GeomTri2DQuality<WorkingMesh<MeshType>> Quality(mesh_wrapper, nthreads);

      // Max-min solver and direction generator for relaxing interior vertices
      dvr::ReconstructiveMaxMinSolver<decltype(Quality)> intSolver(Quality, nthreads);
      dvr::CartesianDirGenerator<2> CartGen;

      // Relaxation neighborhood: gather all neighbors of positive vertices
      std::vector<int> Ir{};
      {
	std::set<int> nbSet{};
	for(auto& comp:mout)
	  {
	    auto my_nbSet = mesh_wrapper.getVertexNeighborhood(comp.PosVerts, Ndist);
	    nbSet.insert(my_nbSet.begin(), my_nbSet.end());
	  }
	
	// Remove all positive vertices from the relaxation list
	for(auto& comp:mout)
	  for(auto& v:comp.PosVerts)
	    nbSet.erase(v);

	// Remove all frozen nodes from the relaxation list
	for(auto& v:frozen_nodes)
	  nbSet.erase(v);
	
	Ir.assign(nbSet.begin(), nbSet.end());
      }
      
      // i. Partial projections of positive vertices
      // ii. Relax interior vertices
      // iii. Relax positive vertices, except terminal vertices,
      //      along tangential directions (unconstrained)
      // iv. If the quality of an element falls below the threshold, mark it for refinement
      double Y[2];
      double cpt[2];
      double sd;
      double dsd[2];
      double qval = 0;
      defElmSet.clear();
	
      for(int piter=0; piter<Nproject; ++piter)
	{
	  // lambda*X + (1-lambda)*pi(X)
	  const double lambda = 1.-static_cast<double>(piter+1)/static_cast<double>(Nproject);

	  // Partial projection for each feature
	  for(int cnum=0; cnum<nFeatures; ++cnum)
	    {
	      // Aliases
	      const auto& sdfunc = gparams[cnum].signed_distance_function;
	      const auto& PosVerts = mout[cnum].PosVerts;
	      const int nPosVerts = static_cast<int>(PosVerts.size());
		  
	      // Partial projection of positive vertices
	      for(int i=0; i<nPosVerts; ++i)
		{
		  const int& v = PosVerts[i];
		  const double* X = WM.coordinates(v);
		  sdfunc(X, sd, dsd);
		  for(int k=0; k<2; ++k)
		    cpt[k] = X[k]-sd*dsd[k];
		      
		  // partial projection
		  for(int k=0; k<2; ++k)
		    Y[k] = lambda*X[k] + (1.-lambda)*cpt[k];
		      
		  // Update
		  WM.update(v, Y);

		  // Check the qualities of elements in the 1-ring of 'v'
		  const auto& oneRingElms = mesh_wrapper.Get1RingElements(v);
		  for(auto& elm_indx:oneRingElms)
		    {
		      qval = Quality.Compute(elm_indx);
		      if(qval<qThreshold)
			defElmSet.insert({cnum,elm_indx});
		    }
		}
	    }

	  // visualize projected vertices
	  if(callback!=nullptr)
	    {
	      (*callback)(WM, piter+1, 0);
	    }
	  
	  // Are there any defective elements?
	  if(defElmSet.empty()==false)
	    return {MeshingReason::Fail_ElementQuality, defElmSet, nRelaxIters, nRelaxVerts};
		
	  // Relax of neighboring vertices
	  for(int riter=0; riter<N_int_relax; ++riter)
	    {
	      // interior vertices
	      CartGen.iteration = riter;
	      dvr::Optimize(Ir, intSolver, CartGen, nullptr);
	      ++nRelaxIters;
	      nRelaxVerts += static_cast<int>(Ir.size());

	      // visualize relaxation
	      if(callback!=nullptr)
		{
		  (*callback)(WM, piter+1, riter+1);
		}
	    }
	}

      
      // Constrained relaxation of boundary vertices
      if(N_bd_relax>0)
	{
	  // Direction generator for relaxing positive vertices along the tangent
	  std::vector<std::unique_ptr<dvr::TangentialDirGenerator<2>>> tgtDirs(nFeatures);
      
	  // Closest point struct for projecting relaxed positive vertices back onto the boundary
	  std::vector<std::unique_ptr<dvr::ClosestPointStruct<2>>> cptProjectors(nFeatures);
      
	  for(int f=0; f<nFeatures; ++f)
	    {
	      tgtDirs[f] = std::make_unique<dvr::TangentialDirGenerator<2>>(gparams[f].signed_distance_function);
	      cptProjectors[f] = std::make_unique<dvr::ClosestPointStruct<2>>(gparams[f].signed_distance_function);
	    }
      
	  // Sampling-based max-min solver for relaxing positive vertices
	  dvr::SamplingMaxMinSolver<decltype(Quality)> bdSolver(Quality, Nsamples);

	  // Do not relax terminal vertices of features
	  std::vector<std::vector<int>> bdNodes(nFeatures);
	  for(int f=0; f<nFeatures; ++f)
	    {
	      bdNodes[f] = mout[f].PosVerts;
	      bdNodes[f].erase(bdNodes[f].begin());
	      bdNodes[f].pop_back();
	    }
      
	  for(int biter=0; biter<N_bd_relax; ++biter)
	    {
	      // Relax boundary vertices component-by-component
	      for(int f=0; f<nFeatures; ++f)
		dvr::Optimize(bdNodes[f], bdSolver, *tgtDirs[f], cptProjectors[f].get());

	      // Visualize boundary node relaxation
	      if(callback!=nullptr)
		{
		  (*callback)(WM, Nproject+biter+1, 0);
		}
	      
	      // Relax interior nodes	  
	      for(int riter=0; riter<2; ++riter)
		{
		  // interior vertices
		  CartGen.iteration = riter;
		  dvr::Optimize(Ir, intSolver, CartGen, nullptr);
		  ++nRelaxIters;
		  nRelaxVerts += static_cast<int>(Ir.size());

		  // Visualize interior node relaxation
		  if(callback!=nullptr)
		    {
		      (*callback)(WM, Nproject+biter+1, riter+1);
		    }
		}
	    }
	}

      // done: success
      return {MeshingReason::Success, defElmSet, nRelaxIters, nRelaxVerts};
    }
  } // algo::
} // um::


