// Sriramajayam

#include <um2_Input.h>

namespace um2 {
  
  void MedialAxisParams::validate() const {

    assert(exclusion_radius>0.);
    assert(maximum_radius>0.);
    assert(eps_radius>0.);
    assert(maximum_radius>eps_radius && maximum_radius>exclusion_radius);

    // done
    return;
  }
  
  void MeshingParams::validate() const {
    
    assert(refinement_factor>0.);
    assert(num_proj_steps>=1);
    assert(num_dvr_int_relax_iters>=1);
    assert(num_dvr_int_relax_dist>=1);
    assert(num_dvr_bd_relax_iters>=0);
    if(num_dvr_bd_relax_iters>0) {
      assert(num_dvr_bd_relax_samples>=1);
    }
    assert(min_quality>0.);
    assert(max_num_trys>=1);

    // done
    return;
  }

  void FeatureParams::validate() const {

    assert(curve_id>=0);
    assert(signed_distance_function!=nullptr);
    assert(sample!=nullptr);
    assert(split!=nullptr);

    // done
    return;
  }
    
} // namespace um2
