// Sriramajayam

#include <um2_algo_QTree_Refine.h>

namespace um2
{
  namespace algo
  {
    void RefineQuadtreeAlongFeature_Local(Quadtree& QT, const FeatureParams& FP, const SplitFeatureSet& fset, const double qfactor)
    {
      // Get samples along the feature
      auto samples = FP.sample();
      
      // loop over sample points
      // find the mesh size to be set at the sample point
      // set the mesh size
      double X[2];
      for(auto& pt:samples)
	{
	  X[0] = pt.first;
	  X[1] = pt.second;
	  const double hreq = qfactor*fset.getDistanceToMedialAxis(X, FP.curve_id);
	  QT.refine(X, hreq);
	}

      // done
      return;
    }


    void RefineQuadtreeAlongFeature_NonLocal(Quadtree& QT, const FeatureParams& FP,
					     const FeatureSet& fset, const double qfactor)
    {
      // Get samples along the feature
      auto samples = FP.sample();

      // loop over sample points
      // find the mesh size to be set at the sample point
      // set the mesh size
      double X[2];
      bool is_computed;
      double rval;
      double cpt[2];
      for(auto& pt:samples)
	{
	  X[0] = pt.first;
	  X[1] = pt.second;
	  fset.getComplementaryFeatureSize(X, FP.curve_id, is_computed, rval, cpt);
	  if(is_computed==true)
	    {
	      const double hreq = rval*qfactor;
	      QT.refine(X, hreq);
	    }
	}

      // done
      return;
    }


    void RefineQuadtreeAroundCorners(Quadtree& QT, const SplitFeatureSet& fset, const double qfactor)
    {
      // loop over terminals
      // access corner coordinates and the local mesh size to impose
      // refine the quadtree
      const int nTerminals = fset.getNumTerminals();
      for(int t=0; t<nTerminals; ++t)
	{
	  const double* X = fset.getTerminalCoordinates(t);
	  const double dist = fset.getDistanceToNearestCorner(t);
	  const double hreq = dist*qfactor;
	  QT.refine(X, hreq);
	}
      // done
      return;
    }
    
  } // namespace um2::algo
} // namespace um2
