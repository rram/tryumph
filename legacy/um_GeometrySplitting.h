// Sriramajayam

#ifndef UM_GEOMETRY_SPLITTING_H
#define UM_GEOMETRY_SPLITTING_H

#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <memory>
#include <iostream>

namespace um
{
  template<class GeomType>
    inline std::pair<void*, void*> Geometry_SplittingFunction(const int left_id, const int right_id, void* params)
  {
      assert(params!=nullptr);
      auto& geom = *static_cast<GeomType*>(params);
      return geom.Split(left_id, right_id);
  }

  //! Explicit instantiations of templated splitting functions
  const auto p1segment_splitfunc        = Geometry_SplittingFunction<LineSegment>;
  const auto p2rationalbezier_splitfunc = Geometry_SplittingFunction<QuadraticRationalBezier>;
  const auto p3bezier_splitfunc         = Geometry_SplittingFunction<CubicBezier>;
  
}

#endif
