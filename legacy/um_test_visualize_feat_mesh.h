// Sriramajayam

#pragma once

namespace um
{
  namespace test
  {
    // write a working mesh to file with feature details
    template<typename MeshType>
      void save_feature_mesh_vtk(const std::string filename,
				 const WorkingMesh<MeshType>& WM,
				 const std::vector<FeatureMeshOutput>& fm,
				 const std::vector<int>& colorMap)
      {
	assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	const int nFeatures = static_cast<int>(fm.size());
	assert(colorMap.size()==fm.size());

	std::fstream out;
	out.open(filename, std::ios::out);
	assert(out.good() && out.is_open());

	// Headers
	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
	out << "ASCII" << std::endl;
	out << "DATASET UNSTRUCTURED_GRID" << std::endl;

	// nodes
	const int nNodes = WM.n_nodes();
	out << "POINTS " << nNodes << " double" << std::endl;
	for(int i=0; i<nNodes; ++i)
	  {
	    const double* X = WM.coordinates(i);
	    out << X[0] << " " << X[1] << " " << 0. << std::endl;
	  }

	// triangles
	const int nTriangles = WM.n_elements();
	int tri_cell_size    = 4*nTriangles; // per element: nodes_element, a1, a2, a3

	// lines = positive edges
	const int nLines = nFeatures;
	int line_cell_size = 0;
	for(auto& f:fm)
	  {
	    line_cell_size += 1 + static_cast<int>(f.PosVerts.size());
	  }

	// vertices = positive vertices + terminal vertices
	const int nVertices = nFeatures + nFeatures;
	int vert_cell_size = line_cell_size + 3*nFeatures;   // #vertice, left terminal, right terminal
	

	// Write cells: triangles (incl positively cut triangles), positive edges, positive vertices, terminal vertices
	out << "CELLS " << nTriangles + nLines + nVertices << " " << tri_cell_size + line_cell_size + vert_cell_size << std::endl;
	// triangles
	for(int e=0; e<nTriangles; ++e)
	  {
	    const int* conn = WM.connectivity(e);
	    out << 3 << " " << conn[0] << " " << conn[1] << " " << conn[2] << std::endl;
	  }
	// positive edges
	for(auto& f:fm)
	  {
	    const auto& PosVerts = f.PosVerts;
	    out << static_cast<int>(PosVerts.size()) << " " ;
	    for(auto& v:PosVerts)
	      {
		out << v << " ";
	      }
	    out << std::endl;
	  }
	// positive vertices 
	for(auto& f:fm)
	  {
	    const auto& PosVerts = f.PosVerts;
	    out << static_cast<int>(PosVerts.size()) << " " ;
	    for(auto& v:PosVerts)
	      {
		out << v << " ";
	      }
	    out << std::endl;
	  }
	// terminal vertices
	for(auto& f:fm)
	  {
	    const int* tv = f.terminalVertices;
	    out << 2 << " " << tv[0] << " " << tv[1] << std::endl;
	  }
	
	out << "CELL_TYPES " << nTriangles + nLines + nVertices << std::endl;
	for(int e=0; e<nTriangles; ++e)
	  {
	    out << VTK_CELL_TYPES::TRIANGLE << std::endl;
	  }
	for(int f=0; f<nLines; ++f)
	  {
	    out << VTK_CELL_TYPES::POLY_LINE << std::endl;
	  }
	for(int f=0; f<nVertices; ++f)
	  {
	    out << VTK_CELL_TYPES::POLY_VERTEX << std::endl;
	  }

	// part ids
	std::vector<int> part_id(nTriangles + nLines + nVertices, PartID::Triangle);

	// colors assigned from the colorMap
	std::vector<int> colors(nTriangles + nLines + nVertices, -1);

	for(int i=0; i<nFeatures; ++i)
	  {
	    const auto& f = fm[i];
	    const int& color = colorMap[i];
	    
	    // pos cut triangles
	    for(auto& it:f.PosCutElmFaces)
	      {
		const int e = std::get<0>(it);
		part_id[e]  = PartID::Positively_Cut_Triangle;
		colors[e]   = color;
	      }

	    // pos cut edges
	    part_id[nTriangles+i] = PartID::Positive_Edge;
	    colors[nTriangles+i]  = color;
	    
	    // pos vertices
	    part_id[nTriangles+nLines+i] = PartID::Positive_Vertex;
	    colors[nTriangles+nLines+i]  = color;
	    
	    // terminal vertices
	    part_id[nTriangles+nLines+nFeatures+i] = PartID::Terminal_Vertex;
	    colors[nTriangles+nLines+nFeatures+i]  = color;
	  }

	// write part id
	out << "CELL_DATA " << nTriangles + nLines + nVertices << std::endl
	    << "SCALARS part_id int 1" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(auto& v:part_id)
	  {
	    out << v << std::endl;
	  }

	// colors
	out << "SCALARS color int 1" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(auto& v:colors)
	  {
	    out << v << std::endl;
	  }

	// done
	out.close();
	return;
      }
    
  } // um::test::
} // um::
