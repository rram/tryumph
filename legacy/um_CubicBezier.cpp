// Sriramajayam

#include <um_CubicBezier.h>
#include <um_Spline_RootFinding_Boost.h>
#include <um_RegisterFeatureParams.h>

namespace um
{
  // Implementation of the class CubicBezier

  // Constructor
  CubicBezier::CubicBezier(int id, std::vector<double>& pts, const int nsamples,
			   const MedialAxisParams& mparams, const NLSolverParams& nlparams)
    :CubicBezier(id, 0., 1., pts, nsamples, mparams, nlparams)
  {}

  // Constructor
  CubicBezier::CubicBezier(int id, const double tmin, const double tmax,
			   std::vector<double>& pts, const int nsamples,
			   const MedialAxisParams& mparams, const NLSolverParams& nlparams)
    :curve_id(id),
     nSamples(nsamples),
     medialAxis(id, mparams),
     NLParams(nlparams),
     tbounds{tmin,tmax}
  {
    // Sanity checks
    assert(static_cast<int>(pts.size())==8);
    assert(nsamples>0);
    assert(tmin>=0. && tmax<=1.);

    // Populate the r-tree
    SetControlPoints(pts);
  }
  
  
  // Set control points
  void CubicBezier::SetControlPoints(const std::vector<double>& control_points)
  {
    assert(static_cast<int>(control_points.size())==8);
    for(int k=0; k<4; ++k)
      {
	P0[k] = control_points[k];
	P1[k] = control_points[2+k];
	P2[k] = control_points[4+k];
	P3[k] = control_points[6+k];
      }

    // Populate the r-tree with sample points
    rtree.clear();
    double t, X[2];
    pointParam pp;
    int indx = 0;
    const double ds = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples-1);
    for(int s=0; s<nSamples; ++s)
      {
	t = tbounds[0] + static_cast<double>(s)*ds;
	for(int k=0; k<2; ++k)
	  X[k] =
	    (1.-t)*(1.-t)*(1.-t)*P0[k] +
	    3.*(1.-t)*(1.-t)*t*P1[k] +
	    3.*(1.-t)*t*t*P2[k] +
	    t*t*t*P3[k];
	pp.first = boost_point2D(X[0], X[1]);
	pp.second = std::pair<int,double>(indx++,t);
	rtree.insert(pp);
      }

    // Re-compute the medial axis
    ComputeMedialAxis();
    
    // done
    return;
  }

  // Compute the medial axis
  void CubicBezier::ComputeMedialAxis()
  {
    // Generate feature samples for computing the medial axis
    std::vector<FeatureSample> samples{};
    samples.reserve(rtree.size());
    FeatureSample fs;
    auto& fs_indx = std::get<0>(fs);
    auto& Point  = std::get<1>(fs);
    auto& Normal = std::get<2>(fs);
    double dX[2], norm;
    for(auto& it:rtree)
      {
	fs_indx = it.second.first;
	auto& t = it.second.second;
	Evaluate(t, &Point[0], dX);
	norm = std::sqrt(dX[0]*dX[0]+dX[1]*dX[1]);
	Normal[0] = -dX[1]/norm;
	Normal[1] = dX[0]/norm;
	samples.push_back(fs);
      }
    samples.shrink_to_fit();

    // compute the medial axis
    medialAxis.Compute(samples);
    
    // done
    return;
  }

  
  // Access control points
  void CubicBezier::GetControlPoints(std::vector<double>& control_points) const
  {
    control_points.resize(8);
    for(int k=0; k<2; ++k)
      {
	control_points[0+k] = P0[k];
	control_points[2+k] = P1[k];
	control_points[4+k] = P2[k];
	control_points[6+k] = P3[k];
      }
    return;
  }

  // Returns the curve id
  int CubicBezier::GetCurveID() const
  { return curve_id; }
  
  // Returns the end points of the curve
  void CubicBezier::GetTerminalPoints(double* tpoints) const
  {
    Evaluate(tbounds[0], tpoints+0, nullptr, nullptr);
    Evaluate(tbounds[1], tpoints+2, nullptr, nullptr);
    return;
  }

  // Returns the coordinates of an interior point
  void CubicBezier::GetSplittingPoint(double* spoint)
  { Evaluate(0.5*(tbounds[0]+tbounds[1]), spoint, nullptr, nullptr); }


  // Evaluate the coordinates of the spline at the requested point
  void CubicBezier::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    // Coordinates
    assert(X!=nullptr);
    
    // Evaluate

    // X
    for(int k=0; k<2; ++k)
      X[k] =
	(1.-t)*(1.-t)*(1.-t)*P0[k] +
	3.*(1.-t)*(1.-t)*t*  P1[k] +
	3.*(1.-t)*t*t*       P2[k] +
	t*t*t*               P3[k];

    // dX
    if(dX!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  dX[k] =
	    3.*(1.-t)*(1.-t)*(P1[k]-P0[k]) +
	    6.*(1.-t)*t*     (P2[k]-P1[k]) +
	    3.*t*t*          (P3[k]-P2[k]);
      }

    // d2X
    if(d2X!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  d2X[k] =
	    6.*(1.-t)*(P2[k]-2.*P1[k]+P0[k]) +
	    6.*t*     (P3[k]-2.*P2[k]+P1[k]);
      }

    // done
    return;
  }
       
  
  // Returns the signed distance function to a point
  void CubicBezier::GetSignedDistance(const double* X, double& sd, double* dsd)
  {
    // Initial guess for the closest point
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);
    const double t0 = result[0].second.second;

    // spacing between sample points
    const double dt = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples);

    // Determine the root
    const double tcpt = detail::Spline_FindRoot_Boost(X, this, t0, dt, tbounds, NLParams);
    assert(tcpt>=tbounds[0] && tcpt<=tbounds[1]);
    
    // Evaluate the spline and its derivatives here.
    // Y is the closest point projection
    double Y[2], dY[2];
    Evaluate(tcpt, Y, dY, nullptr);
    double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
    assert(norm>NLParams.normTol); 
    double nvec[] = {-dY[1]/norm, dY[0]/norm};
    
    // signed distance
    sd = (X[0]-Y[0])*nvec[0] + (X[1]-Y[1])*nvec[1];
    
    // Gradient
    if(dsd!=nullptr)
      { dsd[0] = nvec[0];
	dsd[1] = nvec[1]; }

    // done
    return;
  }
  
    
    
  // Return sampling of the feature
  void CubicBezier::GetSampling(std::vector<std::array<double,2>>& samples) const
  {
    samples.reserve(rtree.size());
    for(auto& it:rtree)
      {
	auto& X = it.first;
	samples.push_back( std::array<double,2>{X.get<0>(), X.get<1>()} );
      }
    samples.shrink_to_fit();
    return;
  }

  // Returns the range of parameters
  std::pair<double,double> CubicBezier::GetParameterBounds() const
  { return std::make_pair(tbounds[0],tbounds[1]); }

  
  // print
  std::ostream& operator << (std::ostream &out, const CubicBezier& geom)
  {
    const int& nSamples = geom.nSamples;
    auto tbounds = geom.GetParameterBounds();
    const double frac = (tbounds.second-tbounds.first)/static_cast<double>(nSamples-1);
    double tval, X[2];
    for(int i=0; i<nSamples; ++i)
      {
	tval = tbounds.first + static_cast<double>(i)*frac;
	geom.Evaluate(tval, X);
	out << X[0] <<",\t" << X[1] <<"\n";
      }
    return out;
  }

  
  // split
  std::pair<FeatureParams, FeatureParams> CubicBezier::Split(const int left_id, const int right_id)
  {
    std::vector<double> control_pts(8);
    GetControlPoints(control_pts);

    // left
    left_split = std::make_unique<CubicBezier>(left_id, tbounds[0], 0.5*(tbounds[0]+tbounds[1]), control_pts, nSamples, medialAxis.GetMedialAxisParams(), NLParams);

    // right
    right_split = std::make_unique<CubicBezier>(right_id, 0.5*(tbounds[0]+tbounds[1]), tbounds[1], control_pts, nSamples,   medialAxis.GetMedialAxisParams(), NLParams);

     // populate feature parameters
    std::pair<FeatureParams, FeatureParams> LRparams;
    left_split->RegisterFeature(LRparams.first);
    right_split->RegisterFeature(LRparams.second);
    
    return LRparams;
  }
  

  double CubicBezier::GetFeatureSize(const double* X)
  {
    double in_dist, out_dist;
    double in_X[2], out_X[2];
    medialAxis.GetDistanceToMedialAxis(X, out_dist, out_X, in_dist, in_X);
    if(out_dist<in_dist) return out_dist;
    else return in_dist;
  }

  // Register feature params
  void CubicBezier::RegisterFeature(FeatureParams& params)
  {
    RegisterFeatureParams(*this, params);
    return;
  }

  std::pair<CubicBezier&, CubicBezier&> CubicBezier::GetSplits()
  {
    assert(left_split!=nullptr && right_split!=nullptr);
    return {*left_split, *right_split}; 
  }
  
} // um::  
