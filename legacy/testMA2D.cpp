// Sriramajayam

#include <um_MedialAxisSampler.h>
#include <fstream>
#include <vector>
#include <cassert>
#include <iostream>
#include <cmath>

using namespace um;
using namespace um::utils;

int main()
{
  // Generate sample points on an ellipse
  const double A = 10.;
  const double B = 5.;
  const double nSamples = 50;
  std::vector<MedialAxisData<2>> mData(nSamples);
  for(int n=0; n<nSamples; ++n)
    {
      const double theta = 2.*M_PI*static_cast<double>(n)/static_cast<double>(nSamples);
      double* Pt = mData[n].Point;
      Pt[0] = A*std::cos(theta);
      Pt[1] = B*std::sin(theta);
      double* N = mData[n].Normal;
      N[0] = B*std::cos(theta)/std::sqrt(A*A+B*B);
      N[1] = A*std::sin(theta)/std::sqrt(A*A+B*B);
    }

  // Print samples to file
  std::fstream pfile;
  pfile.open((char*)"samples.dat", std::ios::out);
  for(auto& m:mData)
    pfile<<m.Point[0]<<" "<<m.Point[1]<<"\n";
  pfile.close();

  // Compute the medial axis transform
  MedialAxisSampler mas(nSamples, &mData[0], 10., 1.e-5);
  
  // Plot the medial ball centers
  pfile.open((char*)"inCenters.dat", std::ios::out);
  for(auto& m:mData)
    pfile<<m.inCenter[0]<<" "<<m.inCenter[1]<<"\n";
  pfile.close();

  pfile.open((char*)"outCenters.dat", std::ios::out);
  for(auto& m:mData)
    pfile<<m.outCenter[0]<<" "<<m.outCenter[1]<<"\n";
  pfile.close();
}
