// Sriramajayam

#ifndef UM_MEDIAL_AXIS_SAMPLES_H
#define UM_MEDIAL_AXIS_SAMPLES_H

#include <utility>
#include <vector>
#include <string>

namespace um
{
  struct MedialBall
  {
    double center[2]; //!< Center of the medial ball
    double radius; //!< Radius of the medial ball
    std::pair<int,int> limiting_point; //!< curve id and index of point determining the medial ball
  };
  
  //! Struct to encapsulate medial axis transform data
  struct MedialAxisSamples
    {
      double Point[2]; //!< Coordinates of the sample point
      double Normal[2]; //!< Unit outward pointing normal 
      std::pair<int,int> id_and_indx; //!< Pairing of curve id and point index

      // Feature-level calculation
      bool is_feature_axis_computed; //!< Whether medial ball has been computed for this point
      MedialBall feature_inner; //!< Computed medial ball centered along -N
      MedialBall feature_outer; //!< Computed medial ball centered along +N

      // Global-level calculation
      bool is_global_axis_computed;  //!< Whether medial ball has been computed for this point
      MedialBall global_inner;  //!< Computed medial ball centered along -N
      MedialBall global_outer; //!< Computed medial ball centered along +N

      //! Print sample points for a specific curve
      static void PrintSamples(const int curve_id,
			       const std::string filename, 
			       const std::vector<MedialAxisSamples>& dataVec);

      //! Print sample points for a specific curve
      static void PrintSamples(const std::string filename, 
			       const std::vector<MedialAxisSamples>& dataVec);
      
      //! Print the medial axis data for a specific curve
      static void PrintFeatureAxes(const int curve_id,
				   const std::string filename, 
				   const std::vector<MedialAxisSamples>& dataVec);
      
      //! Print all medial axis data for all features
      static void PrintFeatureAxes(const std::string filename, 
				   const std::vector<MedialAxisSamples>& dataVec);
      
      //! Print global medial axis data
      static void PrintGlobalAxes(const std::string filename,
				  const std::vector<MedialAxisSamples>& dataVec);
			
    };

  using MedialAxisSamplesVec = std::vector<MedialAxisSamples>;
  
}

#endif
