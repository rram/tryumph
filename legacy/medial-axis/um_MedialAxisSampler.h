// Sriramajayam

#ifndef UM_UTILS_MEDIAL_AXIS_SAMPLER_H
#define UM_UTILS_MEDIAL_AXIS_SAMPLER_H

#include <um_MedialAxisSamples.h>
#include <um_Utils.h>

namespace um
{
  struct MedialAxisParams
  {
    double exclusion_radius;
    double maximum_radius;
    double eps_radius;
  };
  
  class MedialAxisSampler
  {
  public:
    /** Constructor
     * Computes an approximate medial axis transform using the
     * skrinking ball algorithm
     * Templated by the spatial dimension
     * \param[in] nSamples Number of sample points
     * \param[in,out] mArray Medial axis data
     * \param[in] exclusion_radius Account only for sampling points separated by this radius
     * \param[in] Rmax Maximum medial ball radius to consider
     * \param[in] tol Non dimensional tolerance for convergence of medial axis balls
     */
    MedialAxisSampler(MedialAxisSamplesVec& mArray,
		      const double exclusion_radius,
		      const double Rmax, const double tol);

    //! Delegating constructor
    MedialAxisSampler(MedialAxisSamplesVec& mArray, MedialAxisParams& mparams)
      :MedialAxisSampler(mArray, mparams.exclusion_radius, mparams.maximum_radius, mparams.eps_radius) {}
												     
    //! Destructor
    inline ~MedialAxisSampler() {}

    //! Disable copy and assignment
    MedialAxisSampler(const MedialAxisSampler&) = delete;
    MedialAxisSampler& operator=(const MedialAxisSampler&) = delete;

    //! Get the distance to sample points of a given curve
    void GetDistance(const int curve_id, const double* pt,
		     double& dist, double* cpt) const;
    
    //! Get the distance to the medial axes of a curve
    void GetDistanceToMedialAxis(const int curve_id,
				 const double* pt,
				 double& out_dist, double* out_X,
				 double& in_dist, double* in_X) const;

    //! Access all sample points from the sample tree of a vertex that lie within
    //! a box with specified size
    void GetSamples(const int curve_id, const double* pt, const double size,
		    std::vector<std::pair<boost_point2D, int>>& query_result) const;
    
    
  private:
    //! Helper function to create sample trees
    void CreateSampleTrees(MedialAxisSamplesVec& mArray,
			   const double exclusion_radius);

    // Compute the medial axis transform for points in an r-tree
    enum class RTreeType{FEATURE, GLOBAL};
    static void ComputeMedialAxisTransform(const RTreeType rtree_type,
					   const boost_pointID_rtree2D& rtree,
					   MedialAxisSamplesVec& mArray,
					   const double Rmax,
					   const double EPS);
    
    static constexpr int SPD = 2;
    std::map<int, boost_pointID_rtree2D> feature_sampleTrees; //!< Trees with sample points of each feature, indexed by curve-id
    boost_pointID_rtree2D global_sampleTree; //!< Tree with all sample points
    std::map<int, std::pair<boost_point_rtree2D, boost_point_rtree2D>> feature_axesTrees; //!< Local tree of inner and outer medial axes points, indexed by curve ID.
  };

} // um::

#endif
