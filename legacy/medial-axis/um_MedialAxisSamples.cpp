// Sriramajayam

#include <um_MedialAxisSamples.h>
#include <um_Utils.h>
#include <fstream>

namespace um
{
  // Print sample points for a specific curve
  void MedialAxisSamples::PrintSamples(const int curve_id,
				       const std::string filename, 
				       const std::vector<MedialAxisSamples>& dataVec)
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());
    pfile<<"curve_id, X, Y";
    for(auto& data:dataVec)
      if(data.id_and_indx.first==curve_id)
	{
	  pfile<<"\n"<<data.id_and_indx.first<<", "
	       <<data.Point[0]<<", "<<data.Point[1];
	  pfile.flush();
	}
    pfile.close();
    return;
  }


  // Print sample points for a specific curve
  void MedialAxisSamples::PrintSamples(const std::string filename, 
				       const std::vector<MedialAxisSamples>& dataVec)
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());
    pfile<<"X, Y";
    for(auto& data:dataVec)
      {
	pfile<<"\n"<<data.Point[0]<<", "<<data.Point[1];
	pfile.flush();
      }
    pfile.close();
    return;
  }

  
  // Print the medial axis data for a specific curve
  void MedialAxisSamples::PrintFeatureAxes(const int curve_id,
					   const std::string filename, 
					   const std::vector<MedialAxisSamples>& dataVec)
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());
    pfile<<"curve_id, X, Y, inner.centerX, inner.centerY, outer.centerX, outer.centerY";
    for(auto& data:dataVec)
      if(data.is_feature_axis_computed)
	if(data.id_and_indx.first==curve_id)
	  {
	    pfile<<"\n"<<data.id_and_indx.first<<", "
		 <<data.Point[0]<<", "<<data.Point[1]<<", "
		 <<data.feature_inner.center[0]<<", "<<data.feature_inner.center[1]<<", "
		 <<data.feature_outer.center[0]<<", "<<data.feature_outer.center[1];
	    pfile.flush();
	  }
    pfile.close();
    return;
  }
      
  // Print all medial axis data for all features
  void MedialAxisSamples::PrintFeatureAxes(const std::string filename, 
					   const std::vector<MedialAxisSamples>& dataVec)
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());
    pfile<<"curve_id, X, Y, inner.centerX, inner.centerY, outer.centerX, outer.centerY";
    for(auto& data:dataVec)
      if(data.is_feature_axis_computed)
	{
	  pfile<<"\n"<<data.id_and_indx.first<<", "
	       <<data.Point[0]<<", "<<data.Point[1]<<", "
	       <<data.feature_inner.center[0]<<", "<<data.feature_inner.center[1]<<", "
	       <<data.feature_outer.center[0]<<", "<<data.feature_outer.center[1];
	  pfile.flush();
	}
    pfile.close();
    return;
  }
      
  // Print global medial axis data
  void MedialAxisSamples::PrintGlobalAxes(const std::string filename,
					  const std::vector<MedialAxisSamples>& dataVec)
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());
    pfile<<"curve_id, X, Y, inner.centerX, inner.centerY, outer.centerX, outer.centerY";
    for(auto& data:dataVec)
      if(data.is_global_axis_computed)
	{
	  pfile<<"\n"<<data.id_and_indx.first<<", "
	       <<data.Point[0]<<", "<<data.Point[1]<<", "
	       <<data.global_inner.center[0]<<", "<<data.global_inner.center[1]<<", "
	       <<data.global_outer.center[0]<<", "<<data.global_outer.center[1];
	  pfile.flush();
	}
    pfile.close();
    return;
  }
  
} // um::
