// Sriramajayam

#include <um_MedialAxisSampler.h>

namespace um
{
  // Constructor
  MedialAxisSampler::
  MedialAxisSampler(MedialAxisSamplesVec& mArray,
		    const double exclusion_radius, const double Rmax, const double tol)
    :feature_sampleTrees{}, feature_axesTrees{}
  {
    // Unset flag for axis computation
    const int nSamples = static_cast<int>(mArray.size());
    for(int i=0; i<nSamples; ++i)
      { mArray[i].is_feature_axis_computed = false;
	mArray[i].is_global_axis_computed = false; }

    // Create r-trees of sample points for each feature
    CreateSampleTrees(mArray, exclusion_radius);

    // feature_sampleTrees and global_sampleTree are fully populated

    // Compute the medial axes for each curve
    for(auto& it:feature_sampleTrees)
      ComputeMedialAxisTransform(RTreeType::FEATURE, it.second, mArray, Rmax, tol*exclusion_radius);

    // Compute the medial axes at the global level
    if(static_cast<int>(feature_sampleTrees.size())>1)
      ComputeMedialAxisTransform(RTreeType::GLOBAL, global_sampleTree, mArray, Rmax, tol*exclusion_radius);

    // mArray has been populated with centers and radii of medial balls
    // populate medial axis trees with ball centers
    for(auto& it:feature_sampleTrees)
      feature_axesTrees[it.first] = std::make_pair(boost_point_rtree2D(), boost_point_rtree2D());
    
    boost_point2D pt;
    for(int n=0; n<nSamples; ++n)
      if(mArray[n].is_feature_axis_computed)
	{
	  // Curve id for this point
	  const auto& mdata = mArray[n];
	  const int& curveID = mdata.id_and_indx.first;

	  // Access the axes-tree for this point
	  auto it = feature_axesTrees.find(curveID);
	  assert(it!=feature_axesTrees.end());
	  auto& curve_axes_Trees = it->second;

	  // Populate the tree of outer medial ball centers
	  pt = boost_point2D(mdata.feature_outer.center[0], mdata.feature_outer.center[1]);
	  curve_axes_Trees.first.insert(pt);

	  // Populate the tree of inner medial ball centers
	  pt = boost_point2D(mdata.feature_inner.center[0], mdata.feature_inner.center[1]);
	  curve_axes_Trees.second.insert(pt);
	}
    
    // done
  }

  
  // Get the distance to sample points of a curve
  void MedialAxisSampler::GetDistance(const int curve_id, const double* pt,
				      double& dist, double* cpt) const
  {
    auto it = feature_sampleTrees.find(curve_id);
    assert(it!=feature_sampleTrees.end());
    const auto& rtree = it->second;
    std::vector<std::pair<boost_point2D, int>> query_result{};
    boost_point2D bpt(pt[0], pt[1]);
    rtree.query(boost::geometry::index::nearest(bpt, 1), std::back_inserter(query_result));
    assert(static_cast<int>(query_result.size())>0);
    cpt[0] = query_result[0].first.get<0>();
    cpt[1] = query_result[0].first.get<1>();
    dist = std::sqrt((pt[0]-cpt[0])*(pt[0]-cpt[0]) + (pt[1]-cpt[1])*(pt[1]-cpt[1]));
    return;
  }

  // Get the distance to the medial axes of a curve
  void MedialAxisSampler::GetDistanceToMedialAxis(const int curve_id,
						  const double* pt,
						  double& out_dist, double* out_X,
						  double& in_dist, double* in_X) const
  {
    auto it = feature_axesTrees.find(curve_id);
    assert(it!=feature_axesTrees.end());
    auto& lTrees = it->second;
    auto& out_rtree = lTrees.first;
    auto& in_rtree = lTrees.second;

    boost_point2D bp(pt[0], pt[1]);
    std::vector<boost_point2D> out_result{}, in_result{};
    out_rtree.query(boost::geometry::index::nearest(bp, 1), std::back_inserter(out_result));
    in_rtree.query(boost::geometry::index::nearest(bp, 1), std::back_inserter(in_result));
    assert(static_cast<int>(out_result.size())==1 && static_cast<int>(in_result.size())==1);

    out_X[0] = out_result[0].get<0>(); out_X[1] = out_result[0].get<1>();
    out_dist = boost::geometry::distance(bp, out_result[0]);
    
    in_X[0]  = in_result[0].get<0>();  in_X[1]  = in_result[0].get<1>();
    in_dist = boost::geometry::distance(bp, in_result[0]);

    // done
    return;
  }


  // Access all sample points from the sample tree of a vertex that lie within
  // a box with specified size
  void MedialAxisSampler::GetSamples(const int curve_id, const double* pt, const double size,
				     std::vector<std::pair<boost_point2D, int>>& query_result) const
  {
    query_result.clear();
    boost_point2D mincnr(pt[0]-size, pt[1]-size);
    boost_point2D maxcnr(pt[0]+size, pt[1]+size);
    boost_box2D query_box(mincnr, maxcnr);
    auto it = feature_sampleTrees.find(curve_id);
    assert(it!=feature_sampleTrees.end());
    it->second.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));
    return;
  }

  
  // Helper function to create sample trees
  void MedialAxisSampler::CreateSampleTrees(MedialAxisSamplesVec& mArray,
					    const double exclusion_radius)
  {
    // Populate sample-rtrees with samples
    for(auto& it:feature_sampleTrees)
      it.second.clear();
    global_sampleTree.clear();

    // Make a list of all curve IDs represented in mArray
    std::set<int> curveIDs{};
    for(auto& m:mArray)
      curveIDs.insert(m.id_and_indx.first);
    const int nCurves = static_cast<int>(curveIDs.size());
    
    // Initialize all feature-level sample trees to be empty
    for(auto& id:curveIDs)
      feature_sampleTrees.insert( std::make_pair(id, boost_pointID_rtree2D()) );

    // temporaries
    std::pair<boost_point2D, int> pid;
    boost_point2D minCorner, maxCorner;
    const int nSamples = static_cast<int>(mArray.size());
    for(int n=0; n<nSamples; ++n)
      {
	// This point
	auto& mdata = mArray[n];
	const double* X = mdata.Point;

	// Exclusion zone around this point
	minCorner = boost_point2D(X[0]-exclusion_radius, X[1]-exclusion_radius);
	maxCorner = boost_point2D(X[0]+exclusion_radius, X[1]+exclusion_radius);
	boost_box2D query_box(minCorner, maxCorner);

	// Access r-tree for this feature
	const int& curve_id = mdata.id_and_indx.first; // Curve ID for this sample point
	auto it = feature_sampleTrees.find(curve_id);
	assert(it!=feature_sampleTrees.end());
	auto& rtree = it->second;
	
	// Is this point well separated from samples already in this tree?
	std::vector<std::pair<boost_point2D,int>> query_result{};
	rtree.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));
	if(query_result.empty()) 	// If the box is empty, add this point
	  { pid.first = boost_point2D(X[0], X[1]);
	    pid.second = n;
	    rtree.insert(pid); 
	    mdata.is_feature_axis_computed = true; }
	else
	  mdata.is_feature_axis_computed = false;
	
	// Global tree
	if(nCurves==1) // Do not repeat the feature-tree calculation
	  mdata.is_global_axis_computed = false;
	else
	  {
	    // Is this point well separated from points already in the global tree?
	    query_result.clear();
	    global_sampleTree.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));
	    if(query_result.empty()) 	// If the box is empty, add this point
	      { pid.first = boost_point2D(X[0], X[1]);
		pid.second = n;
		global_sampleTree.insert(pid);
		mdata.is_global_axis_computed = true; }
	    else
	      mdata.is_global_axis_computed = false;
	  }
      }

    // feature-wise and global r-trees have been populated
    return;
  }



  // Compute the medial axis transform for points in an r-tree
  void MedialAxisSampler::
  ComputeMedialAxisTransform(const RTreeType rtree_type,
			     const boost_pointID_rtree2D& rtree,
			     MedialAxisSamplesVec& mArray,
			     const double Rmax,
			     const double EPS)
  {
    // Temporaries
    double dot = 0.;
    double normPQ = 0.;
    const double signs[] = {1., -1.}; // outward, inward orientations
    double new_rad;
    boost_point2D center;
    double Q[2];

    // Compute the medial axis for points in this tree
    for(auto it=rtree.begin(); it!=rtree.end(); ++it)
      {
	// Index in mArray of this point
	const auto& n = it->second;
	auto& mdata = mArray[n];
	    
	// This point
	const double* P = mdata.Point;
	const double* N = mdata.Normal;
	double rads[] = {Rmax, Rmax};
	int limiting_indx[] = {-1,-1};
	    
	// Outer & inner radii
	for(int orientation=0; orientation<2; ++orientation)
	  {
	    double& rad = rads[orientation];
	    const double& sign = signs[orientation];
		
	    while(true)
	      {
		// Guess for the center
		for(int k=0; k<2; ++k)
		  Q[k] = P[k]+sign*rad*N[k];
		center = boost_point2D(Q[0], Q[1]);
		    
		// Find the closest sample point to the center
		std::vector<std::pair<boost_point2D,int>> query_result{};
		rtree.query(boost::geometry::index::nearest(center, 1), std::back_inserter(query_result));
		assert(static_cast<int>(query_result.size())==1);
		    
		// Closest point sample: Q
		auto& qid = query_result[0];
		limiting_indx[orientation] = qid.second;
		    
		// If Q = P, stop.
		if(qid.second==n) break;
		    
		// Compute new radius
		Q[0] = qid.first.get<0>();
		Q[1] = qid.first.get<1>();
		normPQ = 0.;
		dot = 0.;
		for(int k=0; k<2; ++k)
		  { normPQ += (P[k]-Q[k])*(P[k]-Q[k]);
		    dot += N[k]*(P[k]-Q[k]); }
		normPQ = std::sqrt(normPQ);
		assert(normPQ>EPS);
		dot /= normPQ;
		assert(std::abs(dot)>EPS);
		new_rad = -0.5*sign*normPQ/dot;
		    
		// radius should be monotonically decreasing
		assert(new_rad<rad+EPS);
		    
		// converged?
		if(std::abs(new_rad-rad)<EPS)
		  break;
		    
		// otherwise, continue
		rad = new_rad;
	      }
	  }
	    
	// Assign the inner and outer medial ball center
	if(rtree_type==RTreeType::FEATURE)
	  {
	    mdata.feature_outer.radius = rads[0];
	    mdata.feature_inner.radius = rads[1];
	    for(int k=0; k<2; ++k)
	      { mdata.feature_inner.center[k] = P[k]-mdata.feature_inner.radius*N[k];
		mdata.feature_outer.center[k] = P[k]+mdata.feature_outer.radius*N[k]; }
	    mdata.feature_outer.limiting_point = mArray[limiting_indx[0]].id_and_indx;
	    mdata.feature_inner.limiting_point = mArray[limiting_indx[1]].id_and_indx;
	  }
	else
	  {
	    mdata.global_outer.radius = rads[0];
	    mdata.global_inner.radius = rads[1];
	    for(int k=0; k<2; ++k)
	      { mdata.global_inner.center[k] = P[k]-mdata.global_inner.radius*N[k];
		mdata.global_outer.center[k] = P[k]+mdata.global_outer.radius*N[k]; }
	    mdata.global_outer.limiting_point = mArray[limiting_indx[0]].id_and_indx;
	    mdata.global_inner.limiting_point = mArray[limiting_indx[1]].id_and_indx;
	  }
	// Proceed to the next point
      }
      
    // done
    return;
  }
    
} // um::

