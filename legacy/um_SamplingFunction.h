// Sriramajayam

#ifndef UM_SAMPLE_FUNCTION_H
#define UM_SAMPLE_FUNCTION_H

#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_CubicSpline.h>

namespace um
{
  //! Sample the geometry
  template<class GeomType>
    inline void Geometry_SamplingFunction(std::vector<std::array<double,2>>& samples, void* params)
    {
      assert(params!=nullptr);
      auto& geom = *static_cast<const GeomType*>(params);

      samples.clear();
      geom.GetSampling(samples);
      return;
    }

  //! Explicit instantiations of templated feature size functions;
  const auto p1segment_samplingfunc        = Geometry_SamplingFunction<LineSegment>;
  const auto p2rationalbezier_samplingfunc = Geometry_SamplingFunction<QuadraticRationalBezier>;
  const auto p3bezier_samplingfunc         = Geometry_SamplingFunction<CubicBezier>;
  const auto p3spline_samplingfunc         = Geometry_SamplingFunction<CubicSpline>;

}

#endif
