// Sriramajayam

#ifndef UM_ALGORITHM_FRONTAL_PROJECT_AND_RELAX_ADAPTIVE_H
#define UM_ALGORITHM_FRONTAL_PROJECT_AND_RELAX_ADAPTIVE_H

#include <um_Algorithm_Structs.h>
#include <um_Algorithm_MeshingReason.h>
#include <um_Algorithm_VertexNeighborhood.h>
#include <um_FeatureSet.h>
#include <dvr_Module>
#include <memory>

namespace um
{
  namespace algo
  {
    struct PosVertCoords
    {
      int compNum;   // component #
      double X[2];   // coordinates from the last successful iteration during frontal projection
      double cpt[2]; // closest point projection
    };
    
    // Helper function to project positive vertices in a front-wise manner and relax nearby regions
    //TODO: Remove vertices which should not be disturbed from relaxation list
    template<typename WMMeshType>
      MeshingReason FrontalProjectAndRelax_Adaptive(WMMeshType& WM,
						    const MeshingParams& mparams,
						    std::set<std::pair<int,int>>& defElmSet,
						    const std::vector<FeatureParams>& gparams,
						    const std::vector<FeatureMeshOutput>& mout,
						    int& nRelaxIters, int& nRelaxVerts)
    {
      const auto& BG = WM.GetParentMesh();
      const int nFeatures = static_cast<int>(gparams.size());

      // Aliaxes
      const int& Ndist = mparams.Ndist;
      const int& Nrelax = mparams.Nrelax;
      const int& Nproject = mparams.Nproject;
      const int& Nsamples = mparams.Nsamples;
      const double& qThreshold = mparams.quality;
      //const auto& dndlist = mparams.dnd_vertices;

      // Pre-compute closest point projections of positive vertices
      std::map<int, PosVertCoords> pvCoordsMap{};
      bool onFeature;
      double sd;
      double dsd[2];
      PosVertCoords pvCoords;

      // Relaxation neighborhood: gather all neighbors of positive vertices
      std::set<int> nbSet{};
      
      for(int cnum=0; cnum<nFeatures; ++cnum)
	{
	  pvCoords.compNum = cnum;
	  const auto& sdfunc = gparams[cnum].sdfunc;
	  void* funcparams = gparams[cnum].funcparams;
	  const auto& PosVerts = mout[cnum].PosVerts;
	  const int nPosVerts = static_cast<int>(PosVerts.size());

	  // closest point projections
	  for(auto& vert:PosVerts)
	    {
	      const double* X = WM.coordinates(vert);
	      sdfunc(X, onFeature, sd, dsd, funcparams);
	      for(int k=0; k<2; ++k)
		{
		  pvCoords.X[k] = X[k];
		  pvCoords.cpt[k] = X[k]-sd*dsd[k];
		}
	      pvCoordsMap.insert(std::make_pair(vert, pvCoords));
	    }

	  // relaxation neighborhood
	  GetVertexNeighborhood(BG, PosVerts, Ndist, nbSet);
	}
      
      // Remove all positive vertices from the relaxation list
      for(auto& comp:mout)
	for(auto& v:comp.PosVerts)
	  nbSet.erase(v);
      
      // HACK HACK: TODO Remove vertices which should not be disturbed from relaxation list
      //for(auto& v:dndlist)
      //nbSet.erase(v);

      // convert relaxation set -> vector
      std::vector<int> Ir(nbSet.begin(), nbSet.end());
      
      // Prepare for DVR on interior vertices
      const int nThreads = 1;
      dvr::GeomTri2DQuality<decltype(WM)> Quality(WM, nThreads);
      dvr::ReconstructiveMaxMinSolver<decltype(Quality)> intSolver(Quality, nThreads);
      dvr::CartesianDirGenerator<2> CartGen;
      
      // Attempt to relax in a single projection step towards the closest point projection
      // If not successful, decrease the projection fraction and retry
      bool isLambdaOne = true;
      double lambda = 1.;
      double Y[2];
      const int* oneRingElms;
      int n1RingElms;
      double qval;
      int nProjSteps = 0;
      std::set<std::pair<int,int>> prev_defectiveSites{}, curr_defectiveSites{}; // mark locations for refinement

      while(nProjSteps<Nproject)
	{
	  ++nProjSteps;
	  prev_defectiveSites = curr_defectiveSites;
	  curr_defectiveSites.clear();
	  
	  // Attempt to project positive vertices by current fraction lambda
	  bool projection_step_success = true;
	  for(auto& it:pvCoordsMap)
	    {
	      const int& vert = it.first;
	      const double* X = it.second.X;
	      const double* cpt = it.second.cpt;
	      for(int k=0; k<2; ++k)
		Y[k] = lambda*cpt[k] + (1.-lambda)*X[k];
	      WM.update(vert, Y);
	    }
	    
	  // Examine element qualities post projection
	  for(auto& it:pvCoordsMap)
	    {
	      const int& vert = it.first;
	      BG.Get1RingElements(vert, &oneRingElms, n1RingElms);
	      for(int eindx=0; eindx<n1RingElms; ++eindx)
		{
		  qval = Quality.Compute(oneRingElms[eindx]);
		  if(qval<qThreshold)
		    {
		      curr_defectiveSites.insert(std::make_pair(it.second.compNum, oneRingElms[eindx]));
		      projection_step_success = false;
		    }
		}
	    }
	  
	  // If the projection was unsuccessful:
	  // (i) undo vertex updates just performed
	  // (ii) reduce the projection fraction for the next iteration
	  if(projection_step_success==false)
	    {
	      // (i) 
	      for(auto& it:pvCoordsMap)
		WM.update(it.first, it.second.X);

	      // (ii) 
	      lambda /= 2.;
	      isLambdaOne = false;
	    }
	  else // projection step was successful
	    {
	      // Projection was successful:
	      // (i) Relax vertices in Ir along (Ex,Ey), i.e., 2 dvr iterations
	      // (ii) Check whether projections are completed- break the loop
	      // (iii) Update current vertex locations of positive vertices
	      // (iv) If the number of projection steps is exceeded, break
	      // (v) Reset lambda = 1 for the next projection attempt
	      	      
	      // (i)
	      for(int riter=0; riter<2; ++riter)
		{
		  CartGen.iteration = riter;
		  dvr::Optimize(Ir, WM, intSolver, CartGen, nullptr);
		  ++nRelaxIters;
		  nRelaxVerts += static_cast<int>(Ir.size());
		}

	      // (ii)
	      if(isLambdaOne==true)
		break;
	      
	      // (iii)
	      for(auto& it:pvCoordsMap)
		{
		  const double* X = WM.coordinates(it.first);
		  it.second.X[0] = X[0];
		  it.second.X[1] = X[1];
		}

	      // (iv)
	      if(nProjSteps==Nproject)
		break;
	      
	      // (v)
	      isLambdaOne = true;
	      lambda = 1.;
	    }
	} // end while

      // If the projection was not complete
      // (i) Copy the locations where refinement is required.
      //     It is possible that the current set of defective sites is empty.
      //     This happens if in the last step, lambda<1 executed successfully
      //     No defect was found but the projection count exceeded the max limit.
      //     Use the defects found in the previous projection step as a proxy
      // (ii) return a failure
      if(nProjSteps>=Nproject && isLambdaOne==false)
	{ 
	  // (i)
	  assert(!curr_defectiveSites.empty() || !prev_defectiveSites.empty());
	  if(curr_defectiveSites.empty()==false)
	    for(auto& it:curr_defectiveSites)
	      defElmSet.insert(it);
	  else
	    for(auto& it:prev_defectiveSites)
	      defElmSet.insert(it);
	  
	  // (ii)
	  return MeshingReason::Fail_ElementQuality;
	}

      // projections completed
      assert(std::abs(lambda-1.)<1.e-4); 
      
      // proceed to constrained relaxation of boundary vertices
      std::vector<std::unique_ptr<dvr::TangentialDirGenerator<2>>> tgtDirs(nFeatures);
      std::vector<std::unique_ptr<dvr::ClosestPointStruct<2>>> cptProjectors(nFeatures);
      for(int f=0; f<nFeatures; ++f)
	{
	  tgtDirs[f] = std::make_unique<dvr::TangentialDirGenerator<2>>(gparams[f].sdfunc, gparams[f].funcparams);
	  cptProjectors[f] = std::make_unique<dvr::ClosestPointStruct<2>>(gparams[f].sdfunc, gparams[f].funcparams);
	}
      dvr::SamplingMaxMinSolver<decltype(Quality)> bdSolver(Quality, Nsamples);
      
      // Do not relax terminal vertices of features
      std::vector<std::vector<int>> bdNodes(nFeatures);
      for(int f=0; f<nFeatures; ++f)
	{
	  bdNodes[f] = mout[f].PosVerts;
	  bdNodes[f].erase(bdNodes[f].begin());
	  bdNodes[f].pop_back();
	}

      for(int biter=0; biter<Nrelax; ++biter)
	{
	  // Relax boundary vertices component-by-component
	  for(int f=0; f<nFeatures; ++f)
	    dvr::Optimize(bdNodes[f], WM, bdSolver, *tgtDirs[f], cptProjectors[f].get());
	  
	  // Relax interior nodes along Ex/Ey	  
	  for(int riter=0; riter<2; ++riter)
	    {
	      // interior vertices
	      CartGen.iteration = riter;
	      dvr::Optimize(Ir, WM, intSolver, CartGen, nullptr);
	      ++nRelaxIters;
	      nRelaxVerts += static_cast<int>(Ir.size());
	    }
	}

      // done: success
      return MeshingReason::Success;
    }

  } // um::algo::
} // um::

#endif
