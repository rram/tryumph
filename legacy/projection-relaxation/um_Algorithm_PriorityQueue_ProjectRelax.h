// Sriramajayam

#ifndef UM_ALGORITHM_PRIORITY_QUEUE_PROJECT_AND_RELAX_H
#define UM_ALGORITHM_PRIORITY_QUEUE_PROJECT_AND_RELAX_H

#include <queue>
#include <um_Algorithm_MeshingReason.h>
#include <um_Algorithm_Structs.h>
#include <um_Algorithm_VertexNeighborhood.h>
#include <um_FeatureSet.h>
#include <dvr_Module>

namespace um
{
  namespace algo
  {
    struct PosVertStruct
    {
      int vert;       //  vertex number 
      int compNum;    // component number
      double cpt[2];  // closest point projection
      double sdval;   // signed distance value
      int nProjIters; // # projection iterations
      std::vector<int> Ir; // relaxation neighborhood

    PosVertStruct()
    :vert(-1),
	compNum(-1),
	sdval(0),
	nProjIters(0),
	Ir({})
      {}
    };
    

    // Comaprator for PosVertStruct: arrange in descending order
    auto PosVertStruct_Compare = [](const PosVertStruct& A, const PosVertStruct& B)
      { return A.sdval<B.sdval; };
    
    
    // Helper function to project positive vertices and relax nearby regions
    // HACK TODO: Remove vertices which cannot be disturbed from relaxation list
    template<typename WMMeshType>
      MeshingReason PriorityQueue_ProjectAndRelax(WMMeshType& WM,
						  const MeshingParams& mparams,
						  std::set<std::pair<int,int>>& defElmSet,
						  const std::vector<FeatureParams>& gparams,
						  const std::vector<FeatureMeshOutput>& mout,
						  int& nRelaxVerts)
      {
	const auto& BG = WM.GetParentMesh();
      
	// Prepare for relaxing interior nodes
      
	// # threads
	const int nthreads = 1;

	// Aliases
	const int& Ndist = mparams.Ndist;           // Relaxation neighborhood
	const double& qThreshold = mparams.quality; // Quality threshold
	const int& Nproject = mparams.Nproject;     // Limit on projection iterations
	const int Nrelax = mparams.Nrelax;          // Number of relaxation iterations
      
	// Quality metric
	dvr::GeomTri2DQuality<decltype(WM)> Quality(WM, nthreads);

	// Max-min solver and direction generator for relaxing interior vertices
	dvr::ReconstructiveMaxMinSolver<decltype(Quality)> intSolver(Quality, nthreads);
	dvr::CartesianDirGenerator<2> CartGen;

	// Accummulate a list of vertices which cannot be relaxed
	std::set<int> notIr{};
	for(auto& comp:mout)
	  for(auto& v:comp.PosVerts)
	    notIr.insert(v);
	//for(auto& v:mparams.dnd_vertices)
	//notIr.insert(v);

	// Accummulate a list of relaxable vertices
	std::set<int> Ir{};
      
	// Data for positive vertices
	std::priority_queue<PosVertStruct, std::vector<PosVertStruct>, decltype(PosVertStruct_Compare)> posVertData(PosVertStruct_Compare);
	const int nFeatures = static_cast<int>(gparams.size());
	double dsd[2];
	for(int cnum=0; cnum<nFeatures; ++cnum)
	  {
	    // Details of this component
	    const auto& comp_posVerts = mout[cnum].PosVerts;
	    const int& splittingVertex = mout[cnum].splittingVertex;
	    const int* terminalVertices = mout[cnum].terminalVertices;
	    const auto& sdfunc = gparams[cnum].sdfunc;
	    void* funcparams = gparams[cnum].funcparams;

	    // Accummulate data on positive vertices of this component
	    for(auto& vert:comp_posVerts)
	      if(vert!=splittingVertex && vert!=terminalVertices[0] && vert!=terminalVertices[1])
		{
		  // This positive vertex needs to be projected.
		  PosVertStruct data;

		  // vertex
		  data.vert = vert;
		
		  // component number
		  data.compNum = cnum;

		  // signed distance and closest point projection
		  const double* X = WM.coordinates(vert);
		  bool onFeature;
		  sdfunc(X, onFeature, data.sdval, dsd, funcparams);
		  for(int k=0; k<2; ++k)
		    data.cpt[k] = X[k]-data.sdval*dsd[k];

		  // number of projection iterations
		  data.nProjIters = 0;

		  // relaxation neighborhood: TODO: is this set_difference expensive?
		  std::set<int> nbSet{};
		  GetVertexNeighborhood(BG, std::vector<int>{vert}, Ndist, nbSet);
		  data.Ir.clear();
		  std::set_difference(nbSet.begin(), nbSet.end(), notIr.begin(), notIr.end(),
				      std::back_inserter(data.Ir));

		  // Update cumulative relaxable vertices
		  for(auto& v:data.Ir)
		    Ir.insert(v);
		
		  posVertData.push(data);
		}
	  }


	// Iteratively project and relax positive vertices
	// In case of a failure to project a vector, note the failure and remove the vertex from the queue
	int iter = 0;
	defElmSet.clear();
	const int* oneRingElms;
	int n1RingElms;
	double qval = 0;
	double X[2], Y[2];
	double lambda;
	int pCount;
	bool overall_flag = true; // track success
	while(!posVertData.empty())
	  {
	    // Access the first positive vertex in the queue
	    auto it = posVertData.top();
	    const int& vert = it.vert;

	    // Success of this projection attempt
	    bool vert_success = true;
	  
	    // Track the number of projection iterations
	    ++it.nProjIters;
	  
	    // 1-ring elements
	    BG.Get1RingElements(vert, &oneRingElms, n1RingElms);

	    // Check the qualities of elements in the 1-ring of vert
	    for(int eindx=0; eindx<n1RingElms; ++eindx)
	      {
		qval = Quality.Compute(oneRingElms[eindx]);
		if(qval<qThreshold)
		  {
		    // Poor quality elements at this stage is a failure in projecting this vertex
		    defElmSet.insert(std::make_pair(it.compNum,oneRingElms[eindx]));
		    vert_success = false;
		    overall_flag = false;
		  }
	      }

	    // Too many projection iterations is a failure in projecting this vertex
	    if(it.nProjIters>Nproject)
	      {
		defElmSet.insert(std::make_pair(it.compNum, oneRingElms[0]));
		vert_success = false;
		overall_flag = false;
	      }

	    // Failure in projecting ths vertex -> remove vertex from the queue
	    if(vert_success==false)
	      {
		posVertData.pop();
		continue;
	      }

	    // Attempt to project this vertex onto the boundary
	    assert(vert_success==true);
	    const double* Z = WM.coordinates(vert);
	    double X[] = {Z[0], Z[1]}; // a copy of the original coordinates
	    double Y[2];
	    const double* cpt = it.cpt;
	    lambda = 2.;
	    pCount = 0; 
	    bool lambda_flag = false;
	    while(lambda_flag==false && vert_success==true)
	      {
		// This iteration for lambda (projection fraction)
		++pCount;
		lambda_flag = true;
		lambda /= 2.;
	      
		// If lambda is too small, note this as a defect in projecting this vertex.
		// This check prevents an infinite loop
		if(lambda<std::pow(2., -static_cast<double>(Nproject)))
		  {
		    // revert coordinate updates for this vertex
		    WM.update(vert, X);

		    // note the defect around this vertex
		    defElmSet.insert(std::make_pair(it.compNum, oneRingElms[0]));
		    lambda_flag = false;
		    vert_success = false;
		    overall_flag = false;
		  }
		else // lambda is acceptable
		  {
		    // Candidate position for this vertex
		    for(int k=0; k<2; ++k)
		      Y[k] = lambda*cpt[k] + (1.-lambda)*X[k];

		    // Examine quality around updated vertex
		    WM.update(vert, Y);
		    for(int eindx=0; eindx<n1RingElms; ++eindx)
		      {
			qval = Quality.Compute(oneRingElms[eindx]);
			if(qval<qThreshold) 
			  {
			    // lambda is not acceptable. more lambda iterations required
			    lambda_flag = false;
			    break;
			  }
		      }
		  }
	      }

	    if(vert_success==true)
	      {
		// Relax around this vertex if required: HACK HERE, HARD-CODED VALUES
		bool do_relax = false;
		for(int eindx=0; eindx<n1RingElms && do_relax==false; ++eindx)
		  {
		    qval = Quality.Compute(oneRingElms[eindx]);
		    if(qval<0.1)
		      do_relax = true;
		  }
		if(do_relax==true)
		  for(int riter=0; riter<2; ++riter)
		    {
		      CartGen.iteration = riter;
		      dvr::Optimize(it.Ir, WM, intSolver, CartGen, nullptr);
		      nRelaxVerts += static_cast<int>(it.Ir.size());
		    }

		// Does this vertex need further consideration?
		if(pCount==1)
		  posVertData.pop(); // no
		else
		  it.sdval *= (1.-lambda); // yes
	      }
	    else
	      {
		// perturbation was unsuccessful. don't bother projecting this vertex further
		assert(overall_flag==false);
		posVertData.pop(); 
	      }

	    // Visualize corner-adjusted mesh
	    //WM.PlotTecMesh("wm-proj-"+std::to_string(iter++)+".tec");
	  }


	// Return in case of a failure
	if(overall_flag==false)
	  return MeshingReason::Fail_ElementQuality;

	// Proceed to improving the mesh
	const std::vector<int> Irvec(Ir.begin(), Ir.end());
      
	// Constrained relaxation of positive vertices along tangents

	// Direction generator for relaxing positive vertices along the tangent
	std::vector<std::unique_ptr<dvr::TangentialDirGenerator<2>>> tgtDirs(nFeatures);

	// Closest point struct for projecting relaxed positive vertices onto the boundary
	std::vector<std::unique_ptr<dvr::ClosestPointStruct<2>>> cptProjectors(nFeatures);

	for(int f=0; f<nFeatures; ++f)
	  {
	    tgtDirs[f] = std::make_unique<dvr::TangentialDirGenerator<2>>(gparams[f].sdfunc, gparams[f].funcparams);
	    cptProjectors[f] =  std::make_unique<dvr::ClosestPointStruct<2>>(gparams[f].sdfunc, gparams[f].funcparams);
	  }
      
	// Sampling-based max-min solver for relaxing positive vertices
	dvr::SamplingMaxMinSolver<decltype(Quality)> bdSolver(Quality, mparams.Nsamples);

	// Vertices to relax along the boundary (exclude terminals)
	std::vector<std::vector<int>> bdNodes(nFeatures);
	for(int f=0; f<nFeatures; ++f)
	  {
	    bdNodes[f] = mout[f].PosVerts;
	    bdNodes[f].erase(bdNodes[f].begin());
	    bdNodes[f].pop_back();
	  }

	// Relax positive vertices
	for(int biter=0; biter<mparams.Nrelax; ++biter)
	  {
	    // Relax boundary vertices component-by-component
	    for(int f=0; f<nFeatures; ++f)
	      dvr::Optimize(bdNodes[f], WM, bdSolver, *tgtDirs[f], cptProjectors[f].get());

	    // Relax interior nodes along Ex and Ey
	    for(int riter=0; riter<2; ++riter)
	      {
		CartGen.iteration = riter;
		dvr::Optimize(Irvec, WM, intSolver, CartGen, nullptr);
		nRelaxVerts += static_cast<int>(Irvec.size());
	      }
	  }
      
	// report success
	return MeshingReason::Success;
      }
      
  }
}
  
#endif
