// Sriramajayam

#ifndef UM_ALGORITHM_POLYGON_CHAIN_H
#define UM_ALGORITHM_POLYGON_CHAIN_H

#include <vector>
#include <map>
#include <cassert>

namespace um
{
  namespace algo
  {
    // Make an open chain polygon-free
    template<typename MeshType>
      void MakeOpenChainPolygonFree(const MeshType& MD,  std::vector<int>& vertices)
      {
	const int nVerts = static_cast<int>(vertices.size());
	assert(vertices[0]!=vertices[nVerts-1]);

	// Create a map: vertex -> index
	std::map<int, int> vert_to_indx_map{};
	for(int vnum=0; vnum<nVerts; ++vnum)
	  vert_to_indx_map[vertices[vnum]] = vnum;

	// Index of the polygon free chain
	std::vector<int> indices{};
	const int* oneRingVerts;
	int n1RingVerts = 0;
	int vnum = 0;
      
	while(vnum!=nVerts-1)
	  {
	    // Insert the current vertex into the chain
	    indices.push_back( vnum );

	    // Identify the next vertex in the chain & pick the one with the largest index
	    const int& vert = vertices[vnum];
	  
	    int next_indx = vnum;
	    MD.Get1RingVertices(vert, &oneRingVerts, n1RingVerts);
	    for(int i=0; i<n1RingVerts; ++i)
	      {
		// Is this vertex in the chain & further ahead?
		auto it = vert_to_indx_map.find(oneRingVerts[i]);
		if(it!=vert_to_indx_map.end())
		  if(it->second>next_indx)
		    next_indx = it->second;
	      }
	    assert(next_indx>=vnum+1);  // sanity check
	    vnum = next_indx;   // Update
	  }
      
	// Append the last vertex
	indices.push_back(vnum);
	assert(static_cast<int>(indices.size())<=nVerts); // sanity checks
	assert(static_cast<int>(vnum)==nVerts-1);
            
	// Prune?
	if(static_cast<int>(indices.size())<nVerts)
	  {
	    //std::cout<<"\nNumber of vertices eliminated: "<<nVerts-indices.size()<<"\n"<<std::flush;
	    std::vector<int> newVertices{};
	    newVertices.reserve(indices.size());
	    for(auto& i:indices)
	      newVertices.push_back( vertices[i] );
	    vertices.clear();
	    vertices = std::move(newVertices);
	  }

	// done
	return;
      }
  }
}

#endif
