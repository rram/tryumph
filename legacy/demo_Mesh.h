// Sriramajayam

#pragma once

#include <vector>
#include <UMlib>

namespace demo {
  class SimpleMesh
  {
  public:
  
    //! Constructor
    inline SimpleMesh(const std::vector<double>& coords,
		      const std::vector<int>& conn)
      :Coordinates(coords), Connectivity(conn),
      nNodes(Coordinates.size()/2), nElements(conn.size()/3) {
      static_assert(um2::CheckMeshTraits<SimpleMesh>());
    }
  
    //! Destructor
    ~SimpleMesh() = default;

    //! Disable copy & assignment
    SimpleMesh(const SimpleMesh&) = delete;
    SimpleMesh operator=(const SimpleMesh&) = delete;

    //! Returns the coordinates of a specified node
    inline const double* coordinates(const int node) const {
      return &Coordinates[2*node];
    }
      
    //! Overwrites nodal coordinates
    inline void update(const int node, const double* X) {
      Coordinates[2*node]   = X[0];
      Coordinates[2*node+1] = X[1];
    }
  
    // Returns the connectivity of a specified element
    inline const int* connectivity(const int elm) const {
      return &Connectivity[3*elm];
    }

    //! Returns the spatial dimension
    inline int spatial_dimension() const {
      return 2;
    }
      
    //! Returns the number of nodes
    inline int n_nodes() const {
      return nNodes;
    }

    // Returns the number of elements
    inline int n_elements() const {
      return nElements;
    }

  private:
    std::vector<double> Coordinates;
    std::vector<int>    Connectivity;
    int                 nNodes;
    int                 nElements;
  };
}
