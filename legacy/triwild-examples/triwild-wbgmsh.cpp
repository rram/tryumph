// Sriramajam

#include <iostream>
#include <json.hpp>
#include <string>
#include <umesh_SimpleMesh.h>
#include <um_Module>

using json = nlohmann::json;

// Create feature curves using .obj and .json files
void CreateFeatures(const std::string objfilename,
		    const std::string jsonfilename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves);

int main()
{
  // Create line and cubic-bezier features
  std::vector<um::LineSegment*> p1curves{};
  std::vector<um::QuadraticRationalBezier*> p2curves{};
  std::vector<um::CubicBezier*> p3curves{};
  CreateFeatures("input.obj", "input.json", p1curves, p2curves, p3curves);
  const int np1curves = static_cast<int>(p1curves.size());
  const int np2curves = static_cast<int>(p2curves.size());
  const int np3curves = static_cast<int>(p3curves.size());

  // Collect sampling points from splines
  std::vector<um::MedialAxisSamplesVec> mDatas(np1curves+np2curves+np3curves);
  for(int i=0; i<np1curves; ++i)
    { auto& m = mDatas[i];
      p1curves[i]->GetSamples(m); }
  for(int i=0; i<np1curves; ++i)
    { auto& m = mDatas[np1curves+i];
      p2curves[i]->GetSamples(m); }
  for(int i=0; i<np3curves; ++i)
    { auto& m = mDatas[np1curves+np2curves+i];
      p3curves[i]->GetSamples(m); }

  // Flatten sampling points
  um::MedialAxisSamplesVec mdata{};
  for(auto& it:mDatas)
    for(auto& jt:it)
      mdata.push_back( jt );

  // Create medial axis sampler
  const double tolEPS = 1.e-6;
  const double maxRadius = 1.;
  const double exclusion_radius = 1.e-3;
  um::MedialAxisSampler mas(mdata, exclusion_radius, maxRadius, tolEPS);
  um::MedialAxisSamples::PrintGlobalAxes("gmedial.csv", mdata);
  for(auto& curve:p3curves)
    { const int curve_id = curve->GetCurveID();
      std::string filename = "medial-"+std::to_string(curve_id)+".csv";
      um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, mdata);
    }

  
  /*
  // Create a quadtree
  const double qCenter[] = {0.,0.};//0.5*(coord_min[0]+coord_max[0]), 0.5*(coord_min[1]+coord_max[1])};
  const double qSize = 1.25; //coord_max[0]-coord_min[0];
  um::Quadtree qtree(qCenter, qSize);

  // Refine the tree using distances to the medial axis
  for(auto& m:mData)
  { const double* Pt = m.Point;
  double cpt[2];
  double dist = mas.GetDistanceToMedialAxis(Pt, cpt);
  qtree.Refine(Pt, 2.*dist); }

  // Visualize the refined quadtree
  qtree.PlotTec("refinedTree.tec");

  // Triangulate the tree
  umesh::SimpleMesh BG;
  BG.nodes_element = 3;
  BG.spatial_dimension = 2;
  qtree.Triangulate(BG.Coordinates, BG.Connectivity);
  BG.nodes = static_cast<int>(BG.Coordinates.size())/2;
  BG.elements = static_cast<int>(BG.Connectivity.size()/3);
  BG.SetupMesh();
  BG.PlotTecMesh("bgmesh.tec");

  // Generate a mesh
  */
  for(auto& it:p1curves)
    delete it;
  for(auto& it:p2curves)
    delete it;
  for(auto& it:p3curves)
    delete it;

}

void CreateFeatures(const std::string objfilename,
		    const std::string jsonfilename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves)
{
  // read the obj file to help define a bounding box
  std::fstream objfile;
  objfile.open(objfilename.c_str(), std::ios::in);
  assert(objfile.good());
  char firstchar;
  std::string fline;
  double X[2], zero;

  // read the first pair of coordinates
  while(true)
    {
      objfile >> firstchar;
      if(firstchar=='v')
	{ // x, y, 0
	  objfile >> X[0]; 
	  objfile >> X[1];
	  objfile >> zero;
	  break;
	}
      else
	std::getline(objfile, fline);
    }
  double coord_min[2] = {X[0], X[1]};
  double coord_max[2] = {X[0], X[1]};

  // Read the rest of the json file
  while(objfile.good())
    {
      objfile >> firstchar;
      if(firstchar=='v')
	{ // x, y, 0
	  objfile >> X[0]; 
	  objfile >> X[1]; 
	  objfile >> zero;
	  if(X[0]<coord_min[0]) coord_min[0] = X[0];
	  if(X[1]<coord_min[1]) coord_min[1] = X[1];
	  if(X[0]>coord_max[0]) coord_max[0] = X[0];
	  if(X[1]>coord_max[1]) coord_max[1] = X[1];
	}
      else
	std::getline(objfile, fline);
    }
  objfile.close();
  
  // Rescale domain to a unit square centered at the origin
  double center[] = {0.5*(coord_min[0]+coord_max[0]), 0.5*(coord_min[1]+coord_max[1])};
  double size =
    (coord_max[0]-coord_min[0])>(coord_max[1]-coord_min[1]) ?
    (coord_max[0]-coord_min[0]) :
    (coord_max[1]-coord_min[1]);

  // Read the json file for bezier curves and line segments
  std::fstream jfile;
  jfile.open(jsonfilename.c_str(), std::ios::in);
  assert(jfile.good());
  json j;
  jfile >> j;
  jfile.close();

  // Read features
  p1curves.clear();
  p2curves.clear();
  p3curves.clear();
  const int nsamples = 40;
  for(auto& it:j)
    {
      int curve_id = it["curve_id"];
      
      if(it["type"]=="Line")
	{
	  auto start = it["start"];
	  auto end = it["end"];
	  // start -> A, end -> B
	  int k;
	  double A[2], B[2];
	  k=0; for(auto& x:start) A[k++] = x;
	  k = 0; for(auto& x:end) B[k++] = x;
	  for(int i=0; i<2; ++i)
	    { A[i] -= center[i]; A[i] /= size;
	      B[i] -= center[i]; B[i] /= size; }
	  p1curves.push_back( new um::LineSegment(curve_id, A, B, nsamples) );
	}
      else if(it["type"]=="RationalBezier")
	{
	  auto poles = it["poles"];
	  std::vector<double> points{};
	  for(auto& it:poles)
	    for(auto& jt:it)
	      points.push_back( jt );
	  assert(static_cast<int>(points.size())==6);
	  for(int p=0; p<3; ++p)
	    { points[2*p]   -= center[0]; points[2*p] /= size;
	      points[2*p+1] -= center[1]; points[2*p+1] /= size; }
	  auto wts = it["weigths"];
	  std::vector<double> weights{};
	  for(auto& it:wts)
	    weights.push_back(it);
	  assert(static_cast<int>(weights.size())==3);
	  p2curves.push_back( new um::QuadraticRationalBezier(curve_id, points, weights, nsamples) );
	}
      else if(it["type"]=="BezierCurve")
	{
	  auto poles = it["poles"];
	  std::vector<double> points{};
	  for(auto& it:poles)
	    for(auto& jt:it)
	      points.push_back( jt );
	  assert(static_cast<int>(points.size())==8);
	  for(int p=0; p<4; ++p)
	    { points[2*p]   -= center[0]; points[2*p] /= size;
	      points[2*p+1] -= center[1]; points[2*p+1] /= size; }
	  p3curves.push_back( new um::CubicBezier(curve_id, points, nsamples) );
	}
      else assert(false && "Unknown feature type encountered");
    }
      
  std::cout<<"\nCreated: "
	   <<"\n"<<p1curves.size()<< " line segments"
	   <<"\n"<<p2curves.size()<< " quadratic rational bezier segments"
	   <<"\n"<<p3curves.size()<< " cubic bezier segments"
	   <<"\n"<<std::flush;
      
  // done
  return;
}


