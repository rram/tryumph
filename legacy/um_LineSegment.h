// Sriramajayam

#ifndef UM_LINE_SEGMENT_H
#define UM_LINE_SEGMENT_H

#include <um_NLSolverParams.h>
#include <um_FeatureMedialAxis.h>
#include <um_FeatureParams.h>

namespace um
{  
  //! Class defining a line segment
  class LineSegment
  {
  public:
    //! Constructor
    //! \param[in] id Curve id
    //! \param[in] A Starting point
    //! \param[out] B End point
    //! \param[in] nsamples Number of sampling points
    LineSegment(const int id,
		const double* A, const double* B,
		const int nSamples,
		const MedialAxisParams& mparams,
		const NLSolverParams& nlparams);

    //! Disable copy and assignment
    LineSegment(const LineSegment&) = delete;
    LineSegment& operator=(const LineSegment) = delete;

    //! Destructor
    inline virtual ~LineSegment() {}

    //! Returns the curve id
    int GetCurveID() const;

    //! Access the control points
    void GetControlPoints(std::vector<double>& control_points) const;
    
    //! Returns the end points of the segment
    void GetTerminalPoints(double* tpoints) const;

    //! Returns the mid point
    void GetSplittingPoint(double* spoint) const;

    std::pair<LineSegment&, LineSegment&> GetSplits();
    
    //! Returns the signed distance function to a point
    //! \param[in] X Point at which to evaluate
    //! \param[in] nlparams Solver parameters
    //! \param[out] sd Computed signed distance
    //! \param[out] dsd Computed derivative of the signed distance. Defaulted to nullptr.
    void GetSignedDistance(const double* X, double& sd, double* dsd=nullptr);

    double GetFeatureSize(const double* X);
    
    //! Samples the feature
    void GetSampling(std::vector<std::array<double,2>>& samples) const;
    
    // Overload extraction operator
    friend std::ostream& operator << (std::ostream &out, const LineSegment& geom);

    //! Split the feacture
    std::pair<FeatureParams, FeatureParams> Split(const int left_id, const int right_id);
    
    //! Register feature params
    void RegisterFeature(FeatureParams& params);
    
  private:
    //! Reset the control points
    void SetControlPoints(const std::vector<double>& control_points);
    
    void ComputeMedialAxis();
    const int curve_id; //!< Curve id
    double left_pt[2], right_pt[2]; //!< Left and right end points
    const int nsamples; //!< Number of sample points
    FeatureMedialAxis medialAxis; //!< Medial axis
    const NLSolverParams& NLParams; //!< Params for the nonlinear solver
    double nvec[2]; //!< Unit normal to the segment
    std::unique_ptr<LineSegment> left_split, right_split;  //!< split features
  };
  
} // um::

#endif
