// Sriramajayam

#include <um_test_animate.h>
#include <um_test_visualize.h>
#include <fstream>

namespace um
{
  namespace test
  {
    // read edges in vtk format
    void read_vtk_edges(const std::string filename,
			std::vector<double>& coordinates,
			std::vector<std::vector<int>>& connectivity, // connectivity[i] -> ith sequence of edges
			std::vector<int>& colorMap)
    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
      std::ifstream file;
      file.open(filename);
      assert(file.good() && file.is_open());

      connectivity.clear();
      coordinates.clear();
      
      // read the number of nodes
      position_cursor_after_word(file, "POINTS");
      int nNodes;
      file >> nNodes;

      // read coordinates
      coordinates.resize(2*nNodes);
      std::string line;
      std::getline(file, line);
      double zcoord;
      for(int n=0; n<nNodes; ++n)
	{
	  file >> coordinates[2*n] >> coordinates[2*n+1] >> zcoord;
	}

      // read the number of lines
      position_cursor_after_word(file, "LINES");
      int nLines;
      file >> nLines;
      
      // connectivities of lines
      connectivity.clear();
      std::getline(file, line);
      int nodes_per_line;
      for(int e=0; e<nLines; ++e)
	{
	  file >> nodes_per_line;
	  assert(nodes_per_line>=2);
	  std::vector<int> conn(nodes_per_line);
	  for(int a=0; a<nodes_per_line; ++a)
	    file >> conn[a];
	  connectivity.push_back(std::move(conn));
	}

      // read the color map
      colorMap.resize(nLines);
      position_cursor_after_word(file, "CELL_DATA");
      position_cursor_after_word(file, "color");
      std::getline(file, line);
      std::getline(file, line);
      for(int a=0; a<nLines; ++a)
	file >> colorMap[a];
      
      // done
      file.close();
    }


    // save edges to a file
    void save_vtk_edges(const std::string filename, const std::vector<double>& coords,
			const std::vector<std::vector<int>>& lines_conn,
			const std::vector<int>& colorMap)
    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	
      std::fstream out;
      out.open(filename, std::ios::out);
      assert(out.good() && out.is_open());

      // Headers
      out << "# vtk DataFile Version 3.0" << std::endl;
      out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
      out << "ASCII" << std::endl;
      out << "DATASET POLYDATA" << std::endl;

      // nodes
      const int nNodes = static_cast<int>(coords.size())/2;
      out << "POINTS " << nNodes << " double" << std::endl;
      for(int i=0; i<nNodes; ++i)
	{
	  const double* X = &coords[2*i];
	  out << X[0] << " " << X[1] << " " << 0. << std::endl;
	}

      // polygons = lines
      const int nLines = static_cast<int>(lines_conn.size());
      assert(colorMap.size()==lines_conn.size());
	
      // cell size = total number of integers in the list
      int cell_size = 0;
      for(auto& f:lines_conn)
	{
	  cell_size += static_cast<int>(f.size())+1;
	}
      out << "LINES " << nLines << " " << cell_size << std::endl;
      for(auto& f:lines_conn)
	{
	  out << static_cast<int>(f.size()) << " ";
	  for(auto& v:f)
	    out << v << " ";
	  out << std::endl;
	}

      // colors for each feature
      out << "CELL_DATA " << nLines << std::endl
	  << "SCALARS color int 1" << std::endl
	  << "LOOKUP_TABLE default" << std::endl;
      for(int i=0; i<nLines; ++i)
	{
	  out << colorMap[i] << std::endl;
	}

      // done
      out.close();
    }

    // Animate positive edge motion from one mesh to another
    void animate_edge_motion(const std::string file1, const std::string file2,
			     const int num_frames, const std::string stem)
    {
      // edge mesh 1
      std::vector<double> coords1{};
      std::vector<std::vector<int>> conn1{};
      std::vector<int> colorMap1{};
      read_vtk_edges(file1, coords1, conn1, colorMap1);

      // edge mesh 2
      std::vector<double> coords2{};
      std::vector<std::vector<int>> conn2{};
      std::vector<int> colorMap2{};
      read_vtk_edges(file1, coords2, conn2, colorMap2);

      // consistency checks
      assert(coords1.size()==coords2.size());
      assert(conn1==conn2);
      assert(colorMap1==colorMap2);

      // animate
      const int nNodes = static_cast<int>(coords1.size()/2);
      std::vector<double> coords(2*nNodes);
      for(int f=0; f<=num_frames; ++f)
	{
	  const double lambda = static_cast<double>(f)/static_cast<double>(num_frames);
	  for(int a=0; a<nNodes; ++a)
	    {
	      const double* X = &coords1[2*a];
	      const double* Y = &coords2[2*a];
	      double Z[2];
	      Z[0] = (1.-lambda)*X[0] + lambda*Y[0];
	      Z[1] = (1.-lambda)*X[1] + lambda*Y[1];
	    }

	  // save to file
	  const std::string filename = stem + "-f" + std::to_string(f) + ".vtk";
	  save_vtk_edges(filename, coords, conn1, colorMap1);
	}

      // done
      return;
    }

  } // um::test::
} // um::
