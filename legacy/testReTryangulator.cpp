// Sriramajayam

#include <iostream>
#include <string>
#include <um_Tryangulator2D.h>
#include <um_ReTryangulator2D.h>
#include <umesh_SimpleMesh.h>
#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_SignedDistanceFunction.h>
#include <boost/filesystem.hpp>

// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves);

// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size);

// Rotate a feature
template<class T>
void RotateFeature(const double* center, const double rot_angle,  T& curve);

// Correctness checks in the output
template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureOrientation& orientation,
			   const um::FeatureMeshOutput& mout,
			   const WMType& wm);

namespace fs = boost::filesystem;
inline fs::path operator+(fs::path left, fs::path right){return fs::path(left)+=right;}

int main()
{
  fs::path currdir = fs::current_path();
  assert(fs::exists(currdir));
  assert(fs::is_directory(currdir));

  fs::path geomdir = currdir + "/geom-data";
  assert(fs::exists(geomdir));
  assert(fs::is_directory(geomdir));

  for(auto& entry:fs::recursive_directory_iterator(geomdir))
    if(fs::is_regular_file(entry) && entry.path().extension()==".fset")
      {
	const std::string gfile = entry.path().filename().stem().string();
	std::cout<<"\nProcessing geometry: "<<gfile<<std::flush;
	
	// output path for this case
	std::string output_path = currdir.string()+"/output-"+gfile+"/";
	
	// Read features
	std::vector<um::LineSegment*> p1curves{};
	std::vector<um::QuadraticRationalBezier*> p2curves{};
	std::vector<um::CubicBezier*> p3curves{};
	CreateFeatures("geom-data/"+gfile+".fset", p1curves, p2curves, p3curves);
	const int np1curves = static_cast<int>(p1curves.size());
	const int np2curves = static_cast<int>(p2curves.size());
	const int np3curves = static_cast<int>(p3curves.size());

	// Get samples on the curve
	um::MedialAxisSamplesVec MASvec{};
	for(auto& geom:p1curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p2curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p3curves)
	  geom->GetSamples(MASvec);

	// Get a bounding box for the geometry
	double center[2], size;
	GetBoundingBox(MASvec, center, size);

	// Compute the medial axes
	um::MedialAxisSampler MAS(MASvec, 1.e-4, size, 1.e-3);
  
	// Print each sample
	for(auto& geom:p1curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	for(auto& geom:p2curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	for(auto& geom:p3curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	um::MedialAxisSamples::PrintGlobalAxes(output_path+"gmedial.csv", MASvec);

  	// Nonlinear solver parameters
	um::NLSolverParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
	// Signed distance functions
	// P1
	std::vector<um::P1Segment_SDParams> p1_sdparams{};
	for(auto& geom:p1curves) p1_sdparams.push_back( um::P1Segment_SDParams{geom, &nlparams} );
	// P2
	std::vector<um::P2RationalBezier_SDParams> p2_sdparams{};
	for(auto& geom:p2curves) p2_sdparams.push_back( um::P2RationalBezier_SDParams{geom, &nlparams} );
	// P3
	std::vector<um::P3Bezier_SDParams> p3_sdparams{};
	for(auto& geom:p3curves) p3_sdparams.push_back( um::P3Bezier_SDParams{geom, &nlparams} );
  
	// Feature set
	um::FeatureSet fset(1.e-4);
	// P1
	for(int n=0; n<np1curves; ++n)
	  { um::FeatureParams fp{.curve_id=p1curves[n]->GetCurveID(), .sdfunc=um::p1segment_sdfunc, .sdparams=&p1_sdparams[n]}; 
	    p1curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p1curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }
	// P2
	for(int n=0; n<np2curves; ++n)
	  { um::FeatureParams fp{.curve_id=p2curves[n]->GetCurveID(), .sdfunc=um::p2rationalbezier_sdfunc, .sdparams=&p2_sdparams[n]};
	    p2curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p2curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }
	// P3
	for(int n=0; n<np3curves; ++n)
	  { um::FeatureParams fp{.curve_id=p3curves[n]->GetCurveID(), .sdfunc=um::p3bezier_sdfunc, .sdparams=&p3_sdparams[n]};
	    p3curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p3curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }
	
	// Process the feature set
	fset.Finalize();
	
	// Create an adaptively refined background mesh for this geometry
	size *= 2.; // buffer
	um::Quadtree QT(center, size);
  
	// Meshing parameters
	um::MeshingParams mparams{.Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10, .quality=0.01};
  
	std::vector<um::FeatureMeshOutput> mout{};
	um::Tryangulator2D<umesh::SimpleMesh> tri2D(QT);
	um::RunInfo run_info;
	run_info.output_path = output_path;
	tri2D.GenerateMesh(mparams, fset, MAS, MASvec, um::PerturbationAlgorithm::Frontal_NonAdaptive, mout, run_info);

	auto& BG = tri2D.GetUniversalMesh();
	BG.PlotTecMesh(output_path+"bg.tec");
	auto& WM = tri2D.GetWorkingMesh();
	WM.PlotTecMesh(output_path+"wm.tec");
	
	// Run correctness checks on the computed mesh
	const int nFeatures = fset.GetNumFeatures();
	assert(static_cast<int>(mout.size())==nFeatures);
	const auto& gparams = fset.GetFeatureParams();
	const auto& orientations = fset.GetFeatureOrientations();
	for(int f=0; f<nFeatures; ++f)
	  TestFeatureMeshOutput(gparams[f], mparams, orientations[f], mout[f], WM);

	// Re-tryangulate
	um::ReTryangulator2D<umesh::SimpleMesh> retri2D(WM, mout);

	// Modify feature set
	const double dtheta = 5.*M_PI/180.;
	for(auto& geom:p1curves) RotateFeature(center, dtheta, *geom);
	for(auto& geom:p2curves) RotateFeature(center, dtheta, *geom);
	for(auto& geom:p3curves) RotateFeature(center, dtheta, *geom);

	// remesh
	retri2D.GenerateMesh(mparams, fset, run_info);

	// print
	auto& reWM = retri2D.GetWorkingMesh();
	reWM.PlotTecMesh(output_path+"re-wm.tec");
	std::fstream pfile;
	std::string filename = output_path+"geom.csv";
	pfile.open(filename.c_str(), std::ios::out);
	pfile << "X, \t Y\n";
	for(auto& geom:p1curves) pfile << *geom;
	for(auto& geom:p2curves) pfile << *geom;
	for(auto& geom:p3curves) pfile << *geom;
	pfile.close();
		
	// Clean up
	for(auto& it:p1curves) delete it;
	for(auto& it:p2curves) delete it;
	for(auto& it:p3curves) delete it;

      }
}


// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves)
{
  p1curves.clear();
  p2curves.clear();
  p3curves.clear();
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());

  // Header contains the file format
  // #Format: curve_id, npoles, poles, nweights, weights";
  std::string line1;
  std::getline(pfile, line1);

  // Read the number of features
  int nFeatures;
  pfile >> nFeatures;

  // Read each feature
  int curve_id, npoles, nweights;
  std::vector<double> poles, weights;
  for(int f=0; f<nFeatures; ++f)
    {
      // Read this feature's parameters
      pfile >> curve_id;
      pfile >> npoles;
      poles.resize(2*npoles);
      for(int p=0; p<2*npoles; ++p) pfile >> poles[p];
      pfile >> nweights;
      weights.resize(nweights);
      for(int p=0; p<nweights; ++p) pfile >> weights[p];

      // Create this feature
      const int nSamples = 50;
      switch(npoles)
	{
	case 2: { assert(nweights==0);
	    p1curves.push_back( new um::LineSegment(curve_id, &poles[0], &poles[2], nSamples) );
	    break; }

	case 3: { assert(nweights==npoles);
	    p2curves.push_back( new um::QuadraticRationalBezier(curve_id, poles, weights, nSamples) );
	    break; }

	case 4: { assert(nweights==0);
	    p3curves.push_back( new um::CubicBezier(curve_id, poles, nSamples) );
	    break; }

	default: assert(false && "CreateFeatures: Unknown feature type");
	}
    }

  std::cout<<"\nCreated: "
	   <<"\n"<<p1curves.size()<< " line segments"
	   <<"\n"<<p2curves.size()<< " quadratic rational bezier segments"
	   <<"\n"<<p3curves.size()<< " cubic bezier segments"
	   <<"\n"<<std::flush;
  // done
  return;
}


// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size)
{
  double coord_min[] = {MASvec[0].Point[0], MASvec[0].Point[1]};
  double coord_max[] = {coord_min[0], coord_min[1]};
  
  for(auto& m:MASvec)
    {
      const double& x = m.Point[0];
      const double& y = m.Point[1];

      if(x<coord_min[0]) coord_min[0] = x;
      else if(x>coord_max[0]) coord_max[0] = x;

      if(y<coord_min[1]) coord_min[1] = y;
      else if(y>coord_max[1]) coord_max[1] = y;
    }

  center[0] = 0.5*(coord_min[0]+coord_max[0]);
  center[1] = 0.5*(coord_min[1]+coord_max[1]);
  double hx = coord_max[0]-coord_min[0];
  double hy = coord_max[1]-coord_min[1];
  size = (hx>hy) ? hx : hy;

  return;
}


template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureOrientation& orientation,
			   const um::FeatureMeshOutput& mout,
			   const WMType& WM)
{
  // curve id
  assert(gparam.curve_id==mout.curve_id);

  // orientation
  assert(orientation!=um::FeatureOrientation::Unassigned);
  
  // end points
  for(int p=0; p<2; ++p)
    { const int& n = mout.terminalVertices[p];
      assert(n>=0);
      const double* X = WM.coordinates(n);
      const double* Y = &gparam.terminalPoints[2*p];
      double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
      assert(dist<1.e-4); // Should be comparable to the merging tolerance used in meshing params.
    }

  // positive vertices should start and terminate at the end points
  assert(static_cast<int>(mout.PosVerts.size())>=2);
  assert(mout.terminalVertices[0]==mout.PosVerts.front());
  assert(mout.terminalVertices[1]==mout.PosVerts.back());

  // positive vertices should lie on the feature
  for(auto& p:mout.PosVerts)
    {
      const double* X = WM.coordinates(p);
      double sd;
      gparam.sdfunc(X, sd, nullptr, gparam.sdparams);
      assert(std::abs(sd)<1.e-3);
    }

  // no repeated positive vertex
  std::set<int> pset(mout.PosVerts.begin(), mout.PosVerts.end());
  assert(pset.size()==mout.PosVerts.size());
  
  // check that the splitting vertex is a positive vertex
  if(orientation==um::FeatureOrientation::Split_Positive2Negative || orientation==um::FeatureOrientation::Split_Negative2Positive)
    {
      assert(mout.splittingVertex!=-1);
      bool flag = false;
      for(auto& v:mout.PosVerts)
	if(v==mout.splittingVertex)
	  { flag=true; break; }
      assert(flag==true);
    }
  else
    { assert(mout.splittingVertex==-1); }
      
  // Check element qualities in WM
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);
  const auto& ElmSet = WM.GetElements();
  for(auto& e:ElmSet)
    { assert(Quality.Compute(e)>0.); }

  // Qualities of elements in the 1-rings of positive vertices should be better than the threshold value
  const int* oneRingElms;
  int n1RingElms;
  for(auto& p:mout.PosVerts)
    {
      WM.Get1RingElements(p, &oneRingElms, n1RingElms);
      for(int indx=0; indx<n1RingElms; ++indx)
	{ assert(Quality.Compute(oneRingElms[indx])>=mparams.quality); }
    }
  
  return;
}


template<class T>
void RotateFeature(const double* center, const double rot_angle, T& curve)
{
  const double Mat[2][2] = {{std::cos(rot_angle), -std::sin(rot_angle)},
			    {std::sin(rot_angle), std::cos(rot_angle)}};
  std::vector<double> pts{};
  curve.GetControlPoints(pts);
  const int nPoints = static_cast<int>(pts.size()/2);
  for(int p=0; p<nPoints; ++p)
    {
      double X[2] = {pts[2*p]-center[0], pts[2*p+1]-center[1]};
      double Y[2] = {0.,0.};
      for(int i=0; i<2; ++i)
	{
	  Y[i] = 0.;
	  for(int j=0; j<2; ++j)
	    Y[i] += Mat[i][j]*X[j];
	  Y[i] += center[i];
	}
      pts[2*p+0] = Y[0];
      pts[2*p+1] = Y[1];
    }
  curve.SetControlPoints(pts);
  return;
}
