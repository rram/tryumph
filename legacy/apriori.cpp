// Sriramajayam

#include <iostream>
#include <string>
#include <umesh_SimpleMesh.h>
#include <um_Module>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
inline fs::path operator+(fs::path left, fs::path right){return fs::path(left)+=right;}

// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves);

// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size);

// Plot corner balls
void PlotCornerBalls(const std::string filename,
		     const std::vector<um::CornerBalls>& cnrballs);

int main()
{
  fs::path currdir = fs::current_path();
  assert(fs::exists(currdir));
  assert(fs::is_directory(currdir));
  
  fs::path geomdir = currdir + "/geom-data";
  assert(fs::exists(geomdir));
  assert(fs::is_directory(geomdir));
  
  for(auto& entry:fs::recursive_directory_iterator(geomdir))
    if(fs::is_regular_file(entry) && entry.path().extension()==".fset")
      {
	std::string gfile = entry.path().filename().string();
	
	std::cout<<"\n\nCase: "<<gfile<<std::flush;
      
	// Read features
	std::vector<um::LineSegment*> p1curves{};
	std::vector<um::QuadraticRationalBezier*> p2curves{};
	std::vector<um::CubicBezier*> p3curves{};
	CreateFeatures("geom-data/"+gfile, p1curves, p2curves, p3curves);
	const int np1curves = static_cast<int>(p1curves.size());
	const int np2curves = static_cast<int>(p2curves.size());
	const int np3curves = static_cast<int>(p3curves.size());

	// Get samples on the curve
	um::MedialAxisSamplesVec MASvec{};
	for(auto& geom:p1curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p2curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p3curves)
	  geom->GetSamples(MASvec);

	// Get a bounding box for the geometry
	double center[2], size;
	GetBoundingBox(MASvec, center, size);

	// Compute the medial axes
	um::MedialAxisSampler MAS(MASvec, 1.e-4, size, 1.e-3);
  
	/*// Print each sample
	  for(auto& geom:p1curves)
	  { const int curve_id = geom->GetCurveID();
	  std::string filename = "sample-"+std::to_string(curve_id)+".csv";
	  um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	  for(auto& geom:p2curves)
	  { const int curve_id = geom->GetCurveID();
	  std::string filename = "sample-"+std::to_string(curve_id)+".csv";
	  um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	  for(auto& geom:p3curves)
	  { const int curve_id = geom->GetCurveID();
	  std::string filename = "sample-"+std::to_string(curve_id)+".csv";
	  um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }*/
	um::MedialAxisSamples::PrintGlobalAxes(gfile+"-medial.csv", MASvec);

  
	// Nonlinear solver parameters
	um::NLSolverParams nlparams{.ftol=1.e-4, .ttol=1.e-3, .max_iter=25, .normTol=1.e-4};
  
	// Signed distance functions
	// P1
	std::vector<um::P1Segment_SDParams> p1_sdparams{};
	for(auto& geom:p1curves) p1_sdparams.push_back( um::P1Segment_SDParams{geom, &nlparams} );
	// P2
	std::vector<um::P2RationalBezier_SDParams> p2_sdparams{};
	for(auto& geom:p2curves) p2_sdparams.push_back( um::P2RationalBezier_SDParams{geom, &nlparams} );
	// P3
	std::vector<um::P3Bezier_SDParams> p3_sdparams{};
	for(auto& geom:p3curves) p3_sdparams.push_back( um::P3Bezier_SDParams{geom, &nlparams} );
  
	// Feature parameters
	std::vector<double> tpoints(4);
	std::vector<um::FeatureParams> feature_params{};
	// P1
	for(int n=0; n<np1curves; ++n)
	  { p1curves[n]->GetTerminalPoints(&tpoints[0]);
	    feature_params.push_back( um::FeatureParams{.curve_id=p1curves[n]->GetCurveID(),
		  .sdfunc=um::p1segment_sdfunc, .sdparams=&p1_sdparams[n], .terminalPoints=tpoints} ); }
	// P2
	for(int n=0; n<np2curves; ++n)
	  { p2curves[n]->GetTerminalPoints(&tpoints[0]);
	    feature_params.push_back( um::FeatureParams{.curve_id=p2curves[n]->GetCurveID(),
		  .sdfunc=um::p2rationalbezier_sdfunc, .sdparams=&p2_sdparams[n], .terminalPoints=tpoints} ); }
	// P3
	for(int n=0; n<np3curves; ++n)
	  { p3curves[n]->GetTerminalPoints(&tpoints[0]);
	    feature_params.push_back( um::FeatureParams{.curve_id=p3curves[n]->GetCurveID(),
		  .sdfunc=um::p3bezier_sdfunc, .sdparams=&p3_sdparams[n], .terminalPoints=tpoints} ); }

  
	// Create an adaptively refined background mesh for this geometry
	size *= 2.; // buffer
	umesh::SimpleMesh BG;
	BG.nodes_element = 3;
	BG.spatial_dimension = 2;
	std::vector<um::CornerBalls> cnrballs{};
	um::CreateBackgroundMesh(center, size, feature_params, MAS, MASvec,
				 BG.Coordinates, BG.Connectivity, cnrballs);
	BG.nodes = static_cast<int>(BG.Coordinates.size())/2;
	BG.elements = static_cast<int>(BG.Connectivity.size())/3;
	BG.SetupMesh();
	BG.PlotTecMesh("BG.tec");

	//PlotCornerBalls( entry.path().filename().stem().string(), cnrballs);
	
	// boundary nodes of BG cannot be disturbed
	const auto bg_bdnodes = BG.GetBoundaryNodes();
	
	// Triangulate the domain

	// Meshing parameters
	um::MeshingParams mparams{.Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10, .dnd_vertices=bg_bdnodes};
  
	std::vector<um::FeatureMeshOutput> mout{};
	um::Triangulator2D<decltype(BG)> tri(BG);
	tri.GenerateMesh(mparams, feature_params, mout);
  
	// Plot
	auto& wm = tri.GetWorkingMesh();
	wm.PlotTecMesh(gfile+"-wm.tec");
  
	// Clean up
	for(auto& it:p1curves) delete it;
	for(auto& it:p2curves) delete it;
	for(auto& it:p3curves) delete it;
      }
}


// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves)
{
  p1curves.clear();
  p2curves.clear();
  p3curves.clear();
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());

  // Header contains the file format
  // #Format: curve_id, npoles, poles, nweights, weights";
  std::string line1;
  std::getline(pfile, line1);

  // Read the number of features
  int nFeatures;
  pfile >> nFeatures;

  // Read each feature
  int curve_id, npoles, nweights;
  std::vector<double> poles, weights;
  for(int f=0; f<nFeatures; ++f)
    {
      // Read this feature's parameters
      pfile >> curve_id;
      pfile >> npoles;
      poles.resize(2*npoles);
      for(int p=0; p<2*npoles; ++p) pfile >> poles[p];
      pfile >> nweights;
      weights.resize(nweights);
      for(int p=0; p<nweights; ++p) pfile >> weights[p];

      // Create this feature
      const int nSamples = 50;
      switch(npoles)
	{
	case 2: { assert(nweights==0);
	    p1curves.push_back( new um::LineSegment(curve_id, &poles[0], &poles[2], nSamples) );
	    break; }

	case 3: { assert(nweights==npoles);
	    p2curves.push_back( new um::QuadraticRationalBezier(curve_id, poles, weights, nSamples) );
	    break; }

	case 4: { assert(nweights==0);
	    p3curves.push_back( new um::CubicBezier(curve_id, poles, nSamples) );
	    break; }

	default: assert(false && "CreateFeatures: Unknown feature type");
	}
    }

  std::cout<<"\nCreated: "
	   <<"\n"<<p1curves.size()<< " line segments"
	   <<"\n"<<p2curves.size()<< " quadratic rational bezier segments"
	   <<"\n"<<p3curves.size()<< " cubic bezier segments"
	   <<"\n"<<std::flush;
  // done
  return;
}


// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size)
{
  double coord_min[] = {MASvec[0].Point[0], MASvec[0].Point[1]};
  double coord_max[] = {coord_min[0], coord_min[1]};
  
  for(auto& m:MASvec)
    {
      const double& x = m.Point[0];
      const double& y = m.Point[1];

      if(x<coord_min[0]) coord_min[0] = x;
      else if(x>coord_max[0]) coord_max[0] = x;

      if(y<coord_min[1]) coord_min[1] = y;
      else if(y>coord_max[1]) coord_max[1] = y;
    }

  center[0] = 0.5*(coord_min[0]+coord_max[0]);
  center[1] = 0.5*(coord_min[1]+coord_max[1]);
  double hx = coord_max[0]-coord_min[0];
  double hy = coord_max[1]-coord_min[1];
  size = (hx>hy) ? hx : hy;

  return;
}


// Plot corner balls
void PlotCornerBalls(const std::string filename,
		     const std::vector<um::CornerBalls>& cnrballs)
{
  umesh::SimpleMesh MD[3];
  const int N = 20;
  for(int k=0; k<3; ++k)
    {
      MD[k].nodes = 0;
      MD[k].elements = 0;
      MD[k].Coordinates.clear();
      MD[k].Connectivity.clear();
    }
  
  const int nballs = static_cast<int>(cnrballs.size());
  for(int b=0; b<nballs; ++b)
    {
      // Append the circles to be drawn around this ball

      // Connectivities: identical for all balls
      for(int k=0; k<3; ++k)
	for(int e=0; e<N-1; ++e)
	  {
	    MD[k].Connectivity.push_back(MD[k].nodes+e);
	    MD[k].Connectivity.push_back(MD[k].nodes+e);
	    MD[k].Connectivity.push_back(MD[k].nodes+e+1);
	  }

      // Coordinates
      const double* center = cnrballs[b].center;
      const double rad[] = {cnrballs[b].rad1, cnrballs[b].rad2, cnrballs[b].rad3};
      for(int k=0; k<3; ++k)
	//if(rad[k]<1.)
	  for(int a=0; a<N; ++a)
	    {
	      double angle = (static_cast<double>(a)/static_cast<double>(N-1))*2.*M_PI;
	      MD[k].Coordinates.push_back( center[0]+rad[k]*std::cos(angle) );
	      MD[k].Coordinates.push_back( center[1]+rad[k]*std::sin(angle) );
	    }

      for(int k=0; k<3; ++k)
	{ MD[k].elements = static_cast<int>(MD[k].Connectivity.size())/3; 
	  MD[k].nodes = static_cast<int>(MD[k].Coordinates.size()/2); }
    }

  // Plot the balls
  MD[0].PlotTecMesh("b1-"+filename+".tec");
  MD[1].PlotTecMesh("b2-"+filename+".tec");
  MD[2].PlotTecMesh("b3-"+filename+".tec");

  return;
}
