// Sriramajayam

#include <iostream>
#include <string>
#include <um_Triangulator2D.h>
#include <umesh_SimpleMesh.h>
#include <um_LineSegment.h>
#include <um_QuadraticRationalBezier.h>
#include <um_CubicBezier.h>
#include <um_SignedDistanceFunction.h>
#include <um_MedialAxisSampler.h>
#include <um_TurningRadius.h>
#include <um_Quadtree.h>
#include <boost/filesystem.hpp>

// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves);

// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size);

//! Create a background universal mesh using medial axis data
void CreateBackgroundMesh(const double* center,
			  const double size,
			  const um::FeatureSet& fset,
			  const um::MedialAxisSampler& MA,
			  const um::MedialAxisSamplesVec& MASvec,
			  const std::vector<um::TurningRadiusSamplesVec>& turning_radii,
			  std::vector<double>& coordinates,
			  std::vector<int>& connectivity);
  
// Correctness checks in the output
template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureOrientation& orientation,
			   const um::FeatureMeshOutput& mout,
			   const WMType& wm);

namespace fs = boost::filesystem;
inline fs::path operator+(fs::path left, fs::path right){return fs::path(left)+=right;}

void PlotBall(const std::string filename, const double* center, const double radius);


int main()
{
  fs::path currdir = fs::current_path();
  assert(fs::exists(currdir));
  assert(fs::is_directory(currdir));

  fs::path geomdir = currdir + "/geom-data";
  assert(fs::exists(geomdir));
  assert(fs::is_directory(geomdir));

  for(auto& entry:fs::recursive_directory_iterator(geomdir))
    if(fs::is_regular_file(entry) && entry.path().extension()==".fset")
      {
	const std::string gfile = entry.path().filename().stem().string();
	std::cout<<"\nProcessing geometry: "<<gfile<<std::flush;
	
	// output path for this case
	std::string output_path = currdir.string()+"/output-"+gfile+"/";
	
	// Read features
	std::vector<um::LineSegment*> p1curves{};
	std::vector<um::QuadraticRationalBezier*> p2curves{};
	std::vector<um::CubicBezier*> p3curves{};
	CreateFeatures("geom-data/"+gfile+".fset", p1curves, p2curves, p3curves);
	const int np1curves = static_cast<int>(p1curves.size());
	const int np2curves = static_cast<int>(p2curves.size());
	const int np3curves = static_cast<int>(p3curves.size());

	// Get samples on the curve
	um::MedialAxisSamplesVec MASvec{};
	for(auto& geom:p1curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p2curves)
	  geom->GetSamples(MASvec);
	for(auto& geom:p3curves)
	  geom->GetSamples(MASvec);

	// Get a bounding box for the geometry
	double center[2], size;
	GetBoundingBox(MASvec, center, size);

	// Compute the medial axes
	um::MedialAxisSampler MAS(MASvec, 1.e-4, size, 1.e-3);
  
	// Print each sample
	for(auto& geom:p1curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	for(auto& geom:p2curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	for(auto& geom:p3curves)
	  { const int curve_id = geom->GetCurveID();
	    std::string filename = output_path+"fmedial-"+std::to_string(curve_id)+".csv";
	    um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, MASvec); }
	um::MedialAxisSamples::PrintGlobalAxes(output_path+"gmedial.csv", MASvec);


	// Get samples on each feature for computing turning radii
	std::vector<um::TurningRadiusSamplesVec> turning_radii{};

	// Compute the turning radius based on an angle threshold
	const double angle_threshold = 15.*M_PI/180.;
	
	for(auto& geom:p1curves)
	  { um::TurningRadiusSamplesVec trads{};
	    geom->GetSamples(trads);
	    um::ComputeFeatureTurningRadius(angle_threshold, trads);
	    turning_radii.push_back(trads);
	  }
	for(auto& geom:p2curves)
	  { um::TurningRadiusSamplesVec trads{};
	    geom->GetSamples(trads);
	    um::ComputeFeatureTurningRadius(angle_threshold, trads);
	    turning_radii.push_back(trads); }
	int findx = 0;
	for(auto& geom:p3curves)
	  { um::TurningRadiusSamplesVec trads{};
	    geom->GetSamples(trads);
	    um::ComputeFeatureTurningRadius(angle_threshold, trads);
	    turning_radii.push_back(trads);

	    // Plot balls
	    int i=0; 
	    for(auto& data:trads)
	      {
		std::string filename = "f"+std::to_string(findx)+"-s"+std::to_string(i)+".tec";
		PlotBall(filename, trads[i].Pt, trads[i].radius);
		++i;
	      }
	    ++findx;
	  }

	// Nonlinear solver parameters
	um::NLSolverParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
	// Signed distance functions
	// P1
	std::vector<um::P1Segment_SDParams> p1_sdparams{};
	for(auto& geom:p1curves) p1_sdparams.push_back( um::P1Segment_SDParams{geom, &nlparams} );
	// P2
	std::vector<um::P2RationalBezier_SDParams> p2_sdparams{};
	for(auto& geom:p2curves) p2_sdparams.push_back( um::P2RationalBezier_SDParams{geom, &nlparams} );
	// P3
	std::vector<um::P3Bezier_SDParams> p3_sdparams{};
	for(auto& geom:p3curves) p3_sdparams.push_back( um::P3Bezier_SDParams{geom, &nlparams} );
  
	// Feature set
	um::FeatureSet fset(1.e-4);
	// P1
	for(int n=0; n<np1curves; ++n)
	  { um::FeatureParams fp{.curve_id=p1curves[n]->GetCurveID(), .sdfunc=um::p1segment_sdfunc, .sdparams=&p1_sdparams[n]};
	    p1curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p1curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }
	// P2
	for(int n=0; n<np2curves; ++n)
	  { um::FeatureParams fp{.curve_id=p2curves[n]->GetCurveID(), .sdfunc=um::p2rationalbezier_sdfunc, .sdparams=&p2_sdparams[n]};
	    p2curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p2curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }
	// P3
	for(int n=0; n<np3curves; ++n)
	  { um::FeatureParams fp{.curve_id=p3curves[n]->GetCurveID(), .sdfunc=um::p3bezier_sdfunc, .sdparams=&p3_sdparams[n]};
	    p3curves[n]->GetTerminalPoints(fp.terminalPoints);
	    p3curves[n]->GetSplittingPoint(fp.splittingPoint);
	    fset.AppendFeature(fp); }

	// Process the feature set
	fset.Finalize();
  
	// Create an adaptively refined background mesh for this geometry
	size *= 2.; // buffer
	umesh::SimpleMesh BG;
	BG.nodes_element = 3;
	BG.spatial_dimension = 2;
	CreateBackgroundMesh(center, size, fset, MAS, MASvec, turning_radii, BG.Coordinates, BG.Connectivity);
	BG.nodes = static_cast<int>(BG.Coordinates.size())/2;
	BG.elements = static_cast<int>(BG.Connectivity.size())/3;
	BG.SetupMesh();
	BG.PlotTecMesh(output_path+"/BG.tec");

	// boundary nodes of BG cannot be disturbed
	const auto bg_bdnodes = BG.GetBoundaryNodes();
	
	// Meshing parameters
	um::MeshingParams mparams{.Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10, .quality=0.01};

	
	// Mesh
	um::RunInfo run_info;
	run_info.output_path = output_path;
	std::vector<um::FeatureMeshOutput> mout{};
	um::Triangulator2D<umesh::SimpleMesh> tri2D(BG);
	tri2D.GenerateMesh(mparams, fset, mout, run_info);

	auto& WM = tri2D.GetWorkingMesh();
	WM.PlotTecMesh(output_path+"wm.tec");

	// Run correctness checks on the computed mesh
	const int nFeatures = fset.GetNumFeatures();
	assert(static_cast<int>(mout.size())==nFeatures);
	const auto& gparams = fset.GetFeatureParams();
	const auto& orientations = fset.GetFeatureOrientations();
	for(int f=0; f<nFeatures; ++f)
	  TestFeatureMeshOutput(gparams[f], mparams, orientations[f], mout[f], WM);

	// Clean up
	for(auto& it:p1curves) delete it;
	for(auto& it:p2curves) delete it;
	for(auto& it:p3curves) delete it;
      }
}



// Create features
void CreateFeatures(const std::string filename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves)
{
  p1curves.clear();
  p2curves.clear();
  p3curves.clear();
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());

  // Header contains the file format
  // #Format: curve_id, npoles, poles, nweights, weights";
  std::string line1;
  std::getline(pfile, line1);

  // Read the number of features
  int nFeatures;
  pfile >> nFeatures;

  // Read each feature
  int curve_id, npoles, nweights;
  std::vector<double> poles, weights;
  for(int f=0; f<nFeatures; ++f)
    {
      // Read this feature's parameters
      pfile >> curve_id;
      pfile >> npoles;
      poles.resize(2*npoles);
      for(int p=0; p<2*npoles; ++p) pfile >> poles[p];
      pfile >> nweights;
      weights.resize(nweights);
      for(int p=0; p<nweights; ++p) pfile >> weights[p];

      // Create this feature
      const int nSamples = 100;
      switch(npoles)
	{
	case 2: { assert(nweights==0);
	    p1curves.push_back( new um::LineSegment(curve_id, &poles[0], &poles[2], nSamples) );
	    break; }

	case 3: { assert(nweights==npoles);
	    p2curves.push_back( new um::QuadraticRationalBezier(curve_id, poles, weights, nSamples) );
	    break; }

	case 4: { assert(nweights==0);
	    p3curves.push_back( new um::CubicBezier(curve_id, poles, nSamples) );
	    break; }

	default: assert(false && "CreateFeatures: Unknown feature type");
	}
    }
  pfile.close();

  std::cout<<"\nCreated: "
	   <<"\n"<<p1curves.size()<< " line segments"
	   <<"\n"<<p2curves.size()<< " quadratic rational bezier segments"
	   <<"\n"<<p3curves.size()<< " cubic bezier segments"
	   <<"\n"<<std::flush;
  // done
  return;
}


// Determine a bounding box 
void GetBoundingBox(const um::MedialAxisSamplesVec& MASvec,
		    double* center, double& size)
{
  double coord_min[] = {MASvec[0].Point[0], MASvec[0].Point[1]};
  double coord_max[] = {coord_min[0], coord_min[1]};
  
  for(auto& m:MASvec)
    {
      const double& x = m.Point[0];
      const double& y = m.Point[1];

      if(x<coord_min[0]) coord_min[0] = x;
      else if(x>coord_max[0]) coord_max[0] = x;

      if(y<coord_min[1]) coord_min[1] = y;
      else if(y>coord_max[1]) coord_max[1] = y;
    }

  center[0] = 0.5*(coord_min[0]+coord_max[0]);
  center[1] = 0.5*(coord_min[1]+coord_max[1]);
  double hx = coord_max[0]-coord_min[0];
  double hy = coord_max[1]-coord_min[1];
  size = (hx>hy) ? hx : hy;

  return;
}


template<typename WMType>
void TestFeatureMeshOutput(const um::FeatureParams& gparam,
			   const um::MeshingParams& mparams,
			   const um::FeatureOrientation& orientation,
			   const um::FeatureMeshOutput& mout,
			   const WMType& WM)
{
  // curve id
  assert(gparam.curve_id==mout.curve_id);

  // orientation
  assert(orientation!=um::FeatureOrientation::Unassigned);
  
  // end points
  for(int p=0; p<2; ++p)
    { const int& n = mout.terminalVertices[p];
      assert(n>=0);
      const double* X = WM.coordinates(n);
      const double* Y = &gparam.terminalPoints[2*p];
      double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
      assert(dist<1.e-4); // Should be comparable to the merging tolerance used in meshing params.
    }

  // positive vertices should start and terminate at the end points
  assert(static_cast<int>(mout.PosVerts.size())>=2);
  assert(mout.terminalVertices[0]==mout.PosVerts.front());
  assert(mout.terminalVertices[1]==mout.PosVerts.back());

  // positive vertices should lie on the feature
  for(auto& p:mout.PosVerts)
    {
      const double* X = WM.coordinates(p);
      double sd;
      bool onFeature;
      gparam.sdfunc(X, onFeature, sd, nullptr, gparam.sdparams);
      assert(std::abs(sd)<1.e-3);
    }

  // no repeated positive vertex
  std::set<int> pset(mout.PosVerts.begin(), mout.PosVerts.end());
  assert(pset.size()==mout.PosVerts.size());
  
  // check that the splitting vertex is a positive vertex
  if(orientation==um::FeatureOrientation::Split_Positive2Negative || orientation==um::FeatureOrientation::Split_Negative2Positive)
    {
      assert(mout.splittingVertex!=-1);
      bool flag = false;
      for(auto& v:mout.PosVerts)
	if(v==mout.splittingVertex)
	  { flag=true; break; }
      assert(flag==true);
    }
  else
    { assert(mout.splittingVertex==-1); }
      
  // Check element qualities in WM
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);
  const auto& ElmSet = WM.GetElements();
  for(auto& e:ElmSet)
    { assert(Quality.Compute(e)>0.); }
  
  // Qualities of elements in the 1-rings of positive vertices should be better than the threshold value
  const int* oneRingElms;
  int n1RingElms;
  for(auto& p:mout.PosVerts)
    {
      WM.Get1RingElements(p, &oneRingElms, n1RingElms);
      for(int indx=0; indx<n1RingElms; ++indx)
	{ assert(Quality.Compute(oneRingElms[indx])>=mparams.quality); }
    }

  return;
}


// Plot balls
void PlotBall(const std::string filename, const double* center, const double radius)
{
  // HACK HACK HACK
  return;
  
  umesh::SimpleMesh MD;
  const int N = 20;
  MD.nodes = 0;
  MD.elements = 0;
  MD.Coordinates.clear();
  MD.Connectivity.clear();

  // connectivities
  for(int e=0; e<N-1; ++e)
    {
      MD.Connectivity.push_back(MD.nodes+e);
      MD.Connectivity.push_back(MD.nodes+e);
      MD.Connectivity.push_back(MD.nodes+e+1);
    }

  // Coordinates
  for(int a=0; a<N; ++a)
    {
      double angle = (static_cast<double>(a)/static_cast<double>(N-1))*2.*M_PI;
      MD.Coordinates.push_back( center[0]+radius*std::cos(angle) );
      MD.Coordinates.push_back( center[1]+radius*std::sin(angle) );
    }
  
  MD.elements = static_cast<int>(MD.Connectivity.size())/3; 
  MD.nodes = static_cast<int>(MD.Coordinates.size()/2); 
  
  // Plot
  MD.PlotTecMesh(filename);
}


// Create a background universal mesh using medial axis data
void CreateBackgroundMesh(const double* center, const double size,
			  const um::FeatureSet& fset,
			  const um::MedialAxisSampler& MA,
			  const um::MedialAxisSamplesVec& MASvec,
			  const std::vector<um::TurningRadiusSamplesVec>& turning_radii,
			  std::vector<double>& coordinates,
			  std::vector<int>& connectivity)
{
  const double qfactor = 2.75; // Ratio of quadtree node to triangle sizes
    
  coordinates.clear();
  connectivity.clear();
  const int nFeatures = fset.GetNumFeatures();
  assert(nFeatures==static_cast<int>(turning_radii.size()));
  const auto& gparams = fset.GetFeatureParams();
  const int& nTerminals = fset.GetNumTerminals();
  const auto& terminalVertices = fset.GetFeaturesToTerminalsMap();
  const auto& t2F = fset.GetTerminalsToFeaturesMap();
        
  // 1. Adapt the quadtree based on mesh size restrictions at the corners
  std::vector<double> rstar(nTerminals, size);
    
  // temporaries
  double out_dist, in_dist;
  double out_center[2], in_center[2];
  for(int f=0; f<nFeatures; ++f)
    {
      const int& curve_id = gparams[f].curve_id;
      const auto& tVerts = terminalVertices[f];
	
      // 1a. Based on distances to neighboring terminals: d(pj,pj+1)/2
      if(tVerts[0]>=0 && tVerts[1]>=0)
	{
	  const double* X = &gparams[f].terminalPoints[0];
	  const double* Y = &gparams[f].terminalPoints[2];
	  double hreq = 0.5*std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	  for(int k=0; k<2; ++k)
	    {
	      if(rstar[tVerts[k]]>hreq)
		rstar[tVerts[k]] = hreq;
	    }
	}

      // 1b. Based on distance to the medial axes of this feature: d(pi, cut(Cj))
      for(int k=0; k<2; ++k)
	if(tVerts[k]>=0)
	  {
	    const double* corner = &gparams[f].terminalPoints[2*k];
	    MA.GetDistanceToMedialAxis(curve_id, corner, out_dist, out_center, in_dist, in_center);
	    if(rstar[tVerts[k]]>out_dist) rstar[tVerts[k]] = out_dist;
	    if(rstar[tVerts[k]]>in_dist)  rstar[tVerts[k]] = in_dist;
	  }
    }

  // 1c. Based on distances to other features

  // Invert terminalVertices to create a map from terminalVertices to curve indices
  // vert2CurveMap[n][0] = index of feature for which 'n' is the left vertex
  // vert2CurveMap[n][1] = index of feature for which 'n' is the right vertex
  std::vector<std::array<int,2>> vert2CurveMap(nTerminals, std::array<int,2>{-1,-1});
  for(int n=0; n<nFeatures; ++n)
    for(int i=0; i<2; ++i)
      { const int& vert = terminalVertices[n][i];
	vert2CurveMap[vert][i] = n; }

  for(int p=0; p<nTerminals; ++p)
    {
      // Coordinates of this terminal
      int kp = 0;
      if(vert2CurveMap[p][0]==-1) kp = 1;
      assert(vert2CurveMap[p][kp]!=-1);
      const int& fp = vert2CurveMap[p][kp];
	
      // 'p' is the kp-th terminal of feature #fp
      // Access the coordinates of corner #p
      const double* corner = &gparams[fp].terminalPoints[2*kp];

      // restrict rstar[p] based on its distance to components that it does not belong to: d(pi, Cj)/2
      for(int f=0; f<nFeatures; ++f)
	if(vert2CurveMap[p][0]!=f && vert2CurveMap[p][1]!=f)
	  // p does not belong to feature #f
	  {
	    // Distance of p to feature #f
	    double dist, cpt[2];
	    MA.GetDistance(gparams[f].curve_id, corner, dist, cpt);
	    if(rstar[p]>0.5*dist)
	      rstar[p] = 0.5*dist;
	  }
    }
		     
  // rstar for each terminal is computed
		     
  // Disable other mesh size requirements for sample points within the r* neighborhood of the corners
  const int nSamples = static_cast<int>(MASvec.size());
  std::vector<double> hsizes(nSamples, size);
  std::vector<bool> is_size_set(nSamples, false);
  for(int f=0; f<nFeatures; ++f)
    {
      const auto& tVerts = terminalVertices[f];
      for(int k=0; k<2; ++k)
	if(tVerts[k]!=-1)
	  {
	    // This terminal vertex
	    const int& p = tVerts[k];
	    const double* cnr = &gparams[f].terminalPoints[2*k];

	    // r* for this terminal
	    const double& rad = rstar[p];

	    // Get samples of feature #f within radius 'rad' of this corner
	    std::vector<std::pair<um::boost_point2D, int>> samples{};
	    MA.GetSamples(gparams[f].curve_id, cnr, rad, samples);

	    // The mesh sizes at these samples should be set to r*
	    for(auto& it:samples)
	      { const auto& n = it.second;
		hsizes[n] = rad;
		is_size_set[n] = true; }
	  }
    }

  // Set the mesh size requirement at all remaining sample points
  for(int n=0; n<nSamples; ++n)
    if(is_size_set[n]==false && MASvec[n].is_global_axis_computed==true)
      {
	// distance of this sample point to the global medial axis
	const double& r1 = MASvec[n].global_inner.radius;
	const double& r2 = MASvec[n].global_outer.radius;
	hsizes[n] = (r1<r2) ? r1 : r2;
	is_size_set[n] = true;
      }

  // Create a quadtree with given center and size
  um::Quadtree QT(center, size);

  // Impose mesh size requirement on the quadtreee at each sample point
  for(int n=0; n<nSamples; ++n)
    if(is_size_set[n]==true)
      QT.Refine(MASvec[n].Point, qfactor*hsizes[n]);

  // Plot the tree
  QT.PlotTec("QT-medial.tec");
    
  // Refine along each feature based on the turning radius
  for(auto& trads:turning_radii)
    for(auto& data:trads)
      QT.Refine(data.Pt, qfactor*data.radius);
    
  // Plot the tree
  QT.PlotTec("QT-turning.tec");
    
  // Balance the quadtree
  QT.StronglyBalanceTree();

  // Plot the tree
  QT.PlotTec("QT-bal.tec");

  // Triangulate
  QT.Triangulate(coordinates, connectivity);
    
  // done
  return;
}
