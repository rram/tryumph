// Sriramajam

#include <iostream>
#include <json.hpp>
#include <string>
#include <umesh_SimpleMesh.h>
#include <um_Module>

using json = nlohmann::json;

// Create feature curves using a .json files
void CreateFeatures(const std::string jsonfilename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves);

int main()
{
  // Create a background mesh of equilateral triangles
  umesh::SimpleMesh BG;
  const double origin[] = {0.,0.};
  umesh::CreateEquilateralMesh(BG, origin, 1., 4);
  BG.SetupMesh();
  //umesh::CreateSquareMesh(BG, origin, 0.75, 6);
  BG.PlotTecMesh("bg.tec");
  
  // Read feature curves (line segments, quadratic rational beziers and cubic beziers)
  std::vector<um::LineSegment*> p1curves{};
  std::vector<um::QuadraticRationalBezier*> p2curves{};
  std::vector<um::CubicBezier*> p3curves{};
  CreateFeatures("sqcirc.json", p1curves, p2curves, p3curves);
  const int np1curves = static_cast<int>(p1curves.size());
  const int np2curves = static_cast<int>(p2curves.size());
  const int np3curves = static_cast<int>(p3curves.size());

  // Solver parameters
  um::NLSolverParams nlparams{.ftol=1.e-4, .ttol=1.e-3, .max_iter=25, .normTol=1.e-4};

  // Feature parameters
  std::vector<um::FeatureParams> feature_params{};
  std::vector<double> terminalPoints(4);
  
  // P1 features
  std::vector<um::P1Segment_SDParams> p1_sdparams{};
  for(auto& geom:p1curves)
    p1_sdparams.push_back( um::P1Segment_SDParams{geom, &nlparams} );
  for(int n=0; n<np1curves; ++n)
    {
      p1curves[n]->GetTerminalPoints(&terminalPoints[0]);
      feature_params.push_back( um::FeatureParams{.sdfunc=um::p1segment_sdfunc,
	    .sdparams=&p1_sdparams[n], .terminalPoints=terminalPoints} );
    }
   
  // P2 features
  std::vector<um::P2RationalBezier_SDParams> p2_sdparams{};
  for(auto& geom:p2curves)
    p2_sdparams.push_back( um::P2RationalBezier_SDParams{geom, &nlparams} );
  for(int n=0; n<np2curves; ++n)
    {
      p2curves[n]->GetTerminalPoints(&terminalPoints[0]);
      feature_params.push_back( um::FeatureParams{.sdfunc=um::p2rationalbezier_sdfunc,
	    .sdparams=&p2_sdparams[n], .terminalPoints=terminalPoints} );
    }
  
  // P3 features
  std::vector<um::P3Bezier_SDParams> p3_sdparams{};
  for(auto& geom:p3curves)
    p3_sdparams.push_back( um::P3Bezier_SDParams{geom, &nlparams} );
  for(int n=0; n<np3curves; ++n)
    {
      p3curves[n]->GetTerminalPoints(&terminalPoints[0]);
      feature_params.push_back( um::FeatureParams{.sdfunc=um::p3bezier_sdfunc,
	    .sdparams=&p3_sdparams[n], .terminalPoints=terminalPoints} );
    }
  
  // Meshing parameters
  um::MeshingParams mparams{.Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=10};
  
  // Collect sampling points from splines
  std::vector<um::MedialAxisSamplesVec> mDatas(np1curves+np2curves+np3curves);
  for(int i=0; i<np1curves; ++i)
    { auto& m = mDatas[i];
      p1curves[i]->GetSamples(m); }
  for(int i=0; i<np1curves; ++i)
    { auto& m = mDatas[np1curves+i];
      p2curves[i]->GetSamples(m); }
  for(int i=0; i<np3curves; ++i)
    { auto& m = mDatas[np1curves+np2curves+i];
      p3curves[i]->GetSamples(m); }

  // Flatten sampling points
  um::MedialAxisSamplesVec mdata{};
  for(auto& it:mDatas)
    for(auto& jt:it)
      mdata.push_back( jt );

  // Create medial axis sampler
  const double tolEPS = 1.e-6;
  const double maxRadius = 1.;
  const double exclusion_radius = 1.e-3;
  um::MedialAxisSampler mas(mdata, exclusion_radius, maxRadius, tolEPS);
  um::MedialAxisSamples::PrintSamples("samples.csv", mdata);
  //um::MedialAxisSamples::PrintGlobalAxes("gmedial.csv", mdata);
  /*for(auto& curve:p3curves)
    { const int curve_id = curve->GetCurveID();
    std::string filename = "arc-"+std::to_string(curve_id)+".csv";
    um::MedialAxisSamples::PrintSamples(curve_id, filename, mdata);
    //std::string filename = "medial-"+std::to_string(curve_id)+".csv";
    //um::MedialAxisSamples::PrintFeatureAxes(curve_id, filename, mdata);
    }*/
  
  // Generate a mesh
  std::vector<um::FeatureMeshOutput> mout{};
  um::Triangulator2D<decltype(BG)> tri(BG);
  tri.GenerateMesh(mparams, feature_params, mout);

  // Plot
  auto& wm = tri.GetWorkingMesh();
  wm.PlotTecMesh("wm.tec");
  
  // Clean up
  for(auto& it:p1curves) delete it;
  for(auto& it:p2curves) delete it;
  for(auto& it:p3curves) delete it;
}

void CreateFeatures(const std::string jsonfilename,
		    std::vector<um::LineSegment*>& p1curves,
		    std::vector<um::QuadraticRationalBezier*>& p2curves,
		    std::vector<um::CubicBezier*>& p3curves)
{
  // Read the json file for curves/segments
  std::fstream jfile;
  jfile.open(jsonfilename.c_str(), std::ios::in);
  assert(jfile.good());
  json j;
  jfile >> j;
  jfile.close();

  double coord_min[2] = {1.e10, 1.e10};
  double coord_max[2] = {-1.e10, -1.e10};
   
  // Read all control points
  for(auto& it:j)
    {
      if(it["type"]=="Line")
	{
	  auto start = it["start"];
	  auto end = it["end"];
	  int k = 0;
	  for(auto& x:start)
	    { if(x<coord_min[k]) coord_min[k] = x;
	      if(x>coord_max[k]) coord_max[k] = x;
	      ++k; }
	  k = 0;
	  for(auto& x:end)
	    { if(x<coord_min[k]) coord_min[k] = x;
	      if(x>coord_max[k]) coord_max[k] = x; }
	}
      else if(it["type"]=="RationalBezier" || it["type"]=="BezierCurve")
	{
	  auto poles = it["poles"];
	  for(auto& p:poles)
	    { int k=0; 
	      for(auto& x:p)
		{ if(x<coord_min[k]) coord_min[k] = x;
		  if(x>coord_max[k]) coord_max[k] = x;
		  ++k; } }
	}
      else assert(false && "Unknown feature type encountered");
    }

   
  // Rescale domain to a unit square centered at the origin
  double center[] = {0.5*(coord_min[0]+coord_max[0]), 0.5*(coord_min[1]+coord_max[1])};
  double size =
    (coord_max[0]-coord_min[0])>(coord_max[1]-coord_min[1]) ?
    (coord_max[0]-coord_min[0]) :
    (coord_max[1]-coord_min[1]);

  // Read features
  p1curves.clear();
  p2curves.clear();
  p3curves.clear();
  const int nsamples = 40;
  for(auto& it:j)
    {
      int curve_id = it["curve_id"];
      
      if(it["type"]=="Line")
	{
	  auto start = it["start"];
	  auto end = it["end"];
	  // start -> A, end -> B
	  int k;
	  double A[2], B[2];
	  k=0; for(auto& x:start) A[k++] = x;
	  k = 0; for(auto& x:end) B[k++] = x;
	  for(int i=0; i<2; ++i)
	    { A[i] -= center[i]; A[i] /= size;
	      B[i] -= center[i]; B[i] /= size; }
	  p1curves.push_back( new um::LineSegment(curve_id, A, B, nsamples) );
	}
      else if(it["type"]=="RationalBezier")
	{
	  auto poles = it["poles"];
	  std::vector<double> points{};
	  for(auto& it:poles)
	    for(auto& jt:it)
	      points.push_back( jt );
	  assert(static_cast<int>(points.size())==6);
	  for(int p=0; p<3; ++p)
	    { points[2*p]   -= center[0]; points[2*p] /= size;
	      points[2*p+1] -= center[1]; points[2*p+1] /= size; }
	  auto wts = it["weigths"];
	  std::vector<double> weights{};
	  for(auto& it:wts)
	    weights.push_back(it);
	  assert(static_cast<int>(weights.size())==3);
	  p2curves.push_back( new um::QuadraticRationalBezier(curve_id, points, weights, nsamples) );
	}
      else if(it["type"]=="BezierCurve")
	{
	  auto poles = it["poles"];
	  std::vector<double> points{};
	  for(auto& it:poles)
	    for(auto& jt:it)
	      points.push_back( jt );
	  assert(static_cast<int>(points.size())==8);
	  for(int p=0; p<4; ++p)
	    { points[2*p]   -= center[0]; points[2*p] /= size;
	      points[2*p+1] -= center[1]; points[2*p+1] /= size; }
	  p3curves.push_back( new um::CubicBezier(curve_id, points, nsamples) );
	}
    }
      
  std::cout<<"\nCreated: "
	   <<"\n"<<p1curves.size()<< " line segments"
	   <<"\n"<<p2curves.size()<< " quadratic rational bezier segments"
	   <<"\n"<<p3curves.size()<< " cubic bezier segments"
	   <<"\n"<<std::flush;
      
  // done
  return;
}


