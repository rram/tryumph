// Sriramajayam

#pragma once

#include <um2_WorkingMesh.h>
#include <set>

namespace um2
{
  namespace algo
  {

    // Identify positive-edge-boundary intersections
    template<typename MeshType>
      void InspectPositiveEdgeBoundaryIntersection(const std::set<int>& frozen_nodes,
						   WorkingMesh<MeshType>& WM,
						   std::set<int>& refine_Verts)
      {
	assert(WM.getMeshStatus()==WorkingMeshStatus::POSITIVE_EDGES_IDENTIFIED);
	if(frozen_nodes.empty())
	  return;
	
	// Inspect only features that are successfully processed
	// Accummulate all positive vertices in a set
	// and examine the intersection with boundary nodes
	auto& fm_correspondences = WM.getSplitFeatureCorrespondences();
	for(auto& fm:fm_correspondences)
	  if(fm.reason==MeshingReason::Success)
	    {
	      std::set<int> posverts(fm.PosVerts.begin(), fm.PosVerts.end());
	      std::set<int> intersection{};
	      std::set_intersection(posverts.begin(), posverts.end(),
				    frozen_nodes.begin(), frozen_nodes.end(),
				    std::inserter(intersection, intersection.begin()));
	      if(intersection.empty()==false)
		{
		  fm.reason = MeshingReason::Fail_PositiveEdge_BoundaryProximity;
		  for(auto& v:intersection)
		    refine_Verts.insert(v);
		}
	    }
	return;
      }
    
  } // um2::algo
} // um2
