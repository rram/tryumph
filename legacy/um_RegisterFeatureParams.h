// Sriramajayam

#ifndef UM_GEOM_REGISTER_FEATURE_PARAMS_H
#define UM_GEOM_REGISTER_FEATURE_PARAMS_H

#include <boost/bind/bind.hpp>
#include <um_FeatureParams.h>

namespace um
{
  template<class GeomType>
    void RegisterFeatureParams(GeomType& obj, FeatureParams& params)
    {
      // curve id
      params.curve_id = obj.GetCurveID();

      // signed distance -> boost::function<void(const double* X, double& sd, double* dsd)> 
      params.sdfunc   = boost::bind(&GeomType::GetSignedDistance, &obj,
				    boost::placeholders::_1,              
				    boost::placeholders::_2,              
				    boost::placeholders::_3);             
      
      // sampling -> boost::function<void(std::vector<std::array<double,2>>& samples)>
      params.samplefunc = boost::bind(&GeomType::GetSampling, &obj,
				      boost::placeholders::_1);        
      
      // feature size -> boost::function<double(const double* X)>
      params.fsfunc = boost::bind(&GeomType::GetFeatureSize, &obj,
				  boost::placeholders::_1);

      // splitting -> boost::function<std::pair<Featureparams, Featureparams>(const int left_id, const int right_id)>
      params.splitfunc = boost::bind(&GeomType::Split, &obj,
				     boost::placeholders::_1,
				     boost::placeholders::_2);

      // terminal points
      obj.GetTerminalPoints(params.terminalPoints);

      // splitting point
      obj.GetSplittingPoint(params.splittingPoint);

      // done
      return;
    }
}

#endif
