// Sriramajayam

#include <um_TurningRadius.h>
#include <cmath>

namespace um
{
  void ComputeFeatureTurningRadius(const double angle_threshold,
				   std::vector<TurningRadiusSamples>& data)
  {
    const int nSamples = static_cast<int>(data.size());
    std::vector<double> dtheta(nSamples-1, 0.);

    // Relative changes in angles
    for(int i=0; i<nSamples-1; ++i)
      {
	const double* t1 = data[i].tgt;
	const double* t2 = data[i+1].tgt;
	dtheta[i] = std::atan2(t1[0]*t2[1]-t1[1]*t2[0], t1[0]*t2[0]+t1[1]*t2[1]);
      }

    // Determine the turning radius for each sample point
    double angle_sum;
    int j_indx;
    double forward_rad, backward_rad;
    for(int i=0; i<nSamples; ++i)
      {
	// Forward march
	angle_sum = 0.;
	j_indx = -1;
	for(int j=i+1; j<nSamples; ++j)
	  {
	    angle_sum += dtheta[j-1];
	    if(std::abs(angle_sum)>angle_threshold)
	      { j_indx = j;
		break; }
	  }
	
	// If the angle_sum exceeded the threshold
	if(j_indx!=-1)
	  {
	    const double* P = data[i].Pt;
	    const double* Q = data[j_indx].Pt;
	    forward_rad = std::sqrt((P[0]-Q[0])*(P[0]-Q[0])+(P[1]-Q[1])*(P[1]-Q[1]));
	  }
	else
	  forward_rad = std::numeric_limits<double>::max();

	// Backward march
	angle_sum = 0.;
	j_indx = -1;
	double backward_rad;
	for(int j=i-1; j>=0; --j)
	  {
	    angle_sum += dtheta[j];
	    if(std::abs(angle_sum)>angle_threshold)
	      { j_indx = j;
		break; }
	  }

	if(j_indx!=-1)
	  {
	    const double* P = data[i].Pt;
	    const double* Q = data[j_indx].Pt;
	    backward_rad = std::sqrt((P[0]-Q[0])*(P[0]-Q[0])+(P[1]-Q[1])*(P[1]-Q[1]));
	  }
	else
	  backward_rad = std::numeric_limits<double>::max();

	// Define the radius at this sample point as min(forward_rad, backward_rad)
	if(forward_rad<backward_rad)
	  data[i].radius = forward_rad;
	else
	  data[i].radius = backward_rad;
      }

    // done
    return;
  }

  
} // um::
