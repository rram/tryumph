// Sriramajayam

#include <um_CubicSpline.h>
#include <um_TurningRadius.h>
#include <fstream>

using namespace um;

int main()
{
  // semi-circle
  const int nSamples = 181;
  TurningRadiusSamplesVec data(nSamples);
  for(int i=0; i<nSamples; ++i)
    {
      double theta = M_PI*static_cast<double>(i)/static_cast<double>(nSamples-1);
      data[i].Pt[0] = std::cos(theta);
      data[i].Pt[1] = std::sin(theta);
      data[i].tgt[0] = -std::sin(theta);
      data[i].tgt[1] = std::cos(theta);
    }

  const double angle_threshold = 90.*M_PI/180.;
  ComputeFeatureTurningRadius(angle_threshold, data);

  // TODO: Add checks
  //for(int i=0; i<nSamples; ++i)
  //std::cout<<"\n"<<data[i].radius<<std::flush;
}
