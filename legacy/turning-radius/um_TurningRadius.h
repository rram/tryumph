// Sriramajayam

#ifndef UM_TURNING_RADIUS_H
#define UM_TURNING_RADIUS_H

#include <limits>
#include <vector>

namespace um
{
  struct TurningRadiusSamples
  {
    double Pt[2]; //!< Coordinates of the point
    double tgt[2]; //!< Local tangent to the feature
    double radius; //!< Local turning radius based on  a given angle threshold
  };
    
  void ComputeFeatureTurningRadius(const double angle_threshold,
				   std::vector<TurningRadiusSamples>& data);

  using TurningRadiusSamplesVec = std::vector<TurningRadiusSamples>;
}

#endif
