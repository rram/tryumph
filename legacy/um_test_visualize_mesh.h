// Sriramajayam

#pragma once

namespace um
{
  namespace test
  {
    template<typename MeshType>
      void save_mesh_vtk(const std::string filename, const MeshType& MD)
      {
	assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	
	std::fstream out;
	out.open(filename, std::ios::out);
	assert(out.good() && out.is_open());

	// Headers
	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
	out << "ASCII" << std::endl;
	out << "DATASET POLYDATA" << std::endl;

	// nodes
	const int nNodes = MD.n_nodes();
	out << "POINTS " << nNodes << " double" << std::endl;
	for(int i=0; i<nNodes; ++i)
	  {
	    const double* X = MD.coordinates(i);
	    out << X[0] << " " << X[1] << " " << 0. << std::endl;
	  }

	// polygons = triangles
	const int nElements = MD.n_elements();
	// cell size = total number of integers in the list
	const int cell_size = nElements*3+nElements;
	
	out << "POLYGONS " << nElements << " " << cell_size << std::endl;
	for(int e=0; e<nElements; ++e)
	  {
	    const int* conn = MD.connectivity(e);
	    out << 3 << " " << conn[0] << " " << conn[1] << " " << conn[2] << std::endl;
	  }

	// done
	out.close();
      }
    
  } // um::test
} // um::
