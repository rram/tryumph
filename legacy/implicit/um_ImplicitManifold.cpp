// Sriramajayam

#include <um_ImplicitManifold.h>
#include <cassert>
#include <cmath>

namespace um
{
  namespace detail
  {
    // Function value
    // Solve psi(X + t N(X)) = 0
    double Implicit_RootFinding_f(double t, void* params)
    {
      assert(params!=nullptr);
      auto* ctx = static_cast<Implicit_RootFinding_Context*>(params);
      assert(ctx!=nullptr);
	
      // Return the value of the level set function at the point Y = X + t*dir
      for(int k=0; k<ctx->SPD; ++k)
	ctx->Y[k] = ctx->X[k] + t*ctx->dir[k];
      ctx->LSFunc(ctx->Y, ctx->F, nullptr, ctx->LSParams);
      return ctx->F;
    }
      
    // Function derivatives
    double Implicit_RootFinding_df(double t, void* params)
    {
      assert(params!=nullptr);
      auto* ctx = static_cast<Implicit_RootFinding_Context*>(params);
      assert(ctx!=nullptr);
	
      // Evaluate the gradient of the level set function at Y = X + t*dir
      for(int k=0; k<ctx->SPD; ++k)
	ctx->Y[k] = ctx->X[k] + t*ctx->dir[k];
      ctx->LSFunc(ctx->Y, ctx->F, ctx->gradF, ctx->LSParams);
      ctx->dF = 0.;
      for(int k=0; k<ctx->SPD; ++k)
	ctx->dF += ctx->gradF[k]*ctx->dir[k];
      return ctx->dF;
    }


    // Function evaluation and derivatives
    void Implicit_RootFinding_fdf(double t, void* params, double* val, double* dval)
    {
      assert(params!=nullptr);
      auto* ctx = static_cast<Implicit_RootFinding_Context*>(params);
      assert(ctx!=nullptr);
	
      // Evaluate the point Y
      for(int k=0; k<ctx->SPD; ++k)
	ctx->Y[k] = ctx->X[k] + t*ctx->dir[k];

      // Evaluate the level set function and its derivative at Y = X + t*dir
      ctx->LSFunc(ctx->Y, ctx->F, ctx->gradF, ctx->LSParams);
      (*dval) = 0.;
      for(int k=0; k<ctx->SPD; ++k)
	(*dval) += ctx->gradF[k]*ctx->dir[k];
      (*val) = ctx->F;

      return;
    }

  } // end namespace detail


    // Constructor
  ImplicitManifold::ImplicitManifold(const int id,
				     const int edim,
				     LevelSetFunction lsfunc,
				     void* lsparams)
    :curve_id(id),
     SPD(edim)
  {
    // Setup the root finding
    rootfinding_ctx.LSFunc = lsfunc;
    rootfinding_ctx.LSParams = lsparams;
    rootfinding_ctx.SPD = SPD;
    rootfinding_fdf.f = &detail::Implicit_RootFinding_f;
    rootfinding_fdf.df = &detail::Implicit_RootFinding_df;
    rootfinding_fdf.fdf = &detail::Implicit_RootFinding_fdf;
    rootfinding_fdf.params = &rootfinding_ctx;
    rootfinding_solver = gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_steffenson);
  }
  
  // Destructor
  ImplicitManifold::~ImplicitManifold()
  { gsl_root_fdfsolver_free(rootfinding_solver); }

  // Returns the curve id
  int ImplicitManifold::GetCurveID() const
  { return curve_id; }
  
  // Computes the normal at the requested point
  void ImplicitManifold::GetNormal(const double* X, const double normTol, double* N)
  {
    // Return the normalized gradient of the implicit function

    // Gradient
    double F;
    rootfinding_ctx.LSFunc(X, F, N, rootfinding_ctx.LSParams);

    // Normalize
    double mag = 0.;
    for(int k=0; k<SPD; ++k)
      mag += N[k]*N[k];
    mag = std::sqrt(mag);
    assert(mag>normTol);
    for(int k=0; k<SPD; ++k)
      N[k] /= mag;

    return;
  }
        
  
  
  // Computes the closest point projection
  // cpt = X - phi(X) N(X)
  void ImplicitManifold::GetClosestPoint(const double* X, const NLSolverParams& nlparams,
					 double* cpt)
  {
    // Aliases
    const double& ttol = nlparams.ttol;
    const double& ftol = nlparams.ftol;
    const int& max_iter = nlparams.max_iter;
    const double& normTol = nlparams.normTol;
      
    // Initialize params
    for(int k=0; k<SPD; ++k)
      rootfinding_ctx.X[k] = X[k];
    GetNormal(X, normTol, rootfinding_ctx.dir);

    // Initialize the solver for this iteration
    double t = 0.;
    gsl_root_fdfsolver_set(rootfinding_solver, &rootfinding_fdf, t);
	  
    // Solve: psi( X + dt dpsi(X)) = 0
    double tprev = t;
    int iter = 0;
    int status = 0;
    double fval = 0.;
    while(iter<max_iter)
      {
	// One iteration
	status = gsl_root_fdfsolver_iterate(rootfinding_solver);
	t = gsl_root_fdfsolver_root(rootfinding_solver);

	// Update the location of the closest point & the normal
	for(int k=0; k<SPD; ++k)
	  cpt[k] = X[k] + t*rootfinding_ctx.dir[k];

	GetNormal(cpt, normTol, rootfinding_ctx.dir);

	// Is this solution good enough

	// Based on tolerance for the root
	status = gsl_root_test_delta(t, tprev, ttol, 0.);
	if(status==GSL_SUCCESS)
	  break;
	  
	// Based on tolerance for the function value
	fval = detail::Implicit_RootFinding_f(t, &rootfinding_ctx);
	status = gsl_root_test_residual(fval, ftol);
	if(status==GSL_SUCCESS)
	  break;

	// Otherwise, continue
	tprev = t;
	++iter;
      }

    assert(iter<max_iter);
      
    // done
    return;
  }



  // Computes the signed distance and its derivatives at the requested point
  void ImplicitManifold::GetSignedDistance(const double* X, const NLSolverParams& nlparams,
					   double& sdval, double* dSD)
  {
    // Compute the closest projection of X
    double cpt[3]; // Allocate more than what is required
    GetClosestPoint(X, nlparams, cpt);

    // Get the normal at the closest point projection
    double nvec[3];
    GetNormal(cpt, nlparams.normTol, nvec);
          
    // Compute the signed distance
    sdval = 0.;
    for(int i=0; i<SPD; ++i)
      sdval += (X[i]-cpt[i])*nvec[i];

    // Gradient
    if(dSD!=nullptr)
      for(int i=0; i<SPD; ++i)
	dSD[i] = nvec[i];

    return;
  }

}

