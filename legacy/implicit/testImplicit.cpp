// Sriramajayam

#include <um_ImplicitManifold.h>
#include <random>

struct CircParams
{
  double radius;
  double center[2];
};

// Level set function
void LSFunction(const double* X, double& F, double* dF, void* params)
{
  const auto& cp = *static_cast<CircParams*>(params);
  const double& radius = cp.radius;
  const double* center = cp.center;
  double r2 =0.;
  for(int k=0; k<2; ++k)
    r2 += (X[k]-center[k])*(X[k]-center[k]);
  F = r2 - radius*radius;
  if(dF!=nullptr)
    { for(int k=0; k<2; ++k)
	dF[k] = 2.*(X[k]-center[k]); }
  return;
}

using namespace um;

// Test class
void Test(const CircParams& cp,
	  ImplicitManifold& Surf, const NLSolverParams& nlparams);

int main()
{
  // Random center and radius
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.,1.);

  NLSolverParams nlparams{.ftol=1.e-7, .ttol=1.e-7, .max_iter=25};
   
  // test
  CircParams cp;
  cp.center[0] = dis(gen);
  cp.center[1] = dis(gen);
  cp.radius = 1.+std::abs(dis(gen));
  const int id = 4;
  ImplicitManifold Obj2D(id, 2, LSFunction, &cp);
  Test(cp, Obj2D, nlparams);
  
  // done
}


std::vector<double> GetNumericalGradSD(ImplicitManifold& Surf,
				       const NLSolverParams& nlparams,
				       const double* pt, const double h)
{
  double mypt[2];
  std::vector<double> gradSD(2);
  double sd1=0, sd2=0;
  for(int i=0; i<2; ++i)
    {
      for(int j=0; j<2; ++j)
	mypt[j] = pt[j];

      mypt[i] = pt[i]+h;
      Surf.GetSignedDistance(mypt, nlparams, sd1);

      mypt[i] = pt[i]-h;
      Surf.GetSignedDistance(mypt, nlparams, sd2);

      gradSD[i] = (sd1-sd2)/(2.*h);
    }  
  return gradSD;
}



// Compute the closest point projection map
void GetClosestPoint(const CircParams& cp, 
		     const double* X, double* closestpt)
{
  const double& radius = cp.radius;
  const double* center = cp.center;
  double dist = 0.;
  for(int i=0; i<2; ++i)
    dist += (X[i]-center[i])*(X[i]-center[i]);
  dist = std::sqrt(dist);
  assert(dist/radius>1.e-6);
  for(int i=0; i<2; ++i)
    closestpt[i] = center[i] + (X[i]-center[i])*radius/dist;
  
  return;
}


// Test class
void Test(const CircParams& cp,
	  ImplicitManifold& Surf, const NLSolverParams& nlparams)
{
  const double& radius = cp.radius;
  const double* center = cp.center;
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-3;

  // Compute the signed distance and its derivatives at a point
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.*radius, 2.*radius);
  double X[2];

  // 100 sample points
  for(int run=0; run<100; ++run)
    {
      // Generate a point away from the center
      while(true)
	{
	  double sum = 0.;
	  for(int k=0; k<2; ++k)
	    { X[k] = dis(gen);
	      sum += std::abs(X[k]-center[k]); }
	  if(sum>1.e-2) break;
	}
      double sd = 0.;
      double dSD[2];
      Surf.GetSignedDistance(X, nlparams, sd, dSD);

      // test consistency of 1st derivatives
      std::vector<double> dSDnum = GetNumericalGradSD(Surf, nlparams, X, pertEPS);
      for(int i=0; i<2; ++i)
	assert(std::abs(dSDnum[i]-dSD[i])<tolEPS);

      // test consistency of closest point projection
      double cpt[2];
      Surf.GetClosestPoint(X, nlparams, cpt);

      // Check with correct closest point
      double cptcheck[2];
      GetClosestPoint(cp, X, cptcheck);
      for(int k=0; k<2; ++k)
	assert(std::abs(cpt[k]-cptcheck[k])<tolEPS);
  
      // cpt should lie on the surface
      double zero;
      Surf.GetSignedDistance(cpt, nlparams, zero);
      assert(std::abs(zero)<tolEPS);
  
      // Check that (x-cpt).n = phi
      double normal[2];
      Surf.GetNormal(cpt, nlparams.normTol, normal);
      double dot = 0.;
      for(int k=0; k<2; ++k)
	dot += (X[k]-cpt[k])*normal[k];
      assert(std::abs(dot-sd)<tolEPS);
    }
}



