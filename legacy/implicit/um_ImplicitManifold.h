// Sriramajayam

#ifndef UM_IMPLICIT_MANIFOLD_H
#define UM_IMPLICIT_MANIFOLD_H

#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <um_Types.h>

namespace um
{
  // Forward declare
  class ImplicitManifold;

  using LevelSetFunction = std::function<void(const double*, double& F, double* dF, void* params)>;
    
  // Helper struct for GSL iterations
  namespace detail
  {
    struct Implicit_RootFinding_Context
    {
      LevelSetFunction LSFunc; //!< Level set function
      void* LSParams; //!< Parameters to pass to the level set function
      int SPD;
      double X[3], dir[3]; //!< Inputs
      double Y[3], gradF[3], F, dF;  //!< mutables. Sufficient space for 2D/3D
    };
  }
    
  //! Class to compute the signed distance function to the boundary
  //! of a domain whose level set function is specified
  class ImplicitManifold
  {
  public:
    
    //! Constructor
    //! \param[in] id Curve id
    //! \param[in] edim Embedding dimension
    //! \param[in] params Nonlinear solver parameters
    ImplicitManifold(int id,
		     const int edim, LevelSetFunction lsf, void* lsfparams);
	
    //! Destructor
    virtual ~ImplicitManifold();

    //! Disable copy and assignment
    ImplicitManifold(const ImplicitManifold&) = delete;
    ImplicitManifold operator=(const ImplicitManifold&) = delete;

    //! Returns the curve ID
    int GetCurveID() const;
      
    //! Computes the signed distance function
    //! \param[in] X Requested point 
    //! \param[out] SD, dSD Computed signed distance and derivatives
    //! \todo 2nd derivatives not implemented
    void GetSignedDistance(const double* X, const NLSolverParams& nlparams,
			   double& SD, double* dSD=nullptr);


  private:
    const int curve_id; //!< Curve id.
    const int SPD; //!< Embedding dimension
    detail::Implicit_RootFinding_Context rootfinding_ctx; //!< Context to pass for root finding
    gsl_root_fdfsolver* rootfinding_solver; //!< Solver for 1D root finding
    gsl_function_fdf rootfinding_fdf; //!< Function for root finding

  public:
    //! Helper function to evaluate the normalized gradient of the level set function
    void GetNormal(const double* X, const double normTol, double* N);

    //! Helper function to computes the closest point projection
    void GetClosestPoint(const double* X, const NLSolverParams& nlparams, double* cpt);
      
  };
  
} // um::

#endif
