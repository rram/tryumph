// Sriramajayam

#ifndef UM_ALGORITHM_PLOT_POSEDGES_H
#define UM_ALGORITHM_PLOT_POSEDGES_H

#include <fstream>
#include <string>
#include <map>
#include <um_Algorithm_Structs.h>

namespace um
{
  namespace algo
  {
    // Helper function to visualize positive edges in tecplot format
    template<typename MeshType>
      void PlotPositiveEdges(const std::string filename,
			     const MeshType& MD,
			     const std::vector<FeatureMeshOutput>& mout)
      {
	// Open file to plot
	std::fstream outfile;
	outfile.open(filename.c_str(), std::ios::out);
	outfile.precision(16);
	outfile.setf( std::ios::scientific );

	// Set of positive vertices
	std::set<int> allPosVerts{};
	int elements = 0;
	for(auto& m:mout)
	  if(static_cast<int>(m.PosVerts.size())!=0)
	    {
	      for(auto& v:m.PosVerts)
		allPosVerts.insert(v);
	      elements += static_cast<int>(m.PosVerts.size())-1; }
	const int nodes = static_cast<int>(allPosVerts.size()); 

	outfile<<"VARIABLES = \"X\", \"Y\" \n"     // Line 1
	       <<"ZONE t=\"t:0\", N="<<nodes
	       <<", E="<<elements
	       <<", F=FEPOINT, ET=TRIANGLE"; // Line 2

    
	// Nodal coordinates
	std::map<int, int> vertMap{};
	int count = 0;
	for(auto& v:allPosVerts)
	  vertMap[v] = count++;

	for(auto& v:allPosVerts)
	  { const auto* X = MD.coordinates(v);
	    outfile<<"\n"<<X[0]<<" "<<X[1]; }
  
	// Element connectivity
	for(auto& m:mout)
	  if(static_cast<int>(m.PosVerts.size())!=0)
	    {
	      const auto& PosVerts = m.PosVerts;
	      const int nEdges = static_cast<int>(PosVerts.size())-1;
	      for(int e=0; e<nEdges; ++e)
		{
		  const int& A = vertMap[PosVerts[e]];
		  const int& B = vertMap[PosVerts[e+1]];
		  outfile<<"\n"<<A+1<<" "<<A+1<<" "<<B+1;
		}
	    }
    
	outfile.flush();
	outfile.close();

	// done
	return;
      }

  } // algo::
} // um::

#endif
