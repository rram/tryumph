// Sriramajayam

#ifndef UM_VISUALIZE_PLOTTING_H
#define UM_VISUALIZE_PLOTTING_H

#include <string>
#include <vector>

namespace um
{
  template<class MeshType, class ContainerType>
    void PlotTec(const std::string filename, const MeshType& MD, const ContainerType& ElmList);

  template<class MeshType>
    void PlotTec(const std::string filename, const MeshType& MD, const std::vector<bool>& ElmList);
}

#endif

#include <um_Plotting_impl.h>
