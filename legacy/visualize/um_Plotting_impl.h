// Sriramajayam

#ifndef UM_VISUALIZE_PLOTTING_IMPL_H
#define UM_VISUALIZE_PLOTTING_IMPL_H

#include <fstream>
#include <cassert>
#include <set>

namespace um
{
  template<class MeshType, class ContainerType>
    void PlotTec(const std::string filename, const MeshType& MD, const ContainerType& ElmList)
  {
    std::fstream stream;
    stream.open(filename.c_str(), std::ios::out);
    assert(stream.good() && "Could not access write stream");
    stream.precision(16);
    stream.setf( std::ios::scientific );

    const int nNodes = MD.n_nodes();
    const int nElements = static_cast<int>(ElmList.size());
    
    // Line 1:
    stream << "VARIABLES = \"X\", \"Y\" \n";
    
    // Line 2:
    stream<<"ZONE t=\"t:0\", N="<<nNodes<<", E="<<nElements<<", F=FEPOINT, ET=TRIANGLE";
  
    // Nodal coordinates
    for(int a=0; a<nNodes; ++a)
      {
	const auto* X = MD.coordinates(a);
	stream<<"\n"<<X[0]<<" "<<X[1];
      }
  
    // Element connectivity
    for(auto& e:ElmList)
      {
	const auto* conn = MD.connectivity(e);
	stream << "\n" << conn[0]+1 <<" "<<conn[1]+1 <<" "<<conn[2]+1;
      }
    stream.flush();
    stream.close();
    return;
  }
     

  template<class MeshType>
    void PlotTec(const std::string filename, const MeshType& MD,
		 const std::vector<bool>& ElmList)
    {
      const int nElements = MD.n_elements();
      std::set<int> ElmSet{};
      for(int e=0; e<nElements; ++e)
	if(ElmList[e]==true)
	  ElmSet.insert(e);
      PlotTec(filename, MD, ElmSet);
      return;
    }
}

#endif
