// Sriramajayam

#pragma once

namespace um2
{
    //! \ingroup output
  struct MeshColoring {
    std::vector<std::list<int>> closedComponents;
    std::vector<std::vector<bool>> elmColors;
  };

  // Compute coloring for a connected component
  //! \tparam MeshType Type of the background mesh
  //! \param[in] WM        Working mesh for which to compute coloring
  //! \param[in] mout      List of mesh outputs for features that constitute a closed component
  //! \param[out] coloring Boolean valued coloring of elements in the working mesh
  //! \ingroup algo
  template<typename MeshType>
    void ComputeComponentColoring(const MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
				  const std::vector<const FeatureMeshCorrespondence*>& mout,
				  std::vector<bool>& coloring);
    
  template<typename MeshType>
    void MeshManager<MeshType>::color_WorkingMesh()
    {
      const int nElements = BG->n_elements();
      const auto& fset    = WM->get_SplitFeatureSet();
      const auto& mout    = WM->get_SplitFeature_Correspondences();
      
      // Aliases
      auto& closedComponents = wm_color.closedComponents;
      auto& elmColors        = wm_color.elmColors;
      closedComponents.clear();
      elmColors.clear();
	
      // closed connected components
      closedComponents = fset.get_closed_components();

      // Initialize element colors
      const int nClosedComponents = static_cast<int>(closedComponents.size());
      elmColors.resize(nClosedComponents, std::vector<bool>(nElements));
		
      // Color closed components
      for(int count=0; count<nClosedComponents; ++count)
	{
	  auto& compNums = closedComponents[count];
	  auto& compColoring = elmColors[count];

	  // Collect feature mesh output objects in order for this component
	  std::vector<const FeatureMeshCorrespondence*> comp_mout{};
	  for(auto& cnum:compNums)
	    comp_mout.push_back( &mout[cnum] );
	    
	  // Color this closed component
	  ComputeComponentColoring(*mesh_wrapper, comp_mout, compColoring);
	}
	
      // done
      return;
    }


  // Compute coloring for a connected component
  template<typename MeshType>
    void ComputeComponentColoring(const MeshWrapper<WorkingMesh<MeshType>>& mesh_wrapper,
				  const std::vector<const FeatureMeshCorrespondence*>& mout,
				  std::vector<bool>& coloring)
    {
      const auto& WM = mesh_wrapper.GetConstMesh();
      const auto& BG = WM.get_ParentMesh();
      const int nElements = BG.n_elements();
      coloring.resize(nElements);
      std::fill(coloring.begin(), coloring.end(), false);

      // number of faces per element
      const int num_faces = 3;

      // Accummulate a map of element-face pairs to check
      std::unordered_map<int, std::array<bool,3>> ElmFaces2Check{};

      std::vector<bool> isElmChecked(nElements, false);
      
      // Start with the list of positively cut elements
      // Add all faces excluding the positive face if orientation is positive
      // For negative orientation, treat the neighbor as positively cut
      std::array<bool,3> ElmFaceFlags{};
      int elm, face;
      for(auto& fmo:mout)
	{
	  const auto& PosCutElmFaces = fmo->PosCutElmFaces;
	  for(auto& it:PosCutElmFaces)
	    {
	      // Orientation
	      const bool& orient = std::get<2>(it);

	      // if positive, treat positive cut element and face
	      if(orient==true) 
		{
		  elm = std::get<0>(it);
		  face = std::get<1>(it);
		}
	      // otherwise treat the neighbor as positively cut for the purpose of flooding
	      else
		{
		  // Get the neighbors
		  int neg_elm = std::get<0>(it);
		  int neg_face = std::get<1>(it);
		  const int* elm_nbs = mesh_wrapper.GetElementNeighbors(neg_elm);
		  elm = elm_nbs[2*neg_face];
		  face = elm_nbs[2*neg_face+1];
		}

	      // Nothing to do for a free face
	      if(elm==-1) continue;

	      auto jt = ElmFaces2Check.find(elm);
	      
	      // If "elm" is a nu addition, create nu inspection info
	      if(jt==ElmFaces2Check.end())
		{
		  // This is a nu addition. Create nu info
		  std::fill(ElmFaceFlags.begin(), ElmFaceFlags.end(), false); 
		  ElmFaceFlags[face] = true;
		  ElmFaces2Check.insert({elm, ElmFaceFlags});
		  isElmChecked[elm] = false;
		}
	      // Otherwise, update the inspection info. Mark "face" as inspected
	      else
		{
		  jt->second[face] = true;
		}
	    }
	}

      // Accummulate elements
      std::set<int> coloredElms{};
      while(!ElmFaces2Check.empty())
	{
	  // Extract the first element from the list
	  const int elm = ElmFaces2Check.begin()->first;
	  const auto faceFlags = ElmFaces2Check.begin()->second;
	  ElmFaces2Check.erase( ElmFaces2Check.begin() );
	  isElmChecked[elm] = true;
	  coloring[elm] = true;

	  // Sanity check: no element should be inserted into the list again
	  auto result = coloredElms.insert(elm);
	  assert(result.second==true); 
	  
	  // Get the neighbors
	  const int* elm_nbs = mesh_wrapper.GetElementNeighbors(elm);

	  // Inspect face-by-face
	  for(int f=0; f<num_faces; ++f)
	    if(faceFlags[f]==false)      // This face needs to be inspected
	      if(elm_nbs[2*f]>=0)        // Not a free face
		{
		  // Neighboring element and its mating face
		  const int& nb_elm  = elm_nbs[2*f];
		  const int& nb_face = elm_nbs[2*f+1];

		  // Nothing to do if the neighbor has already been inspected
		  if(isElmChecked[nb_elm]==false)
		    {
		      auto jt = ElmFaces2Check.find(nb_elm);
		      
		      // If the neighbor is not in the queue, append it to the list and mark nb_face as inspected
		      if(jt==ElmFaces2Check.end())
			{
			  // add this element's info to the list
			  std::fill(ElmFaceFlags.begin(), ElmFaceFlags.end(), false);
			  ElmFaceFlags[nb_face] = true;
			  ElmFaces2Check.insert({nb_elm, ElmFaceFlags});
			}
		      // Otherwise, note that nb_face of nb_elm has already been inspected
		      else
			{
			  jt->second[nb_face] = true;
			}
		    }
		}

	  // Proceed to the next element to inspect
	}
      
      // done
      return;
    }


}
