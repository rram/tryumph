
/** \file um_Triangulator_2D_H
 * \brief Defines the class um::Triangulator2D
 * \author Ramsharan Rangarajan
 * Last modified: February 24, 2021
 */

#ifndef UM_TRIANGULATOR_2D_H
#define UM_TRIANGULATOR_2D_H

#include <um_WorkingMesh.h>
#include <um_Algorithm_Triangulate.h>

namespace um
{
  /** \brief Class implementing the Universal Meshes algorithm for 2D domains with piecewice \f${\rm C}^2\f$ interfaces.
   * 
   * \tparam UMeshType Mesh type of the background mesh.
   * 
   * The main functionality of the class is the um::Triangulator2D::GenerateMesh method, which computes a 
   * mesh that conforms to a specified set of geometric features by adjusting node locations in a given
   * the background mesh.
   *
   * <b> Nomenclature/Notations</b>
   * <ul>
   * <li> \f${\cal T}\f$: Background or universal mesh consisting of planar triangles. </li>
   * <li> \f$\{\Gamma_i\}_{i=1}^n\f$: Set of curves immersed in the domain triangulated by \f${\cal T}\f$. 
   * The end points of an open curve \f$\Gamma_i\$ are denote by \f$p_i^1,p_i^2\f$. </li>
   * <li> \f${\cal S}\f$: Mesh computed using the universal meshes algorithm by perturbing nodes in 
   * \f${\cal T}\f$ to conform to each curve \f$\{\Gamma_i\}_i\f$. \f${\cal S}\f$ and \f${\cal T}\f$ have identical
   * connectivit</li>
   * </ul>
   *
   * <b> Algorithmic assumptions </b>
   * A rigorous discussion of sufficient conditions guaranteeing success of the universal meshes algorithm can be found in [LINK HERE].
   * Roughly speaking:
   * <ul> 
   * <li> Each geometric feature \f$\Gamma_i\f$ is an open \f${\rm C}^2\f$ curve. </li>
   * <li> The end points \f$p_i^1\f$ and \f$p_i^2\f$ do not conincide. </li>
   * <li> A pair of curves \f$\Gamma_i\f$  and \f$\Gamma_j\f$ may only intersect at a common end point.\f$.
   * <li> At most two curves intersect at an end point. </li>
   * <li> A pair of curves \f$\Gamma_i\f$  and \f$\Gamma_j\f$ that intersect at a common end point, intersect transversely. <\li>
   * <li> The background mesh is sufficiently refined in the vicinities of each curves \f$\{\Gamma_i\}\f$.</li>
   * <li> Triangles in the vicinity of each curve \f$\Gamma_i\f$ are acute angled. This assumption can be relaxed (at the expense of practicality). </li>
   * </ul>
   * 
   * <b> Specifying the background mesh \f${\cal T}\f$ </b>
   * <ul>
   * <li> The background mesh is provided at construction.
   * It is therefore assumed that there is one instance of the class um::Triangulator2D for each background mesh to be considered. </li>
   * 
   * <li> The class is templated by the type of the background mesh.
   * This is done in order to provide flexibility in the choice of mesh-related data structures. 
   * Assumptions on the data structure are listed [LINK HERE]. </li>
   * </ul>
   *
   * <b> Specifying the geometry </b>:
   * Details of each curve  \f${\Gamma_i}\f$ is specified by an object of type um::FeatureParams.
   * <ul>
   * <li> Each curve \f$\Gamma_i\f$ is assigned a unique integer-valued identifier. 
   * This is set by the member um::FeatureParams::curve_id. </li>
   *
   * <li> Cartesian coordinates of the two end points of \f$\Gamma_i\f$ are provided by
   * um::FeatureParams::terminalPoints. It is a vector of length 4. </li>
   *
   * <li> The two end points  are assumed to be distint. That is, \f$\Gamma_i\f$ is assumed to be open. 
   * Closed curves will therefore need to be split into two or more open curves. </li>
   *
   * <li> The member um::FeatureParams::sdfunc is invoked to compute the signed distance function to a geometric feature.
   * It effectively conveys all the information required by the algorithm about the geometry without enforcing
   * any assumptions on how curves are represented (i.e., implicitly, parametrically, etc). 
   * Evaluations of the signed distance function to a curve is limited to a small neighborhood, typically restricted to about
   * twice the local mesh size of the background mesh. </li>
   * 
   * <li> Parameters required to evaluate the signed distance function are conveyed by um::FeatureParams::funcparams.
   * It is simply passed on to the function pointer um::FeatureParams::sdfunc during each call. </li>
   *
   * </ul>
   *						  
   * <b>Meshing-related parameters</b>
   * Options related to meshing are conveyed using an object of type um::MeshingParams.
   * <ul>
   * <li> um::MeshingParams::Nproject: Number of steps in which to project positive vertices identified for a geometric feature to
   * its closest point projection. Eg:  Nproject = 5.</li>
   * 
   * <li> um::MeshingParams::Nrelax: Number of relaxation iterations to use with the DVR-library. Eg: Nrelax = 10 result in performing 10 relaxation iterations
   * after each step of projecting a positive vertex towards its closest point. </li>
   * 
   * <li> um::MeshingParams::Ndist: Size of the relaxation neighborhood around each geometric feature, specified in terms of local 
   * edge length from the set of positive edges. Eg: Ndist = 3 relaxes vertices that are connected to a positive vertex by 3 or fewer edges. </li>
   *
   * <li> um::MeshingParams::Nsamples: Number of sampling points to use in DVR for  optimizing nodes that are constrained to lie on a geometric feature.
   * Eg: Nsamples = 10 samples element qualities at 2x10=20 locations on a curve to decide where to relocate a node. </li>
   * 
   * <li> um::MeshingParams::dnd_vertices: Indices of vertices in the background mesh which should not be disturbed during mesh generation. 
   * For instance, dnd_vertices could contain the list of vertices lying along the boundary of the mesh. </li>
   *
   * </ul>
   *
   * <b>Mesh output </b>
   * After successful execution of the um::Triangulator2D::GenerateMesh method, 
   * the mesh conforming to the provided set of geometric features is available as a um::WorkingMesh object.
   * It can be accessed using the um::Triangulator2D::GetWorkingMesh method.
   *
   * Further details about the conforming mesh are available as objects of type um::FeatureMeshOutput,
   * which are returned by reference by the um::Triangulator2D::GenerateMesh method.
   * There is one instance of type um::FeatureMeshOutput for each geometric feature.
   * <ul>
   * <li> um::FeatureMeshOutput::curve_id provides the identity of the curve for which mesh-related information is returned. </li>
   *
   * <li> </li>
   *
   * </ul>
   *
   *
   * <b> Key steps in the implementation </b>
   *  
   *
   * How is the geometry specified
   * Details of the universal mesh algorithm can be found HERE (PROVIDE A LINK).
   * Roughly speaking, mesh generation involves the following steps:
   * <ul>
   * <li> Snapping vertices of the background mesh to terminal pointsLocally perturbing </li>
   * </ul>the GenerateMesh()The steps involved in the mesh generation process areThe implementation closRoughly speakingRoughly speaking,
   */
  
  template<typename UMeshType>
    class Triangulator2D
    {
    public:
      
      /** \brief Constructor 1.
       * \param[in] bgmesh Universal mesh. Referenced, not copied.
       */
      inline Triangulator2D(const UMeshType& bgmesh)
	:BG(bgmesh), WM(BG) {}
      
      //! Destructor, does nothing
      inline virtual ~Triangulator2D() {}

      // Disable copy and assignment
      Triangulator2D(const Triangulator2D<UMeshType>&) = delete;
      Triangulator2D& operator=(const Triangulator2D&) = delete;

      //! Access the background mesh
      //! \return A const reference to the background mesh
      inline const UMeshType& GetUniversalMesh() const
      { return BG; }

      //! Returns a reference to the working mesh
      inline WorkingMesh<UMeshType>& GetWorkingMesh()
      { return WM; }

      /** \brief Perturb/relax a working mesh to a set of features
       * \param[in] mparams Meshing parameters
       * \param[in] gparams Geometry parameters of a single connected component
       * gparams[seg] provides of the segment numbered seg.
       * \param[out] mout Meshed output for this call
       */
      inline void GenerateMesh(const MeshingParams& mparams,
			       const FeatureSet& fset, 
			       std::vector<FeatureMeshOutput>& mout,
			       RunInfo& run_info)
      {
	auto refine_Verts = algo::Triangulate(WM, mparams, fset, mout, run_info); 
	assert(refine_Verts.empty());
      }

    private:
      const UMeshType& BG; //!< Background mesh
      WorkingMesh<UMeshType> WM; //!< Working mesh
    };
}

#endif
