// Sriramajayam

#include <um_QuadraticRationalBezier.h>
#include <um_Spline_RootFinding_Boost.h>
#include <um_RegisterFeatureParams.h>

namespace um
{
  // Implementation of class QuadraticRationalBezier

  // Constructor
  QuadraticRationalBezier::
  QuadraticRationalBezier(const int id,
			  const std::vector<double>& pts,
			  const std::vector<double>& wts,
			  const int nsamples,
			  const MedialAxisParams& mparams,
			  const NLSolverParams& nlparams)
    :QuadraticRationalBezier(id, 0., 1., pts, wts, nsamples, mparams, nlparams) {}
  
  // Constructor
  QuadraticRationalBezier::
  QuadraticRationalBezier(const int id,
			  const double tmin, const double tmax,
			  const std::vector<double>& pts,
			  const std::vector<double>& wts,
			  const int nsamples,
			  const MedialAxisParams& mparams,
			  const NLSolverParams& nlparams)
    :curve_id(id),
     weights{wts[0],wts[1],wts[2]},
     nSamples(nsamples),
     medialAxis(id, mparams),
     NLParams(nlparams),
     tbounds{tmin, tmax}
  {
    // Sanity checks
    assert(static_cast<int>(pts.size())==6);
    assert(static_cast<int>(wts.size())==3);
    assert(nSamples>0);
    assert(tmin>=0. && tmax<=1.);

    // Set the control points
    SetControlPoints(pts);
  }
  
  // Reset control points
  void QuadraticRationalBezier::SetControlPoints(const std::vector<double>& control_points)
  {
    assert(static_cast<int>(control_points.size())==6);
    for(int k=0; k<2; ++k)
      {
	P0[k] = control_points[k];
	P1[k] = control_points[2+k];
	P2[k] = control_points[4+k];
      }

    // Populate the r-tree with sample points
    rtree.clear();
    double t, X[2], b0, b1, b2, denom;
    pointParam pp;
    int indx = 0;
    const double ds = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples-1);
    for(int s=0; s<nSamples; ++s)
      {
	// basis functions
	t = tbounds[0] + static_cast<double>(s)*ds;
	b0 = weights[0]*(1.-t)*(1.-t);
	b1 = weights[1]*2.*(1.-t)*t;
	b2 = weights[2]*t*t;
	denom = b0+b1+b2;
	
	// Interpolate
	for(int k=0; k<2; ++k)
	  X[k] = (b0*P0[k] + b1*P1[k] + b2*P2[k])/denom;

	pp.first = boost_point2D(X[0], X[1]);
	pp.second = std::pair<int,double>(indx++,t);
	rtree.insert(pp);
      }

    // Re-compute the medial axis
    ComputeMedialAxis();
    
    // done
    return;
  }

  
  // Compute the medial axis
  void QuadraticRationalBezier::ComputeMedialAxis()
  {
    // Generate feature samples for computing the medial axis
    std::vector<FeatureSample> samples{};
    samples.reserve(rtree.size());
    FeatureSample fs;
    auto& fs_indx = std::get<0>(fs);
    auto& Point   = std::get<1>(fs);
    auto& Normal  = std::get<2>(fs);
    double dX[2], norm;
    for(auto& it:rtree)
      {
	fs_indx = it.second.first;
	auto& t = it.second.second;
	Evaluate(t, &Point[0], dX);
	norm = std::sqrt(dX[0]*dX[0]+dX[1]*dX[1]);
	Normal[0] = -dX[1]/norm;
	Normal[1] = dX[0]/norm;
	samples.push_back(fs);
      }
    samples.shrink_to_fit();

    // compute the medial axis
    medialAxis.Compute(samples);
    
    // done
    return;
  }

  
  // Access control points
  void QuadraticRationalBezier::GetControlPoints(std::vector<double>& control_points) const
  {
    control_points.resize(6);
    for(int k=0; k<2; ++k)
      {
	control_points[0+k] = P0[k];
	control_points[2+k] = P1[k];
	control_points[4+k] = P2[k];
      }
    return;
  }
    
  // Return the curve id
  int QuadraticRationalBezier::GetCurveID() const
  { return curve_id; }

  // Returns the end points of the curve
  void QuadraticRationalBezier::GetTerminalPoints(double* tpoints) const
  {
    Evaluate(tbounds[0], tpoints+0, nullptr, nullptr);
    Evaluate(tbounds[1], tpoints+2, nullptr, nullptr);
    return;
  }

  void QuadraticRationalBezier::GetSplittingPoint(double* spoint)
  { Evaluate(0.5*(tbounds[0]+tbounds[1]), spoint, nullptr, nullptr); }

  // Evaluate the coordinates of the spline at the requested point
  void QuadraticRationalBezier::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    // Coordinates
    assert(X!=nullptr);

    const double b0=(1-t)*(1-t);
    const double b1=2*(1-t)*t;
    const double b2=t*t;

    const double c0x = P0[0];
    const double c0y = P0[1];

    const double c1x = P1[0];
    const double c1y = P1[1];

    const double c2x = P2[0];
    const double c2y = P2[1];

    const double weights0 = weights[0];
    const double weights1 = weights[1];
    const double weights2 = weights[2];
		
    const double denom = b0*weights[0] + b1*weights[1] + b2*weights[2];

    X[0] = (b0*c0x*weights[0] + b1*c1x*weights[1] + b2*c2x*weights[2])/denom;
    X[1] = (b0*c0y*weights[0] + b1*c1y*weights[1] + b2*c2y*weights[2])/denom;

    if(dX!=nullptr)
      {
	dX[0] = (((((-2 * c0x + 2 * c1x) * weights1 + 2 * weights2 * (c0x - c2x)) * weights0 - 2 * weights1 * weights2 * (c1x - c2x)) * t * t) + 4. * weights0 * (((c0x - c1x) * weights1) - (weights2 * (c0x - c2x)) / 2.) * t - (2 * weights0 * weights1 * (c0x - c1x))) * std::pow(((weights0 - 2 * weights1 + weights2) * t * t + (-2 * weights0 + 2 * weights1) * t + weights0), (-2));

	dX[1] = (((((-2 * c0y + 2 * c1y) * weights1 + 2 * weights2 * (c0y - c2y)) * weights0 - 2 * weights1 * weights2 * (c1y - c2y)) * t * t) + 4. * weights0 * (((c0y - c1y) * weights1) - (weights2 * (c0y - c2y)) / 2.) * t - (2 * weights0 * weights1 * (c0y - c1y))) * std::pow(((weights0 - 2 * weights1 + weights2) * t * t + (-2 * weights0 + 2 * weights1) * t + weights0), (-2));
      }

    if(d2X!=nullptr)
      {
	d2X[0] = (4. * (((c0x - c1x) * weights1 - weights2 * (c0x - c2x)) * t + (-c0x + c1x) * weights1 - weights2 * (c0x - c2x) / 2.) * std::pow(t - 1., 2.) * weights0 * weights0 + (((-8. * c0x + 8. * c1x) * weights1 * weights1 + 12. * weights2 * (c0x - c2x) * weights1 - 4. * weights2 * weights2 * (c0x - c2x)) * std::pow(t, 3.) + 24. * (weights1 - weights2 / 2.) * ((c0x - c1x) * weights1 - weights2 * (c0x - c2x) / 2.) * t * t - 24. * (weights1 - weights2 / 2.) * weights1 * (c0x - c1x) * t + 8. * weights1 * weights1 * (c0x - c1x)) * weights0 - 8. * (weights1 - weights2 / 2.) * (c1x - c2x) * std::pow(t, 3.) * weights1 * weights2) * std::pow(std::pow(t - 1., 2.) * weights0 - 2. * t * ((weights1 - weights2 / 2.) * t - weights1), -3.);

	d2X[1] = (4. * (((c0y - c1y) * weights1 - weights2 * (c0y - c2y)) * t + (-c0y + c1y) * weights1 - weights2 * (c0y - c2y) / 2.) * std::pow(t - 1., 2.) * weights0 * weights0 + (((-8. * c0y + 8. * c1y) * weights1 * weights1 + 12. * weights2 * (c0y - c2y) * weights1 - 4. * weights2 * weights2 * (c0y - c2y)) * std::pow(t, 3.) + 24. * (weights1 - weights2 / 2.) * ((c0y - c1y) * weights1 - weights2 * (c0y - c2y) / 2.) * t * t - 24. * (weights1 - weights2 / 2.) * weights1 * (c0y - c1y) * t + 8. * weights1 * weights1 * (c0y - c1y)) * weights0 - 8. * (weights1 - weights2 / 2.) * (c1y - c2y) * std::pow(t, 3.) * weights1 * weights2) * std::pow(std::pow(t - 1., 2.) * weights0 - 2. * t * ((weights1 - weights2 / 2.) * t - weights1), -3.);
      }
    return;
  }
  /*{
  // Coordinates
  assert(X!=nullptr);

  // Basis functions and derivatives
  const double b0 = (1.-t)*(1.-t)*weights[0];
  const double b1 = 2.*t*(1.-t)*weights[1];
  const double b2 = t*t*weights[2];
  const double denom = b0+b1+b2;

  // Evaluate
  for(int k=0; k<2; ++k)
  X[k] = (b0*P0[k] + b1*P1[k] + b2*P2[k])/denom;

  // First derivatives
  if(dX!=nullptr || d2X!=nullptr)
  {
  const double db0 = weights[0]*2.*(t-1.);
  const double db1 = weights[1]*(2.-4.*t);
  const double db2 = weights[2]*2.*t;
  const double ddenom = db0+db1+db2;
  if(dX!=nullptr)
  for(int k=0; k<2; ++k)
  dX[k] = (db0*P0[k] + db1*P1[k] + db2*P2[k])/denom - X[k]*ddenom/denom;

  if(d2X!=nullptr)
  {
  const double d2b0 = weights[0]*2.;
  const double d2b1 = -4.*weights[1];
  const double d2b2 = 2.*weights[2];
  const double d2denom = d2b0+d2b1+d2b2;
  for(int k=0; k<2; ++k)
  d2X[k] =
  (d2b0*P0[k] + d2b1*P1[k] + d2b2*P2[k])/denom
  -(db0*P0[k] + db1*P1[k] + db2*P2[k])*ddenom/(denom*denom)
  -(dX[k]*ddenom+X[k]*d2denom)/denom + X[k]*ddenom*ddenom/(denom*denom);
  }
  }
    
  // done
  return;
  }*/

  // Returns the signed distance function to a point
  void QuadraticRationalBezier::GetSignedDistance(const double* X, double& sd, double* dsd)
  {
    // Initial guess for the closest point
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);
    const double t0 = result[0].second.second;

    // spacing between sample points
    const double dt = (tbounds[1]-tbounds[0])/static_cast<double>(nSamples);

    // Determine the root
    const double tcpt = detail::Spline_FindRoot_Boost(X, this, t0, dt, tbounds, NLParams);
    assert(tcpt>=tbounds[0] && tcpt<=tbounds[1]);
        
    // Evaluate the spline and its derivatives here.
    // Y is the closest point projection
    double Y[2], dY[2];
    Evaluate(tcpt, Y, dY, nullptr);
    double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
    assert(norm>NLParams.normTol); 
    double nvec[] = {-dY[1]/norm, dY[0]/norm};

    
    // signed distance
    sd = (X[0]-Y[0])*nvec[0] + (X[1]-Y[1])*nvec[1];
    
    // Gradient
    if(dsd!=nullptr)
      { dsd[0] = nvec[0];
	dsd[1] = nvec[1]; }

    // done
    return;
  }
    


  // Return sampling of the feature
  void QuadraticRationalBezier::GetSampling(std::vector<std::array<double,2>>& samples) const
  {
    samples.reserve(rtree.size());
    for(auto& it:rtree)
      {
	auto& X = it.first;
	samples.push_back( std::array<double,2>{X.get<0>(), X.get<1>()} );
      }
    samples.shrink_to_fit();
  }


  // Parameter bounds
  std::pair<double,double> QuadraticRationalBezier::GetParameterBounds() const
  {
    return std::make_pair(tbounds[0], tbounds[1]);
  }


  // split
  std::pair<FeatureParams, FeatureParams> QuadraticRationalBezier::Split(const int left_id, const int right_id)
  {
    std::vector<double> control_pts(6);
    GetControlPoints(control_pts);
    std::vector<double> wts(weights, weights+3);

    // left
    left_split = std::make_unique<QuadraticRationalBezier>(left_id, tbounds[0], 0.5*(tbounds[0]+tbounds[1]), control_pts, wts, nSamples,  medialAxis.GetMedialAxisParams(), NLParams);

    // right
    right_split = std::make_unique<QuadraticRationalBezier>(right_id, 0.5*(tbounds[0]+tbounds[1]), tbounds[1], control_pts, wts, nSamples, medialAxis.GetMedialAxisParams(), NLParams);

    // register split features
    std::pair<FeatureParams, FeatureParams> LRparams;
    left_split->RegisterFeature(LRparams.first);
    right_split->RegisterFeature(LRparams.second);

    return LRparams;
  }

  // Register feature params
  void QuadraticRationalBezier::RegisterFeature(FeatureParams& params)
  {
     RegisterFeatureParams(*this, params);
     return;
  }

  double QuadraticRationalBezier::GetFeatureSize(const double* X)
  {
    double in_dist, out_dist;
    double in_X[2], out_X[2];
    medialAxis.GetDistanceToMedialAxis(X, out_dist, out_X, in_dist, in_X);
    if(out_dist<in_dist) return out_dist;
    else return in_dist;
  }
  
  // print
  std::ostream& operator << (std::ostream &out, const QuadraticRationalBezier& geom)
  {
    const int& nSamples = geom.nSamples;
    auto tbounds = geom.GetParameterBounds();
    const double frac = (tbounds.second-tbounds.first)/static_cast<double>(nSamples-1);
    double tval, X[2];
    for(int i=0; i<nSamples; ++i)
      {
	tval = tbounds.first + static_cast<double>(i)*frac;
	geom.Evaluate(tval, X);
	out << X[0] <<",\t" << X[1] <<"\n";
      }
    return out;
  }

  std::pair<QuadraticRationalBezier&, QuadraticRationalBezier&>
  QuadraticRationalBezier::GetSplits()
  {
    assert(left_split!=nullptr && right_split!=nullptr);
    return {*left_split, *right_split}; 
  }
  
  
} // um::  

