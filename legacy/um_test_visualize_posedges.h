// Sriramajayam

namespace um
{
  namespace test
  {
    // write positive edges to file
    template<typename MeshType>
      void save_posedges_vtk(const std::string filename, const MeshType& MD,
			     const std::vector<FeatureMeshOutput>& fm,
			     const std::vector<int>& colorMap)
      {
	assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	
	std::fstream out;
	out.open(filename, std::ios::out);
	assert(out.good() && out.is_open());

	// Headers
	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
	out << "ASCII" << std::endl;
	out << "DATASET POLYDATA" << std::endl;

	// nodes
	const int nNodes = MD.n_nodes();
	out << "POINTS " << nNodes << " double" << std::endl;
	for(int i=0; i<nNodes; ++i)
	  {
	    const double* X = MD.coordinates(i);
	    out << X[0] << " " << X[1] << " " << 0. << std::endl;
	  }

	// polygons = lines
	const int nFeatures = static_cast<int>(fm.size());
	assert(colorMap.size()==fm.size());
	
	// cell size = total number of integers in the list
	int cell_size = 0;
	for(auto& f:fm)
	  {
	    cell_size += static_cast<int>(f.PosVerts.size())+1;
	  }
	out << "LINES " << nFeatures << " " << cell_size << std::endl;
	for(auto& f:fm)
	  {
	    const auto& PosVerts = f.PosVerts;
	    out << static_cast<int>(PosVerts.size()) << " " ;
	    for(auto& v:PosVerts)
	      out << v << " ";
	    out << std::endl;
	  }

	// colors for each feature
	out << "CELL_DATA " << nFeatures << std::endl
	    << "SCALARS color int 1" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(int i=0; i<nFeatures; ++i)
	  {
	    out << colorMap[i] << std::endl;
	  }
	
	// RGB output
	//out << "CELL_DATA " << nFeatures << std::endl
	// << "COLOR_SCALARS feat_colors 4" << std::endl;
	//for(auto& f:fparams)
	//{
	//  out << f.rgba[0] << " " << f.rgba[1] << " " << f.rgba[2] << " " << f.rgba[3] << std::endl;
	//  }
	
	// done
	out.close();
      }
  } // test::
} // um::
