// Sriramajayam

#include <iostream>
#include <string>
#include <umesh_SimpleMesh.h>
#include <um_Module>
#include <chrono>

// Read the crack geometries
void CreateCracks(const std::string filename,
		  std::vector<um::CubicSpline*>& cracks);

int main()
{
  // Begin
  std::chrono::steady_clock::time_point tbegin = std::chrono::steady_clock::now();

  // Read the background mesh
  umesh::SimpleMesh BG;
  BG.ReadTecplotFile("UM0.tec");
  BG.SetupMesh();
  BG.PlotTecMesh("bg.tec");
  
  // Read crack geometries
  std::vector<um::CubicSpline*> cracks{};
  CreateCracks("MappedPoints0.txt", cracks);
  const int nCracks = static_cast<int>(cracks.size());
  std::cout<<"\nNumber of cracks: "<<nCracks;
  
  // Solver parameters
  um::NLSolverParams nlparams{.ftol=1.e-4, .ttol=1.e-3, .max_iter=25, .normTol=1.e-4};

  // Signed distance functions to crack geometries
  std::vector<um::P3Spline_SDParams> crack_sdparams{};
  for(int n=0; n<nCracks; ++n)
    crack_sdparams.push_back( um::P3Spline_SDParams{cracks[n], &nlparams} );
  
  // Parameters for crack geometry
  std::vector<um::FeatureParams> feature_params{};
  std::vector<double> terminalPoints(4);

  // Append details of cracks
  for(int n=0; n<nCracks; ++n)
    { cracks[n]->GetTerminalPoints(&terminalPoints[0]);
      feature_params.push_back( um::FeatureParams{
	  .curve_id=cracks[n]->GetCurveID(),
	  .sdfunc=um::p3spline_sdfunc,
	    .sdparams=&crack_sdparams[n],
	    .terminalPoints=terminalPoints} );
      
      // visualize sample points on the crack
      std::vector<um::MedialAxisSamples> mdata{};
      cracks[n]->GetSamples(mdata);
      std::string filename = "crack-"+std::to_string(n)+".csv";
      um::MedialAxisSamples::PrintSamples(n, filename, mdata);
    }
  
  // Meshing parameters
  um::MeshingParams mparams{.Nproject=5, .Nrelax=10, .Ndist=3, .Nsamples=0, .dnd_vertices=std::set<int>{}};

  std::cout<<"\nMeshing..."<<std::flush;
  
  // Generate a conforming mesh
  std::vector<um::FeatureMeshOutput> mout{};
  um::Triangulator2D<decltype(BG)> tri(BG);
  tri.GenerateMesh(mparams, feature_params, mout);

  // Plot
  auto& wm = tri.GetWorkingMesh();
  wm.PlotTecMesh("wm.tec");

  std::chrono::steady_clock::time_point tend = std::chrono::steady_clock::now();
  std::cout << "Time elapsed = " <<
    std::chrono::duration_cast<std::chrono::milliseconds>(tend - tbegin).count() <<" ms "<<"\n"<<std::flush;

  // Clean up
  for(auto& geom:cracks)
    delete geom;
}


// Read the crack geometries
void CreateCracks(const std::string filename,
		  std::vector<um::CubicSpline*>& cracks)
{
  cracks.clear();

  // File format
  // Number of Cracks
  // N1 (number of points for crack 1)
  // X_1 Y_1
  // X_2 Y_2
  //...
  // X_N1 Y_N1
  // N2 (number of points for crack 2)
  // X_1 Y_1
  // X_2 Y_2
  // ...
  // X_N2 Y_N2
  // ...
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());
  int nCracks;
  pfile >> nCracks;
  for(int c=0; c<nCracks; ++c)
    {
      // Points to interpolate
      int nPoints;
      pfile >> nPoints;
      assert(nPoints>0);
      std::vector<double> points(2*nPoints);
      for(int p=0; p<nPoints; ++p)
	{ pfile >> points[2*p];
	  points[2*p] += 0.5;
	  pfile >> points[2*p+1];
	}

      // Uniform knot vector
      std::vector<double> tvec(nPoints);
      for(int i=0; i<nPoints; ++i)
	tvec[i] = static_cast<double>(i)/static_cast<double>(nPoints-1);

      // Create a spline geometry
      cracks.push_back( new um::CubicSpline(c, tvec, points, 40) );
    }
  pfile.close();

  // done
  return;
}
