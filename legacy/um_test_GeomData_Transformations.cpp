// Sriramajayam

#include <um_test_GeomData.h>

namespace um
{
  namespace test
  {
    // Rotation body motion
    void RigidBodyMotionFunc(const double* inPt, double* outPt, void* params)
    {
      assert(params!=nullptr);
      auto* rotParams = static_cast<const RigidBodyMotionStruct*>(params);
      const double* center = rotParams->center;
      const double& theta = rotParams->angle;
      const double* tvec = rotParams->tvec;

      const double Mat[2][2] = {{std::cos(theta), -std::sin(theta)},
				{std::sin(theta), std::cos(theta)}};
      outPt[0] = 0.;
      outPt[1] = 0.;
      for(int i=0; i<2; ++i)
	{
	  for(int j=0; j<2; ++j)
	    outPt[i] += Mat[i][j]*(inPt[j]-center[j]);
	  outPt[i] += center[i];
	  outPt[i] += tvec[i];
	}

      // done
      return;
    }


    // Polar mapping
    void PolarMapFunc(const double* inPt, double* outPt, void* params)
    {
      assert(params!=nullptr);
      auto* polarParams = static_cast<const PolarMapStruct*>(params);
      const double* center = polarParams->center;
      const double& R = polarParams->Rnorm;
      const double& b = polarParams->b;
      const double& theta0 = polarParams->theta0;

      // Cartesian to polar coordinates, normalize radius to 1
      const double X[] = {inPt[0]-center[0], inPt[1]-center[1]};
      double r = std::sqrt(X[0]*X[0]+X[1]*X[1])/R;
      double theta = std::atan2(X[1],X[0]);
      if(theta<0.) theta += 2.*M_PI;

      // Apply radial + azimuthal transformations
      if(r<1.)
	{
	  r += -b*std::pow(r*(r-1.),3.);
	  theta -= 64.*theta0*std::pow(r*(r-1),3.);
	}

      // Polar to Cartesian, translate origin
      outPt[0] = R*r*std::cos(theta) + center[0];
      outPt[1] = R*r*std::sin(theta) + center[1];

      // done
      return;
    }

    // Trigonometric function
    void TrigMapFunc(const double* inPt, double* outPt, void* params)
    {
      assert(params!=nullptr);
      auto* trigParams = static_cast<const TrigMapStruct*>(params);
      const double* center = trigParams->center;
      const double& factor = trigParams->factor;

      outPt[0] = inPt[0]+factor*std::sin(inPt[1]-center[1]);
      outPt[1] = inPt[1]+factor*std::sin(inPt[0]-center[0]);

      // done
      return;
    }
  }
}
