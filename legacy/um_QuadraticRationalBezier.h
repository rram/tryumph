// Sriramajayam

#ifndef UM_QUADRATIC_RATIONAL_BEZIER_H
#define UM_QUADRATIC_RATIONAL_BEZIER_H

#include <um_NLSolverParams.h>
#include <um_FeatureMedialAxis.h>
#include <um_FeatureParams.h>

namespace um
{
  //! Class defining a quadratic rational bezier curve
  //! Not thread safe
  class QuadraticRationalBezier
  {
  public:
    //! Constructor
    //! \param[in] id Curve id
    //! \param[in] pts Set of 3 control points in order
    //! \param[in] wts Set of 3 weights in order
    //! \param[in] nsamples Number of sampling points to use
    QuadraticRationalBezier(const int id,
			    const std::vector<double>& pts,
			    const std::vector<double>& wts,
			    const int nsamples,
			    const MedialAxisParams& mparams,
			    const NLSolverParams& nlparams);

    //! Constructor
    QuadraticRationalBezier(const int id,
			    const double tmin, const double tmax,
			    const std::vector<double>& pts,
			    const std::vector<double>& wts,
			    const int nsamples,
			    const MedialAxisParams& mparams,
			    const NLSolverParams& nlparams);

    //! Disable copy and assignment
    QuadraticRationalBezier(const QuadraticRationalBezier&) = delete;
    QuadraticRationalBezier& operator=(const QuadraticRationalBezier) = delete;

    //! Destructor
    inline virtual ~QuadraticRationalBezier() {}

    //! Access the control points
    void GetControlPoints(std::vector<double>& control_points) const;

    //! Access the weights
    const double* GetWeights() const
    { return weights; }
    
    //! Returns the curve id
    int GetCurveID() const;

    //! Returns the end points of the curve
    void GetTerminalPoints(double* tpoints) const;

    //! Returns an interior point on the curve
    void GetSplittingPoint(double* spoint);

    std::pair<QuadraticRationalBezier&, QuadraticRationalBezier&> GetSplits();
    
    //! Returns the signed distance function to a point
    //! \param[in] X Point at which to evaluate
    //! \param[in] nlparams Solver parameters
    //! \param[out] sd Computed signed distance
    //! \param[out] dsd Computed derivative of the signed distance. Defaulted to nullptr.
    void GetSignedDistance(const double* X, double& sd, double* dsd=nullptr);

    //! Evaluate the coordinates of the spline at the requested point
    //! \param[in] t Parameter value
    //! \param[out] X evaluate coordinate
    //! \param[out] dX evaluated derivative
    //! \param[out] d2X evaluated 2nd derivative
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;

    //! Access the medial axis
    inline const FeatureMedialAxis& GetMedialAxis() const
    { return medialAxis; }

    double GetFeatureSize(const double* X);
    
    //! Samples the feature
    void GetSampling(std::vector<std::array<double,2>>& samples) const;

    //! split
    std::pair<FeatureParams, FeatureParams> Split(const int left_id, const int right_id);
    
    //! Parameter bounds
    std::pair<double,double> GetParameterBounds() const;

    //! Register feature params
    void RegisterFeature(FeatureParams& params);
    
    // Overload extraction operator
    friend std::ostream& operator << (std::ostream &out, const QuadraticRationalBezier& geom);

  private:

    //! Reset control points
    void SetControlPoints(const std::vector<double>& control_points);
    
    void ComputeMedialAxis();
    const int curve_id; //!< Curve id
    double P0[2], P1[2], P2[2]; //!< Coordinates of control points
    const double weights[3]; //!< Weights for control points
    const int nSamples; //!< Number of sample points
    FeatureMedialAxis medialAxis; //!< Medial axis
    const NLSolverParams NLParams; //!< Nonlinear sovler parameters
    typedef std::pair<int,double> tID; //!< Pairing of point index and its parametric coordinate
    typedef std::pair<boost_point2D,tID> pointParam;
    boost::geometry::index::rtree<pointParam, boost::geometry::index::quadratic<8>> rtree; //!< boost r-tree for closest point searches
    const double tbounds[2];
    std::unique_ptr<QuadraticRationalBezier> left_split, right_split;
  };
  
}
#endif
