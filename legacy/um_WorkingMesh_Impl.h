
/** \file um_WorkingMesh_Impl.h
 * File implementing the class um::WorkingMesh.
 * \author Ramsharan Rangarajan
 * Last modified: February 24, 2021.
 */

#ifndef UM_WORKING_MESH_IMPL_H
#define UM_WORKING_MESH_IMPL_H

#include <set>
#include <fstream>

namespace um
{

  // Helper function to compute the element neighbor list.
  // Only neighbor lists differing from the reference mesh are saved
  template<typename UMeshType> void
    WorkingMesh<UMeshType>::ComputeElementNeighborList()
    {
      // #nodes per element, # faces
      const int nodes_element = RefMesh.GetNumNodesPerElement();
      const int nFaces = nodes_element;

      // restrict neighbors to the ElmList
      ElmNbs.clear();
      for(auto& elm:ElmList)
	{
	  const int* ref_elmnbs = RefMesh.GetElementNeighbors(elm);
	  
	  // Save this list if any neighboring element is not in ElmList
	  bool flag = false;
	  for(int f=0; f<nFaces && flag==false; ++f)
	    {
	      // Neighbor along face 'f'
	      if(ref_elmnbs[2*f]!=-1)
		if(ElmList.find(ref_elmnbs[2*f])==ElmList.end()) 
		  {
		    // All modificaitons for this element will be done here
		    // No need to inspect remaining faces
		    flag = true;
		    
		    // connectivity of 'elm' has changed
		    // save this element's modified neighbor list
		    // write the element neighbor list just once
		    auto it = ElmNbs.find(elm);         
		    if(it==ElmNbs.end())                         
		      {
			// Determine the new neighbor list for 'elm'
			std::vector<int> newconn(2*nFaces);
			std::fill(newconn.begin(), newconn.end(), -1);
			for(int g=0; g<nFaces; ++g)
			  if(g!=f) // we know f is free
			    if(ref_elmnbs[2*g]!=-1) // no conn changes along a free face
			      if(ElmList.find(ref_elmnbs[2*g])!=ElmList.end()) // this neighbor on face #g exists
				{ newconn[2*g] = ref_elmnbs[2*g];
				  newconn[2*g+1] = ref_elmnbs[2*g+1]; }

			// Save the new neighbor list
			ElmNbs[elm] = newconn;

		      } } }
	}
  
      // done
      return;
    }


  // Helper function to compute 1-ring elements
  template<typename UMeshType> void
    WorkingMesh<UMeshType>::Compute1RingElements() 
    {
      Elm1Rings.clear();
      const int nodes_element = RefMesh.GetNumNodesPerElement();

      // Candidate vertices to consider: all vertices of elements in the working mesh
      std::set<int> VertSet{};
      for(auto& e:ElmList)
	{ const int* conn = RefMesh.connectivity(e);
	  for(int a=0; a<nodes_element; ++a)
	    VertSet.insert(conn[a]); }

      // Save details for a vertex in VertSet only if its 1-ring information has changed
      const int* oneRingElms;
      int n1RingElms = 0;
      std::vector<bool> inElementList(RefMesh.GetMaxElementValency());
      for(auto& vert:VertSet)
	{
	  // 1-ring from the reference mesh
	  RefMesh.Get1RingElements(vert, &oneRingElms, n1RingElms);

	  // Is there any change in the 1-ring?
	  bool flag = false;
	  std::fill(inElementList.begin(), inElementList.end(), true);
	  for(int i=0; i<n1RingElms; ++i)
	    if(ElmList.find(oneRingElms[i])==ElmList.end())
	      { inElementList[i] = false;
		flag = true; }
	  
	  // Save this vertex information if there is a change
	  if(flag==true)
	    {
	      std::vector<int> new1Ring{};
	      for(int i=0; i<n1RingElms; ++i)
		if(inElementList[i]==true)
		  new1Ring.push_back(oneRingElms[i]);

	      Elm1Rings[vert] = new1Ring;
	    }
	}

      // done
      return;
    }


  // Helper function to compute 1-ring vertices
  template<typename UMeshType> void
    WorkingMesh<UMeshType>::Compute1RingVertices()
    {
      Vert1Rings.clear();
      const int nodes_element = RefMesh.GetNumNodesPerElement();
      
      // Save information corresponding to vertices whose 1-ring elements have changed
      for(auto& it:Elm1Rings)
	{
	  const int& vert = it.first;
	  const std::vector<int>& oneringElms = it.second;

	  // all vertices of all elements in the 1-ring are neighbors of vert.
	  std::set<int> nbverts{};
	  for(auto& elm:oneringElms)
	    {
	      //assert(ElmList.find(elm)!=ElmList.end());
	      const int* conn = RefMesh.connectivity(elm);
	      for(int a=0; a<nodes_element; ++a)
		if(conn[a]!=vert)
		  nbverts.insert(conn[a]);
	    }
	  Vert1Rings[vert] = std::vector<int>(nbverts.begin(), nbverts.end());
	}
      
      // done
      return;
    }
  
      
  // Method to setup the mesh
  // Determine element neighbors where connectivities are altered
  // Determine 1-rings of vertices where connectivities are altered
  template<typename UMeshType> void
    WorkingMesh<UMeshType>::SetupMesh()
    {
      // Setup mesh call should be necessary
      assert(isSetup==false);
      
      // order of function calls is important
      ComputeElementNeighborList();
      Compute1RingElements();
      Compute1RingVertices();

      // Note that the mesh has been setup
      isSetup = true;

      // done
      return;
    }
  
  // Helper method to plot the working mesh to a file in tecplot format
  template<typename UMeshType> void
    WorkingMesh<UMeshType>::PlotTecMesh(const std::string filename) const
    {
      const int nNodes = RefMesh.GetNumNodes();
      
      // Open file to plot
      std::fstream outfile;
      outfile.open(filename, std::ios::out);
      outfile.precision(16);
      outfile.setf( std::ios::scientific );
      
      // Line 1:

      outfile<<"VARIABLES = \"X\", \"Y\" \n";

      // Line 2:
      outfile<<"ZONE t=\"t:0\", N="<<nNodes
	     <<", E="<<static_cast<int>(ElmList.size())
	     <<", F=FEPOINT, ET=TRIANGLE";
      
      // Nodal coordinates
      for(int a=0; a<nNodes; ++a)
	{
	  const auto* X = &CoordList[SPD*a];
	  outfile<<"\n";
	  for(int k=0; k<SPD; ++k)
	    outfile<<X[k]<<" ";
	}
  
      // Element connectivity
      const int NPE = RefMesh.GetNumNodesPerElement();
      for(auto& e:ElmList)
	{
	  outfile<<"\n";
	  const int* conn = RefMesh.connectivity(e);
	  for(int a=0; a<NPE; ++a)
	    outfile<<conn[a]+1<<" ";
	}
      outfile.flush();
      outfile.close();

      // -- done --
      return;
    }
}
      
#endif
