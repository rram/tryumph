// Sriramajayam

namespace um
{
  namespace test
  {
    // write positive edges to file
    template<typename MeshType>
      void save_poselms_vtk(const std::string filename, const MeshType& MD,
			    const std::vector<FeatureMeshOutput>& fm,
			    const std::vector<int>& colorMap)
      {
	assert(std::string(std::filesystem::path(filename).extension())==".vtk");
	
	std::fstream out;
	out.open(filename, std::ios::out);
	assert(out.good() && out.is_open());

	// Headers
	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
	out << "ASCII" << std::endl;
	out << "DATASET POLYDATA" << std::endl;

	// nodes
	const int nNodes = MD.n_nodes();
	out << "POINTS " << nNodes << " double" << std::endl;
	for(int i=0; i<nNodes; ++i)
	  {
	    const double* X = MD.coordinates(i);
	    out << X[0] << " " << X[1] << " " << 0. << std::endl;
	  }

	// polygons = positively cut triangles
	int nElements = 0;
	for(auto& f:fm)
	  {
	    nElements +=  static_cast<int>(f.PosCutElmFaces.size());
	  }
	
	// cell size = total number of integers in the list
	const int cell_size = nElements*3+nElements;
	
	out << "POLYGONS " << nElements << " " << cell_size << std::endl;
	for(auto& f:fm)
	  for(auto& it:f.PosCutElmFaces)
	    {
	      const int& e = std::get<0>(it);
	      const int* conn = MD.connectivity(e);
	      out << 3 << " " << conn[0] << " " << conn[1] << " " << conn[2] << std::endl;
	    }
	
	// colors for each feature
	const int nFeatures = static_cast<int>(fm.size());
	assert(fm.size()==colorMap.size());
	out << "CELL_DATA " << nElements << std::endl
	    << "SCALARS color int 1" << std::endl
	    << "LOOKUP_TABLE default" << std::endl;
	for(int i=0; i<nFeatures; ++i)
	  {
	    const auto& PosCutElmFaces = fm[i].PosCutElmFaces;
	    for(auto& it:PosCutElmFaces)
	      {
		out << colorMap[i] << std::endl;
	      }
	  }
	
	// done
	out.close();
      }
  } // test::
} // um::
