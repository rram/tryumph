// Sriramajayam

#include <um_LineSegment.h>
#include <um_RegisterFeatureParams.h>

namespace um
{
  // Implementation of the class um::LineSegment
  
  // Constructor
  LineSegment::LineSegment(const int id,
			   const double* A, const double* B,
			   const int nSamples,
			   const MedialAxisParams& mparams,
			   const NLSolverParams& nlparams)
    :curve_id(id),
     left_pt{A[0], A[1]},
     right_pt{B[0], B[1]},
     nsamples(nSamples),
     medialAxis(id, mparams),
     NLParams(nlparams)
  {
    SetControlPoints(std::vector<double>{A[0], A[1], B[0], B[1]});
  }
    

  // Reset the control points
  void LineSegment::SetControlPoints(const std::vector<double>& control_points)
  {
    assert(static_cast<int>(control_points.size())==4);
    for(int k=0; k<2; ++k)
      { left_pt[k] = control_points[k];
	right_pt[k] = control_points[2+k]; }
    
    const double len = std::sqrt((right_pt[0]-left_pt[0])*(right_pt[0]-left_pt[0]) + (right_pt[1]-left_pt[1])*(right_pt[1]-left_pt[1]));
    assert(len>1.e-6 && "um::LineSegment- segment length is less than 1e-6");

    nvec[0] = -(right_pt[1]-left_pt[1])/len;
    nvec[1] = (right_pt[0]-left_pt[0])/len;

    // Recompute the medial axis
    ComputeMedialAxis();
  }


  // Compute the medial axis
  void LineSegment::ComputeMedialAxis()
  {
    // Generate feature samples for computing the medial axis
    std::vector<FeatureSample> samples{};
    samples.reserve(nsamples);
    const double frac = 1./static_cast<double>(nsamples-1);
    FeatureSample fs;
    auto& fs_indx = std::get<0>(fs);
    auto& Point   = std::get<1>(fs);
    auto& Normal  = std::get<2>(fs);
    double dX[2], norm;
    
    for(int i=0; i<nsamples; ++i)
      {
	Point[0] = left_pt[0]+static_cast<double>(i)*frac*(right_pt[0]-left_pt[0]);
	Point[1] = left_pt[1]+static_cast<double>(i)*frac*(right_pt[1]-left_pt[1]);
	Normal[0] = nvec[0];
	Normal[1] = nvec[1];
	fs_indx = i;
	samples.push_back(fs);
      }
    samples.shrink_to_fit();

    // compute the medial axis
    medialAxis.Compute(samples);
    
    // done
    return;
  }

  
  // Access the control points
  void LineSegment::GetControlPoints(std::vector<double>& control_points) const
  {
    control_points.resize(4);
    for(int k=0; k<2; ++k)
      {
	control_points[k] = left_pt[k];
	control_points[2+k] = right_pt[k];
      }
    return;
  }
				     
  // Return the curve id
  int LineSegment::GetCurveID() const
  { return curve_id; }

  // Returns the end points of the curve
  void LineSegment::GetTerminalPoints(double* tpoints) const
  {
    for(int k=0; k<2; ++k)
      { tpoints[k] = left_pt[k];
	tpoints[2+k] = right_pt[k]; }
    return;
  }


  // Returns the mid point
  void LineSegment::GetSplittingPoint(double* spoint) const 
  {
    spoint[0] = 0.5*(left_pt[0]+right_pt[0]);
    spoint[1] = 0.5*(left_pt[1]+right_pt[1]);
    return;
  }
  
    
  // Returns the signed distance function to a point
  void LineSegment::GetSignedDistance(const double* X, double& sd, double* dsd)
  {
    sd = (X[0]-left_pt[0])*nvec[0] + (X[1]-left_pt[1])*nvec[1];
    if(dsd!=nullptr)
      {
	dsd[0] = nvec[0];
	dsd[1] = nvec[1];
      }

    // done
    return;
  }


  double LineSegment::GetFeatureSize(const double* X)
  {
    double in_dist, out_dist;
    double in_X[2], out_X[2];
    medialAxis.GetDistanceToMedialAxis(X, out_dist, out_X, in_dist, in_X);
    if(out_dist<in_dist) return out_dist;
    else return in_dist;
  }

  
  // Return sampling of the feature
  void LineSegment::GetSampling(std::vector<std::array<double,2>>& samples) const
  {
    samples.reserve(nsamples);
    FeatureSample fs;
    const double frac = 1./static_cast<double>(nsamples-1);
    for(int i=0; i<nsamples; ++i)
      samples.push_back( std::array<double,2>{
	  left_pt[0]+static_cast<double>(i)*frac*(right_pt[0]-left_pt[0]),
	    left_pt[1]+static_cast<double>(i)*frac*(right_pt[1]-left_pt[1])} );
    
    samples.shrink_to_fit();
    return;
  }


  std::pair<FeatureParams, FeatureParams> LineSegment::Split(const int left_id, const int right_id)
  {
    double mid_pt[] = {0.5*(left_pt[0]+right_pt[0]), 0.5*(left_pt[1]+right_pt[1])};

    // left
    left_split = std::make_unique<LineSegment>(left_id, left_pt, mid_pt, nsamples, medialAxis.GetMedialAxisParams(), NLParams);

    // right
    right_split = std::make_unique<LineSegment>(right_id, mid_pt, right_pt, nsamples, medialAxis.GetMedialAxisParams(), NLParams);

    // populate feature parameters
    std::pair<FeatureParams, FeatureParams> LRparams;
    left_split->RegisterFeature(LRparams.first);
    right_split->RegisterFeature(LRparams.second);
    
    return LRparams;
  }

  std::pair<LineSegment&, LineSegment&> LineSegment::GetSplits()
  {
    assert(left_split!=nullptr && right_split!=nullptr);
    return {*left_split, *right_split}; 
  }
  
  // Register feature params
  void LineSegment::RegisterFeature(FeatureParams& params)
  {
    RegisterFeatureParams(*this, params);
    return;
  }

  
  // print
  std::ostream& operator << (std::ostream &out, const LineSegment& geom)
  {
    const int& nsamples = geom.nsamples;
    const double* left_pt = geom.left_pt;
    const double* right_pt = geom.right_pt;
    const double frac = 1./static_cast<double>(nsamples-1);
    for(int i=0; i<nsamples; ++i)
      out << left_pt[0]+static_cast<double>(i)*frac*(right_pt[0]-left_pt[0])
	  <<",\t"
	  << left_pt[1]+static_cast<double>(i)*frac*(right_pt[1]-left_pt[1]) <<"\n";
    return out;
  }
  
} // um::
