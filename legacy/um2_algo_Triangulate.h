// Sriramajayam

//! \file um_Algorithm_Tryangulate
//! \brief Implements the tryangulate algorithm
//! \author Ramsharan Rangarajan

#pragma once

#include <um2_algo_Tryangulate.h>
#include <um2_algo_QTree_Refine.h>
#include <memory>

namespace um2
{
  namespace algo
  {
    //! \tparam UMeshType Type of the background mesh
    //! \tparam WMMeshType Type of the working mesh
    //! \ingroup algo
    template<typename MeshType>
      RunInfo Triangulate(const MeshingParams& mparams,
			  const SplitFeatureSet& fset,
			  Quadtree& QT,
			  std::unique_ptr<MeshType>& BG,
			  std::unique_ptr<WorkingMesh<MeshType>>& WM,
			  std::unique_ptr<MeshWrapper<WorkingMesh<MeshType>>>& mesh_wrapper,
			  CallbackInterface<MeshType> *callback)
      {
	// Cumulative run info
	RunInfo run_info;
	
	// Start the clock
	std::chrono::steady_clock::time_point timer_begin = std::chrono::steady_clock::now();

	// Aliases
	const int nFeatures = fset.getNumFeatures();
	const auto& gparams = fset.getFeatureParams();

	// Ensure that the quadtree is sufficiently refined at the corners
	// algo::RefineQuadtreeAtTerminals(QT, fset.GetFeatureParams());
      
	while(run_info.success==false &&
	      run_info.nTrys<mparams.max_num_trys)
	  {
	    std::cout << "Mesh refinement iteration "<< run_info.nTrys << std::endl;
	    ++run_info.nTrys;

	    // Set the try number in the callback
	    if(callback!=nullptr)
	      callback->setTryNumber(run_info.nTrys);
	    
	    mesh_wrapper.reset();
	    WM.reset();
	    BG.reset();
	    
	    // Plot the quadtree
	    if(callback!=nullptr)
	      (*callback)(QT);
	    
	    // Create a fresh background mesh by tessellating the quadtree
	    std::vector<double> bgcoord{};
	    std::vector<int> bgconn{};
	    QT.triangulate(bgcoord, bgconn);
	    BG = std::make_unique<MeshType>(bgcoord, bgconn);
	    bgcoord.clear();
	    bgconn.clear();

	    // Create the working mesh 
	    WM = std::make_unique<WorkingMesh<MeshType>>(*BG, fset);
	    WM->setMeshStatus(WorkingMeshStatus::UNPROCESSED);
	    
	    // Plot the background mesh
	    if(callback!=nullptr)
	      (*callback)(*WM);
	    
	    // Create the mesh wrapper for WM
	    mesh_wrapper = std::make_unique<MeshWrapper<WorkingMesh<MeshType>>>(*WM);
	    
	    // Try meshing
	    auto try_result = Tryangulate(*mesh_wrapper, mparams, callback);
	    const auto& refine_Verts = try_result.first;
	    const auto& try_info     = try_result.second;
	    run_info.success        = try_info.success;
	    run_info.try_info.push_back(try_info);

	    // Is further refinement required?
	    if(refine_Verts.empty())
	      {
		assert(try_info.success==true);
		std::cout << "Success! "<< std::endl;
	      }
	    else
	      {
		assert(try_info.success==false);
		RefineQuadtreeAtVertices(*WM, refine_Verts, QT);
	      }

	    // Proceed to the next iteration
	  
	  } // Completed/Exceeded refinement iterations

	// stop the clock
	std::chrono::steady_clock::time_point timer_end = std::chrono::steady_clock::now();

	// Track the run time
	run_info.runtime = std::chrono::duration_cast<std::chrono::milliseconds>(timer_end - timer_begin).count();
	
	// done
	return run_info;
      }

  } // um::algo::
} // um::

