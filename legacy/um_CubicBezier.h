// Sriramajayam

#ifndef UM_CUBIC_BEZIER_H
#define UM_CUBIC_BEZIER_H

#include <um_NLSolverParams.h>
#include <um_FeatureMedialAxis.h>
#include <um_FeatureParams.h>
#include <memory>

namespace um
{
  //! Class defining a Cubic Bezier curve
  //! Not thread safe.
  class CubicBezier
  {
  public:
    //! Constructor
    //! \param[in] id Curve id
    //! \param[in] pts Set of 4 control points in order.
    //! \param[in] nsamples Number of sampling points to use
    CubicBezier(int id, std::vector<double>& pts,
		const int nsamples,
		const MedialAxisParams& mparams,
		const NLSolverParams& nlparams);

    //! Constructor
    CubicBezier(int id, const double tmin, const double tmax,
		std::vector<double>& pts, const int nsamples,
		const MedialAxisParams& mparams,
		const NLSolverParams& nlparams);

    //! Disable copy and assignment
    CubicBezier(const CubicBezier&) = delete;
    CubicBezier& operator=(const CubicBezier) = delete;

    //! Destructor
    inline virtual ~CubicBezier() {}

    //! Access control points
    void GetControlPoints(std::vector<double>& control_points) const;
    
    //! Returns the curve id
    int GetCurveID() const;

    //! Returns the end points of the curve
    void GetTerminalPoints(double* tpoints) const;

    //! Returns the coordinates of an interior point
    void GetSplittingPoint(double* spoint);

    std::pair<CubicBezier&, CubicBezier&> GetSplits();
    
    //! Returns the signed distance function to a point
    //! \param[in] X Point at which to evaluate
    //! \param[in] nlparams Solver parameters
    //! \param[out] sd Computed signed distance
    //! \param[out] dsd Computed derivative of the signed distance. Defaulted to nullptr.
    void GetSignedDistance(const double* X, double& sd, double* dsd=nullptr);

    //! Evaluate the coordinates of the spline at the requested point
    //! \param[in] t Parameter value
    //! \param[out] X evaluate coordinate
    //! \param[out] dX evaluated derivative
    //! \param[out] d2X evaluated 2nd derivative
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;

    //! Access the medial axis
    inline const FeatureMedialAxis& GetMedialAxis() const
    { return medialAxis; }

    double GetFeatureSize(const double* X);
    
    //! Samples the feature
    void GetSampling(std::vector<std::array<double,2>>& samples) const;

    //! split
    std::pair<FeatureParams, FeatureParams> Split(const int left_id, const int right_id);

    //! Returns the range of parameters
    std::pair<double,double> GetParameterBounds() const;

    //! Register feature params
    void RegisterFeature(FeatureParams& params);
    
    // Overload extraction operator
    friend std::ostream& operator << (std::ostream &out, const CubicBezier& geom);
    
  private:

    //! Set control points
    void SetControlPoints(const std::vector<double>& control_points);
    
    void ComputeMedialAxis();
    const int curve_id; //!< Curve id.
    const int nSamples; //!< Number of sample points
    FeatureMedialAxis medialAxis; //!< Medial axis
    const NLSolverParams& NLParams; //!< Parameters for nonlinear solver
    double P0[2], P1[2], P2[2], P3[2]; //!< Coordinates of control points
    typedef std::pair<int,double> tID; //!< Pairing of point index and its parametric coordinate
    typedef std::pair<boost_point2D,tID> pointParam;
    boost::geometry::index::rtree<pointParam, boost::geometry::index::quadratic<8>> rtree; //!< boost r-tree for closest point searches
    const double tbounds[2];
    std::unique_ptr<CubicBezier> left_split, right_split;
  };

 
}

#endif
