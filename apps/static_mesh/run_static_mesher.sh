#! /bin/bash

set -e
set -x
count=0

# option number provided
srcgeom="geom"
echo "Accessing geometries from folder "$srcgeom

outdir="output"
echo "Saving outputs to folder "$output

mkdir -p $outdir

for geomname in $srcgeom/*.fset; do
    count=$((count+1))
    echo -e "\n\n"
    echo "Geometry number "$count
    echo "Running performance-fixed with geometry ${geomname}"
    gname="$(basename $geomname .fset)"
    ./static_mesher -g $geomname -o $outdir -j mesh_options.json
done
