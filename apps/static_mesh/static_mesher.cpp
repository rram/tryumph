// Sriramajayam

#include <app_utils.h>

std::ostream& operator << (std::ostream& out, const um2::RunInfo& runinfo);
   
int main(int argc, char** argv)
{
  // command line options: geometry, output directory and meshing options
  std::string geom_file, outdir, json_file;
  int geom_id = 0;
  CLI::App cli;
  cli.add_option("-g", geom_file,  "Geometry file in fset format")->required()->check(CLI::ExistingFile);
  auto opt_geom_id = cli.add_option("-n", geom_id,   "integer id for the geometry for visualization");
  cli.add_option("-o", outdir,    "Output directory")->required()->check(CLI::ExistingDirectory);
  cli.add_option("-j", json_file, "JSON file with meshing-related parameters")->required()->check(CLI::ExistingFile);

  CLI11_PARSE(cli, argc, argv);
  
  // read parameters from json file
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.is_open() && jfile.good());
  auto box                = app::read_json_domain_params("domain", jfile);
  auto medial_axis_params = app::read_json_medial_axis_params("medial axis", jfile);
  auto root_params        = app::read_json_root_finding_params("root finding", jfile);
  auto meshing_params     = app::read_json_meshing_params("meshing", jfile);
  auto geom_pair          = app::read_json_geometry_params("geometry", jfile);
  const int &nSamples     = geom_pair.first;
  const double &cnr_tol   = geom_pair.second;
  const std::string geom_name = std::string(std::filesystem::path(geom_file).stem());
  
  // generate geom_id in case one wasn't provided
  if(!(*opt_geom_id))
    geom_id = std::stoi(geom_name);
      
  // feature factory for curves provided in geom_file
  um2::utils::FeatureFactory feature_factory(geom_file, nSamples, cnr_tol, medial_axis_params, root_params);
  auto& featureset = feature_factory.getFeatureSet();
  
  // Mesh manager
  using MeshType = um2::TriangleMesh;
  um2::MeshManager<MeshType> manager(box);

  // callback for visualization
  outdir += "/";
  um2::utils::Final_Try_Visualization_Callback<MeshType> cb(outdir);
  //um2::utils::Try_Visualization_Callback<MeshType> cb(outdir);
  manager.setCallback(cb);

  // triangulate
  auto run_info = manager.triangulate(geom_id, featureset, meshing_params);
  assert(run_info.success==true);
  
  // run correctness checks
  manager.validate(geom_id, featureset, meshing_params, 1.e-3);
  const auto& WM = manager.getWorkingMesh(geom_id);
  
  // save the run information for post processing/examination
  const std::string file_runinfo = outdir+"runinfo.dat";
  const bool addHeader = std::filesystem::exists(file_runinfo)? false : true;
  std::fstream out;
  out.open(file_runinfo, std::ios::app|std::ios::out);
  if(addHeader)
    out << "#1.geom_id, 2.features, 3.n_nodes, 4.n_elements, 5.runtime, 6.trys, 7.nRelaxVerts, 8-16:counters of failure modes"<< std::endl;
  out << geom_id << "\t" << featureset.getNumFeatures() << "\t" << WM.n_nodes() << "\t" << WM.n_elements() << "\t" << run_info;
  out.close();
  
  // inspect the mesh size relative to feature sizes
  auto mesh_size_ratios = app::inspect_mesh_size(geom_id, manager, featureset);
  out.open(outdir+"meshsize-ratio.dat", std::ios::app|std::ios::out);
  for(auto& it:mesh_size_ratios)
    out << it[0] << "\t" << it[1] << "\t" << it[2] << "\t" << ((it[1]<it[2]) ? it[1] : it[2]) << std::endl;
  out.close();

  // inspect the approx of the geometry relative to the mesh size
  auto geom_ratio = app::inspect_geometry_approximation(geom_id, manager, featureset);
  out.open(outdir+"geom-ratio.dat", std::ios::app|std::ios::out);
  for(auto& it:geom_ratio)
    out << it.first << "\t" << it.second << std::endl;
  out.close();

  // inspect element qualities
  const std::list<double> elm_qualities = app::inspect_element_qualities(geom_id, manager);
  out.open(outdir+"elm-quality.dat", std::ios::app|std::ios::out);
  for(auto& it:elm_qualities)
    out << it << std::endl;
  out.close();

  // done
}




std::ostream& operator << (std::ostream& out, const um2::RunInfo& runinfo) {

  assert(runinfo.nTrys==static_cast<int>(runinfo.try_info.size()));
    
  // accummulate try-info
  int nRelaxVerts = 0;
  std::map<um2::MeshingReason,int> failures{};
  const int count = static_cast<int>(um2::MeshingReason::Count);
  for(int i=0; i<count; ++i)
    failures[static_cast<um2::MeshingReason>(i)] = 0;
  assert(failures.size()==static_cast<int>(um2::MeshingReason::Count));
    
  for(auto& it:runinfo.try_info)  {
    nRelaxVerts += it.nRelaxVerts;
    for(auto& jt:it.failures)
      failures[jt.first] += jt.second;
  }
    
  // Format: runtime, #trys, nRelaxVerts, counters of failure modes
  out << runinfo.runtime << "\t" << runinfo.nTrys << "\t" << nRelaxVerts << "\t";
    
  for(auto& it:failures)
    out << it.second << "\t";
  out << std::endl;
  return out;
}
