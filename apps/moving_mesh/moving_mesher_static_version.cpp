// Sriramajayam

#include <app_utils.h>

using MeshType = um2::TriangleMesh;

// control point transformation for a rigid body rotation
struct RigidMotion {
  double omega_t;
  void operator()(double* X) const;
};

// control point transformation for a non-isometric motion
struct NonRigidMotion {
  double omega_t;
  void operator()(double* X) const;
};


int main(int argc, char** argv)
{
  // command line options: geometry, output directory and meshing options
  std::string geomfile, outdir, json_file;
  int num_steps;
  double dtheta;
  CLI::App cli;
  cli.add_option("-g", geomfile,  "Geometry file in fset format")->required()->check(CLI::ExistingFile);
  cli.add_option("-o", outdir,    "Output directory")->required()->check(CLI::ExistingDirectory);
  cli.add_option("-j", json_file, "JSON file with meshing-related parameters")->required()->check(CLI::ExistingFile);
  cli.add_option("-n", num_steps, "number of rotation steps in a complete rotation")->required();
  
  CLI11_PARSE(cli, argc, argv);
  assert(num_steps>=1);

  // read parameters from json file
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.is_open() && jfile.good());
  auto box                = app::read_json_domain_params("domain", jfile);
  auto medial_axis_params = app::read_json_medial_axis_params("medial axis", jfile);
  auto root_params        = app::read_json_root_finding_params("root finding", jfile);
  auto meshing_params     = app::read_json_meshing_params("meshing", jfile);
  auto geom_pair          = app::read_json_geometry_params("geometry", jfile);
  const int &nSamples     = geom_pair.first;
  const double &cnr_tol   = geom_pair.second;

  // feature factory for curves provided in geomfile
  auto feature_factory = std::make_unique<um2::utils::FeatureFactory>(geomfile, nSamples, cnr_tol,
								      medial_axis_params, root_params);

  // Create control point transformations
  RigidMotion rigid;
  NonRigidMotion nonrigid;
  
  // save the run information for post processing/examination
  const std::string file_runinfo = outdir+"/runinfo.dat";
  const bool addHeader = std::filesystem::exists(file_runinfo)? false : true;
  std::fstream out;
  out.open(file_runinfo, std::ios::app|std::ios::out);
  if(addHeader)
    out << "#1.theta, 2.n_nodes, 3.n_elements, 4.trys, 5.failures, 6.runtime"<< std::endl;
  
  // incremental motion
  for(int iter=0; iter<=num_steps; ++iter)  {

    std::cout << std::endl << "Geometry iteration #: " << iter << std::endl;

    // this geometry iterate
    nonrigid.omega_t = 2.*M_PI*static_cast<double>(iter)/static_cast<double>(num_steps);
    rigid.omega_t = 2.*M_PI*static_cast<double>(iter)/static_cast<double>(num_steps);
    auto iter_feature_factory = feature_factory->transform(nonrigid); 
    auto& featureset = iter_feature_factory->getFeatureSet();

    // Mesh manager
    um2::MeshManager<MeshType> manager(box);
  
    // callback for visualization
    um2::utils::Final_Try_Visualization_Callback<MeshType> cb(outdir);
    manager.setCallback(cb);
  
    um2::RunInfo run_info = manager.triangulate(iter, featureset, meshing_params);
    assert(run_info.success==true);

    // accummulate failure count across trys
    int nfailures = 0;
    for(auto& it:run_info.try_info)
      for(auto& jt:it.failures)
	if(jt.first!=um2::MeshingReason::Unassigned && jt.first!=um2::MeshingReason::Success && jt.first!=um2::MeshingReason::Count)
	  nfailures += jt.second;

    if(run_info.nTrys==0)
      assert(nfailures==0);
			    
    // record execution data
    auto& WM = manager.getWorkingMesh(iter);
    out << rigid.omega_t << "\t" << WM.n_nodes() << "\t" << WM.n_elements()
	<< "\t" << run_info.nTrys << "\t" << nfailures << "\t" << run_info.runtime << std::endl;
    out.flush(); 
  }
  
  out.close();

  // done
  return 0;
}


// Rigid body motion
void RigidMotion::operator()(double* X) const {
  
  double R[2][2] = {{std::cos(omega_t), -std::sin(omega_t)},
		    {std::sin(omega_t), std::cos(omega_t)}};
  double y0 = R[0][0]*X[0]+R[0][1]*X[1];
  double y1 = R[1][0]*X[0]+R[1][1]*X[1];
  X[0] = y0;
  X[1] = y1;
  return;
}


// Affine motion
void NonRigidMotion::operator()(double* X) const {

  // elements of the transformation matrix
  double a = std::cos(omega_t)  + std::sin(2 * omega_t) / 2 + std::sin(3 * omega_t) / 3;
  double b = -std::sin(omega_t) + std::cos(2 * omega_t) / 2 - std::sin(4 * omega_t) / 4;
  double c = std::sin(omega_t)  + std::sin(3 * omega_t) / 3 - std::cos(4 * omega_t) / 4; 
  double d = std::cos(omega_t)  + std::cos(2 * omega_t) / 2 + std::sin(4 * omega_t) / 4;
  double det = a*d-b*c;
  assert(det>0.);
  
  double T[2][2] = {{a,b},{c,d}};
  double y0 = T[0][0]*X[0] + T[0][1]*X[1];
  double y1 = T[1][0]*X[0] + T[1][1]*X[1];
  X[0] = y0;
  X[1] = y1;
  return;
}
