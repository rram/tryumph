#! /bin/bash

set -e
set -x
count=0

# Check if the user provided an argument
if [ $# -eq 0 ]; then
    echo "Provide a number"
    exit 1
fi

# option number provided
num=$1
srcgeom="geom"

extension=".fset"  

# access the num-th file in the folder
file=$(ls -1 "$srcgeom" | grep "$extension" | head -n $num | tail -n 1)

if [[ -z "$file" ]]; then
    echo "Problem finding the geometry file. Aborting."
    exit 1
fi

#filename without extension
filenum="${file%.*}"

#output directory
outdir="static_version/output-$filenum"
mkdir -p $outdir

file="$srcgeom/$file"
    
echo -e "\n\n"
echo "Runnning with geometry ${file}"
./moving_mesher_static_version -g $file -o $outdir -j mesh_options.json $mesh_opt -n 360

