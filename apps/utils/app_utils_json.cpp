// Sriramajayam

#include <app_utils.h>
#include <fstream>
#include <filesystem>

namespace app
{
   // domain parameters
  um2::Box read_json_domain_params(const std::string tag, std::istream& stream)
  {
    stream.seekg(std::ios::beg);
    auto J = nlohmann::json::parse(stream);
    auto it = J.find(tag);
    assert(it!=J.end());
    assert(it->contains("center") && it->contains("size"));
    um2::Box box;
    double size;
    (*it)["center"].get_to(box.center);
    (*it)["size"].get_to(box.size);

    return box;
  }

  // medial axis parameters
  um2::MedialAxisParams read_json_medial_axis_params(const std::string tag, std::istream& stream)
  {
    stream.seekg(std::ios::beg);
    auto J = nlohmann::json::parse(stream);
    auto it = J.find(tag);
    assert(it!=J.end());
    assert(it->contains("max medial axis radius") &&
	   it->contains("medial axis exclusion radius") &&
	   it->contains("medial axis radius tol"));
    um2::MedialAxisParams maxis_params;
    (*it)["medial axis radius tol"].get_to(maxis_params.eps_radius);
    (*it)["max medial axis radius"].get_to(maxis_params.maximum_radius);
    (*it)["medial axis exclusion radius"].get_to(maxis_params.exclusion_radius);

    um2::validate_input(maxis_params);
    return maxis_params;
  }

  // root finding parameters
  um2::utils::RootFindingParams read_json_root_finding_params(const std::string tag, std::istream& stream)
  {
    stream.seekg(std::ios::beg);
    auto J = nlohmann::json::parse(stream);
    auto it = J.find(tag);
    assert(it!=J.end());
    assert(it->contains("num converged digits") &&
	   it->contains("max iters") &&
	   it->contains("non dim tolerance"));
    
    um2::utils::RootFindingParams root_params;
    (*it)["num converged digits"].get_to(root_params.digits);
    (*it)["max iters"].get_to(root_params.max_iter);
    (*it)["non dim tolerance"].get_to(root_params.normTol);

    return root_params;
  }
  
  // meshing parameters
  um2::MeshingParams read_json_meshing_params(const std::string tag, std::istream& stream)
  {
    stream.seekg(std::ios::beg);
    auto J = nlohmann::json::parse(stream);
    auto it = J.find(tag);
    assert(it!=J.end());
    assert(it->contains("nonlocal refinement") &&
	   it->contains("refinement factor") && 
	   it->contains("num projection steps") &&
	   it->contains("num interior relaxation iters") &&
	   it->contains("num interior relaxation rings") &&
	   it->contains("num boundary relaxation iters") &&
	   it->contains("num boundary relaxation samples") &&
	   it->contains("min quality") && 
	   it->contains("max num trys"));

    um2::MeshingParams mesh_params;
    (*it)["nonlocal refinement"].get_to(mesh_params.nonlocal_refinement);
    (*it)["refinement factor"].get_to(mesh_params.refinement_factor);
    (*it)["num projection steps"].get_to(mesh_params.num_proj_steps);
    (*it)["num interior relaxation iters"].get_to(mesh_params.num_dvr_int_relax_iters);
    (*it)["num interior relaxation rings"].get_to(mesh_params.num_dvr_int_relax_dist);
    (*it)["num boundary relaxation iters"].get_to(mesh_params.num_dvr_bd_relax_iters);
    (*it)["num boundary relaxation samples"].get_to(mesh_params.num_dvr_bd_relax_samples);
    (*it)["min quality"].get_to(mesh_params.min_quality);
    (*it)["max num trys"].get_to(mesh_params.max_num_trys);

    um2::validate_input(mesh_params);
    return mesh_params;
  }


  // geometry parameters
  std::pair<int,double> read_json_geometry_params(const std::string tag, std::istream& stream)
  {
    stream.seekg(std::ios::beg);
    auto J = nlohmann::json::parse(stream);
    auto it = J.find(tag);
    assert(it!=J.end());
    assert(it!=J.end());
    assert(it->contains("num samples per feature") &&
	   it->contains("corner separation tol"));

    int nSamples;
    double corner_tol;
    (*it)["num samples per feature"].get_to(nSamples);
    (*it)["corner separation tol"].get_to(corner_tol);

    assert(nSamples>0 && corner_tol>0.);
    return {nSamples, corner_tol};
  }
  
} // app::
