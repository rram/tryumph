// Sriramajayam

#include <app_utils.h>

namespace app {
  std::list<std::pair<double,double>> inspect_geometry_approximation(const int case_id,
								     const um2::MeshManager<um2::TriangleMesh>& mesh_manager,
								     const um2::FeatureSet& fset)
  {
    const auto& WM = mesh_manager.getWorkingMesh(case_id);
    std::list<std::pair<double,double>> dist_approx{};
    const int num_features = fset.getNumFeatures();
    const auto& feature_params = fset.getFeatureParams();
    const auto& fm_correspondences = mesh_manager.getFeatureMeshCorrespondence(case_id);
    for(int f=0; f<num_features; ++f)
      {
	const int curve_id = feature_params[f].curve_id;
	auto it = fm_correspondences.find(curve_id);
	assert(it!=fm_correspondences.end());
	const auto& fc = it->second;
	const auto& PosVerts = fc.PosVerts;
	const int nPosVerts = static_cast<int>(PosVerts.size());
	for(int i=0; i<nPosVerts-1; ++i)
	  {
	    const double* X = WM.coordinates(PosVerts[i]);
	    const double* Y = WM.coordinates(PosVerts[i+1]);
	    double Z[] = {0.5*(X[0]+Y[0]), 0.5*(X[1]+Y[1])};
	    double sd, dsd[2];
	    feature_params[f].signed_distance_function(Z, sd, dsd);
	    double h = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	    dist_approx.push_back({std::abs(sd),h});
	  }
      }
    return std::move(dist_approx);
  }
}
