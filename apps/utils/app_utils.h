// Sriramajayam

#pragma once

#include <TryUMph>
#include <TryUMph_utils>

#include <json.hpp>
#include <CLI11.hpp>

namespace app
{
  // read from a json file

  // domain parameters
  um2::Box read_json_domain_params(const std::string tag, std::istream& stream);

  // medial axis parameters
  um2::MedialAxisParams read_json_medial_axis_params(const std::string tag, std::istream& stream);

  // root finding parameters
  um2::utils::RootFindingParams read_json_root_finding_params(const std::string tag, std::istream& stream);

  // meshing parameters
  um2::MeshingParams read_json_meshing_params(const std::string tag, std::istream& stream);

  // geometry parameters
  std::pair<int,double> read_json_geometry_params(const std::string tag, std::istream& stream);

  // examine the mesh size at positive vertices relative to local and global features
  std::list<std::array<double,3>> inspect_mesh_size(const int case_id,
						    const um2::MeshManager<um2::TriangleMesh>& mesh_manager,
						    const um2::FeatureSet& fset);

  // examine the distances to features at midpoints of positive edges
  std::list<std::pair<double,double>> inspect_geometry_approximation(const int case_id,
								     const um2::MeshManager<um2::TriangleMesh>& mesh_manager,
								     const um2::FeatureSet& fset);

  // examine element qualities
  std::list<double> inspect_element_qualities(const int case_id,
					      const um2::MeshManager<um2::TriangleMesh>& mesh_manager);
}
  

