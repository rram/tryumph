// Sriramajayam

#include <app_utils.h>

namespace app {
  
  // examine the mesh size at positive vertices relative to local and global features
  std::list<std::array<double,3>> inspect_mesh_size(const int case_id,
						    const um2::MeshManager<um2::TriangleMesh>& mesh_manager,
						    const um2::FeatureSet& fset) {
    const auto& WM = mesh_manager.getWorkingMesh(case_id);
    const auto& mesh_wrapper = mesh_manager.getWorkingMeshWrapper(case_id);
    const auto& fm_correspondences = mesh_manager.getFeatureMeshCorrespondence(case_id);
    
    // loop over positive vertices for each feature
    // compute the local mesh size
    // compute the distance to the feature's medial axis
    // record the ratio
    std::list<std::array<double,3>> ratios{};
    const int num_features = fset.getNumFeatures();
    const auto& feature_params = fset.getFeatureParams();
    for(int f=0; f<num_features; ++f)
      {
	const int& curve_id = feature_params[f].curve_id;
	auto it = fm_correspondences.find(curve_id);
	assert(it!=fm_correspondences.end());
	const auto& fc = it->second;
	const auto& PosVerts = fc.PosVerts;

	// examine mesh size ratio at each positive vertex
	for(auto& v:PosVerts)
	  {
	    const double* X = WM.coordinates(v);
	    const auto& nbs = mesh_wrapper.Get1RingVertices(v);
	    double h = 0.;
	    for(auto& w:nbs)
	      {
		const double* Y = WM.coordinates(w);
		h += std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      }
	    h /= static_cast<double>(nbs.size());

	    // local feature size
	    const double r_local = fset.getDistanceToMedialAxis(X, curve_id);

	    // global feature size
	    bool is_computed;
	    double r_global;
	    double cpt_global[2];
	    fset.getComplementaryFeatureSize(X, curve_id, is_computed, r_global, cpt_global);
	    if(is_computed==true)
	      ratios.push_back({h, r_local, r_global});
	  }
      }
    return std::move(ratios);
  }
  
}
