// Sriramajayam

#include <app_utils.h>
#include <DVRlib>

namespace app {

  // examine element qualities
  std::list<double> inspect_element_qualities(const int case_id,
					      const um2::MeshManager<um2::TriangleMesh>& mesh_manager)
  {
    using WM_Type = um2::WorkingMesh<um2::TriangleMesh>;
    using WM_Wrapper_Type = um2::MeshWrapper<WM_Type>;
      
    // access the working mesh and wrapper
    const auto& WM = mesh_manager.getWorkingMesh(case_id);
    auto& wm_wrapper = const_cast<WM_Wrapper_Type&>(mesh_manager.getWorkingMeshWrapper(case_id));
    
    // create element quality metric
    dvr::GeomTri2DQuality<WM_Type> quality(wm_wrapper, 1);

    // evaluate element qualities
    std::list<double> elm_qualities{};
    const int nelm = WM.n_elements();
    for(int e=0; e<nelm; ++e)
      elm_qualities.push_back(quality.Compute(e));

    return elm_qualities;
  }
  
  
} // namespace app
