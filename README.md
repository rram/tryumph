# About TryUMph {#introduction}

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

**TryUMph** is an open-source C++ library to generate
geometrically-accurate planar triangle meshes.  The algorithm it
implements is a specific realization of the idea of **Universal
Meshes(UM)** introduced in [Rangarajan & Lew. "Universal meshes: A method
for triangulating planar curved domains immersed in nonconforming
meshes." IJNME 98.4 (2014): 236-264.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4624).


**Authors** <br />
__[Ramsharan Rangarajan](https://mecheng.iisc.ac.in/~rram)__,  Indian
Institute of Science Bangalore, India.  <br /> 
   __[Adrian Lew](https://stanford.edu/~lewa)__, Stanford University,
USA. 

## [Library documentation](https://rram.bitbucket.io/tryumph-docs/html/index.html)
- [Getting started](https://rram.bitbucket.io/tryumph-docs/html/getstarted.html)
- [User guide](https://rram.bitbucket.io/tryumph-docs/html/userguide.html)
- [Performance](https://rram.bitbucket.io/tryumph-docs/html/performance.html)
- [Sample geometries](https://rram.bitbucket.io/tryumph-geoms.zip)
  

## Why a new meshing library?
TryUMph is more than an alternative to existing mesh generation
libraries (eg., [Gmsh](http://gmsh.info),
[CGAL](https://doc.cgal.org/latest/Mesh_2/index.html#Chapter_2D_Conforming_Triangulations_and_Meshes),
[Triangle](https://www.google.com/search?client=safari&rls=en&q=quake+triangle&ie=UTF-8&oe=UTF-8))

**TryUMph**:

- implements the UM algorithm. Unlike conventional triangulation
  algorithms (e.g., Delaunay, advancing front), TryUMph transforms
  background meshes to conform to immersed geometries while
  preserving element connectivities. It does so without resorting to
  remeshing techniques (e.g., trimming cells, edge-flips).

- is designed to mesh not one, but a *sequence of geometries*. By
  maintaining persistent data structures (background meshes and
  quadtrees), it computes meshes conforming to successive geometries
  efficiently and robustly.

- enables efficient simulations of moving boundary problems involving
  large changes in geometry. Whenever possible, TryUMph retains the
  background mesh unchanged when meshing successive geometry iterates.
  Simulation algorithms can exploit this feature to reduce overheads
  in allocating and distributing data structures.

- achieves conformity by projecting a select collection of nodes of
  the background mesh onto the immersed geometry. In particular, mesh
  conformity is *not limited to a predetermined set of points
  sampling the geometry (boundaries, interfaces)*.

- generates and adapts background meshes automatically, ensuring mesh
  refinement commensurate with geometric features.

- is for all practical purposes, automatic. It requires a few 
  algorithmic parameters that do not strongly affect its performance.
  
- provides users considerable freedom in reperesentations and data
  structures for geometries and meshes that can enable its integration
  with numerical simulation codes.


## Capabilities
**TryUMph**:
- generates triangle meshes conforming to non-smooth geometries
- adapts background meshes to conform to evolving geometries through constructive
  connectivity-preserving vertex perturbations 
- handles large geometry changes
- automatically sizes and refines meshes

<br />

![capability1](docs/images/capability-1.png "Fig 1. Meshing with TryUMph") 

The image on the left in Fig 1 shows a collection of 10 features
(f1-f10). Each feature is a simple open curve, and pairs of features
intersect at common endpoints. Given this input, TryUMph returns:

- a background mesh refined commensurate to the geometry
- a mesh conforming to the features that differs form the bakground mesh only in the posititions of vertices in the vicinity of the features
- detailed correspondences between vertices/edges/triangles of the conforming mesh and the set of features.

<br />
![capability-2](docs/images/capability-2.png "Fig 2. Meshing a moving geometry with TryUMph ('conn' = mesh connectivity)")
<br />
 
Fig 2 illustrates how TryUMph handles an evolving geometry. The
inner triangle rotates by 180° in increments of
22.5°. At each step, TryUMph first attempts to reuse the existing
background mesh to compute a conforming triangulation. If successful,
the resulting mesh differs from the previous one in just the locations
of a few vertices in the vicinity of the geometry. The number of
vertices, the number of triangles, as well as the connectivity of the
conforming mesh remains unchanged. Otherwise, TryUMph adapts the
background mesh to compute a conforming triangulation to the new
geometry.  In the example shown, over the course of rotating the
triangle by 180°, the background mesh is recomputed *just once*.

## Incapabilities

**TryUMph does not**:
- handle *dirty* geometry, e.g., features with self-intersections, or distinct features intersecting at points that are not their common corners
- handle geometries with *cusps*, i.e., features with non-transverse intersections
- handle geometries with more than two features intersecting at a
  corner (e.g., triple-junctions)
- support interactive mesh editing
- generate non-simplicial meshes (e.g., quads)


## Acknowledge
If you use TryUMph in your research, please <a
 href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=TryUMph"
 target="_top">let us know</a> and acknowledge its use as 
 ```verbatim
 @article{rangarajan2020dvrlib,
  title = {TryUMph: A C++ library for triangulating planar evolving non-smooth geometries with Universal Meshes},
  author = {Rangarajan, Ramsharan and Lew, Adrian},
  journal = {Journal of Open Source Software},
  volume = {X},
  number = {X},
  pages = {X},
  year = {2024}
}

## Contributing to TryUMph
Please <a
 href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=Universal Meshes"
 target="_top">get in touch</a> if you would like to contribute to the
development of TryUMph.
<br />
Found a bug? Please raise an issue on [bitbucket](https://bitbucket.org/rram/um/src/) page of the library.

## More on Universal Meshes

Bibtex entries of research articles on Universal Meshes is [here](https://rram.bitbucket.io/tryumph-docs/tryumph-bibtex.bib).

### Articles on UM

- [Rangarajan et al., "An algorithm for triangulating smooth three‐dimensional domains immersed in universal meshes." IJNME 117.1
  (2019): 84-117.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.5949)  

- [Rangarajan et al., "Universal meshes: A method for triangulating planar curved domains immersed in nonconforming meshes." IJNME 98.4
  (2014): 236-264.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4624)

- [Rangarajan et al., "Analysis of a method to parameterize planar
  curves immersed in triangulations." SINUM 51.3 (2013): 1392-1420.](https://epubs.siam.org/doi/abs/10.1137/110831805)

### Representative applications 

#### Fracture mechanics
- [Chiaramonte et al., "Numerical analyses of crack path instabilities in quenched plates." Extreme Mech.Letters 40 (2020):100878.](https://www.sciencedirect.com/science/article/pii/S2352431620301577)
- [Grossman‐Ponemon et al., "An algorithm for the simulation of curvilinear plane‐strain and axisymmetric hydraulic fractures with lag using the universal meshes." Int. J. Num. Analytical Meth. Geomechanics 43.6 (2019): 1251-1278.](https://onlinelibrary.wiley.com/doi/full/10.1002/nag.2896)
- [Rangarajan et al., "Simulating curvilinear crack propagation in two dimensions with universal meshes." IJNME 102.3-4 (2015): 632-670.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4731)

#### Fluid-structure interaction
- [Gawlik et al., "High‐order methods for low Reynolds number flows around moving obstacles based on universal meshes." IJNME 104.7 (2015): 513-538.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.4891)

#### Shape optimization
- [Sharma et al., "A shape optimization approach for simulating contact of elastic membranes with rigid obstacles." IJNME 117.4 (2019): 371-404.](https://onlinelibrary.wiley.com/doi/abs/10.1002/nme.5960)

## License

MIT License

Copyright (c) [2024] [Ramsharan Rangarajan]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
