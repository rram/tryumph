// Sriramajayam

#pragma once

#include <vector>
#include <cassert>
#include <memory>

namespace um2
{
  //! \brief Simple class for a quadtree node.
  /** This is a simple class for a quadtree node. It inherits the class
      TreeNode. Instances of this class will be put together to form a
      quadtree structure. Each node is physically a square in R2. The
      main contents of a quadtree node are:
      1. Ability to add children, by which a node is subdivided into 4.
      2. Pointers to each of its 4 children.
      3. Pointer to its parent.
    
      To create a quadtree node, a pointer to its parent node has to be
      supplied along with the coordinates of its center and its size. At
      construction, pointers to children as set to null.
    
      The main functionality of adding children to a quadtree nodes
      proceeds by subdividing the current square into 4 smaller
      ones. The addresses of the nuly created child nodes are copied
      and returned.
    
      Child nodes are numbered from 0 throught 3 with the following
      convention, based on which corner node the child contains: XpYp,
      XmYp, XmYm, XpYm.

      This is a simple class for a tree node. Instances of this class
      will be put together to form a quadtree/octree structure. Each
      node is physically a square in R2/R3. The main contents of a tree
      node are: 
      1. Coordinates of the center.  
      2. Size of the node
      3. Ability to add children, by which a node is subdivided into 
      4. Pointers to each of its children.  
      5. Pointer to its parent.
      
      To create a tree node, the coordinates of its center and its size
      have to be provided. At construction, the integer and double
      vectors are cleared. 
    
      For convenience, the addresses to parent nodes, the ability to add
      children and the access to childnodes are to be implemented in the
      derived classes.
  */

  class QuadtreeNode: public std::enable_shared_from_this<QuadtreeNode>
  {
  public:
    //! Constructor
    //! \param iParentAddress Address of parent node.
    //! \param iCenter Coordinates of center of node.
    //! \param iMySize Length of side of square
  QuadtreeNode(std::shared_ptr<QuadtreeNode> paddress,
	       const double* icenter,
	       const double mysize)
    :Center{icenter[0],icenter[1]},
      MySize(mysize),
	Corners{icenter[0]+0.5*mysize,icenter[1]+0.5*mysize,
	  icenter[0]-0.5*mysize,icenter[1]+0.5*mysize,
	  icenter[0]-0.5*mysize,icenter[1]-0.5*mysize,
	  icenter[0]+0.5*mysize,icenter[1]-0.5*mysize},
	ParentAddress(paddress),
	  Children{}
	{}
      
	//! Destructor
	inline virtual ~QuadtreeNode() {}
	
	//! Disable copy and assignment
	QuadtreeNode(const QuadtreeNode&) = delete;
	QuadtreeNode operator=(const QuadtreeNode&) = delete;

	//! Returns coordinate of the requested corner
	//! \param cnrnum Corner number
	const double* getCornerCoordinates(int cnrnum) const
	{ return &Corners[2*cnrnum]; }

	//! Returns coordinates of the center of this node.
	inline const double* getCenterCoordinates() const
	{ return Center; }
  
	//! Returns the size of this node
	inline double getNodeSize() const
	{ return MySize; }

	//! Return parent address
	inline std::shared_ptr<QuadtreeNode>& getParentAddress() 
	{ return ParentAddress; }
	
  
	//! Returns pointer to requested child
	//! \param childnum Childnumber whose pointer to return
	inline std::shared_ptr<QuadtreeNode>& getChild(const int childnum) 
	{
	  if(Children.empty())
	    assert(false && "um2::QuadtreeNode: accessesing non-existent child");
	    //return std::shared_ptr<QuadtreeNode>(nullptr);
	  else
	    return Children[childnum];
	}
      
	//! Returns the number of children.
	inline int getNumberOfChildren() const
	{ return static_cast<int>(Children.size()); }
      
	//! Functionality to add children
	inline void addChildren()
	{
	  // Add four children
	  assert(Children.empty());
	  const double child_centers[] = {Center[0]+0.25*MySize,Center[1]+0.25*MySize,
					  Center[0]-0.25*MySize,Center[1]+0.25*MySize,
					  Center[0]-0.25*MySize,Center[1]-0.25*MySize,
					  Center[0]+0.25*MySize,Center[1]-0.25*MySize};
	  const double child_size = 0.5*MySize;
	  for(int c=0; c<4; ++c)
	    Children.push_back( std::make_shared<QuadtreeNode>( shared_from_this(), child_centers+2*c, child_size) );
	}


	//! Functionality to prune children
	inline void prune()
	{
	  if(!Children.empty())
	    for(int c=0; c<4; ++c)
	      {
		assert(Children[c]->getNumberOfChildren()==0);
		Children[c].reset();
	      }
	  Children.clear();
	}

  private:
	const double Center[2]; //!< Coordinates of the center of this node
	const double MySize; //!< Size of this node
	const double Corners[8]; //! Coordinates of cornersstd::array<double>
	std::shared_ptr<QuadtreeNode> ParentAddress; //! Parent node address
	std::vector<std::shared_ptr<QuadtreeNode>> Children; //! Addresses of children
  };
}

