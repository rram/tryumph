// Sriramajayam

#include <um2_Quadtree.h>

namespace um2
{
  // Determine the neighbor in the requested direction
  std::shared_ptr<QuadtreeNode> Quadtree::findNeighbor(const std::shared_ptr<QuadtreeNode>& QN,
						       const std::string dir) const
  {
    // If given node is the root, return NULL.
    if(QN==RootNode)
      return std::shared_ptr<QuadtreeNode>(nullptr);

    // Check direction spec
    assert(dir[0]=='X' || dir[0]=='Y');
    assert(dir[1]=='P' || dir[1]=='M');

    // Index I: direction requested X=0, Y=1.
    const int I = (dir[0]=='X') ? 0 : 1;
      
    // Index J for remaining direction.
    const int J = (I+1)%2;
  
    // Orientation in coordinate I: 0 for negative and 1 for positive.
    const int orientI = (dir[1]=='P') ? 1 : 0;
      
    // Encode negative direction with 0 and positive with 1.
    // Each of the four quadrants is assigned a two digit identification.
    // Assign the child number for each encoded quadrant
    std::map<std::vector<int>,int> QdToChildMap{};
    std::vector<int> qd(2);
    { qd[0] = 1; qd[1] = 1; QdToChildMap[qd] = 0;
      qd[0] = 0; qd[1] = 1; QdToChildMap[qd] = 1;
      qd[0] = 0; qd[1] = 0; QdToChildMap[qd] = 2;
      qd[0] = 1; qd[1] = 0; QdToChildMap[qd] = 3; }

    // Get the parent of this node.
    auto parent = QN->getParentAddress();
      
    // If QN has an opposite orientation along direction I in its parent node,
    // return the corresponding orientI node of its parent.
    for(int orientJ=0; orientJ<2; orientJ++)
      {
	qd[I] = (orientI+1)%2;
	qd[J] = orientJ;
	int childnum = QdToChildMap[qd];
	if( QN==parent->getChild(childnum) )
	  {
	    // Return child on side I.
	    qd[I] = orientI;
	    childnum = QdToChildMap[qd];
	    return parent->getChild(childnum);
	  }
      }
      
    // Neighbor of QN lies in the neighbor of its parent node.
    // Find the neighbor of the parent in the requested direction
    auto nb = findNeighbor(parent, dir);
  
    // If neighbor is non-existant or a leaf, return it.
    if(nb==nullptr)
      return nb;
    else if(nb->getNumberOfChildren()==0)
      return nb;
      
    // Otherwise, return the corresponding node of the neighbor
    for(int orientJ=0; orientJ<2; orientJ++)
      {
	qd[I] = orientI;
	qd[J] = orientJ;
	int childnum = QdToChildMap[qd];
	  
	if( QN==parent->getChild(childnum) )
	  {
	    // Return the coresponding child of the parent's neighbor's child
	    qd[I] = (orientI+1)%2;
	    childnum = QdToChildMap[qd];
	    return nb->getChild(childnum);
	  }
      }
      
    assert(false && "um2::Quadtree::findNeighbor()- No reason to be here");
    return std::shared_ptr<QuadtreeNode>(nullptr);
  }


  
  // Find the corner neighbor
  // Explanation:
  // Suppose cnr = XPYP.
  
  // 1. If QN is the XMYM child of its parent, return the XPYP child of the parent.
  // The returned node has the same size as QN.

  // 2. If QN is the XMYP child of its parent. Let nb1 be the neighbor of QN along YP.
  // Let nb2 be the neighbor of the XPYP sibling along YP.
  // If nb1=nb2, then this neighbor is necessarily a leaf node. The required neighbor
  // does not exist.
  // On the otherhand, if nb1 and nb2 are different, return nb2.
  // Again, notice that nb2 has same size as QN or is larger. If larger, it is a leaf node.

  // 3. If QN is the XPYM child of its parent. Let nb1 be XP neighbor of QN.
  // Let nb2 be the XP neighbor of sibling XPYP.
  // If nb1=nb2, there is no neighbor since nb1 is a larger leaf node.
  // Otherwise, return nb2.

  // 4. Finally, if QN is the XPYP child of its parent.
  // Find the XPYP corner neighbor of the parent and return its XMYM child.

  std::shared_ptr<QuadtreeNode> Quadtree::findCornerNeighbor(const std::shared_ptr<QuadtreeNode>& QN,
							      const std::string cnr) const
  {
    // Encode child numbers with strings.
    const std::vector<std::string> ChildNumToString{"XPYP", "XMYP", "XMYM", "XPYM"};
      
    std::map<std::string, int> StringToChildNum{};
    StringToChildNum["XPYP"] = 0;
    StringToChildNum["XMYP"] = 1;
    StringToChildNum["XMYM"] = 2;
    StringToChildNum["XPYM"] = 3;
      
    const char Xorient = cnr[1];
    const char Yorient = cnr[3];
  
    char notXorient = 'P';
    if( Xorient=='P' )
      notXorient = 'M';
  
    char notYorient = 'P';
    if( Yorient=='P' )
      notYorient = 'M';
  
    std::string cnrflip;
    cnrflip.clear();
    cnrflip.push_back( 'X' );
    cnrflip.push_back( notXorient );
    cnrflip.push_back( 'Y' );
    cnrflip.push_back( notYorient );
  
    // Parent of QN.
    auto parent = QN->getParentAddress();
    
    // QN is the root node
    if( parent==nullptr ) 
      return std::shared_ptr<QuadtreeNode>(nullptr);
    else
      {
	// Determine which child QN is of its parent.
	int mynum = -1;
  
	for(int c=0; c<4; c++)
	  if( QN==parent->getChild(c) )
	    { mynum = c;
	      break; }
	assert(mynum!=-1);

	std::string mylabel = ChildNumToString[mynum];
	  
	// Compare "mylabel" and "cnr".
  
	// 1. If "mylabel" equals "cnr", 
	// - Find "cnr" neighbor of parent.
	// - Return its "cnrflip" child if present.
	if( mylabel==cnr )
	  {
	    auto parent_cnr_nb = findCornerNeighbor(parent, cnr);
      
	    // No neighbor if parent has no neighbor
	    if( parent_cnr_nb==nullptr )
	      return 0;
      
	    // Return parent's neighbor if it is a leaf.
	    if( parent_cnr_nb->getNumberOfChildren()==0 )
	      return parent_cnr_nb;
      
	    // Otherwise, return "cnrflip" child of this neighbor
	    int childnum = StringToChildNum[cnrflip];
	    return parent_cnr_nb->getChild( childnum );
	  }
     
	// 2. If "mylabel" equals "cnrflip",
	// - return "cnr" sibling.
	else if( mylabel==cnrflip )
	  {
	    int childnum = StringToChildNum[cnr];
	    return parent->getChild( childnum );
	  }
  
	// 3. Precisely one of the X and Y orientation of "mylabel" and "cnr" match.
	// - Find "cnr" sibling.
	// - Find neighbors along direction where orientations match
	// - If these neighors are same, return NULL.
	// - Otherwise, return the neighbor of the sibling.
	else
	  {
	    auto sibling = parent->getChild( StringToChildNum[cnr] );
      
	    // Direction along with to find neighbor- one where mylabel and cnr match
	    std::string dir;
	    dir.clear();
	    dir.resize(2);
	    if( mylabel[1]==cnr[1] )
	      {
		dir[0] = 'X';
		dir[1] = mylabel[1];
	      }
	    else
	      {
		dir[0] = 'Y';
		dir[1] = mylabel[3];
	      }
      
	    auto nb = findNeighbor(QN, dir);
	    auto sibling_nb = findNeighbor(sibling, dir);
      
	    // If both neighbors are the same, we have a leaf. No corner neighbor.
	    if( nb==sibling_nb )
	      return nullptr;
	    // Otherwise, return the neighbor of the sibling
	    else
	      return sibling_nb;
	  }
      }

    assert(false && "Quadtree::findCornerNeighbor()- No reason to be here.");

    return nullptr;
  }



  
  // Return the list of neighbors larger than the given node
  std::list<std::shared_ptr<QuadtreeNode>> Quadtree::getLargerNeighbors(const std::shared_ptr<QuadtreeNode>& QN) const
  {
    char dirName[] = "XY";
    char orientName[] = "MP";
  
    // Size of current node
    const double mysize = QN->getNodeSize();
  
    // Use some tolerance
    const double eps = mysize/100.;

    std::list<std::shared_ptr<QuadtreeNode>> LargerNbs{};
    if( QN==nullptr )
      return LargerNbs;
  
    // Encode negative direction with 0, positive with 1.
    // Each of the 4 quadtrants is assigned a two digit identification.
    // Assign the child number of each encoded quadrant.
    std::map<std::vector<int>, int> QdToChildMap{};
    {
      std::vector<int> qd(2);
      qd[0] = 1; qd[1] = 1; QdToChildMap[qd] = 0;
      qd[0] = 0; qd[1] = 1; QdToChildMap[qd] = 1;
      qd[0] = 0; qd[1] = 0; QdToChildMap[qd] = 2;
      qd[0] = 1; qd[1] = 0; QdToChildMap[qd] = 3;
    }
  
    // Loop pver two directions
    for(int I=0; I<2; ++I)
      // Loop over two possible orientations
      for(int orientI=0; orientI<2; ++orientI)
	{
	  // Request the neighbor in direction I along orientation orientI.
	  const std::string dir{dirName[I], orientName[orientI]};
	  auto nb = findNeighbor(QN, dir);
	
	  // Is neighbor larger than QN
	  if(nb!=nullptr)
	    if(nb->getNodeSize() > mysize+eps)
	      LargerNbs.push_back( nb );
	}
    return LargerNbs;
  }


  // Identify larger corner neighbors
  std::list<std::shared_ptr<QuadtreeNode>> Quadtree::getLargerCornerNeighbors(const std::shared_ptr<QuadtreeNode>& QN) const
  {
    const std::vector<std::string> Corners{"XPYP", "XMYP", "XMYM", "XPYM"};
    const std::vector<int> FlipCorners{2,3,0,1};
    std::list<std::shared_ptr<QuadtreeNode>> LargerNbs{};
      
    if( QN==nullptr )
      return LargerNbs;
  
    const double h = QN->getNodeSize();
    const double eps = h/100.;
  
    for(int c=0; c<4; ++c)
      {
	auto nb = findCornerNeighbor( QN, Corners[c] );
	if( nb!=nullptr )
	  if( nb->getNodeSize() > h+eps )
	    LargerNbs.push_back( nb );   
      }

    // Debugging: Check that all nodes are leaf nodes
    for(auto& qnode:LargerNbs)
      assert(qnode->getNumberOfChildren()==0);

    return LargerNbs;
  }


  // Decide whether a given node needs to be split for balancing
  bool Quadtree::splitNode(const std::shared_ptr<QuadtreeNode>& QN) const
  {
    char dirName[] = "XY";
    char orientName[] = "MP";

    // Encode negative direction with 0, positive with 1.
    // Each of the 4 quadtrants is assigned a two digit identification.
    // Assign the child number of each encoded quadrant.
    std::map<std::vector<int>, int> QdToChildMap{};
    {
      std::vector<int> qd(2);
      qd[0] = 1; qd[1] = 1; QdToChildMap[qd] = 0;
      qd[0] = 0; qd[1] = 1; QdToChildMap[qd] = 1;
      qd[0] = 0; qd[1] = 0; QdToChildMap[qd] = 2;
      qd[0] = 1; qd[1] = 0; QdToChildMap[qd] = 3;
    }
  
    // Loop over each of the two directions.
    for(int I=0; I<2; I++)
      // Loop over the two possible orientations.
      for(int orientI=0; orientI<2; orientI++)
	{
	  // Request the neighbor in direction "I" along orientation "orientI"
	  const std::string dir{dirName[I], orientName[orientI]};
	  auto nb = findNeighbor(QN, dir);
	
	  // Neighbor nb has same size as QN or is larger than it.
	  // So, if nb is a leaf node, no need to split ON based on nb.
	  if(nb!=nullptr)
	    if(nb->getNumberOfChildren()>0)
	      {
		// Check if nb has children in the quadrant (~I)J that are not leaf nodes.
		int J = (I+1)%2;
		for(int orientJ=0; orientJ<2; orientJ++)
		  {
		    // quadrant code
		    std::vector<int> qd(2);
		    qd[I] = (orientI+1)%2;
		    qd[J] = orientJ;
		    int childnum = QdToChildMap[qd];
		    if(nb->getChild(childnum)->getNumberOfChildren()>0)
		      return true;
		  }
	      }
	}
    return false;
  }

  // Split a node baded on the corner neighbor
  bool Quadtree::splitNodeForCorner(const std::shared_ptr<QuadtreeNode>& QN) const
  {
    const std::vector<std::string> Corners{"XPYP", "XMYP", "XMYM", "XPYM"};

    // Corresponding numbers of diagonally opposite corners.
    const std::vector<int> FlipCorners{2,3,0,1};

    for(int c=0; c<4; ++c)
      {
	auto nb = findCornerNeighbor( QN, Corners[c] );
      
	// If the neighbor is larger, it is a leaf node.
	// No need to split based on it.
      
	// If the neighbor is the same size, and has no children,
	// no need to split based on it.
      
	// Otherwise, check the flip-corner and check if it has children.
	// Then, split.
	if( nb!=nullptr )
	  if( nb->getNumberOfChildren()>0 )
	    // nb is the same size as QN.
	    if( nb->getChild( FlipCorners[c] )->getNumberOfChildren()>0 )
	      return true;
      }
    return false;
  }
  
}
