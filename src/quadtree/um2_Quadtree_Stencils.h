// Sriramajayam

#pragma once

#include <vector>

namespace um2
{
  namespace detail
  {
    //!< Coordinates and connectivities of stencils for meshing a balanced quadtree
    struct QTreeStencils
    {
      // 1. AAAA
      static const std::vector<double> coord_AAAA;
      static const std::vector<int> conn_AAAA;
     
      // 2. AAAB
      static const std::vector<double> coord_AAAB;
      static const std::vector<int> conn_AAAB;
     
      // 3. AABB
      static const std::vector<double> coord_AABB;
      static const std::vector<int> conn_AABB;
    
      // 4. ABAB
      static const std::vector<double> coord_ABAB;
      static const std::vector<int> conn_ABAB;
     
      // 5. ABBB
      static const std::vector<double> coord_ABBB;
      static const std::vector<int> conn_ABBB;
    
      // 6. ABCB
      static const std::vector<double> coord_ABCB;
      static const std::vector<int> conn_ABCB;
    
      // 7. BBBB
      static const std::vector<double> coord_BBBB;
      static const std::vector<int> conn_BBBB;
     
      // 8. BBBC
      static const std::vector<double> coord_BBBC;
      static const std::vector<int> conn_BBBC;
    
      // 9. BBCC
      static const std::vector<double> coord_BBCC;
      static const std::vector<int> conn_BBCC;
    };
  }
}

