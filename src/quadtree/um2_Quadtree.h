// Sriramajayam

#pragma once

#include <um2_QuadtreeNode.h>
#include <list>
#include <map>
#include <cmath>
#include <array>
#include <set>
#include <unordered_map>
#include <fstream>
#include <cstdlib>
#include <algorithm>

//! \brief This is the class for an quadtree.
/** This is a simple class for an quadtree. An quadtree is a tree
    structure in which each node of the tree is a square in R2.  Each
    node either has 4 children or none at all. A node that does not
    have children is called a leaf node. 

    An quadtree structure is initiated by providing the center and
    size of its root node. The actual construction of the tree itself
    is a dynamic process. An existing tree can be refined at any time
    by various means. For examples-
    
    1. Uniform refinement- each leaf node is refined until the
    terminal size is less than a given size.
    
    2. Spatial decomposition of points- the tree is refined until a
    given set of points is decomposed into seperate leaf nodes in the
    tree.
    
    
    The important functionalities of the class are 
    
    1. Search- given a point in R2, determine the leaf node in which
    it belongs.
    
    2. Neighbors- determining neighbors of nodes.
    
    3. Balancing- recursively subdivide the tree so that any two
    nieghboring leaf nodes either have equal size or differ in size by
    a factor of two.
    
    4. plotting- plot all the leaf nodes in a given tree.
    
    5. Writing the quadtree structure to a file.
    
    The user has to keep track of what variables the integer and
    double containers in each node contains.
*/

namespace um2
{
  
  class Quadtree
  {
  public:
  
    //! Constructor
    //! \param iRootCenter Coordinates of center of root node
    //! \param iRootSize Size of root node.
    inline Quadtree(const double* rcenter, const double rsize)
      :RootCenter{rcenter[0], rcenter[1]},
      RootSize(rsize)
      {
	RootNode = std::make_shared<QuadtreeNode>(nullptr, RootCenter, RootSize);
      }
  
      //! Destructor
      inline ~Quadtree() {}
      
      // Disable copy and assignment
      Quadtree(const Quadtree&) = delete;
      Quadtree& operator=(const Quadtree&) = delete;

      //! Returns the center of the root node.
      inline const double* getRootNodeCenter() const
      { return RootCenter; }
      
      //! Returns the size of the root node.
      inline double getRootNodeSize() const
      { return RootSize; }
  
      //! Returns the root node.
      inline std::shared_ptr<QuadtreeNode> getRootNode() const
      { return RootNode; }
      
      //! Returns pointer to node in which a given point lies.
      //! \param Pt Point in R2 that needs to be located in the tree
      std::shared_ptr<QuadtreeNode> query(const double* Pt) const;
      
      //! Refine tree uniformly until all leaf nodes are small than a given size.
      //! \param Hmax Upper bound for leaf node size.
      void refine(const double Hmax);

      //! Refine tree so that the leaf node size at a point is smaller than specified
      //! \param Hmax Upper bound for leaf node size.
      void refine(const double* Pt, const double Hmax);
      
      //! Balances a quadtree so that two leaf node neighbors have size ratio at most 2.
      void balanceTree();
  
      //! Balance a tree strongly
      //! Make sure that no extended neighbors have size ratio grater than 2.
      void stronglyBalanceTree();

      //! Compute a mesh over a quadtree
      void triangulate(std::vector<double>& coordinates, std::vector<int>& connectivity);
      
      //! Plots all the leaf nodes in the tree
      //! \param filename Name of file
      void plotTec(const std::string filename) const;

      //! Returns all the leaf nodes in the tree.
      inline std::list<std::shared_ptr<QuadtreeNode>> getLeafNodesInTree() const
      { return getLeavesInNode(RootNode); }

      //! Returns all the leaf nodes inside the given node
      //! \param QN QuadtreeNode
      std::list<std::shared_ptr<QuadtreeNode>> getLeavesInNode(const std::shared_ptr<QuadtreeNode>& QN) const;

      //! Prune a quadtree by one level
      void prune();
      
      //! Determines the neighbor of a given node in the requested direction.
      //! The input or output nodes need not be leaf nodes.
      //! However, the returned node either has the same size as the given node or a larger size.
      //! The direction is a string: XP,XM,YP,YM with obvious direction and orientation.
      //! \param QN QuadtreeNode whose neighbor to find
      //! \param dir string for direction with orientation.
      std::shared_ptr<QuadtreeNode> findNeighbor(const std::shared_ptr<QuadtreeNode>& QN, const std::string dir) const;
      
      //! Determines the diagonal neighbor of a given node at requested corner
      //! The input or output nodes need not be leaf nodes.
      //! However, the returned node either has the same size as the given node or is larger.
      //! When the returned node is larger, it is necessarily a leaf node.
      //! The corner is specified as: XPYP, XPYM, XMYP, XMYM.
      //! \param QN QuadyreeNode whose corner neighbor to find
      //! \param cnr string for corner
      std::shared_ptr<QuadtreeNode> findCornerNeighbor(const std::shared_ptr<QuadtreeNode>& QN, const std::string cnr) const;

  private:
      //! Determines all the neighbors of given node that are larger than itself.
      //! \param QN QuadtreeNode whose neighbors to check
      std::list<std::shared_ptr<QuadtreeNode>> getLargerNeighbors(const std::shared_ptr<QuadtreeNode>& QN) const;
      
      //! Determines all the corner neighbors of a given node that are larger than itself.
      //! \param QN QuadtreeNode
      std::list<std::shared_ptr<QuadtreeNode>> getLargerCornerNeighbors(const std::shared_ptr<QuadtreeNode>& QN) const;

      //! Determines if a given node needs to be split.
      //! A node is split if it has neighbor of size smaller than half its size.
      //! This is done using the neighbor finding algorithm.
      //! For eg., check the XP neighbor.
      //! If the neighbor has children that themselves has children,
      //! then the current node is split. Repeat this check in all directions.
      //! \param QN QuadtreeNode which may have to be split
      bool splitNode(const std::shared_ptr<QuadtreeNode>& QN) const;
      
      //! Determines if a given node needs to be split based on extended neighbors.
      //! A node is split if it has an corner neighbor of size smaller than half its size.
      //! \param QN QuadtreeNode which needs to be split.
      bool splitNodeForCorner(const std::shared_ptr<QuadtreeNode>& QN) const;

      // Duplication map for a cloud of points
      static void pointDuplicationMap(const std::vector<double>& coordinates,
				      const double htol,
				      std::vector<int>& IndxMap);
   
      // Remove unused nodes and renumber connectivities in a simplicial mesh
      static void cullMesh(std::vector<double>& coordinates,
			   std::vector<int>& connectivity);
    
  private:
      const double RootCenter[2];   //! Center of root node
      const double RootSize; //! Size of root node
      std::shared_ptr<QuadtreeNode> RootNode; //! Root node
  };
   
}


