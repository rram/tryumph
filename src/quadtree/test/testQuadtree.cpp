// Sriramajayam

#include <um2_Quadtree.h>
#include <cstdlib>
#include <random>
#include <iostream> 

using namespace um2;

// Identify boundary ndoes in a mesh
std::set<int> GetBoundaryNodes(const std::vector<int>& connectivity);

void PrintNeighbors(Quadtree &QT, std::shared_ptr<QuadtreeNode> QN);

void PrintCornerNeighbors(Quadtree &QT, std::shared_ptr<QuadtreeNode> QN);

void TestBalancing(Quadtree &QT);

void TestStrongBalancing(Quadtree &QT);

void TestCornerNeighbors(Quadtree & QT);

// plot triangle mesh as a tec file
void plotTec(const std::string filename,
	     const std::vector<double>& coord,
	     const std::vector<int>& conn);

// Check that nodes in a mesh are sequentially numbered.
// Check that all boundary nodes lie on edges of the root node of a quadtree
void TestMesh(const std::vector<double>& coordinates,
	      const std::vector<int>& connectivity,
	      const double* center, const double hsize);

int main()
{
  const double center[] = {1.,2.};
  const double h = 10.;

  // Random number generators for location and size
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> LocDis(-1.,1.);
  std::uniform_real_distribution<> hDis(0.05, 1.);
  
  // Test uniform refinement
  {
    Quadtree QT(center, h);
    
    std::cout<<"\nCenter of tree: ("
	     <<QT.getRootNodeCenter()[0]<<","
	     <<QT.getRootNodeCenter()[1]<<").";
    
    std::cout<<"\nSize of tree: "
	     <<QT.getRootNodeSize();
    
    // plot the tree as it is now
    QT.plotTec("One.tec");
    
    // Refine tree for given size
    QT.refine( 5.0 );
    QT.plotTec("Four.tec");
    
    QT.refine( 2.5 );
    QT.plotTec("Eight.tec");
  }
  
  // Test for point-based refinement
  {
    // Set random sizes at random points within a quadtree
    const double zero[] = {0.,0.};
    const double H = 2.5;
    Quadtree QT(zero, H);
    for(int n=0; n<20; ++n)
      {
	double pt[] = {LocDis(gen), LocDis(gen)};
	double hval = hDis(gen);
	QT.refine(pt, hval);
      }
    
    // plot decomposing tree
    QT.plotTec("DecompTree.tec");
    
    // Test balancing
    QT.balanceTree();
    QT.plotTec((char*)"BalDecompTree.tec");
    std::cout<<"\nTesting balancing tree: ";
    TestBalancing(QT);
  }

  // Test neighbor finding
  {
    Quadtree QT(center, h);
    
    auto Root = QT.getRootNode();
    Root->addChildren();
    
    Root->getChild(0)->addChildren();
    
    Root->getChild(0)->getChild(0)->addChildren();
    
    QT.plotTec((char*)"NbTree.tec");
    
    std::cout<<"\nTesting neighbors of nodes: \n";

    auto QN = Root->getChild(0)->getChild(0)->getChild(0);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(5.375,6.375): NA, NA, (4.125,6.375), (5.375,5.125),\n";
    
    QN = Root->getChild(0)->getChild(0)->getChild(1);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(4.125,6.375): (5.375,6.375), NA, (2.25,5.75), (4.125,5.125),\n";
    
    QN = Root->getChild(0)->getChild(0)->getChild(2);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(4.125,5.125): (5.375,5.125), (4.125,6.375), (2.25,5.75), (4.75,3.25),\n";
    
    QN = Root->getChild(0)->getChild(0)->getChild(3);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(5.375,5.125): NA, (5.375,6.375), (4.125,5.125), (4.75,3.25),\n";
    
    QN = Root->getChild(0)->getChild(0);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(4.75,5.75): NA, NA, (2.25,5.75), (4.75,3.25),\n";
    
    QN = Root->getChild(0)->getChild(1);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(2.25,5.75): (4.75,5.75), NA, (-1.5,4.5), (2.25,3.25),\n";
    
    QN = Root->getChild(0)->getChild(2);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(2.25,3.25): (4,75,3.25), (2.25,5.75), (-1.5,4.5), (3.5,-0.5),\n";
    
    QN = Root->getChild(0)->getChild(3);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(4,75,3.25): NA, (4.75,5.75), (2.25,3.25), (3.5,-0.5),\n";
    
    QN = Root->getChild(0);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(3.5,4.5): NA, NA, (-1.5,4.5), (3.5,-0.5),\n";

    QN = Root->getChild(1);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(-1.5,4.5): (3.5,4.5), NA, NA, (-1.5,-0.5),\n";

    QN = Root->getChild(2);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(-1.5,-0.5): (3.5,-0.5), (-1.5,4.5), NA, NA,\n";
    
    QN = Root->getChild(3);
    PrintNeighbors(QT, QN);
    std::cout<<"\nShould read as \n"
	     <<"(3.5,-0.5): NA, (3.5,4.5), (-1.5,-0.5), NA,\n";
    
    // Test balancing
    Root->getChild(0)->getChild(0)->getChild(1)->addChildren();
    QT.plotTec("PreBalTree.tec");
    std::cout<<"\nTesting balancing the tree: ";
    QT.balanceTree();
    QT.plotTec((char*)"BalTree.tec");
    TestBalancing(QT);
  }
  
  // Test corner neighbor finding
  {
    const double cen[] = {0.,0.};
    Quadtree QT(cen, h);
    
    auto Root = QT.getRootNode();
    Root->addChildren();
    Root->getChild(0)->addChildren();
    Root->getChild(0)->getChild(0)->addChildren();
    
    std::cout<<"\nTESTING CORNER NEIGHBORS.";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(0)->getChild(0) );
    std::cout<<"\nShould read as \n"
	     <<"(4.375,4.375): NA, NA, (3.125,3.125), NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(0)->getChild(1) );
    std::cout<<"\nShould read as \n"
	     <<"(3.125,4.375): NA, NA, NA, (4.375,3.125), ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(0)->getChild(2) );
    std::cout<<"\nShould read as \n"
	     <<"(3.125,3.125): (4.375,4.375), NA, (1.25,1.25), NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(0)->getChild(3) );
    std::cout<<"\nShould read as \n"
	     <<"(4.375,3.125): NA, (3.125,4.375), NA, NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(0) );
    std::cout<<"\nShould read as \n"
	     <<"(3.75,3.75): NA, NA, (1.25,1.25), NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(1) );
    std::cout<<"\nShould read as \n"
	     <<"(1.25,3.75): NA, NA, NA, (3.75,1.25), ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(2) );
    std::cout<<"\nShould read as \n"
	     <<"(1.25,1.25): (3.75,3.75), NA, (-2.5,-2.5), NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(3) );
    std::cout<<"\nShould read as \n"
	     <<"(3.75,1.25): NA, (1.25,3.75), NA, NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0)->getChild(3) );
    std::cout<<"\nShould read as \n"
	     <<"(3.75,1.25): NA, (1.25,3.75), NA, NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(0) );
    std::cout<<"\nShould read as \n"
	     <<"(2.5,2.5): NA, NA, (-2.5,-2.5), NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(1) );
    std::cout<<"\nShould read as \n"
	     <<"(-2.5,2.5): NA, NA, NA, (2.5,-2.5), ";
    
    PrintCornerNeighbors( QT, Root->getChild(2) );
    std::cout<<"\nShould read as \n"
	     <<"(-2.5,-2.5): (2.5,2.5), NA, NA, NA, ";
    
    PrintCornerNeighbors( QT, Root->getChild(3) );
    std::cout<<"\nShould read as \n"
	     <<"(2.5,-2.5): NA, (-2.5,2.5), NA, NA, ";
  }
  
  // Test identification of leaf nodes within each node
  {
    // Set random sizes at random points within a quadtree
    const double zero[] = {0.,0.};
    const double H = 2.5;
    Quadtree QT(zero, H);
    for(int n=0; n<20; ++n)
      {
	double pt[] = {LocDis(gen), LocDis(gen)};
	double hval = hDis(gen);
	QT.refine(pt, hval);
	auto QN = QT.query(pt);
	assert(QN->getNodeSize()<hval+hval/100.);
      }
    
    // Loop over all nodes and test leaves
    std::list<std::shared_ptr<QuadtreeNode>> NodeList{QT.getRootNode()};
    while(!NodeList.empty())
      {
	auto QN = NodeList.front();
	
	const double* QNcenter = QN->getCenterCoordinates();
	const double QNh = QN->getNodeSize();
	
	auto MyLeaves = QT.getLeavesInNode(QN);
	
	// Check that MyLeaves are all leaf nodes
	// and that their centers lie in QN
	for(auto& lnode:MyLeaves)
	  {
	    assert(lnode->getNumberOfChildren()==0);
	    const double* mycenter = lnode->getCenterCoordinates();
	    const double myH = lnode->getNodeSize();
	    assert(myH<QNh+1.e-6);
	    assert( !(std::abs(QNcenter[0]-mycenter[0])>0.5*QNh ||
		      std::abs(QNcenter[1]-mycenter[1])>0.5*QNh)  );
	  }
	
	NodeList.pop_front();
	for(int i=0; i<QN->getNumberOfChildren(); i++)
	  NodeList.push_back( QN->getChild(i) );
      }
    
    // get all the leaf nodes in this tree
    auto Leaves = QT.getLeafNodesInTree();
    // This should coincide with all leaf nodes in Root
    auto RootLeaves = QT.getLeavesInNode( QT.getRootNode() );
    assert(Leaves.size() == RootLeaves.size() );
    std::cout<<"\n\nTest for leaves within nodes OK.";

    TestCornerNeighbors(QT);
    
    QT.plotTec("Test.tec");
    QT.stronglyBalanceTree();
    QT.plotTec("BalTest.tec");
    TestStrongBalancing(QT);

    std::vector<double> coordinates;
    std::vector<int> connectivity;
    QT.triangulate(coordinates, connectivity);
    plotTec("SBMesh.tec", coordinates, connectivity);
    TestMesh(coordinates, connectivity, QT.getRootNodeCenter(), QT.getRootNodeSize());

    // Prune the quadtree
    QT.prune();
    QT.plotTec("Pruned.tec");
  }
  
  
  // Test strongly balancing
  {
    Quadtree QT(center, h);
    auto Root = QT.getRootNode();
    Root->addChildren();
    Root->getChild(0)->addChildren();
    Root->getChild(0)->getChild(2)->addChildren();
    
    QT.balanceTree();
    QT.plotTec((char*)"WeakBal.tec");
    
    QT.stronglyBalanceTree();
    QT.plotTec((char*)"StrongBal.tec");
    TestStrongBalancing(QT);

    std::vector<double> coordinates;
    std::vector<int> connectivity;
    QT.triangulate(coordinates, connectivity);
    plotTec("mesh.tec", coordinates, connectivity);
    TestMesh(coordinates, connectivity, QT.getRootNodeCenter(), QT.getRootNodeSize());
  }
  
  std::cout<<"\n";
}


void PrintNeighbors(Quadtree &QT, std::shared_ptr<QuadtreeNode> QN)
{
  // Center of this node
  std::cout<<"\n("
	   <<QN->getCenterCoordinates()[0]<<","
	   <<QN->getCenterCoordinates()[1]<<"): ";
 
  // Centers of neighbors
  char dir[] = "XYXY";
  char orient[] = "PPMM";
  for(int i=0; i<4; i++)
    {
      std::string dirName;
      dirName.clear();
      dirName.push_back( dir[i] );
      dirName.push_back( orient[i] );
      auto nb = QT.findNeighbor(QN, dirName);
	if( nb==0 )
	  std::cout<<"NA, ";
	else
	  std::cout<<"("<<nb->getCenterCoordinates()[0]
		   <<","<<nb->getCenterCoordinates()[1]
		   <<"), ";
    }
}


void TestBalancing(Quadtree &QT)
{
  auto LeafNodes = QT.getLeafNodesInTree();
  char dir[] = "XYXY";
  char orient[] = "PPMM";
  for(auto& lnode:LeafNodes)
    {
      // Size of this node
      const double H = lnode->getNodeSize();
      
      // Check sizes of neighbors
      for(int i=0; i<4; i++)
	{
	  std::string dirName;
	  dirName.clear();
	  dirName.push_back( dir[i] );
	  dirName.push_back( orient[i] );
	  auto nb = QT.findNeighbor(lnode, dirName);
	  if( nb!=0 )
	    {
	      double h =  nb->getNodeSize();
	      if( h>2.*H+1.e-6 || h<0.5*H-1.e-6 )
		std::cout<<"\nBalanced tree has non-permissible size ratio.\n";
	    }
	}
    }  
  std::cout<<"OK.\n";
}


void PrintCornerNeighbors(Quadtree &QT, std::shared_ptr<QuadtreeNode> QN)
{
  
  std::vector< std::string > cnrs;
  cnrs.clear();
  cnrs.push_back( "XPYP" );
  cnrs.push_back( "XMYP" );
  cnrs.push_back( "XMYM" );
  cnrs.push_back( "XPYM" );
  
  std::cout<<"\n\n("<<QN->getCenterCoordinates()[0]
	   <<","<<QN->getCenterCoordinates()[1]<<"): ";
  for(int c=0; c<4; c++)
    {
      auto nb = QT.findCornerNeighbor(QN, cnrs[c]);
      if( nb==0 )
	std::cout<<"NA, ";
      else
	std::cout<<"("<<nb->getCenterCoordinates()[0]
		 <<","<<nb->getCenterCoordinates()[1]<<"), ";
    }
}


void TestCornerNeighbors(Quadtree &QT)
{
  // get all the leaf nodes.
  auto LeafNodes = QT.getLeafNodesInTree();
  
  const std::vector<std::string> cnrs{"XPYP", "XMYP", "XMYM", "XPYM"};
  
  for(auto& lnode:LeafNodes)
    for(int c=0; c<4; c++)
      {
	auto nb = QT.findCornerNeighbor( lnode, cnrs[c] );
	if( nb!=nullptr )
	  {
	    // Check that there are a common corner.
	    const double* A = lnode->getCornerCoordinates(c);
	    const double* B = nb->getCornerCoordinates( (c+2)%4 );
	    assert(std::abs(A[0]-B[0])+std::abs(A[1]-B[1])<1.e-6 );
	    
	    double h = lnode->getNodeSize();
	    double h_nb = nb->getNodeSize();
	    
	    double eps = h/100.;
	    assert( h_nb>h-eps );
	    
	    assert(!( h_nb>h+eps && nb->getNumberOfChildren()>0 ));
	  }
      }
  
  std::cout<<"\n\nTested corner neighbors for a random tree: OK\n";
}



void TestStrongBalancing(Quadtree &QT)
{
  // get all the leaf nodes.
  // Check that all face and corner neighbors are within the 2 size ratio.
  const std::vector<std::string> Faces{"XP", "YP", "XM", "YM"};
  const std::vector< std::string > Corners{"XPYP", "XMYP", "XMYM", "XPYM"};

  auto LeafNodes = QT.getLeafNodesInTree();
  
  for(auto& lnode:LeafNodes)
    {
      double h = lnode->getNodeSize();
      double eps = h/100.;
      
      // Check face neighbors.
      for(int f=0; f<4; f++)
	{
	  auto nb = QT.findNeighbor(lnode, Faces[f]);
	  if( nb!= 0)
	    {
	      double H = nb->getNodeSize();
	      assert(!( h<0.5*H-eps || h>2.*H+eps ));
	    }
	}
      
      // Check corner neighbors.
      for(int c=0; c<4; c++)
	{
	  auto nb = QT.findCornerNeighbor(lnode, Corners[c]);
	  if( nb!=0 )
	    {
	      double H = nb->getNodeSize();
	      assert(!( h<0.5*H-eps || h>2.*H+eps ));
	    }
	}
    }
  std::cout<<"\nTesting for strong balancing: OK.\n";
}


// plot triangle mesh as a tec file
void plotTec(const std::string filename,
	     const std::vector<double>& coord,
	     const std::vector<int>& conn)
{
  const int nNodes = static_cast<int>(coord.size()/2);
  const int nElements = static_cast<int>(conn.size()/3);

  // plot mesh
  std::fstream plot;
  plot.open(filename, std::ios::out);
  plot<<"VARIABLES = \"X\", \"Y\" ";
  plot <<"\n"
       <<"ZONE t=\"t:0\", "
       <<"N="<<nNodes<<", "
       <<"E="<<nElements<<", "
       <<"F=FEPOINT, "
       <<"ET=TRIANGLE";
  
  // Write coordinates 
  for(int n=0; n<nNodes; ++n)
    plot<<"\n"<<coord[2*n]<<" "<<coord[2*n+1];

  // Write connectivity
  for(int e=0; e<nElements; ++e)
    plot<<"\n"<<conn[3*e]+1<<" "<<conn[3*e+1]+1<<" "<<conn[3*e+2]+1;

  // Close file
  plot.close();
}


// Check that nodes in a mesh are sequentially numbered.
// Check that all boundary nodes lie on edges of the root node of a quadtree
void TestMesh(const std::vector<double>& coordinates,
	      const std::vector<int>& connectivity,
	      const double* center, const double hsize)
{
  // get all boundary nodes
  std::set<int> BdNodes = GetBoundaryNodes(connectivity);

  // Check that boundary nodes lie along the bounding box
  for(auto& n:BdNodes)
    {
      const double* X = &coordinates[2*n];
      double Y[] = {std::abs(X[0]-center[0])/hsize, std::abs(X[1]-center[1])/hsize};
      assert(std::abs(Y[0]-0.5)<1.e-6 || std::abs(Y[1]-0.5)<1.e-6);
    }

  // Check that nodes are numbered sequentially from zero
  std::set<int> VertList(connectivity.begin(), connectivity.end());
  const int nNodes = static_cast<int>(coordinates.size())/2;
  assert(static_cast<int>(VertList.size())==nNodes);
  int nCount = 0;
  for(auto& n:VertList)
    assert(n==nCount++);

  // done
  return;
}


// Identify boundary ndoes in a mesh
std::set<int> GetBoundaryNodes(const std::vector<int>& connectivity)
{
  // Create a list of all edges
  // Save the number of elements that each edge belongs to.
  // A free edge belongs to just one element.
  
  std::map<std::array<int,2>,int> EdgeList{};
  const int nElements = static_cast<int>(connectivity.size()/3);
  std::array<int,2> edgeconn;
  for(int e=0; e<nElements; ++e)
    for(int f=0; f<3; f++)
      {
	edgeconn[0] = connectivity[3*e+f];
	edgeconn[1] = connectivity[3*e+(f+1)%3];
	if( edgeconn[1]<edgeconn[0] )
	  { int temp = edgeconn[0];
	    edgeconn[0] = edgeconn[1];
	    edgeconn[1] = temp; }
	
	// Is this a nu edge
	if( EdgeList.find(edgeconn)==EdgeList.end() )
	  EdgeList[edgeconn] = 1;
	else // second count
	  EdgeList[edgeconn] += 1;
      }
	
  std::set<int> BdNodes{};
  for(auto& it:EdgeList)
    if(it.second==1)
      for(int k=0; k<2; ++k)
	BdNodes.insert( it.first[k] );

  // done
  return BdNodes;
}
