// Sriramajayam

#include <um2_QuadtreeNode.h>
#include <iostream>

using namespace um2;

void PrintNodeDetails(std::shared_ptr<QuadtreeNode> QN); 

int main()
{
  // Create a node
  const double center[2] = {1.0,2.0};
  double h = 2.0;
  std::shared_ptr<QuadtreeNode> Root = std::make_shared<QuadtreeNode>(nullptr, center, h);
    
  // Add children
  Root->addChildren();
    
  // Add grand-children
  Root->getChild(0)->addChildren();
    
  // Print details of root node
  std::cout<<"\n\nDetails of root node: ";
  PrintNodeDetails(Root);
    
  // Print details of child nodes 
  std::cout<<"\n\nDetails of children of root node: ";
  for(int c=0; c<Root->getNumberOfChildren(); c++)
    PrintNodeDetails( Root->getChild(c) );
    
  // Print details of child 0 of child 0
  std::cout<<"\n\nDetails of child 0 of child 0: ";
  PrintNodeDetails( Root->getChild(0)->getChild(0) );
    
  std::cout<<"\n\n";
}


void PrintNodeDetails(std::shared_ptr<QuadtreeNode> QN)
{
  std::cout<<"\nCenter of node:  ("
	   <<QN->getCenterCoordinates()[0]<<","
	   <<QN->getCenterCoordinates()[1]<<"), ";
  
  std::cout<<"\nSize of node: "<<QN->getNodeSize();
	
  
  std::cout<<"\nCoordinates of corners: ";
  for(int a=0; a<4; a++)
    std::cout<<"("<<QN->getCornerCoordinates(a)[0]
	     <<","<<QN->getCornerCoordinates(a)[1]<<")  ";
  
  std::cout<<"\nNumber of children: " 
	   <<QN->getNumberOfChildren();
  
  
}
