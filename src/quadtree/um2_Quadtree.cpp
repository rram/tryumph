// Sriramajayam

#include <um2_Quadtree.h>

namespace um2
{
  // Locate point in the tree
  std::shared_ptr<QuadtreeNode> Quadtree::query(const double* Pt) const
    {
      // Check if this point belongs to the root node.
      const double eps = RootSize*1.e-6;
      if( std::abs(Pt[0]-RootCenter[0])>RootSize/2.+eps ||
	  std::abs(Pt[1]-RootCenter[1])>RootSize/2.+eps )
	return std::shared_ptr<QuadtreeNode>(nullptr);
      
      // Start with the root node.
      // Decide which child this point belongs to.
      // Doing so is much more efficient than searching through all leaf node.
      auto QN = RootNode;
  
      while( QN->getNumberOfChildren()!= 0 )
	{
	  const double* center = QN->getCenterCoordinates();
	  const double x = Pt[0]-center[0];
	  const double y = Pt[1]-center[1];
        
	  int childnum;
	  if( x>0. && y>0. )
	    childnum = 0;
	  else if(x<=0. && y>0. )
	    childnum = 1;
	  else if(x<=0. && y<=0. )
	    childnum = 2;
	  else
	    childnum = 3;
	  QN = QN->getChild(childnum);
	}
      return QN;
    }

  
  // Prune a quadtree by one level
  void Quadtree::prune()
  {
    // Get all the leaf nodes in the tree
    auto LeafNodes = getLeafNodesInTree();

    // Prune: delete a leaf only if all its siblings are leaves as well (i.e., same size)
    std::set<std::shared_ptr<QuadtreeNode>> parents_to_prune{};
    for(auto& node:LeafNodes)
      if(node!=nullptr && node!=RootNode)
	{
	  bool delete_me = true;
	  auto parent = node->getParentAddress();
	  for(int c=0; c<4 && delete_me==true; ++c)
	    {
	      auto child = parent->getChild(c);
	      if(child->getNumberOfChildren()!=0)
		delete_me = false;
	    }

	  if(delete_me==true)
	    parents_to_prune.insert(parent);
	}

    for(auto& node:parents_to_prune)
      node->prune();

    return;
  }
      

  // Plot all the leaf nodes in the tree
  void Quadtree::plotTec(const std::string filename) const
  {
    // Get all the leaf nodes in the tree
    auto LeafNodes = getLeafNodesInTree();

    // Number of leaf nodes
    const int nLeaves = static_cast<int>(LeafNodes.size());
  
    // Plot as a disconnected quad mesh
    std::fstream plot;
    plot.open(filename, std::ios::out);
    plot<<"VARIABLES = \"X\", \"Y\" ";
    plot <<"\n"
	 <<"ZONE t=\"t:0\", "
	 <<"N="<<nLeaves*4<<", "
	 <<"E="<<nLeaves<<", "
	 <<"F=FEPOINT, "
	 <<"ET=QUADRILATERAL";
  
    // Write coordinates and field values
    for(auto& QN:LeafNodes)
      {
	// Four corners
	for(int c=0; c<4; ++c)
	  { const double* cnr = QN->getCornerCoordinates(c);
	    plot<<"\n"<<cnr[0]<<" "<<cnr[1]; }
      }
  
    // Write connectivity
    for(int e=0; e<nLeaves; ++e)
      plot<<"\n"<<4*e+1<<" "<<4*e+2<<" "<<4*e+3<<" "<<4*e+4;
    
    // Close file
    plot.close();
  }

}
