// Sriramajayam

#include <um2_Quadtree.h>

namespace um2
{
  // Refine tree until each leaf node is smaller than Hmax
  void Quadtree::refine(const double Hmax) 
  {
    // Initialize list to all leaf nodes
    // If a node needs to be refined, add children and append all its children to the list.
    // Delete the node itself from the list.
    
    // Get the list of all leaf nodes in tree
    auto NodesToRefine = getLeafNodesInTree();

    // Allow some tolerance in size checks.
    const double eps = Hmax/100.;
    
    while(!NodesToRefine.empty())
      {
	auto QN = NodesToRefine.front();
      	const double QNSize = QN->getNodeSize();
      	if(QNSize>Hmax+eps)
	  {
	    QN->addChildren(); 	    // Add children to this node
	    for(int i=0; i<4; i++)  // Append all children to the list
	      NodesToRefine.push_back( QN->getChild(i) );
	  }
	
	// Delete the current node from the list
	NodesToRefine.pop_front();
      }
  }



  // Refine tree so that the leaf node size at a point is smaller than specified
  void Quadtree::refine(const double* Pt, const double Hmax)
  {
    // Allow some tolerance in size checks.
    const double eps = Hmax/100.;

    // Locate this node in the tree
    // Check its size
    // If satisfactory, break. Otherwise, add children and refine again
    auto QN = query(Pt);
    assert(QN!=nullptr);
    const double QNSize = QN->getNodeSize();
    if(QNSize>Hmax+eps)
      { QN->addChildren();
	refine(Pt, Hmax); }

    return;
  }
  
}
