// Sriramajayam

#include <um2_Quadtree.h>

namespace um2
{
  // Balance the quadtree
  // Create a list of all leaf nodes in the tree.
  // Pop out the first leaf node
  // Determine if it needs to be split based on the size of its neighbors
  // If it is split, append all its children to the list of leaf nodes.
  // With the split node, check which of its larger neighbors now need to be split.
  // Append these to the list of leaf nodes.
  // Note that a larger neighbor is necessarily a leaf node.
  void Quadtree::balanceTree()
  {
    // Get all the leaf nodes in the tree
    auto NodeList = getLeafNodesInTree();
  
    while(!NodeList.empty())
      {
	// Pop the first element
	auto QN = NodeList.front();
      
	// Make sure QN is a leaf node.
	// If not, it was once a leaf node that has now been split.
	// Delete it.
      
	if( QN->getNumberOfChildren()==0 )
	  // Check if QN needs to be split
	  if( splitNode(QN) )
	    {
	      // Split this node
	      QN->addChildren();
	      
	      // Append all children to the NodeList
	      for(int i=0; i<4; ++i)
		NodeList.push_back( QN->getChild(i) );
	    
	      // Get the list of neighbors of QN that were larger than QN
	      // These nodes will now have to be split
	      auto LargerNbs = getLargerNeighbors(QN);
	      for(auto& qnode:LargerNbs)
		NodeList.push_back( qnode );
	    }
      
	// Erase QN from NodeList as it has already been dealt with
	// or it was once a leaf node that is now split.
	NodeList.pop_front();
      }

    // done
    return;
  }




  // Strongly balance the quadtree
  // Create a list of all leaf nodes.
  // Pop out the first leaf node
  // Determine if it needs to be split based on face and corner neighbors
  // If the node is split, append all the children to the list of leaf nodes
  // With the split node,append all the larger neighbors of the original node to the list of leaf nodes.
  void Quadtree::stronglyBalanceTree()
  {
    // Get all the leaf nodes in the tree
    auto LeafNodes = getLeafNodesInTree();
    
    while( !LeafNodes.empty() )
      {
	// Pop the first element
	auto QN = LeafNodes.front();
      
	// If QN is a leaf node
	if( QN->getNumberOfChildren()==0 )
	  // Check if QN needs to be split based on face or corner neighbors
	  if( splitNode(QN) || splitNodeForCorner(QN) )
	    {
	      // // Get the list of larger face neighbors and append them to list of leaf nodes
	      auto LargerFaceNbs = getLargerNeighbors( QN );
	      for(auto& qnode:LargerFaceNbs)
		LeafNodes.push_back( qnode );
	    
	      // Get the list of larger corner neighbors and append them to list of leaf nodes
	      auto LargerCnrNbs = getLargerCornerNeighbors( QN );
	      for(auto& qnode:LargerCnrNbs)
		LeafNodes.push_back( qnode );
	      
	      // Split this node
	      QN->addChildren();
	      
	      // Append all children to the list of leaf nodes
	      for(int c=0; c<4; ++c)
		LeafNodes.push_back( QN->getChild(c) );
	    }
      
	// Delete the node that has now been accounted for
	LeafNodes.pop_front();
      }

    // done
    return;
  }
  
}
