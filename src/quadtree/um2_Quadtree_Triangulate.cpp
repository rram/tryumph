// Sriramajayam

#include <um2_Quadtree.h>
#include <um2_Quadtree_Stencils.h>
#include <um2_BoostAliases.h>

namespace um2
{
  // Triangulate a strongly balanced quadtree  
  void Quadtree::triangulate(std::vector<double>& coordinates,
			     std::vector<int>& connectivity)
  {
    // Strongly balance quadtree
    stronglyBalanceTree();

    // Aliases to stencils

    // 1. AAAA
    const auto& coord_AAAA = detail::QTreeStencils::coord_AAAA;
    const auto& conn_AAAA = detail::QTreeStencils::conn_AAAA;

    // 2. AAAB
    const auto& coord_AAAB = detail::QTreeStencils::coord_AAAB;
    const auto& conn_AAAB = detail::QTreeStencils::conn_AAAB;
    
    // 3. AABB
    const auto& coord_AABB = detail::QTreeStencils::coord_AABB;
    const auto& conn_AABB = detail::QTreeStencils::conn_AABB;
    
    // 4. ABAB
    const auto& coord_ABAB = detail::QTreeStencils::coord_ABAB;
    const auto& conn_ABAB = detail::QTreeStencils::conn_ABAB;
    
    // 5. ABBB
    const auto& coord_ABBB = detail::QTreeStencils::coord_ABBB;
    const auto& conn_ABBB = detail::QTreeStencils::conn_ABBB;
    
    // 6. ABCB
    const auto& coord_ABCB = detail::QTreeStencils::coord_ABCB;
    const auto& conn_ABCB = detail::QTreeStencils::conn_ABCB;
    
    // 7. BBBB
    const auto& coord_BBBB = detail::QTreeStencils::coord_BBBB;
    const auto& conn_BBBB = detail::QTreeStencils::conn_BBBB;
    
    // 8. BBBC
    const auto& coord_BBBC = detail::QTreeStencils::coord_BBBC;
    const auto& conn_BBBC = detail::QTreeStencils::conn_BBBC;
    
    // 9. BBCC
    const auto& coord_BBCC = detail::QTreeStencils::coord_BBCC;
    const auto& conn_BBCC = detail::QTreeStencils::conn_BBCC;
    
    // Create a diconnected mesh for the quadtree
    coordinates.clear();
    connectivity.clear();
    int nodeCount = 0;
    
    // Get the list of leaf nodes
    auto LeafNodes = getLeafNodesInTree();
    
    // Loop over leaf nodes and use MyStencil for each node
    for(auto& lnode:LeafNodes)
      {
	const double* center = lnode->getCenterCoordinates();
	const double H = lnode->getNodeSize();
	
	// Determine neighbors: top, right, bottom, left.
	const std::vector<std::shared_ptr<QuadtreeNode>> Nbs{findNeighbor(lnode,"YP"), findNeighbor(lnode,"XP"),
	    findNeighbor(lnode,"YM"), findNeighbor(lnode,"XM")};
	
	
	// A neighbor need not be a leaf node.
	// But if it is larger, it is necessarily a leaf node.
      
	// Encode required stencil based on neighbor sizes.
	// A: neighbor is same size and has children
	// B: neighbor is same size but no children
	// C: neighbor is larger (and has no children).
	// In case of no neighbor, extend by neighbor of same size. So use 'B'.
      	std::string MyStencil{};
	
	// Count the number of A's, B's and C's.
	int nA = 0, nB = 0, nC = 0;
	
	for(int i=0; i<4; ++i)
	  if ( Nbs[i]==nullptr )
	    { MyStencil.push_back( 'B' ); ++nB; }
	  else
	    {
	      const double h = Nbs[i]->getNodeSize();
	      const int nChild = Nbs[i]->getNumberOfChildren();
	      if( h>H+1.e-6 )
		{ MyStencil.push_back( 'C' ); ++nC;
		  assert(nChild==0 && "um::Quadtree::Triangulate- Larger neighbor is not a leaf?"); }
	      else // Neighbor has same size
		{
		  if( nChild>0 )
		    { MyStencil.push_back( 'A' ); ++nA; }
		  else
		    { MyStencil.push_back( 'B' ); ++nB; }
		}
	    }
	
	// Coordinates and connectivity for this node
	const std::vector<double>* MyCoord = nullptr;
	const std::vector<int>* MyConn = nullptr;
	
	// How much to rotate:
	double theta = 0.;
	
	// MyStencil has to be a rotation of one of the 9 standard ones.
	
	if(nA==4)
	  {
	    // This is the stencil AAAA
	    MyCoord = &coord_AAAA;
	    MyConn = &conn_AAAA;
	    theta = 0.;
	  }
	else if( nA==3 )
	  {
	    // This is the stencil that is some rotation of AAAB
	    const std::string AAAB = "AAAB";
	    bool PatternFound = false;
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==AAAB[(r+0)%4] && MyStencil[1]==AAAB[(r+1)%4] &&
		  MyStencil[2]==AAAB[(r+2)%4] && MyStencil[3]==AAAB[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyCoord = &coord_AAAB;
		  MyConn = &conn_AAAB;
		  PatternFound = true;
		  break;
		}
	    assert(PatternFound && "um2::Quadtree::triangulate- Could not find match for stencil AAAB");
	  }
	else if( nA==2 && nB==2 )
	  {
	    // This is a rotation of either AABB or ABAB
	    bool PatternFound = false;
	    // Check for AABB
	    std::string AABB = "AABB";
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==AABB[(r+0)%4] && MyStencil[1]==AABB[(r+1)%4] &&
		  MyStencil[2]==AABB[(r+2)%4] && MyStencil[3]==AABB[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyConn = &conn_AABB;
		  MyCoord = &coord_AABB;
		  PatternFound = true;
		  break;
		}
	    
	    if(!PatternFound)
	      {
		// Check for ABAB
		std::string ABAB = "ABAB";
		for(int r=0; r<4; ++r)
		  if( MyStencil[0]==ABAB[(r+0)%4] && MyStencil[1]==ABAB[(r+1)%4] &&
		      MyStencil[2]==ABAB[(r+2)%4] && MyStencil[3]==ABAB[(r+3)%4]  )
		    {
		      theta = static_cast<double>(r)*M_PI/2.;
		      MyConn = &conn_ABAB;
		      MyCoord = &coord_ABAB;
		      PatternFound = true;
		      break;
		    }
		assert(PatternFound && "um2::Quadtree::triangulate- Could not match pattern with 2A's and 2B's");
	      }
	  }
	else if(nA==1 && nB==3)
	  {
	    // This stencil is some rotation of ABBB
	    std::string ABBB = "ABBB";
	    bool PatternFound = false;
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==ABBB[(r+0)%4] && MyStencil[1]==ABBB[(r+1)%4] &&
		  MyStencil[2]==ABBB[(r+2)%4] && MyStencil[3]==ABBB[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyConn = &conn_ABBB;
		  MyCoord = &coord_ABBB;
		  PatternFound = true;
		  break;
		}
	    assert(PatternFound && "um2::Quadtree::triangulate- Could not find match for stencil ABBB.");
	  }
	else if(nA==1 && nB==2)
	  {
	    // This stencil is some rotation of ABCB
	    std::string ABCB = "ABCB";
	    bool PatternFound = false;
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==ABCB[(r+0)%4] && MyStencil[1]==ABCB[(r+1)%4] &&
		  MyStencil[2]==ABCB[(r+2)%4] && MyStencil[3]==ABCB[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyConn = &conn_ABCB;
		  MyCoord = &coord_ABCB;
		  PatternFound = true;
		  break;
		}
	    assert(PatternFound && "um2::Quadtree::triangulate- Could not find match for stencil ABCB.");
	  }
	else if( nB==4 )
	  {
	    // This is the stencil BBBB
	    MyCoord = &coord_BBBB;
	    MyConn = &conn_BBBB;
	    theta = 0.;
	  }
	else if( nB==3 )
	  {
	    // This is the stencil BBBC
	    std::string BBBC = "BBBC";
	    bool PatternFound = false;
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==BBBC[(r+0)%4] && MyStencil[1]==BBBC[(r+1)%4] &&
		  MyStencil[2]==BBBC[(r+2)%4] && MyStencil[3]==BBBC[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyConn = &conn_BBBC;
		  MyCoord = &coord_BBBC;
		  PatternFound = true;
		  break;
		}
	    assert(PatternFound && "um2::Quadtree::triangulate- Could not find match for stencil BBBC.");
	  }
	else if( nB==2 )
	  {
	    // This is the stencil BBCC
	    std::string BBCC = "BBCC";
	    bool PatternFound = false;
	    for(int r=0; r<4; ++r)
	      if( MyStencil[0]==BBCC[(r+0)%4] && MyStencil[1]==BBCC[(r+1)%4] &&
		  MyStencil[2]==BBCC[(r+2)%4] && MyStencil[3]==BBCC[(r+3)%4]  )
		{
		  theta = static_cast<double>(r)*M_PI/2.;
		  MyConn = &conn_BBCC;
		  MyCoord = &coord_BBCC;
		  PatternFound = true;
		  break;
		}
	    assert(PatternFound && "um2::Quadtree::triangulate- Could not find match for stencil BBCC.");
	  }
	else
	  { assert(false && "um2::Quadtree::triangulate- Unknown stencil type encountered."); }

	// Append rotated stencils to the mesh

	// coordinates
	const int nStencilNodes = static_cast<int>(MyCoord->size()/2);
	for(int a=0; a<nStencilNodes; ++a)
	  { const double& x = (*MyCoord)[2*a];
	    const double& y = (*MyCoord)[2*a+1];
	    coordinates.push_back( center[0] + H*(x*std::cos(theta)-y*std::sin(theta)) );
	    coordinates.push_back( center[1] + H*(x*std::sin(theta)+y*std::cos(theta)) ); }

	// connectivity
	for(auto& n:*MyConn)
	  connectivity.push_back( nodeCount+n-1 ); // -1 since stencil connectivities are numbered from 1.

	nodeCount += static_cast<int>(MyCoord->size()/2);
      }

    // Connect the mesh by removing duplicated nodes
    const double htol = RootSize*1.e-6;
    std::vector<int> IndxMap{};
    pointDuplicationMap(coordinates, htol, IndxMap);
    for(auto& n:connectivity)
      n = IndxMap[n];
    cullMesh(coordinates, connectivity);
    
    // done
    return;
  }


  // Duplication map for a cloud of points
  void Quadtree::pointDuplicationMap(const std::vector<double>& coordinates,
				     const double htol,
				     std::vector<int>& IndxMap)
  {
    const int SPD = 2;
	
    // Insert all points into the tree
    const int nPoints = static_cast<int>(coordinates.size()/SPD);
    std::vector<boost_pointID> cloud(nPoints);
    for(int n=0; n<nPoints; ++n)
      cloud[n] = boost_pointID(boost_point(coordinates[2*n],coordinates[2*n+1]),n);
    boost_pointID_rtree rtree(cloud);
    cloud.clear();

    // Merge points
    IndxMap.resize(nPoints);
    std::fill(IndxMap.begin(), IndxMap.end(), -1);
    double minCnr[SPD], maxCnr[SPD];
    for(int n=0; n<nPoints; ++n)
      if(IndxMap[n]==-1) // This node has not been merged yet
	{
	  // Merge nerby points into this node
	      
	  // Coordinates
	  const double* X = &coordinates[SPD*n];
	      
	  // Min and max corners of the bounding box around X
	  for(int k=0; k<SPD; ++k)
	    { minCnr[k] = X[k]-htol;
	      maxCnr[k] = X[k]+htol; }
	  boost_point minCorner = boost_point(minCnr[0], minCnr[1]);
	  boost_point maxCorner = boost_point(maxCnr[0], maxCnr[1]);
	      
	  // Create a bounding box
	  boost_box query_box(minCorner, maxCorner);
	      
	  // Identify all vertices within this box
	  std::vector<boost_pointID> query_result{};
	  rtree.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));
	      
	  // Debugging check: X should lie within the box
	  { assert(static_cast<int>(query_result.size())>0);
	    bool flag = false;
	    for(auto& r:query_result)
	      if(r.second==n)
		flag = true;
	    assert(flag);
	  }
	      
	  // Associate all duplicated nodes to #n and note them as 'merged'. This includes the node #n
	  for(auto& r:query_result)
	    {
	      // No point should lie within the bounding boxes of two points, unless the tolerance is poorly chosen
	      assert(IndxMap[r.second]==-1 && "um::Quadtree::pointDuplicationMap- tolerance does not separate point cloud.");
		  
	      // Merge
	      IndxMap[r.second] = n;
	    }
	}
	
    // done
    return;
  }



    
  // Remove duplicate nodes and renumber connectivities in a simplicial mesh
  void Quadtree::cullMesh(std::vector<double>& coordinates,
			  std::vector<int>& connectivity)
  {
    const int SPD = 2;
	
    // Renumber nodes sequentially
    std::set<int> VertList(connectivity.begin(), connectivity.end());
    std::map<int,int> RenumMap{};
    int nCount = 0;
    for(auto& n:VertList)
      RenumMap.insert({n,nCount++});

    // Remeshed connectivities & coordinates
    std::vector<double> nu_coords(SPD*VertList.size());
    for(auto& n:connectivity)
      { auto it = RenumMap.find(n);
	assert(it!=RenumMap.end());

	// old number is n
	// nu number is it->second

	// Overwrite coordinates
	for(int k=0; k<SPD; ++k)
	  nu_coords[SPD*it->second+k] = coordinates[SPD*n+k];
	    
	// Overwrite connectivity
	n = it->second; }

    coordinates.clear();
    coordinates = std::move(nu_coords);
      
    // done
    return;
  }
  
}
