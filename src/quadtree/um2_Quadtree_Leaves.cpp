// Sriramajayam

#include <um2_Quadtree.h>

namespace um2
{
  // Return all leaf nodes contained within a node
  std::list<std::shared_ptr<QuadtreeNode>> Quadtree::getLeavesInNode(const std::shared_ptr<QuadtreeNode>& QN) const
    {
      std::list<std::shared_ptr<QuadtreeNode>> Leaves{};

      std::list<std::shared_ptr<QuadtreeNode>> NodesToCheck{QN};
      while( !NodesToCheck.empty() )
	{
	  // First node
	  auto qnode = NodesToCheck.front();

	  // If this node is a leaf node, append it.
	  if(qnode->getNumberOfChildren()==0)
	    Leaves.push_back( qnode );
	  
	  // Otherwise, append all its children to list of nodes to be checked.
	  else for(int i=0; i<4; i++)
		 NodesToCheck.push_back( qnode->getChild(i) );
	  
	  // Delete the current node from list of nodes to be checked.
	  NodesToCheck.pop_front();
	}
      
      return Leaves;
    }
  
}
