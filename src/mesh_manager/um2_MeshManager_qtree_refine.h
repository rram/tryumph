// Sriramajayam

#pragma once

namespace um2
{
  template<typename MeshType>
    void MeshManager<MeshType>::algo_RefineQuadtreeAtVertices(const std::set<int>& refine_Verts)
    {
      // Avoid refining the same quadtree node twice.
      // To this end, identify a subset of vertices falling in distinct quadtree nodes
      std::set<int> nodeSet{};
      std::set<std::shared_ptr<QuadtreeNode>> qnSet{};
      for(auto& v:refine_Verts)
	{
	  const double* X = WM->coordinates(v);
	  const auto QN = QT->query(X);
	  assert(QN!=nullptr);
	  if(qnSet.find(QN)==qnSet.end())   
	    {
	      nodeSet.insert(v);
	      qnSet.insert(QN);
	    }
	}
      qnSet.clear();

      // Refine quadtree nodes
      for(auto& v:nodeSet)
	{
	  const double* X = WM->coordinates(v);
	  auto QN = QT->query(X);
	  QN->addChildren();
	}

      // done
      return;
    }


  template<typename MeshType>
    void MeshManager<MeshType>::algo_RefineQuadtreeAlongFeature_Local(const FeatureParams& FP, const internal::SplitFeatureSet& fset, const double qfactor)
    {
      // Get samples along the feature
      auto samples = FP.sample_feature();
      
      // loop over sample points
      // find the mesh size to be set at the sample point
      // set the mesh size
      double X[2];
      for(auto& pt:samples)
	{
	  X[0] = pt[0];
	  X[1] = pt[1];
	  const double hreq = qfactor*fset.getFeatureSize(X, FP.curve_id);
	  QT->refine(X, hreq);
	}

      // done
      return;
    }


  template<typename MeshType>
    void MeshManager<MeshType>::algo_RefineQuadtreeAlongFeature_NonLocal(const FeatureParams& FP, const FeatureSet& fset, const double qfactor)
    {
      // Get samples along the feature
      auto samples = FP.sample_feature();

      // loop over sample points
      // find the mesh size to be set at the sample point
      // set the mesh size
      double X[2];
      bool is_computed;
      double rval;
      double cpt[2];
      for(auto& pt:samples)
	{
	  X[0] = pt[0];
	  X[1] = pt[1];
	  fset.getComplementaryFeatureSize(X, FP.curve_id, is_computed, rval, cpt);
	  if(is_computed==true)
	    {
	      const double hreq = rval*qfactor;
	      QT->refine(X, hreq);
	    }
	}

      // done
      return;
    }


  template<typename MeshType>
    void MeshManager<MeshType>::algo_RefineQuadtreeAroundCorners(const internal::SplitFeatureSet& fset, const double qfactor)
    {
      // loop over terminals
      // access corner coordinates and the local mesh size to impose
      // refine the quadtree
      const int nTerminals = fset.getNumTerminals();
      for(int t=0; t<nTerminals; ++t)
	{
	  const double* X = fset.getTerminalCoordinates(t);
	  const double dist = fset.getDistanceToNearestCorner(t);
	  const double hreq = dist*qfactor;
	  QT->refine(X, hreq);
	}
      // done
      return;
    }

}
