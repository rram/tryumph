// Sriramajayam

#include <iostream>
#include <string>
#include <um2_MeshManager.h>
#include <um2_TriangleMesh.h>
#include <um2_utils_FeatureFactory.h>
#include <um2_utils_visualize.h>
#include <um2_utils_visualize_callback.h>
#include <random>

int main()
{
  // Read the list of test cases
  std::fstream pfile;
  pfile.open("geom-data/geom-list.txt", std::ios::in);
  assert(pfile.good());
  std::vector<std::string> geom_list{};
  std::string gname;
  pfile >> gname;
  while(pfile.good())
    {
      geom_list.push_back(gname);
      pfile >> gname;
    }
  pfile.close();

  // Randomly choose one of the test cases
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(0, geom_list.size()-1);
  const int case_number = dist(gen);
  const std::string gfile = "227688.fset"; // geom_list[case_number]; //   // "201636.fset";

  // Tryangulate the chosen case
  std::cout<<"\nTryangulating geometry: "<<gfile<<std::flush;
  
  // Nonlinear solver parameters
  um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
  // Medial axis calc parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // # samples
  const int nSamples = 50;

  // corner tolerance
  const double cornerTol = 1.e-4;
  
  // Read features
  std::string fname = "geom-data/" + gfile;
  um2::utils::FeatureFactory gdata(fname, nSamples, cornerTol, mas_params, nlparams);
  
  // Get the feature set
  const auto& fset = gdata.getFeatureSet();

  // Bounding box
  auto bbox = gdata.getBoundingBox();
  bbox.size *= 1.5;
    
  // Callback for visualization
  um2::utils::Try_Visualization_Callback<um2::TriangleMesh> callback("output-fixed");
  
  // Create the mesh manager
  um2::MeshManager<um2::TriangleMesh> manager(bbox);
  manager.setCallback(callback);
  
  // Meshing parameters
  um2::MeshingParams mparams{
    .nonlocal_refinement = false,
    .refinement_factor=1.,
    .num_proj_steps=5,
      .num_dvr_int_relax_iters=10,
      .num_dvr_int_relax_dist=3,
      .num_dvr_bd_relax_iters=3,
      .num_dvr_bd_relax_samples=10,
      .min_quality=0.01,
      .max_num_trys=10};
    
  // Mesh
  const int case_id = 0;
  auto run_info = manager.triangulate(case_id, fset, mparams);

  // Visualize
  auto& BG = manager.getUniversalMesh(case_id);
  auto& WM = manager.getWorkingMesh(case_id);

  // Run correctness checks on the computed mesh
  manager.validate(case_id, fset, mparams, 1.e-3); 
 
  // done
}
