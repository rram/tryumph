// Sriramajayam

#include <iostream>
#include <string>
#include <um2_MeshManager.h>
#include <um2_TriangleMesh.h>
#include <um2_utils_FeatureFactory.h>
#include <um2_utils_visualize.h>
#include <um2_utils_visualize_callback.h>
#include <random>

// rigid body rotation
class IncrementalRotation
{
public:
  inline IncrementalRotation(const double dtheta)
  {
    Rot[0][0] = std::cos(dtheta); Rot[0][1] = -std::sin(dtheta);
    Rot[1][0] = std::sin(dtheta); Rot[1][1] = std::cos(dtheta);
  }

  void operator()(double* X)
  {
    double Y[2] = {0.,0.};
    for(int i=0; i<2; ++i)
      for(int j=0; j<2; ++j)
	Y[i] += Rot[i][j]*X[j];
    X[0] = Y[0];
    X[1] = Y[1];
    return;
  }

private:
  double Rot[2][2];
};

int main()
{
  // Read the list of test cases
  std::fstream pfile;
  pfile.open("geom-data/geom-list.txt", std::ios::in);
  assert(pfile.good());
  std::vector<std::string> geom_list{};
  std::string gname;
  pfile >> gname;
  while(pfile.good())
    {
      geom_list.push_back(gname);
      pfile >> gname;
    }
  pfile.close();

  // Randomly choose one of the test cases
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(0, geom_list.size()-1);
  const int case_number = dist(gen);
  const std::string gfile = "201636.fset"; //geom_list[case_number];

  std::cout<<"\n\nProcessing geometry: "<<gfile<<std::flush;
  
  // output path for this case
  std::string output_path = "output-moving/"; 

  // Non dimensional medial axis parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-3, .maximum_radius=2., .eps_radius=1.e-3};

  // Nonlinear solver parameters
  um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=40, .normTol=1.e-4};

  // # samples
  const int nSamples = 50;

  // corner tolerance
  const double cornerTol = 1.e-4;
  
  // Read features
  std::string fname = "geom-data/" + gfile;
  std::unique_ptr<um2::utils::FeatureFactory> feature_factory =
    std::make_unique<um2::utils::FeatureFactory>(fname, nSamples, cornerTol, mas_params, nlparams);
  
  // Initial bounding box for the geometry
  auto bbox = feature_factory->getBoundingBox();
  bbox.size *= 2.;
  
  // Create the common mesh manager
  um2::MeshManager<um2::TriangleMesh> manager(bbox);

  // callback
  um2::utils::Final_Try_Visualization_Callback<um2::TriangleMesh> callback("output-moving");
  manager.setCallback(callback);
  
  // Meshing parameters
  um2::MeshingParams mparams{
    .nonlocal_refinement=false,
    .refinement_factor=1.,
    .num_proj_steps=5,
      .num_dvr_int_relax_iters=10,
      .num_dvr_int_relax_dist=3,
      .num_dvr_bd_relax_iters=3,
      .num_dvr_bd_relax_samples=10,
      .min_quality=0.01,
      .max_num_trys=10};
    
  // Info for transforming geometry
  IncrementalRotation transformation(2.*M_PI/180.);

  // Perturb & mesh
  std::string prev_label = "";
  for(int pnum=0; pnum<20; ++pnum)
    {
      const int case_id = pnum;
      
      std::cout<<"\nPnum = "<<pnum<<std::flush;

      // Get the feature set
      const auto& fset = feature_factory->getFeatureSet();

      // Tryangulate
      um2::RunInfo run_info;
      if(pnum==0)
	run_info = manager.triangulate(case_id, fset, mparams); // first mesh
      else
	run_info = manager.triangulate(case_id-1, case_id, fset, mparams); // remesh
	  
      // Run correctness checks on the computed mesh
      manager.validate(case_id, fset, mparams, 1.e-3);

      // new factory for rotated geometries
      auto new_factory = feature_factory->transform(transformation);
      feature_factory.reset();
      feature_factory = std::move(new_factory);

    }
  
  // done
}
