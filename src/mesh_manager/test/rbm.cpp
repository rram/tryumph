// Sriramajayam

#include <um2_UMeshManager.h>
#include <um2_utils_GeomData.h>
#include <um2_utils_TriangleMesh.h>

int main(int argc, char** argv)
{
  const std::string gname = "234265";
  const std::string gfile = gname + ".fset";

  // Nonlinear solver parameters
  um2::utils::NLSolverParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
  // Medial axis calc parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // Create the mesh manager
  const double center[] = {0.,0.};
  const double size = 4.0;

  // Meshing parameters
  um2::MeshingParams mparams{
    .num_proj_steps=5,
      .num_dvr_int_relax_iters=10,
      .num_dvr_int_relax_dist=3,
      .num_dvr_bd_relax_iters=3,
      .num_dvr_bd_relax_samples=10,
      .min_quality=0.01,
      .max_num_trys=10};
    
  // Mesh manager
  um2::UMeshManager<um2::utils::TriangleMesh> meshManager(center, size);
  
  // Read features
  um2::utils::GeomData gdata(50, mas_params, nlparams);
  std::fstream stream;
  stream.open("geom-data/"+gfile, std::ios::in);
  stream >> gdata;
  stream.close();

  // Rotation iterations
  std::string prev_label = "";
  //um2::utils::RigidBodyMotionStruct motionStruct{.tvec={0.,0.}, .center={center[0],center[1]}, .angle=0.};
  um2::utils::PolarMapStruct motionStruct{.center={center[0],center[1]},.Rnorm=2.5,.b=0.5,.theta0=0.};
    
  const std::string outdir = "moving/";
  
  for(int iter=0; iter<=150; ++iter)
    {
      std::cout<<"\n\nRotation iteration: "<<iter << std::endl;

      //motionStruct.angle = 3.*static_cast<double>(iter)*M_PI/180.;
      //motionStruct.tvec[0] = 0.5*std::cos(motionStruct.angle);
      //motionStruct.tvec[1] = 0.5*std::sin(motionStruct.angle);
      //auto pert_gdata = gdata.Transform(um2::utils::RigidBodyMotionFunc, &motionStruct);

      motionStruct.theta0 = -static_cast<double>(iter)*M_PI/180.;
      auto pert_gdata = gdata.Transform(um2::utils::PolarMapFunc, &motionStruct);
      
      // Visualize
      stream.open(std::string(outdir+"geom-"+std::to_string(iter)+".csv").c_str(), std::ios::out);
      stream << *pert_gdata;
      stream.close();
      
      // Create feature set
      um2::FeatureSet fset(1.e-4);
      pert_gdata->CreateFeatureSet(fset);
  
      // Mesh
      std::string label = std::to_string(iter);
      um2::RunInfo run_info;
      run_info.output_path = outdir;
      if(iter==0)
	meshManager.Tryangulate(label, fset, mparams, run_info);       // first mesh
      else
	meshManager.Tryangulate(prev_label, label, fset, mparams, run_info); // remesh
    
      // Visualize background mesh
      auto& BG = meshManager.GetUniversalMesh(label);
      {
	stream.open(std::string(outdir+"bg"+std::to_string(iter)+".tec").c_str(), std::ios::out);
	stream << BG;
	stream.close();
      } 

      // Visualize working mesh
      auto& WM = meshManager.GetWorkingMesh(label);
      stream.open(std::string(outdir+"wm-"+std::to_string(iter)+".tec").c_str(), std::ios::out);
      stream << WM;
      stream.close();
      
      // mesh components
      const auto& meshColor = meshManager.GetMeshColoring(label);
      const int nComponents = static_cast<int>(meshColor.elmColors.size());
      //for(int comp=0; comp<nComponents; ++comp)
      //um2::PlotTec(outdir+"comp-"+std::to_string(iter)+"-"+std::to_string(comp)+".tec", WM, meshColor.elmColors[comp]);

      // print element qualities
      const int nElements = BG.n_elements();
      dvr::GeomTri2DQuality<um2::WorkingMesh<um2::utils::TriangleMesh>> Quality(meshManager.GetWorkingMeshWrapper(label));
      std::vector<double> elmQualities(nElements);
      for(int e=0; e<nElements; ++e)
	elmQualities[e] = Quality.Compute(e);
      std::sort(elmQualities.begin(), elmQualities.end());
      stream.open(std::string(outdir+"q-"+std::to_string(iter)+".dat").c_str(), std::ios::out);
      assert(stream.good());
      stream << "# Index \t Element Quality";
      for(int e=0; e<nElements; ++e)
	stream<<"\n"<<e<<"\t"<<elmQualities[e];
      stream.close();
      
      // Update the label
      prev_label = label;
    }
  
  // done
}
