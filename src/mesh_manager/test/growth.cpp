// Sriramajayam

#include <iostream>
#include <string>
#include <um2_UMeshManager.h>
#include <um2_utils_TriangleMesh.h>
#include <um2_utils_GeomData.h>
#include <random>

void Mesh(const std::vector<um2::FeatureParams>& fparams, const um2::MeshingParams& mparams,
	  um2::UMeshManager<um2::utils::TriangleMesh>& manager, bool expect_iterations,  std::string outdir);

int main()
{
  const std::string gfile = "151621.fset";

  // Nonlinear solver parameters
  um2::utils::NLSolverParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
  // Medial axis calc parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // Create the mesh manager
  const double center[] = {0.,0.};
  const double size = 1.5;

  // Meshing parameters
  um2::MeshingParams mparams{
    .num_proj_steps=5,
      .num_dvr_int_relax_iters=10,
      .num_dvr_int_relax_dist=3,
      .num_dvr_bd_relax_iters=3,
      .num_dvr_bd_relax_samples=10,
      .min_quality=0.01,
      .max_num_trys=10};
    
  // Read features
  um2::utils::GeomData gdata(50, mas_params, nlparams);
  std::string fname = "geom-data/" + gfile;
  std::fstream pfile;
  pfile.open(fname.c_str(), std::ios::in);
  assert(pfile.good());
  pfile >> gdata;
  pfile.close();
  
  // Visualize
  fname = "full-geom.csv";
  pfile.open(fname.c_str(), std::ios::out);
  pfile << gdata;
  pfile.close();
  
  // Create feature set
  um2::FeatureSet fset(1.e-4);
  gdata.CreateFeatureSet(fset);
  
  // Feature params
  auto original_fparams = fset.GetFeatureParams();

  // Split features
  std::vector<um2::FeatureParams> split_fparams = original_fparams;

  for(int iter=0; iter<2; ++iter) // number of splitting iterations
    {
      split_fparams.clear();
      
      // Largest feature id
      int id_count = -1;
      for(auto& fparam:original_fparams)
	if(fparam.curve_id>id_count)
	  id_count = fparam.curve_id;

      // Split all features once

      for(auto& fparam:original_fparams)
	{
	  auto LR_split_params = fparam.splitfunc(id_count+1, id_count+2);
	  split_fparams.push_back( LR_split_params.first );
	  split_fparams.push_back( LR_split_params.second );
	  id_count += 2;
	}
      original_fparams = split_fparams;
    }

  
  // Create feature set with split features
  um2::FeatureSet split_fset(1.e-4);
  for(auto& fparam:split_fparams)
    split_fset.AppendFeature(fparam);
  split_fset.Preprocess();
  
  const int nTotalFeatures = split_fset.GetNumFeatures();
  const auto& fparams = split_fset.GetFeatureParams();
  auto conn_comps = split_fset.GetConnectedComponents();

  // order features in sequence
  std::vector<um2::FeatureParams> ordered_fparams{};
  for(auto& comp:conn_comps)
    {
      // closed component: first component is repeated
      if(comp.front()==comp.back())
	comp.pop_back();
      for(auto& it:comp)
	ordered_fparams.push_back(fparams[it]);
    }

  // Triangulator
  um2::UMeshManager<um2::utils::TriangleMesh> manager(center, 2.*size);
  
  // Now try meshing using the current pre-refined mesh
  Mesh(ordered_fparams, mparams, manager, true, "adaptive/"); // expect trys, don't plot

  // Try meshing again with the pre-refined background mesh in the manager
  Mesh(ordered_fparams, mparams, manager, false, "universal/"); // expect 1 try, plot
}


void Mesh(const std::vector<um2::FeatureParams>& fparams, const um2::MeshingParams& mparams,
	  um2::UMeshManager<um2::utils::TriangleMesh>& manager, bool expect_iterations, std::string outdir)
{
  const int nTotalFeatures = static_cast<int>(fparams.size());
  std::cout<<"Number of total features: "<<nTotalFeatures << std::endl;
  
  for(int fnum=1; fnum<=nTotalFeatures; ++fnum)
    {
      std::cout<< std::endl <<"fnum: "<<fnum << std::endl;
      
      // Partial feature set
      um2::FeatureSet partial_fset(1.e-4);

      // Populate this partial feature set
      for(int gnum=0; gnum<fnum; ++gnum)
	partial_fset.AppendFeature(fparams[gnum]);
      
      partial_fset.Preprocess();
      
      // Visualize partial feature set
      auto& partial_fparams = partial_fset.GetFeatureParams();
      //assert(static_cast<int>(partial_fparams.size())==fnum);
      
      const std::string label = "label";
      std::fstream pfile;
      pfile.open(outdir+"partial-"+std::to_string(fnum)+".csv", std::ios::out);
      pfile <<"X, \t Y";
      for(auto& gparams:partial_fparams)
	{
	  auto samples = gparams.samplefunc();
	  for(auto& s:samples)
	    pfile << std::endl << s.first <<",\t"<<s.second;
	}
      pfile.close();
	  
      // Mesh
      um2::RunInfo run_info;
      run_info.output_path = "./";
      manager.Tryangulate(label, partial_fset, mparams, run_info);

      if(expect_iterations==false)
	if(run_info.nTrys!=1)
	  std::cout<<std::endl <<" REMESHING HERE "<<std::endl;
      //assert(run_info.nTrys==1);
      
      // Visualize
      auto& BG = manager.GetUniversalMesh(label);
      auto& WM = manager.GetWorkingMesh(label);
      pfile.open(std::string(outdir+"bg-"+std::to_string(fnum)+".tec").c_str(), std::ios::out);
      pfile << BG;
      pfile.close();
      pfile.open(std::string(outdir+"wm-"+std::to_string(fnum)+".tec").c_str(), std::ios::out);
      pfile << WM;
      pfile.close();

      // Plot positive edges and positive elements
      auto& mesh_details = manager.GetMeshDetails(label);

      // List of positively cut elements
      std::vector<int> poselms{};
      std::vector<int> posfaces{};
      for(auto& it:mesh_details)
	{
	  auto& poscutElmFaces = it.PosCutElmFaces;
	  for(auto& jt:poscutElmFaces)
	    {
	      poselms.push_back(std::get<0>(jt));
	      posfaces.push_back(std::get<1>(jt));
	    }
	}
	  
      const int nNodes = WM.n_nodes();
      std::vector<double> coordinates(2*nNodes);
      for(int n=0; n<nNodes; ++n)
	{
	  const auto* X = WM.coordinates(n);
	  coordinates[2*n+0] = X[0];
	  coordinates[2*n+1] = X[1];
	}

      // positively cut elements
      std::vector<int> connectivity{};
      connectivity.reserve(3*poselms.size());
      for(auto& e:poselms)
	{
	  const auto* conn = WM.connectivity(e);
	  for(int a=0; a<3; ++a)
	    connectivity.push_back(conn[a]);
	}

      um2::utils::TriangleMesh MD(coordinates, connectivity);
      pfile.open(std::string(outdir+"poselm-"+std::to_string(fnum)+".tec").c_str(), std::ios::out);
      assert(pfile.good());
      pfile << MD;
      pfile.close();

      // positive edges
      std::vector<int> posedge_connectivity{};
      posedge_connectivity.reserve(3*poselms.size());
      const int nElm = static_cast<int>(poselms.size());
      const auto& wm_wrapper = manager.GetWorkingMeshWrapper(label);
      for(int i=0; i<nElm; ++i)
	{
	  const int& e = poselms[i];
	  const int& f = posfaces[i];
	  const int* conn = WM.connectivity(e);
	  const int* face_conn =  wm_wrapper.GetLocalFaceNodes(f);
	  posedge_connectivity.push_back( conn[face_conn[0]] );
	  posedge_connectivity.push_back( conn[face_conn[1]] );
	  posedge_connectivity.push_back( conn[face_conn[1]] );
	}

      um2::utils::TriangleMesh edgeMD(coordinates, posedge_connectivity);
      pfile.open(std::string(outdir+"posedge-"+std::to_string(fnum)+".tec").c_str(), std::ios::out);
      assert(pfile.good());
      pfile << edgeMD;
      pfile.close();
    }
  
}
