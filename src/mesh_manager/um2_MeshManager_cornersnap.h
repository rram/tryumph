// Sriramajayam

#pragma once

namespace um2
{
  template<typename MeshType>
    void MeshManager<MeshType>::algo_SnapMeshToFeaturePoints(const std::set<int>& frozen_nodes, const MeshingParams& mparams, std::set<int>& refine_Verts)
    {
      // Aliases
      const auto& fset             = WM->getSplitFeatureSet();
      const auto& gparams          = fset.getFeatureParams();
      const int& nFeatures         = fset.getNumFeatures();
      const int& nTerminals        = fset.getNumTerminals();
      const auto& feature_to_terminal_map = fset.getFeaturesToTerminalsMap();
      const auto& terminal_to_feature_map = fset.getTerminalsToFeaturesMap();
      auto& mout                   = WM->getSplitFeatureCorrespondences();

      // status of working mesh
      assert(WM->getMeshStatus()==WorkingMeshStatus::UNPROCESSED);
	
      // Create an r-tree with vertices of the bg mesh for closest point + neighborhood queries
      const int nNodes = BG->n_nodes();
      std::vector<boost_pointID> bgVerts(nNodes);
      for(int n=0; n<nNodes; ++n)
	{
	  const double* X = BG->coordinates(n);
	  bgVerts[n] = boost_pointID(boost_point(X[0],X[1]),n);
	}
      boost_pointID_rtree rtree(bgVerts);
      bgVerts.clear(); // no longer useful

      // Make vertices coincide with terminal points in a feature-wise manner
      // Terminal vertices once set, should not be disturbed later
      // This step can fail if the closest vertex to a terminal appears in the do-not-disturb list.
      // Mark such vertex locations for refinement. Do not attempt to identify positive edges for such features 
      std::set<int> dndlist = frozen_nodes; // vertices which should not be disturbed
      std::vector<MeshingReason> terminal_reason(nTerminals, MeshingReason::Unassigned);
      std::vector<int> terminal_vertex(nTerminals, -1);
      for(int t=0; t<nTerminals; ++t)
	{
	  const double* corner = fset.getTerminalCoordinates(t);
	  terminal_reason[t] = algo_PerturbMeshForTerminalPoint(corner,
								mparams.num_dvr_int_relax_dist, 1,
								mparams.num_dvr_int_relax_iters,
								mparams.min_quality,
								dndlist, rtree, terminal_vertex[t]);
	  assert(terminal_vertex[t]!=-1 && terminal_reason[t]!=MeshingReason::Unassigned);

	  // if successful in snapping, mark snapped vertex as frozen. Otherwise mark for refinement
	  if(terminal_reason[t]==MeshingReason::Success)
	    dndlist.insert(terminal_vertex[t]);
	  else
	    refine_Verts.insert(terminal_vertex[t]);
	}

	
      // Map reasons and terminal vertices to features
      for(int f=0; f<nFeatures; ++f)
	{
	  // feature -> terminals
	  const int lindx = feature_to_terminal_map[f][0];
	  const int rindx = feature_to_terminal_map[f][1];
	  assert(terminal_to_feature_map[lindx][1]==f);
	  assert(terminal_to_feature_map[rindx][0]==f);
	    
	  // note vertices at corners
	  mout[f].terminalVertices[0] = terminal_vertex[lindx];
	  mout[f].terminalVertices[1] = terminal_vertex[rindx];
	    
	  // success <-> both corners successfully snapped
	  if(terminal_reason[lindx]==MeshingReason::Success     && terminal_reason[rindx]==MeshingReason::Success)
	    mout[f].reason = MeshingReason::Success;
	  else if(terminal_reason[lindx]!=MeshingReason::Success && terminal_reason[rindx]==MeshingReason::Success)
	    mout[f].reason = terminal_reason[lindx];
	  else if(terminal_reason[lindx]==MeshingReason::Success && terminal_reason[rindx]!=MeshingReason::Success)
	    mout[f].reason = terminal_reason[rindx];
	  else
	    mout[f].reason = terminal_reason[lindx]; // (arbitrary) assign failure mode of the left corner
	}
	    
	
      // Sanity check
      for(auto& it:mout)
	assert(it.reason!=MeshingReason::Unassigned);
	
      // done
      return;
    }
    
    

  // Helper function to make vertices coincide with points
  template<typename MeshType>
    MeshingReason MeshManager<MeshType>::algo_PerturbMeshForTerminalPoint(const double* point,
									  const int Ndist,
									  const int Nproject,
									  const int Nrelax,
									  const double qThreshold,
									  const std::set<int>& dndlist,
									  boost_pointID_rtree& rtree, 
									  int& coincidentVertex)
    {
      // Query the closest point from the range tree 
      std::vector<boost_pointID> result{};
      boost_point pt(point[0], point[1]);
      rtree.query(boost::geometry::index::nearest(pt, 1) &&
		  boost::geometry::index::satisfies([&dndlist](std::pair<boost_point,int> const& v) {return dndlist.find(v.second)==dndlist.end();}),
		  std::back_inserter(result));
      
      assert(static_cast<int>(result.size())==1);
      coincidentVertex = result[0].second;

      // Is the closest vertex in the do-not-disturb list?
      if(dndlist.find(coincidentVertex)!=dndlist.end())
	return MeshingReason::Fail_Corner_Coincident_Vertex;
	
      // Get vertex layers around the coincident vertex (excluding it)
      // These vertices will be relaxed
      std::vector<int> Ir{};
      {
	std::set<int> nbSet = mesh_wrapper->getVertexNeighborhood(std::vector<int>{coincidentVertex}, Ndist);

	// Do not include the coincident vertex and vertices in the do-not-disturb list
	nbSet.erase(coincidentVertex);
	for(auto& v:dndlist)
	  nbSet.erase(v);
	Ir.assign(nbSet.begin(), nbSet.end());
      }

      // If Ir is empty, do not snap
      if(Ir.empty())
	return MeshingReason::Fail_Corner_Relaxation_Nbd;
	
      // Save un-perturbed coordinates of perturbable nodes to undo coordinate updates in WM in case of a failure
      std::map<int, std::array<double,2>> prePertCoords{};
      {
	const double* X = WM->coordinates(coincidentVertex);
	prePertCoords.insert(std::make_pair(coincidentVertex, std::array<double,2>{X[0],X[1]}));
	for(auto& n:Ir)
	  {
	    const double* Y = WM->coordinates(n);
	    prePertCoords.insert(std::make_pair(n, std::array<double,2>{Y[0],Y[1]}));
	  }
      }
	

      // Identify 1-ring of elements around coincidentVertex.
      // Inspect these element qualities will be inspected
      const auto& oneRingElms = mesh_wrapper->Get1RingElements(coincidentVertex);
      double qval;
	
      // Mesh relaxation

      // # threads
      const int nthreads = 1;

      // Quality metric
      dvr::GeomTri2DQuality<WorkingMesh<MeshType>> Quality(*mesh_wrapper, nthreads);

      // max-min solver for relaxation
      dvr::ReconstructiveMaxMinSolver<decltype(Quality)> dvrSolver(Quality, nthreads);

      // relaxation directions
      dvr::CartesianDirGenerator<2> CartGen;
      
      // Project the coincident vertex towards the terminal point
      // and concurrently relax vertices in the neighborhood
      for(int pstep=0; pstep<Nproject; ++pstep)
	{
	  // Nudge the coincidentVertex towards "point"
	  {
	    // lambda*X + (1-lambda)*pi(X)
	    const double lambda = 1.-static_cast<double>(pstep+1)/static_cast<double>(Nproject);
	    const double* X = WM->coordinates(coincidentVertex);
	    double Y[2];
	    for(int k=0; k<2; ++k)
	      Y[k] = lambda*X[k] + (1.-lambda)*point[k];
	    WM->update(coincidentVertex, Y);
	  }

	  // Check qualities of elements perturbed by this nudge
	  for(auto& elm_indx:oneRingElms)
	    {
	      qval = Quality.Compute(elm_indx);
	      if(qval<qThreshold)
		{
		  // Undo all vertex perturbations in WM performed in this loop
		  for(auto& it:prePertCoords)
		    WM->update(it.first, &it.second[0]);
		    
		  // return failure
		  return MeshingReason::Fail_Corner_Coincident_Vertex;
		}
	    }
	    
	  // Relax nearby vertices
	  for(int riter=0; riter<Nrelax; ++riter)
	    { CartGen.iteration = riter;
	      dvr::Optimize(Ir, dvrSolver, CartGen, nullptr); }
	}

      // Note coordinates of updated nodes in the rtree
      for(auto& it:prePertCoords)
	{
	  // remove old point
	  boost_pointID pid(boost_point(it.second[0],it.second[1]),it.first);
	  int nremoved = rtree.remove(pid);
	  assert(nremoved==1);

	  // insert fresh point
	  const double* X = WM->coordinates(it.first);
	  boost_pointID qid(boost_point(X[0],X[1]), it.first);
	  rtree.insert(qid);
	}
	
      // done
      return MeshingReason::Success;
    }

} // um2::

