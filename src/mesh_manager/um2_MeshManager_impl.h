// Sriramajayam

#pragma once

namespace um2
{
  // Main functionality: triangulate
  template<typename MeshType>
    RunInfo MeshManager<MeshType>::triangulate(const int num,
					       const FeatureSet& unsplit_fset,
					       const MeshingParams& meshing_params)

    {
      // checks on meshing parameters
      validate_input(meshing_params);

      // mesh id
      case_id = num;
      
      // set the geom num in the callback
      // callback with feature parameters
      if(callback!=nullptr) {
	callback->setMeshID(case_id);
	(*callback)(unsplit_fset);
      }
      
      // split features
      const auto& split_fset = unsplit_fset.getSplitFeatureSet();
      
      // Ensure that the quadtree is sufficiently refined along split features
      const auto& gparams = split_fset.getFeatureParams();
      const int nFeatures = static_cast<int>(gparams.size());
      for(int f=0; f<nFeatures; ++f) {
	algo_RefineQuadtreeAlongFeature_Local(gparams[f], split_fset, 3.0/meshing_params.refinement_factor); // factor of 3: each quadtree node ~ 3 triangles.
      }

      // Refine the qudatree around corners
      algo_RefineQuadtreeAroundCorners(split_fset, 2.5*3.0/meshing_params.refinement_factor); // additional factor of 2.5 for coarsening around corners
      
      // non-local mesh refinement based on unsplit features
      if(meshing_params.nonlocal_refinement==true)
	{
	  const auto& unsplit_gparams = unsplit_fset.getFeatureParams();
	  const int nunsplit_Features = static_cast<int>(unsplit_gparams.size());
	  for(int f=0; f<nunsplit_Features; ++f)
	    algo_RefineQuadtreeAlongFeature_NonLocal(unsplit_gparams[f], unsplit_fset, 3.0/meshing_params.refinement_factor);
	}
	 
      // Triangulate
      auto run_info = algo_Triangulate(meshing_params, split_fset);
      
      // Sanity check
      if(run_info.success==true) {
	assert(WM->getMeshStatus()==WorkingMeshStatus::CONFORMING);
	computeFeatureMeshCorrespondences();
	if(callback!=nullptr) {
	  (*callback)(*QT, *WM, fm_correspondences);
	}
      }
      else {
	assert(WM->getMeshStatus()==WorkingMeshStatus::NONCONFORMING);
      }
      
      // done
      return run_info;
    }
      
  // Main functionality: tryangulate
  template<typename MeshType>
    RunInfo MeshManager<MeshType>::triangulate(const int prev_num, const int curr_num, 
					       const FeatureSet& unsplit_fset,
					       const MeshingParams& meshing_params)
    {
      // checks on meshing parameters
      validate_input(meshing_params);
      
      // Use the existing background mesh if possible
      assert(prev_num!=curr_num && case_id==prev_num);
      case_id = curr_num;

      // set the geom num in the callback
      if(callback!=nullptr) {
	callback->setMeshID(case_id);
	(*callback)(unsplit_fset);
      }

      // split features
      const auto& split_fset = unsplit_fset.getSplitFeatureSet();
      
      // reset WM with the new feature set & try
      WM->reset(split_fset);
      auto try_result = algo_Tryangulate(meshing_params, split_fset);
      auto& try_info = std::get<1>(try_result);
      if(try_info.success==true)
	{
	  std::cout << "Success with existing background mesh! " << std::endl;
	  computeFeatureMeshCorrespondences();
	  // visualization callback
	  if(callback!=nullptr)
	    {
	      (*callback)(*QT, *WM, fm_correspondences);
	    }
	  RunInfo run_info;
      	  run_info.success  = true;
	  run_info.nTrys    = 0; 
 	  run_info.runtime  = try_info.runtime;
	  run_info.try_info = {try_info};
	  return run_info;
	}
      
      // Did not get lucky. Prune the quadtree
      QT->prune();

      // triangulate
      auto run_info = triangulate(curr_num, unsplit_fset, meshing_params);

      // Alternative that gives a coarser mesh at the risk of problems due to too coarse meshes
      // Attempt triangulation without checking for sufficient refinement
      /*
	auto run_info = algo_Triangulate(meshing_params, split_fset);
      
	// Sanity check
	if(run_info.success==true) {
	assert(WM->getMeshStatus()==WorkingMeshStatus::CONFORMING);
	computeFeatureMeshCorrespondences();
	if(callback!=nullptr) {
	(*callback)(*QT, *WM, fm_correspondences);
	}
	}
	else {
	assert(WM->getMeshStatus()==WorkingMeshStatus::NONCONFORMING);
	}
      */
      
      // done
      return run_info; 
    }  


  // Compute feature-mesh correspondences
  template<typename MeshType>
    void MeshManager<MeshType>::computeFeatureMeshCorrespondences() {

    fm_correspondences.clear();
    
    // split feature & mesh correspondences
    const auto& split_fset = WM->getSplitFeatureSet();
    const auto& split_fm_correspondences = WM->getSplitFeatureCorrespondences();
    const auto& split_gparams = split_fset.getFeatureParams();
    const int num_split_Features = static_cast<int>(split_gparams.size());


    // lambda to join a pair of feature-mesh-correspondence objects
    auto join_FeatureMeshCorrespondences = [](const FeatureMeshCorrespondence& prev,
					      const FeatureMeshCorrespondence& next,
					      FeatureMeshCorrespondence& join) {
      assert(prev.reason==next.reason);
      assert(prev.terminalVertices[1]==next.terminalVertices[0]);
      assert(prev.PosVerts.back()==next.PosVerts.front());

      join.reason = prev.reason;

      join.PosVerts = prev.PosVerts;
      join.PosVerts.pop_back();
      for(auto& v:next.PosVerts)
	join.PosVerts.push_back(v);

      join.PosCutElmFaces = prev.PosCutElmFaces;
      for(auto& it:next.PosCutElmFaces)
	join.PosCutElmFaces.push_back(it);

      join.terminalVertices[0] = prev.terminalVertices[0];
      join.terminalVertices[1] = next.terminalVertices[1];
      join.splittingVertex = prev.terminalVertices[1];
      return;
    };

    
    for(int f=0; f<num_split_Features; ++f)
      {
	// split curve id
	const int& split_curve_id = split_gparams[f].curve_id;

	// parent curve id
	const int parent_curve_id = split_fset.getParentCurveID(split_curve_id);

	// mesh correspondence of split curve
	const auto& split_fmc = split_fm_correspondences[f];
	
	// mesh correspondence of the parent curve id
	auto it = fm_correspondences.find(parent_curve_id);

	// new insertion
	if(it==fm_correspondences.end()) {
	  auto result = fm_correspondences.insert({parent_curve_id, split_fmc});
	  assert(result.second==true);
	}
	// splice split features
	else  {
	  // decide whether to prepend or append
	  assert(it->second.terminalVertices[0]==split_fmc.terminalVertices[1] ||
		 it->second.terminalVertices[1]==split_fmc.terminalVertices[0]);

	  FeatureMeshCorrespondence join_fmc;
	  if(it->second.terminalVertices[0]==split_fmc.terminalVertices[1]) {
	    join_FeatureMeshCorrespondences(split_fmc, it->second, join_fmc);
	  }
	  else {
	    join_FeatureMeshCorrespondences(it->second, split_fmc, join_fmc);
	  }

	  // overwrite
	  fm_correspondences.erase(it);
	  auto result = fm_correspondences.insert({parent_curve_id, join_fmc});
	  assert(result.second==true);
	}
      }

    // done
    return;
  }
    
} // namespace um


