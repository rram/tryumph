// Sriramajayam

#pragma once

#include <um2_Quadtree.h>
#include <um2_WorkingMesh.h>
#include <um2_FeatureSet.h>

namespace um2 {
    
    /** @brief Interface class for mesh and geometry visualization.
	
	The UM algorithm executes multiple trys, with each one
	involving multiple steps.  It is often helpful to visualize
	the mesh transformations implemented by the algorithm during
	these steps. Such visualization can help examine mesh
	refinement produced by the algorithm, or the reason behind a
	failed try.

	The class is templated by the mesh type. Derived classes are
	expected to overload the abstract operator methods of the
	class. 

	A callback is registered with the MeshManager using the
	MeshManager::set_Callback method. The MeshManager invokes
	operators of the class at different stages of the execution of
	the meshing algorithm. Specifically, the following methods of the interface
	are invoked (in the order given):
	
	- MeshManager::triangulate invokes set_geom_id once per geometry

	- MeshManager::triangulate invokes operator()(const FeatureSet&) once per geometry. 
	This method is expected to plot/save the geometry in a user-defined manner.

	- algo::Triangulate invokes set_try_num once per try.
	The try number helps enumerate filenames of meshes realized during the execution
	of the particular try.

	- algo::Triangulate invokes operator()(const Quadtree&) once per try.
	The routine helps inspect quadtree refinement at the start of the try.

	- algo::Triangulate and algo::Tryangulate invoke operator(const WorkingMesh&) 
	at different stages of a try to help inspect mesh perturbations during the course of a try.
	The routine is expected to query the status of the working mesh using the 
	WorkingMesh::get_mesh_status method. 

	- algo::Tryangulate invokes operator()(const WorkingMesh&, const int proj_num, const int relax_num)
	at each projection/mesh relaxation step executing in a try. It can be used for detailed
	visualization of incremental projection of positive vertices towards the immersed features
	and the subsequent relaxation of nearby mesh vertices using DVR.

	- algo::Triangulate invokes operator()(const Quadtree&, const WorkingMesh&) at the 
	end of a successful try. The quadtree, background mesh and the conforming mesh
	can be visualized using this routine.
	
	In summary, 
	MeshManager::triangulate invokes relevant operators once per triangulation call, 
	algo::Triangulate invokes relevant operators once per try, and
	algo::Tryangulate invokes relevant operators during the execution of a try.

	Derived classes can enumerate meshes/filenames in the different 
	overloaded operators using an appropriate 
	combination of the geometry id, the try number and the projection/relaxation iterate.

	There are two concrete implementations of the callback
	interface that provide two extreme variants of mesh visualization.

	The derived class um2::test::Try_Visualization_Callback
	implements operators to provide the most detailed mesh outputs-
	at each step of each try of the algorithm. In fact, every
	operator of the class triggers a mesh output.  It is useful
	during testing/debugging, as well as to visualize how the UM
	algorithm works.

	The derived class um2::test::Final_Try_Visualization_Callback
	provides an implementation of the interface that triggers a
	mesh output only at the end of a successful try. Hence, only
	the method operator()(const Quadtree&, const WorkingMesh&)
	triggers an output for the quadtree, background mesh and the
	conforming mesh. The remaining operators do nothing.
              
	@sa um2::test::Try_Visualization_Callback and um2::test::Final_Try_Visualization_Callback

	@tparam MeshType Mesh type satisfying um2::CheckMeshTraits.

	@note It may be possible to use instance of CallbackInterface
	for more than just visualization, since it helps to query the
	progress of the algorithm.

	@author Ramsharan Rangarajan
    */
    template<typename MeshType> 
      class CallbackInterface {
    public:

      virtual ~CallbackInterface() = default;

      //! Set the try number. Invoked once per try by um2::algo::Triangulate.
      //! @param[in] n try number
      inline void setTryNumber(const int n) {
	try_num = n;
      }

      //! Access to the try number for derived classes
      //! @return try number
      inline int getTryNumber() {
	return try_num;
      }

      //! Set an identifier for the geometry. Invoked once per triangulation call by MeshManager::triangulate.
      //! @param[in] gid geometry identifier
      inline void setMeshID(const int gid) {
	geom_id = gid;
      }

      //! Access the geometry id for derived classes.
      //! @return geometry identifier
      inline int getMeshID() {
	return geom_id;
      }
	
      //! Operator to visualize a feature set.  Invoked once per triangulation call by MeshManager::triangulate.
      //! @param[in] fset Feature set to be visualized
      virtual void operator()(const FeatureSet& fset) = 0;

      //! Operator to visualize the quadtree. Invoked once per try by MeshManager::triangulate.
      //! @param[in] QT quadtree to be visualized
      virtual void operator()(const Quadtree& QT) = 0;

      /** \brief Operator to visualize the working at various stages of the algorithm. 
	  Derived class use WorkingMesh::get_mesh_status to query the context of the invocation.
	  Invoked by algo::Triangulate, algo::Tryangulate and algo::Project_Relax.
	  
	  @param[in] WM working mesh to be visualized
      */
      
      virtual void operator()(const WorkingMesh<MeshType>& WM) = 0;

      /** \brief Operator to visualize the quadtree, background mesh and the working mesh at the end of a successful try. 
	  Invoked once per triangulation call by MeshManager::triangulate.
	  
	  @param[in] manager MeshManager object
      */
      virtual void operator()(const Quadtree& QT, const WorkingMesh<MeshType>& WM,
			      const std::map<int,FeatureMeshCorrespondence>& fm_correspondences) = 0;
	
      /** \brief Operator to visualize the working mesh at different stages of execution of a try.
	  Invoked multiple times by algo::Tryangulate and algo::Project_Relax.
	  @param WM Working mesh
	  @param proj_num Projection iteration of positive vertices
	  @param relax_num Relaxation iteration for DVR
      */
      virtual void operator()(const WorkingMesh<MeshType>& WM,
			      const int proj_num, const int relax_num) = 0;
	
    private:
      int try_num; //!< try number. Useful for mesh enumeration
      int geom_id; //!< geometry identified. Useful for geometry enumeration.
    };
    
} // namespace um2
