// Sriramajayam

namespace um2
{
  // Check computed mesh
  template<typename MeshType>
    void MeshManager<MeshType>::validate(const int num,  const FeatureSet& fset,
					 const MeshingParams& meshing_params, const double distTOL) const
    {
      assert(num==case_id);
      const int nFeatures = fset.getNumFeatures();
      
      assert(static_cast<int>(fm_correspondences.size())==nFeatures);

      const auto& gparams = fset.getFeatureParams();
      const auto& orientations = fset.getFeatureOrientations();
      for(int f=0; f<nFeatures; ++f)
	{
	  const auto& gparam = gparams[f];
	  const auto& orientation = orientations[f];
	  auto it = fm_correspondences.find(gparam.curve_id);
	  assert(it!=fm_correspondences.end());
	  const auto& fmout = it->second;
	  
	  // check orientation
	  assert(orientation!=FeatureOrientation::Unassigned);
  
	  // check terminal vertices
	  const std::array<double,4> terminalPoints{gparam.leftCnr[0], gparam.leftCnr[1], gparam.rightCnr[0], gparam.rightCnr[1]};
	  for(int p=0; p<2; ++p)
	    { const int& n = fmout.terminalVertices[p];
	      assert(n>=0);
	      const double* X = WM->coordinates(n);
	      const double* Y = &terminalPoints[2*p];
	      double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      assert(dist<distTOL); 
	    }

	  // check that positive vertices start and terminate at the end points
	  assert(static_cast<int>(fmout.PosVerts.size())>=2);
	  assert(fmout.terminalVertices[0]==fmout.PosVerts.front());
	  assert(fmout.terminalVertices[1]==fmout.PosVerts.back());

	  // check that the splitting vertex is a positive vertex
	  if(orientation==FeatureOrientation::Split_Positive2Negative || orientation==FeatureOrientation::Split_Negative2Positive)
	    {
	      assert(fmout.splittingVertex!=-1);
	      assert(std::find(fmout.PosVerts.begin(), fmout.PosVerts.end(), fmout.splittingVertex)!=fmout.PosVerts.end());
	    }
	  else
	    { assert(fmout.splittingVertex==-1); }
  
	  // check that positive vertices lie on the feature
	  for(auto& p:fmout.PosVerts)
	    {
	      const double* X = WM->coordinates(p);
	      double sd;
	      gparam.signed_distance_function(X, sd, nullptr);
	      assert(std::abs(sd)<distTOL);
	    }

	  // chech that there is no repeated positive vertex
	  std::set<int> pset(fmout.PosVerts.begin(), fmout.PosVerts.end());
	  assert(pset.size()==fmout.PosVerts.size());
	  pset.clear();

	  // Check element qualities in WM
	  dvr::GeomTri2DQuality<WorkingMesh<MeshType>> Quality(*mesh_wrapper);
	  const int nElements = WM->n_elements();
	  for(int e=0; e<nElements; ++e)
	    { assert(Quality.Compute(e)>meshing_params.min_quality); }

	  // Qualities of elements in the 1-rings of positive vertices should be better than the threshold value
	  for(auto& p:fmout.PosVerts)
	    {
	      const auto& oneRingElms = mesh_wrapper->Get1RingElements(p);
	      for(auto& elm_indx:oneRingElms)
		{ assert(Quality.Compute(elm_indx)>=meshing_params.min_quality); }
	    }
	}

      // Check global topology of positive vertices, omitting terminals
      int nVerts = 0;
      std::set<int> allPosVerts{};
      for(auto& it:fm_correspondences)
	{
	  const auto& fm = it.second;
	  const int nPosVerts = static_cast<int>(fm.PosVerts.size());
	  nVerts += nPosVerts-2;
	  for(int i=1; i<nPosVerts-1; ++i)
	    allPosVerts.insert(fm.PosVerts[i]);
	}
      assert(static_cast<int>(allPosVerts.size())==nVerts);
            
      return;
    }
}
