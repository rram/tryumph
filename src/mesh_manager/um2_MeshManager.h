// Sriramajayam

#pragma once

#include <um2_Quadtree.h>
#include <um2_WorkingMesh.h>
#include <um2_MeshWrapper.h>
#include <um2_CallbackInterface.h>
#include <um2_MeshTraits.h>
#include <DVRlib>
#include <memory>
#include <functional>

namespace um2 {

  template <typename MeshType> class MeshManager {

  public:
    //! Constructor
    inline MeshManager(Box box)
      : case_id(-1), QT(std::make_unique<Quadtree>(&box.center[0], box.size)), BG(nullptr),
      WM(nullptr), mesh_wrapper(nullptr), callback(nullptr), fm_correspondences({}) {
      static_assert(CheckMeshTraits<MeshType>());
    }

    //! Destructor
    virtual ~MeshManager() = default;

    // Disable copy and assignment
    MeshManager(const MeshManager &) = delete;
    MeshManager operator=(const MeshManager &) = delete;

    //! Register a callback function
    void setCallback(CallbackInterface<MeshType> &cb) {
      callback = &cb;
    }

    //! Access the mesh label
    inline int getMeshID() const { return case_id; }

    //! Access the quadtree
    inline const Quadtree &getQuadtree(const int num) const {
      assert(num == case_id && QT != nullptr);
      return *QT;
    }

    //! Access the background mesh
    inline const MeshType &getUniversalMesh(const int num) const {
      assert(num == case_id && BG != nullptr);
      return *BG;
    }

    //! Access the working mesh
    inline const WorkingMesh<MeshType> &getWorkingMesh(const int num) const {
      assert(num == case_id && WM != nullptr);
      return *WM;
    }

    //! Unrefine the quadree
    inline void coarsenQuadtree() {
      assert(QT != nullptr);
      QT->prune();
    }

    //! Main functionality: triangulate
    RunInfo triangulate(const int num, const FeatureSet &fset,
			const MeshingParams &meshing_params);

    //! Main functionality: triangulate
    RunInfo triangulate(const int prev_num, const int num, const FeatureSet &fset,
			const MeshingParams &meshing_params);

    //! Access the feature mesh correspondence for a feature
    const std::map<int,FeatureMeshCorrespondence>& getFeatureMeshCorrespondence(const int id) const
      {
	assert(id==case_id && WM->getMeshStatus()==WorkingMeshStatus::CONFORMING);
	return fm_correspondences;
      }
    	
    //! Check computed mesh
    void validate(const int num, const FeatureSet& fset, const MeshingParams &meshing_params, const double distTOL) const;

  private:

    //! Compute feature-mesh correspondences
    void computeFeatureMeshCorrespondences();

    //! triangulate
    RunInfo algo_Triangulate(const MeshingParams& mparams, const internal::SplitFeatureSet& fset);

    //! tryangulate
    std::pair<std::set<int>, TryInfo> algo_Tryangulate(const MeshingParams& mparams, const internal::SplitFeatureSet& fset);

    void algo_RefineQuadtreeAlongFeature_Local(const FeatureParams& gparam, const internal::SplitFeatureSet& fset, const double qfactor);

    void algo_RefineQuadtreeAlongFeature_NonLocal(const FeatureParams& gparam, const FeatureSet& fset, const double qfactor);

    void algo_RefineQuadtreeAroundCorners(const internal::SplitFeatureSet& fset, const double qfactor);

    void algo_RefineQuadtreeAtVertices(const std::set<int>& refine_Verts);

    MeshingReason algo_PerturbMeshForTerminalPoint(const double* point, const int Ndist,
						   const int Nproject, const int Nrelax,
						   const double qThreshold, const std::set<int>& dndlist,
						   boost_pointID_rtree& rtree, int& coincidentVertex);
      

    void algo_SnapMeshToFeaturePoints(const std::set<int>& frozen_nodes, const MeshingParams& mparams, std::set<int>& refine_Verts);

    MeshingReason algo_IdentifyPositiveVerticesForFeature(const FeatureParams& segParams, const FeatureOrientation& orientation,
							  const int* terminalVerts, const std::vector<int>& onFeatureVerts,
							  const double qthreshold, std::vector<int>& PosVerts,
							  std::vector<std::pair<int,int>>& PosCutElmFaces);

    void algo_IdentifyPositiveEdges(const double qthreshold, std::set<int>& refine_Verts);

    // Make an open chain triangle-free
    void algo_MakeOpenChainTriangleFree(std::vector<int>& PosVerts, std::vector<std::pair<int,int>>& PosCutElms);

    void algo_InspectPositiveEdgesTopology(std::set<int>& refine_Verts);

    // Identify positive-edge-boundary intersections
    void algo_InspectPositiveEdgeBoundaryIntersection(const std::set<int>& frozen_nodes, std::set<int>& refine_Verts);
      
    std::tuple<MeshingReason, std::set<std::pair<int,int>>, int, int>
      algo_ProjectRelax(const std::set<int>& frozen_nodes,  const MeshingParams& mparams);

	  
    int case_id;
    std::unique_ptr<Quadtree> QT;
    std::unique_ptr<MeshType> BG;
    std::unique_ptr<WorkingMesh<MeshType>> WM;
    std::unique_ptr<internal::MeshWrapper<WorkingMesh<MeshType>>> mesh_wrapper;
    CallbackInterface<MeshType> *callback;
    std::map<int, FeatureMeshCorrespondence> fm_correspondences;
  };
  
} // namespace um2

#include <um2_MeshManager_qtree_refine.h>
#include <um2_MeshManager_cornersnap.h>
#include <um2_MeshManager_posedges.h>
#include <um2_MeshManager_projectrelax.h>
#include <um2_MeshManager_tryangulate.h>
#include <um2_MeshManager_triangulate.h>
#include <um2_MeshManager_impl.h>
#include <um2_MeshManager_validate.h>
