// Sriramajayam

/** \file um2_Output.h
 * \brief Defines output data structures for TryUMph
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <vector>
#include <tuple>
#include <list>
#include <map>
#include <string>
#include <fstream>

namespace um2 {

  /** \brief Enumeration of all possible statuses of the working mesh
   * during the execution of trys. The status is used in two ways:
   * - internally in the triangulation algorithm to check that status of
   * the working mesh ar different stages of execution
   * - by the CallbackInterface to deduce the output to trigger. 
   *
   * \sa CallbackInterface, utils::Try_Visualization_Callback, utils::Final_Try_Visualization_Callback
   *
   * \ingroup output
   */
  enum class WorkingMeshStatus {
    UNPROCESSED,                         //!< coincides with the background mesh
      CORNERS_SNAPPED,                   //!< Vertices have been snapped to feature corners
      POSITIVE_EDGES_IDENTIFIED,         //!< Positive edges/elements/vertices have been identified
      POSITIVE_EDGE_TOPOLOGY_INSPECTED,  //!< Positive edges topology has been inspected
      VERTICES_PROJECTED_RELAXED,        //!< Positive vertices have been snapped, nearby vertices relaxed
      NONCONFORMING,                     //!< Processing is complete but does not conform to features
      CONFORMING                         //!< Mesh conforms to all features
      };

  /** \brief Enumeration of modes of failure (or success) encountered
   * when processessing a feature during meshing trys.  
   *
   * These reasons are available to users through instances of TryInfo
   * returned after meshing trys. 
   * 
   * \sa TryInfo, RunInfo, MeshManager::triangulate
   * 
   * \ingroup output
   */
  enum class MeshingReason: int {
    Unassigned,                            //!< No reason assigned, i.e., default
      Success,                             //!< Successful
      Fail_Corner_Coincident_Vertex,       //!< Cannot find vertex to snap to a feature corner
      Fail_Corner_Relaxation_Nbd,          //!< Set of vertices to relax around a corner is empty
      Fail_Corner_Separation,              //!< Vertices snapped onto feature belong to an edge
      Fail_PositiveEdge_Obtuse,            //!< Positive edge makes an obtuse angle with the tangent
      Fail_PositiveEdge_BoundaryProximity, //! Positive edges intersect the boundary of the mesh
      Fail_PositiveEdge_FeatureTopology,   //!< Violation of the condition that precisely 2 pos edges intersect at each positive vertex
      Fail_PositiveEdge_GlobalTopology,    //!< Unexpected intersection of positive edges at the global level
      Fail_ElementQuality,                 //!< Quality of an element fell below the threshold during node projection
      Count                                //!< Counter of failure modes
      };

  /** \brief Information about correspondences between mesh
   * vertices/edges conforming to a feature.
   *
   * The MeshManager class returns an instance of this struct per
   * feature.  Information conveyed by members are useful only when
   * FeatureMeshCorrespondence::reason reflects a success. 
   *
   * In such a case, PosVerts.first and PosVerts.last coincide with
   * terminalVertices[0] and terminalVertices[1], respectively.
   * Positive vertices are vertices of positive edges of positively
   * cut triangles. Hence PosVerts[i] and PosVerts[i+1] are vertices
   * of the i-th positively cut triangle.
   *
   * The triplet PosCutElmFaces[i] contains the positively cut
   * element, the local index of the edge of the triangle that is its
   * positive edge (in the range 0, 1 or 2), and the relative
   * orientation of the positive edge. Edges of a triangle with
   * vertices v0, v1, v2 are enumerated as (v0,v1), (v1,v2) and
   * (v2,v0).
   *
   * \sa MeshManager::getFeatureMeshCorrespondence
   *
   * \ingroup output
   */
  struct FeatureMeshCorrespondence {
    MeshingReason reason;           //!< record of success or failure type in processing this feature
    std::vector<int> PosVerts;      //!< ordered sequence of positive vertices.
    std::vector<std::pair<int, int>> PosCutElmFaces; //!< ordered sequence of positive elements and positive edges 
    int terminalVertices[2];        //!< vertices coinciding with the left and right endpoint of the feature
    int splittingVertex;            //!< vertex coinciding with the interior point specified for the feature. Set to -1 if there isn't one.

    //!< Constructor
    FeatureMeshCorrespondence();
  };


  /** \brief Information about execution of a try.
   *
   * The MeshManager class returns one instance of TryInfo per try
   * through the RunInfo struct.  
   *
   * \ingroup output
   *
   * \sa RunInfo MeshManager::triangulate
   */
  struct TryInfo {
    bool success;     //!< whether the try was successful
    int nRelaxVerts;  //!< Total number of relaxed vertices
    int nRelaxIters;  //!< Total number of relaxation iterations
    double runtime;   //!< Run time measuring using a clock
    std::map<MeshingReason, int> failures;  //!< counter of failure modes

    //! Constructor
    TryInfo();
  };

  /** \brief Information about the execution of a triangulate call.
   *
   * The MeshManager class returns an instance of RunInfo for each
   * triangulate call, containing detailed information about
   * individual trys, run times, etc. Users should inspect
   * RunInfo::success before using other information conveyed by the
   * other members.
   *
   * \sa TryInfo, MeshManager::triangulate
   *
   * \ingroup output
   */
  struct RunInfo {
    bool success;                   //!< whether triangulation was successful
    int nTrys;                      //!< total number of refinement iterations for the quadtree
    double runtime;                 //!< Run time measuring using a clock
    std::vector<TryInfo> try_info;  //!< Diagnostics of individual trys

    //! Constructor
    RunInfo();
  };


}  // namespace um2
