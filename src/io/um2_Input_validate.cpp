// Sriramajayam

/** \file um2_Input_validate.cpp
 * \brief Implementation of validate methods for input parameters
 * \author Ramsharan Rangarajan
 */

#include <um2_Input.h>
#include <cassert>

namespace um2 {
  
  // Specialize validate methods
  
  //! \brief validate instance of Box 
  template<>
  void validate_input<Box>(const Box& box) {
    assert(box.size>0.);
  }

  //! \brief validate instance of MedialAxisParams
  template<>
  void validate_input<MedialAxisParams>(const MedialAxisParams& obj) {
    assert(obj.exclusion_radius>0.);
    assert(obj.maximum_radius>0.);
    assert(obj.eps_radius>0.);
    assert(obj.maximum_radius>obj.eps_radius && obj.maximum_radius>obj.exclusion_radius);
  }

  //! \brief validate instance of FeatureParams
  template<>
  void validate_input<FeatureParams>(const FeatureParams& obj) {
    assert(obj.curve_id>=0);
    assert(obj.signed_distance_function!=nullptr);
    assert(obj.sample_feature!=nullptr);
  }

  //! \brief validate instance of MeshingParams
  template<>
  void validate_input<MeshingParams>(const MeshingParams& obj) {
    assert(obj.refinement_factor>0.);
    assert(obj.num_proj_steps>=1);
    assert(obj.num_dvr_int_relax_iters>=1);
    assert(obj.num_dvr_int_relax_dist>=1);
    assert(obj.num_dvr_bd_relax_iters>=0);
    if(obj.num_dvr_bd_relax_iters>0) {
      assert(obj.num_dvr_bd_relax_samples>=1);
    }
    assert(obj.min_quality>0.);
    assert(obj.max_num_trys>=1);
  }
    
} // namespace um2
