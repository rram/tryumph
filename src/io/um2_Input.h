// Sriramajayam

/** \file um2_Input.h
 * \brief Defines input data structures for TryUMph
 * \author Ramsharan Rangarajan
 */
  
#pragma once

#include <functional>
#include <list>
#include <utility>

namespace um2 {

  /** \brief Axis-aligned square-shaped planar bounding box defining the
   * extent of the background/universal mesh. The box is specified by
   * a center and the length of its edge.
   *
   * \ingroup input
   * \sa MeshManager::MeshManager
   *
   * Example usage: hello_tryumph.cpp, rotating_smiley.cpp
   *
   * Use the validate_input function to run sanity checks.
   * 
   */
  struct Box {
    std::array<double,2> center; //!< Cartesian coordinates of the center 
    double size;                 //!< Edge length
  };
    
  /** \brief Algorithmic parameters for estimating medial axes of
   * features in the shrinking-ball algorithm.  
   *
   * All parameters are assumed to be dimensionless. When used in the
   * shrinking ball algorithm by the FeatureMedialAxis class, these
   * parameters are scaled by the size of the bounding box estimated
   * for the points provided.
   *
   * \ingroup input 
   *
   * \sa FeatureMedialAxis::FeatureMedialAxis, FeatureSet::FeatureSet
   *
   * Example usage: hello_tryumph.cpp, rotating_smiley.cpp
   *
   * Use the validate_input function to run sanity checks.
   */   
  struct MedialAxisParams {
    double exclusion_radius; //!< Distance separating sample points. Points closer to each othen than this distance are excluded in the shrinking ball algorithm calculation.
    double maximum_radius;   //!< Upper bound on the medial radius. Medial axis samples farther than this distance are capped to the specified value.
    double eps_radius;       //!< Tolerance for convergence of the radius of medial balls.
  };

  /** \brief Algorithmic parameters for mesh generation
   * 
   * Suggested defaults (used in all tutorials and performance tests):
   * 
   * |Parameter| Value| Remarks | Significance |
   * |:-------:|:----:|:-----|:---|
   * | `nonlocal_refinement`| false | Setting to true generally yields more refined meshes while requiring fewer trys | High |
   * | `refinement_factor` | 1.0 | Large value yields more refinement. Is sensitive generally only when `nonlocal_refinement` is set to true| High |
   * | `num_proj_steps` | 5 | Too few steps can result in larger number of trys and poorer element qualities| Low |
   * | `num_dvr_int_relax_iters` | 10 | More iterations can improve element qualities at the expense of higher runtimes | Low |
   * | `num_dvr_int_relax_dist` | 2 | Larger neighborhoods can improve element qualities around features, will lead to higher runtimes | Low |
   * | `num_dvr_bd_relax_iters` | 3 | More iterations can improve element qualities along features | Low |
   * | `num_dvr_bd_relax_samples` | 10 | Unrelated to sampling features. Is meaningful if the geometry contains features with large curvatures | Low |
   * | `min_quality` | 0.01 | Not a parameter for improving element quality, but for detecting element collapse. Qualities usually are far better. | Low |
   * | `max_num_trys` | 10 | Provided because the algorithm implemented lacks a proof of termination. | High |

   * \ingroup input
   * \sa MeshManager::triangulate
   *
   * Example usage: hello_tryumph.cpp, rotating_smiley.cpp
   *
   * Use the validate_input function to run sanity checks.
   */
  struct MeshingParams {
    bool   nonlocal_refinement;        //!< Whether to initialize background mesh size based on nonlocal feature medial axes estimates. 
    double refinement_factor;          //!< Factor by which to subdivide initial estimate for mesh size
    int    num_proj_steps;             //!< Number of steps over which to project positive vertices onto the geometry
    int    num_dvr_int_relax_iters;    //!< Number of DVR relaxation iterations for interior nodes
    int    num_dvr_int_relax_dist;     //!< Graph-distance around features over which to relax nodes
    int    num_dvr_bd_relax_iters;     //!< Number of DVR relaxation iterations for feature nodes
    int    num_dvr_bd_relax_samples;   //!< Number of sampling points to use for constrained relaxation in DVR for nodes lying on features
    double min_quality;                //!< Threshold for element quality (lower bound)
    int    max_num_trys;               //!< Max number of trys
  };


  /** \brief Parameters specifying details of a feature.
   *
   * A feature is a twice continuously differentiable planar, simple
   * and open curve. The struct encapsulates all geometric information
   * required by TryUMph. These include functions to sample the curve
   * and to compute the signed distance function, see @ref userguide
   * for a detailed discussion.
   * 
   * Signed distance queries to the feature are limited to points
   * lying in the vicinity of the feature (within an approximate
   * tubular neighborhood). Hence, implementations of
   * `signed_distance_function` need not cater to queries at far away
   * locations.
   * 
   *
   * Sample points on the feature returned by `sample_feature` are
   * used by FeatureMedialAxis to estimate the medial axis. In
   * particular, these points are not used to seed mesh vertices.
   * 
   *
   *  Meshes computed by MeshManager necessarily have a vertex
   * coincided with each of the two endpoints `leftCnr` and
   * `rightCnr`. The third point, `intPoint` is required to lie on the
   * feature and be distinct from these endpoints. It is used
   * internally by TryUMph (to assign a feature polarities). It is
   * possible that a vertex in the mesh computed by MeshManager has a
   * vertex coinciding with `intPoint`.
   *
   * The ordering of left and right endpoints should be consistent
   * with the orientation implicitly assigned by the signed distance
   * function provided.
   *
   * Note that FeatureParams does not enforce a specific
   * representation for the feature (e.g., as a parametric curve).
   *
   * \ingroup input
   * \sa FeatureSet::FeatureSet, utils::LineSegment, utils::QuadraticRationalBezier, utils::CubicBezier
   *
   *  Example initialization for a circular arc: demo::CircularArc
   *   
   * Use the validate_input function to run sanity checks.
   */
  struct FeatureParams {
    int curve_id;                  //!< Unique integer-valued identified for this feature    
    std::array<double,2> leftCnr;  //!< Cartesian coordinates of left endpoint 
    std::array<double,2> rightCnr; //!< Cartesian coordinates of right endpoint
    std::array<double,2> intPoint; //!< Cartesian coordinates of a point lying on the feature away from the endpoints
    std::function<void(const double* X, double& sd, double* dsd)> signed_distance_function; //!< Function to compute the signed distance function to the feature
    std::function<std::list<std::array<double,2>>()> sample_feature;                        //!< Function to compute an (unordered) list of sample points on the feature
  };

  
  /** \brief Helper method to validate inputs
   * 
   * Specialized for: 
   * - Box
   * - MedialAxisParams
   * - FeatureParams
   * - MeshingParams
   */ 
  template<typename T>
    void validate_input(const T&);
  
} // namespace um2
