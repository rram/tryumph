// Sriramajayam

/** \file um2_Output.cpp
 * \brief Implements constructor output structs
 * \author Ramsharan Rangarajan
 */

#include <um2_Output.h>
#include <cassert>

namespace um2
{
  FeatureMeshCorrespondence::FeatureMeshCorrespondence()
  {
    reason = MeshingReason::Unassigned;
    PosVerts.clear();
    PosCutElmFaces.clear();
    terminalVertices[0] = -1;
    terminalVertices[1] = -1;
    splittingVertex     = -1;
  }
  
  TryInfo::TryInfo()
    :success(false),
     nRelaxVerts(0),
     nRelaxIters(0),
     runtime(0.),
     failures{}
  {
    const int count = static_cast<int>(MeshingReason::Count);
    for(int i=0; i<count; ++i)
      failures[static_cast<MeshingReason>(i)] = 0;
  }

  
  RunInfo::RunInfo()
    :success(false),
     nTrys(0),
     runtime(0),
     try_info{} {}

} // namespace um2 
