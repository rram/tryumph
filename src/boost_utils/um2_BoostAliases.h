// Sriramajayam

/** \file um2_BoostAliases.h
 * \brief Define aliases for frequently used functionalities in boost::geometry.
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace um2 {
  
  //! Boost representation of a point in the real plane using Cartesian coordinates
  using boost_point = boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian>;

  //! Boost representation of an index-tagged point
  using boost_pointID = std::pair<boost_point,int>;
  
  //! Boost representation of an axis-aligned box with corners specified as instances of um2::boost_point
  using boost_box = boost::geometry::model::box<boost_point>;

  //! Boost representation of a planar axis-aligned R-tree containing instances of um2::boost_point
  using boost_point_rtree = boost::geometry::index::rtree<boost_point, boost::geometry::index::quadratic<8>>;

  //! Boost representation of a planar axis-aligned R-tree containing integer-tagged instanced um2::boost_point
  using boost_pointID_rtree = boost::geometry::index::rtree<boost_pointID, boost::geometry::index::quadratic<8>>;
       
} // namespace um2
