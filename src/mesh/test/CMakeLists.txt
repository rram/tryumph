# Sriramajayam

# Project
project(working-mesh-unit-tests)

# Identical for all targets
foreach(TARGET testWorkingMesh)
    
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC um2_mesh)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${UM2_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
