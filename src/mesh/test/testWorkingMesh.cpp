// Sriramajayam

#include <um2_FeatureSet.h>
#include <um2_TriangleMesh.h>
#include <um2_WorkingMesh.h>
#include <um2_MeshWrapper.h>
#include <cassert>
#include <random>
#include <iostream>
#include <cmath>
#include <map>


int main()
{
  // check traits
  dvr::CheckMeshTraits<um2::WorkingMesh<um2::TriangleMesh>>();
  
  // Generate a random set of coordinates and connectivity
  const int nElements = 20;
  const int nNodes = 15;
  std::vector<double> Coordinates(2*nNodes);
  std::vector<int>    Connectivity(3*nElements);

  std::random_device rand_dev;
  std::mt19937 generator(rand_dev());
  std::uniform_int_distribution<int> int_dist(0, 14);

  for(int e=0; e<nElements; ++e)
    for(int a=0; a<3; ++a)
      Connectivity[3*e+a] = int_dist(generator);
  
  std::uniform_real_distribution<double> real_dist(-2.,2.);
  for(int n=0; n<nNodes; ++n)
    for(int k=0; k<2; ++k)
      Coordinates[2*n+k] = real_dist(generator);

  um2::TriangleMesh MD(Coordinates, Connectivity);

  // empty feature set
  std::vector<um2::FeatureParams> fparams{};
  um2::MedialAxisParams mparams{.exclusion_radius=1.e-3, .maximum_radius=1., .eps_radius=1.e-5};
  um2::FeatureSet fset(fparams, 1.e-4, mparams);
  
  // Create working mesh
  um2::WorkingMesh<decltype(MD)> WM(MD, fset.getSplitFeatureSet());

  // Mesh wrappers
  dvr::MeshWrapper<decltype(MD)> MD_mesh_wrapper(MD);
  um2::internal::MeshWrapper<decltype(WM)>  WM_mesh_wrapper(WM);
  
  // Insert some modified nodes (all even numbered)
  std::vector<double> nucoord(2);
  for(auto a=0; a<nNodes; a+=2)
    {
      nucoord[0] = real_dist(generator);
      nucoord[1] = real_dist(generator);
      WM.update(a, &nucoord[0]);
      const double* X = WM.coordinates(a);
      assert(std::abs(X[0]-nucoord[0])+std::abs(X[1]-nucoord[1])<1e-8);
    }

  for(int e=0; e<nElements; ++e)
    assert(WM.connectivity(e)==MD.connectivity(e));

  // Access tests
  assert(WM_mesh_wrapper.GetMaxElementValency()==MD_mesh_wrapper.GetMaxElementValency());
  assert(WM_mesh_wrapper.GetMaxVertexValency()==MD_mesh_wrapper.GetMaxVertexValency());

  for(int vert=0; vert<nNodes; ++vert)
    {
      const auto& wm_oneRingElms = WM_mesh_wrapper.Get1RingElements(vert);
      std::set<int> wm_set(wm_oneRingElms.begin(), wm_oneRingElms.end());
      const auto& md_oneRingElms = MD_mesh_wrapper.Get1RingElements(vert);
      std::set<int> md_set(md_oneRingElms.begin(), md_oneRingElms.end());
      assert(md_set==wm_set);  
    }

  // Brute-force test of element 1-rings
  std::map<int, std::set<int>> wm_1rings{};
  std::set<int> vertSet{};
  for(int e=0; e<nElements; ++e)
    {
      const int* conn = WM.connectivity(e);
      for(int a=0; a<3; ++a)
	{
	  auto it = wm_1rings.find(conn[a]);
	  if(it==wm_1rings.end())
	    wm_1rings[conn[a]] = std::set<int>{e};
	  else
	    it->second.insert(e);
	  vertSet.insert(conn[a]);
	}
    }
  for(auto& v:vertSet)
    {
      const auto& onering = WM_mesh_wrapper.Get1RingElements(v);
      std::set<int> testring(onering.begin(), onering.end());
      auto it = wm_1rings.find(v);
      assert(it!=wm_1rings.end());
    }
  
  // test 1-ring vertices of all vertices in WM
  for(int vert=0; vert<nNodes; ++vert)
    {
      const auto& wm_oneRingVerts = WM_mesh_wrapper.Get1RingVertices(vert);
      std::set<int> wm_set(wm_oneRingVerts.begin(), wm_oneRingVerts.end());
      const auto& md_oneRingVerts = MD_mesh_wrapper.Get1RingVertices(vert);
      std::set<int> md_set(md_oneRingVerts.begin(), md_oneRingVerts.end());
      assert(wm_set==md_set);
    }
  
  // reset the feature
  // WM.reset(fset.getSplitFeatureSet());
}
