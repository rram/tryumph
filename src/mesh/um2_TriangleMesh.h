
/** \file um_test_TriangleMesh.h
 * \brief Definition of the class um2::TriangleMesh.
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <vector>
#include <cassert>
#include <um2_MeshTraits.h>

namespace um2
{
  class TriangleMesh
  {
  public:
    //! Constructor
    inline TriangleMesh(const std::vector<double>& coord, const std::vector<int>& conn)
      :Coordinates(coord),
      Connectivity(conn),
      nodes(static_cast<int>(coord.size()/2)),
      elements(static_cast<int>(conn.size()/3))
	{
	  static_assert(CheckMeshTraits<TriangleMesh>());
	}

    //! Destructor
    inline ~TriangleMesh() {}

    //! Disable copy & assignment
    TriangleMesh(const TriangleMesh&) = delete;
    TriangleMesh operator=(const TriangleMesh&) = delete;

    //! Returns the coordinates of a specified node
    //! \param[in] node Node number
    //! \return Coordinates of the requested node
    //! \remark Required by dvr and um
    inline const double* coordinates(const int node) const 
    { return &Coordinates[spd*node]; }
      
    //! Overwrites nodal coordinates
    //! Hence node number should equal the current number of nodes.
    //! \param[in] node Node number
    //! \param[in] X Coordinates
    //! \remark Required by dvr and um
    inline void update(const int node, const double* X) 
    {
      assert(node<nodes);
      for(int k=0; k<spd; ++k)
	Coordinates[spd*node+k] = X[k];
    }
     
    //! Returns the connectivity of a specified element
    //! \param[in] elm Element number
    //! \return Connectivity of the requested element.
    //! \remark Required by dvr and um
    inline const int* connectivity(const int elm) const 
    { return &Connectivity[nodes_element*elm]; }

    //! Returns the spatial dimension
    //! \remark Required by dvr
    inline int spatial_dimension() const
    { return spd; }
      
    //! Returns the number of nodes
    //! Node indices range from 0 to nNodes-1
    //! \return Total number of nodes in the mesh.
    //! \remark Required by um
    inline int n_nodes() const
    { return nodes; }

    //! Returns the number of elements
    //! \return Total number of elements in the mesh
    //! \remark required by dvr and um
    inline int n_elements() const 
    { return elements; }
      
  private:

    std::vector<double>    Coordinates;        //!< Vector of coordinates
    const std::vector<int> Connectivity;       //!< Vector of connectivity
    const int              spd = 2;            //!< Spatial dimension
    const int              nodes_element = 3;  //!< Number of nodes per element
    const int              nodes;              //!< Number of nodes in the mesh
    const int              elements;           //!< Number of elements in the mesh
  };
    
}
