// Sriramajayam

/** \file um_WorkingMesh_Impl.h
 * \brief File implementing the class um2::WorkingMesh.
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <set>
#include <fstream>

namespace um2
{
  // Constructor
  template<typename MeshType>
    WorkingMesh<MeshType>::WorkingMesh(const MeshType& umesh, const internal::SplitFeatureSet& fs)
    :RefMesh(umesh),
    nNodes(RefMesh.n_nodes()),
    nElements(RefMesh.n_elements()),
    Coordinates(nNodes*SPD),
    fset(&fs),
    nFeatures(fset->getNumFeatures()),
    mesh_status(WorkingMeshStatus::UNPROCESSED),
    fm_correspondence(nFeatures)
      {
	dvr::CheckMeshTraits<MeshType>();

	// initialize coordinates to the background mesh
	for(int a=0; a<nNodes; ++a)
	  {
	    const double* X = RefMesh.coordinates(a);
	    for(int k=0; k<SPD; ++k)
	      Coordinates[SPD*a+k] = X[k];
	  }
      }


  // reset features, coordinates and feature correspondence
  template<typename MeshType>
    void WorkingMesh<MeshType>::reset(const internal::SplitFeatureSet& fs)
    {
      fset = &fs;
      for(int a=0; a<nNodes; ++a)
	{
	  const double* X = RefMesh.coordinates(a);
	  for(int k=0; k<SPD; ++k)
	    Coordinates[SPD*a+k] = X[k];
	}
      nFeatures = fset->getNumFeatures();
      fm_correspondence.clear();
      fm_correspondence.resize(nFeatures);
      mesh_status = WorkingMeshStatus::UNPROCESSED;
    }

  
  // access the correspondence with a specified feature
  template<typename MeshType>
    std::map<int,FeatureMeshCorrespondence> WorkingMesh<MeshType>::getSplitFeatureCorrespondenceMap() const
    {
      std::map<int,FeatureMeshCorrespondence> fm_map{};
      const auto& gparams = fset->getFeatureParams();
      for(int i=0; i<nFeatures; ++i)
	fm_map[gparams[i].curve_id] = fm_correspondence[i];
      return fm_map;
    }
      
} // namespace um2
      
