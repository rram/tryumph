
/** \file um2_WorkingMesh.h
 * \brief Definition of the class um2::WorkingMesh
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <um2_SplitFeatureSet.h>
#include <um2_Output.h>
#include <vector>
#include <dvr_MeshTraits.h>

namespace um2
{
  template<typename MeshType>
    class MeshManager;
  
  /** \brief Class defining a working mesh.  
   * 
   * A working mesh is a transform of a background mesh in which just
   * the vertex locations may be altered. The purpose of the class is
   * to enable efficient reuse of data through the background
   * mesh. Conforming meshes computed by MeshManager have this type.

   * The class satisfies traits assumed of the mesh class in TryUMph
   * and is templated by the type of the background mesh.
   *
   * Objects of this class are instantiated by MeshManager.
   *
   * \ingroup mesh \sa TriangleMesh, CheckMeshTraits,
   * MeshManager::triangulate.
   */
  template<class MeshType>
    class WorkingMesh
    {
      // MeshManager requires mutable access to the feature set and the working mesh status.
      friend class MeshManager<MeshType>;
      
    public:
      //! \brief Constructor
      //! \param[in] umesh Parent triangulation, referenced.
      //! \param[in] fs Geometry
      WorkingMesh(const MeshType& umesh, const internal::SplitFeatureSet& fs);

      // Disable copy and assignment
      WorkingMesh(const WorkingMesh&) = delete;
      WorkingMesh operator=(const WorkingMesh&) = delete;
      
      //! Destructor, does nothing
      virtual ~WorkingMesh() = default;
	
      //! \brief Access the parent mesh
      //! \return Const reference to the parent mesh
      inline const MeshType& getParentMesh() const { return RefMesh; }  

      //! \brief Access the number of nodes in the mesh. Will be identical to the parent mesh
      inline int n_nodes() const { return nNodes; }

      //! \brief Access the number of elements in the mesh. Will be identical to the parent mesh
      inline int n_elements() const
      { return nElements; }

      //! \brief Access the embedded dimension, equals 2.
      inline int spatial_dimension() const
      { return SPD; }
      
      //! \brief Access the coordinates of a node.
      //! \param[in] vert_index Index of the node, should be in the range 0 to n_nodes()-1
      inline const double* coordinates(const int vert_indx) const {
	assert(vert_indx>=0 && vert_indx<nNodes);
	return &Coordinates[SPD*vert_indx];
      }
      
      /** \brief Update the coordinates of a node
       * \param[in] vert_indx Node number
       * \param[in] X Nodal coordinates vert_indx
       * \warning NOT thread-safe
       */
      inline void update(const int vert_indx, const double* X) {
	assert(vert_indx>=0 && vert_indx<nNodes);
	for(int k=0; k<SPD; ++k)
	  Coordinates[SPD*vert_indx+k] = X[k];
	return;
      }

      /** \brief Returns the connectivity of a specified element.
       * \param[in] elm_indx Index of the element
       * \return pointer to the connectivity of the element. conn[a] equals the index of vertex #a, where a = 0, 1 or 2.
       */
      inline const int* connectivity(const int elm) const {
	assert(elm>=0 && elm<nElements);
	return RefMesh.connectivity(elm);
      }
      
      //! \brief Returns the status of the working mesh
      //! Used by the MeshManager class to verify the sequence of modifications
      //! and by visualization callbacks
      inline WorkingMeshStatus getMeshStatus() const { return mesh_status; }

      //! \brief Access the geometry to which this mesh should conform to
      //! Used by the MeshManager class and visualization callbacks
      inline const internal::SplitFeatureSet& getSplitFeatureSet() const { return *fset; }

      //! \brief Non-mutable access the correspondence with the geometry
      //! Used by the MeshManager class and visualization callbacks
      inline const std::vector<FeatureMeshCorrespondence>& getSplitFeatureCorrespondences() const { return fm_correspondence; }

      //! \brief Access the correspondence with a specified feature
      //! Used by visualization callbacks
      std::map<int, FeatureMeshCorrespondence> getSplitFeatureCorrespondenceMap() const;
      
    private:
      //! \brief reset featureset. Invoked by MeshManager.
      //! \param[in] fs New feature set
      void reset(const internal::SplitFeatureSet& fs);
      
      //! Mutable access to correspondence with the geometry. Invoked by MeshManager.
      inline std::vector<FeatureMeshCorrespondence>& getSplitFeatureCorrespondences() { return fm_correspondence; }
      
      // Set the status of the working mesh. Invoked by MeshManager.
      inline void setMeshStatus(const WorkingMeshStatus new_status)  { mesh_status = new_status; }
      
      const MeshType        &RefMesh;     //!< Reference mesh
      const internal::SplitFeatureSet *fset; //!< pointer to the feature set
      const int             nNodes;       //!< Number of nodes
      const int             nElements;    //!< Number of elements
      const int             SPD = 2;      //!< Spatial dimension
      std::vector<double>   Coordinates;  //!< List of nodal coordinates
      int                  nFeatures;    //!< Number of features

      WorkingMeshStatus mesh_status; //!< status of the working mesh, maintained by MeshManager
      std::vector<FeatureMeshCorrespondence> fm_correspondence; //! correspondences with the geometry, maintained by MeshManager
    };
}


// Implementation of class
#include <um2_WorkingMesh_impl.h>
