// Sriramajayam

#pragma once

#include <dvr_MeshTraits.h>
#include <type_traits>

namespace um2
{
  //! Check that a class satisfies all the mesh traits
  template<class T>
    inline constexpr bool CheckMeshTraits()
    {
      static_assert(dvr::CheckMeshTraits<T>());

      static_assert(std::is_constructible<T, const std::vector<double>&, const std::vector<int>&>::value,
		    "Mesh class missing constructor (const std::vector<double>& coords, const std::vector<int>& conn)");
      
      return true;
    }
}
