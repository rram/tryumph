// Sriramajayam

/** \file um2_MeshWrapper.h
 * \brief Definition of the class um2::MeshWrapper
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <dvr_MeshWrapper.h>

namespace um2
{
  namespace internal
  {
    /** \brief Class implementing mesh utility functions.
     *
     * The class inherits from another of the same name provided by
     * dvrlib to add functionalities to compute element neighbors, vertex rings
     * and vertices on the boundary of a mesh.
     *
     * The class is templated by the mesh type, as is the case in dvrlib
     * as well.
     *
     * \sa TriangleMesh, WorkingMesh, dvr::MeshWrapper
     *
     * \ingroup mesh
     */
    template<typename MeshType>
      class MeshWrapper: public dvr::MeshWrapper<MeshType>
      {
      public:
      
	//! \brief Constructor
	//! \param[in] M Input mesh, satisfying the traits enforced by CheckMeshTraits.
      MeshWrapper(MeshType& M):
	dvr::MeshWrapper<MeshType>(M),
	  mesh(M),
	  nElements(M.n_elements())
	    { computeElmNeighbors(); }
      
	//! Destructor
	inline virtual ~MeshWrapper() {}

	//! Disable copy & assignment
	MeshWrapper(const MeshWrapper<MeshType>&) = delete;
	MeshWrapper<MeshType> operator=(const MeshWrapper<MeshType>&) = delete;

	/** \brief Access element neighbor list
	 * 
	 * \param[in] elm_index Index of element whose neighbors to fetch
	 * 
	 * \return integer array elmnbs, such that elmnbs[2*f] <- index
	 * of element neighboring the f-th face of elm_indx,
	 * elmnbs[2*f+1] <- local index of the face of the neighboring
	 * element.  In case a neighbor is not present, i.e., if the
	 * face f of elm_indx lies on the boundary of the mesh, then
	 * elmnbs[2*f] = elmnbs[2*f+1] = -1.
	 */
	inline const int* getElementNeighbors(const int elm_indx) const
	{ assert(elm_indx>=0 && elm_indx<nElements);
	  return &ElmNbs[2*nodes_element*elm_indx]; }

	/** \brief Access vertex neighbors.
	 *
	 * \param[in] vertices List of vertices whose neighbors to identify 
	 *
	 * \param[in] nLayers Number of rings of vertex neighbors to accummulate.
	 *
	 * \return Set of vertices in the mesh, such that vertex returned
	 * is connected to at least one of the given list of vertices by at most nLayers edges in the mesh.
	 */ 
	std::set<int> getVertexNeighborhood(const std::vector<int>& vertices,
					    const int nLayers);

	/** \brief Returns local node numbers on a specified face.
	 *
	 * Local node numbers lying on faces 0, 1 and 2 are (0,1), (1,2) and (2,0).
	 *
	 * \param[in] locface Local face number. Should range from 0 to nodes_element-1
	 *
	 * \return An integer array of size 2, containing the local node
	 * numbers of the face.
	 */
	inline const int* getLocalFaceNodes(const int locface) const
	{ return LocalFaceNodes[locface]; }

	//! \brief Returns the set of boundary nodes of the mesh
	std::set<int> getBoundaryNodes() const;
      
      private:
	//! Helper method to compute element neighbors
	void computeElmNeighbors();

	const MeshType   &mesh;                 //!< Reference to the mesh
	const int        nElements;             //!< Number of elements
	std::vector<int> ElmNbs;                //!< Elm1Rings[indx] = List of elements
	static const int LocalFaceNodes[3][2];  //!< Enumeration of local nodes on faces
	static constexpr int nodes_element = 3;  //!< #nodes per element
      };

    template<typename MeshType>
      const int MeshWrapper<MeshType>::LocalFaceNodes[3][2] = {{0,1}, {1,2}, {2,0}};
  }
}
  
// Implementation
#include <um2_MeshWrapper_impl.h>
