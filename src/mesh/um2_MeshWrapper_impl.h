// Sriramajayam

#pragma once

#include <unordered_set>

namespace um2
{
  namespace internal
  {
    // Helper method to compute element neighbors
    template<typename MeshType>
      void MeshWrapper<MeshType>::computeElmNeighbors()
      {
	// Helper struct
	struct facestructure
	{
	  int element;
	  int face;
	  std::vector<int> conn;
	  bool operator<(const facestructure &other) const
	  { return conn<other.conn; }
	};
  
	const int nFaces = nodes_element;
	const int nFaceNodes = nodes_element-1;
  
	// Enumerate all faces in the mesh
	facestructure FS;
	FS.conn.resize(nFaceNodes);
	std::vector<facestructure> ElmFacePairs(nElements*nFaces);

	int pcount = 0;
	for(int e=0; e<nElements; ++e)
	  {
	    const auto* elmconn = mesh.connectivity(e);
	    for(int f=0; f<nFaces; ++f)
	      {
		// This face
		FS.element = e;
		FS.face = f;
	  
		// local nodes on this face
		const auto* locnodes = getLocalFaceNodes(f);
		for(int i=0; i<nFaceNodes; ++i)
		  FS.conn[i] = elmconn[locnodes[i]];
		std::sort(FS.conn.begin(), FS.conn.end());
		ElmFacePairs[pcount++] = FS;
	      }
	  }
  
	// Sort the pairs of element-face pairs
	std::sort(ElmFacePairs.begin(), ElmFacePairs.end());
  
	// Resize
	ElmNbs.resize( nElements*nFaces*2 );
	std::fill(ElmNbs.begin(), ElmNbs.end(), -1);
  
	// Faces that have exactly the same connectivity should appear consecutively
	// If cannot find a pair, the face has no neighbor.
	int e, f, nbelm, nbface;
	for(auto it=ElmFacePairs.begin(); it!=ElmFacePairs.end(); ++it)
	  {
	    e = it->element;
	    f = it->face;
	    if((it+1)!=ElmFacePairs.end())
	      {
		nbelm = (it+1)->element;
		nbface = (it+1)->face;
		if(it->conn==(it+1)->conn)
		  {
		    ElmNbs[2*e*nFaces+2*f] = nbelm;
		    ElmNbs[2*e*nFaces+2*f+1] = nbface;
		    ElmNbs[2*nbelm*nFaces+2*nbface] = e;
		    ElmNbs[2*nbelm*nFaces+2*nbface+1] = f;
	      
		    // Skip next element
		    ++it;
		  }
	      }
	  }  

	// done
	return;
      }


    // Method to identify vertices lying within a specified # of 1-rings
    // Does not include "vertex"
    template<typename MeshType>
      std::set<int> MeshWrapper<MeshType>::
      getVertexNeighborhood(const std::vector<int>& vertices, const int nLayers)
      {
	// Append neighbors to VertSet
	std::set<int> VertSet{};
      
	// Work with 2 layers of nodes
	std::unordered_set<int> curr_layer{};
	std::unordered_set<int> next_cluster{};

	// Initial condition:
	// curr_layer = provided list of nodes. Remove at the end of the calculation
	// next_cluster: aggregrate of 1-ring neighbors
	for(auto& v:vertices)
	  curr_layer.insert(v);
      
	for(int layer=0; layer<nLayers; ++layer)
	  {
	    // Accummulate 1-ring neighbors of vertices in the current layer into the next cluster
	    next_cluster.clear();
	    for(auto& vert:curr_layer)
	      {
		const auto& oneRingVerts = this->Get1RingVertices(vert);
		for(auto& v:oneRingVerts)
		  next_cluster.insert(v);
	      }

	    // curr_layer  = next_cluster\VertSet
	    curr_layer.clear();
	    for(auto& vert:next_cluster)
	      if(VertSet.find(vert)==VertSet.end())
		curr_layer.insert(vert);
	  
	    // Append all vertices from curr_layer into VertSet
	    for(auto& vert:curr_layer)
	      VertSet.insert(vert);
	  }

	// done
	return std::move(VertSet);
      }


    // Returns the set of boundary nodes of the mesh
    template<typename MeshType>
      std::set<int> MeshWrapper<MeshType>::getBoundaryNodes() const {

      std::set<int> bdnodes{};
      for(int e=0; e<nElements; ++e) {
	const int* nbs = &ElmNbs[6*e];
	for(int f=0; f<3; ++f)
	  if(nbs[2*f]==-1) // free face
	    {
	      const int* conn = mesh.connectivity(e);
	      bdnodes.insert(conn[LocalFaceNodes[f][0]]);
	      bdnodes.insert(conn[LocalFaceNodes[f][1]]);
	    }
      }

      return std::move(bdnodes);
    }
  }
} // um2::
