// Sriramajayam

/** \file um2_SplitFeatureSet.cpp
 *
 * \brief Implementation of methods of the class SplitFeatureSet
 *
 * \author Ramsharan Rangarajan
 */

#include <um2_SplitFeatureSet.h>

namespace um2 {
  namespace internal {
  // Constructor
  SplitFeatureSet::SplitFeatureSet(const std::vector<FeatureParams>& fpvec,
				   const std::map<int,int>& split_parent_id_map,
				   const std::vector<FeatureOrientation>& orientations,
				   const double cornerTol,
				   const MedialAxisParams& mparams)
    :corner_tolerance(cornerTol),
     medial_axis_params(mparams),
     data(),
     curve_parent_id_map(split_parent_id_map),
     closest_corner_distance({})
  {
    assert(fpvec.size()==orientations.size());
    for(auto& fp:fpvec)
      {
	// check distance between terminals
	const double* A = fp.leftCnr.data();
	const double* B = fp.rightCnr.data();
	double dAB = std::sqrt((A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]));
	assert(dAB>2.*cornerTol);

	// should have a parent id
	assert(curve_parent_id_map.find(fp.curve_id)!=curve_parent_id_map.end());
	
	// append feature
	auto result = data.curveid2IndexMap.insert({fp.curve_id, data.nFeatures});
	assert(result.second==true);
	data.gparams.push_back(fp);
	
	++data.nFeatures;
      }
    data.orientations = orientations;
    for(auto& it:data.orientations)
      assert(it==FeatureOrientation::Positive || it==FeatureOrientation::Negative);

    // enumerate corners
    internal::enumerateFeatureTerminals(data.gparams, cornerTol, // in
				    data.nTerminals, data.feature2Terminals, data.terminal2Features); // out
    internal::checkTerminals(data, cornerTol);

    // compute inter-corner distances
    computeCornerDistances();
    
    // identify connected components
    internal::connectFeatures(data.gparams, data.feature2Terminals, data.terminal2Features, // in
			  data.connectedComponents); // out
    internal::checkConnectedComponents(data);
    
    // identify closed components
    internal::closedComponents(data.connectedComponents, // in
			   data.closedComponents); // out
    
    // Sanity checks
    checkOrientations();

    // Compute the medial axis
    internal::computeMedialAxes(data, mparams);

    // done
  }


  // Check orientations of connected components with split features
  void SplitFeatureSet::checkOrientations() const
  {
    const auto& connComponents = data.connectedComponents;
    const auto& orientations   = data.orientations;
    const auto& gparams        = data.gparams;
    const int nComponents = static_cast<int>(connComponents.size());

    // all orientations should be +/-
    for(auto& s:orientations)
      assert(s==FeatureOrientation::Positive || s==FeatureOrientation::Negative);
    
    // mutables
    double nMinus[2], nPlus[2];
    double tMinus[2], tPlus[2];
    double sdval, theta;
    
    // avoid ambiguity of orientation assignment at 90 using a 1deg tolerance
    const double angleTol = 1.*M_PI/180.;
    
    for(int c=0; c<nComponents; ++c)
      {
	// this set of features
	const auto& component = std::vector<int>(connComponents[c].begin(), connComponents[c].end());
	const int nsegments   = static_cast<int>(component.size());

	for(int s=0; s<nsegments-1; ++s)
	  {
	    // this feature & the next
	    const auto& this_geom = gparams[component[s]];
	    const auto& next_geom = gparams[component[s+1]];

	    // the corner to inspect
	    const double* cnr = this_geom.rightCnr.data();
	    const double* dupl_cnr = next_geom.leftCnr.data();
	    assert(std::abs(cnr[0]-dupl_cnr[0]) + std::abs(cnr[1]-dupl_cnr[1])<corner_tolerance);
	    
	    // tangent at the corner from the left
	    this_geom.signed_distance_function(cnr, sdval, nMinus);
	    tMinus[0] = nMinus[1];
	    tMinus[1] = -nMinus[0];

	    // tangent at the corner from the right
	    next_geom.signed_distance_function(cnr, sdval, nPlus);
	    tPlus[0] = nPlus[1];
	    tPlus[1] = -nPlus[0];

	    // Angle between the tangents in the range (-pi,pi)
	    // atan2(sin=cross(tminus,tplus), cos=dot(tminus,tplus))
	    theta = std::atan2(tMinus[0]*tPlus[1]-tMinus[1]*tPlus[0],
			       tMinus[0]*tPlus[0]+tMinus[1]*tPlus[1]);

	    // assigned orientations
	    const auto& sminus = orientations[component[s]];
	    const auto& splus  = orientations[component[s+1]];

	    if((std::abs(theta)-M_PI/2.)>angleTol) // avoid ambiguity at 90 degrees
	      {
		if(std::abs(theta)<M_PI/2.) // expect opposite orientations
		  assert(sminus!=splus);
		else if(theta>=M_PI/2.)     // both negative
		  assert(sminus==splus && sminus==FeatureOrientation::Negative);
		else if(theta<=-M_PI/2.)    // both positive
		  assert(sminus==splus && sminus==FeatureOrientation::Positive);
	      }
	  }
      }
    
    return; 
  }


  // Access the coordinates of a terminal
  const double* SplitFeatureSet::getTerminalCoordinates(const int t) const
  {
    assert(t>=0 && t<data.nTerminals);
    assert(data.terminal2Features[t][0]>=0 || data.terminal2Features[t][1]>=0);
    
    // identify a feature to which this corner belongs
    // return coordinates of this corner
    if(data.terminal2Features[t][0]>=0)
      return &data.gparams[data.terminal2Features[t][0]].rightCnr[0];
    else
      return &data.gparams[data.terminal2Features[t][1]].leftCnr[0];
  }
  
  

  // compute inter-corner distances
  void SplitFeatureSet::computeCornerDistances()
  {
    // aliases
    const int& nTerminals = data.nTerminals;
    const auto& terminal_to_feature_map = data.terminal2Features;
    const auto& gparams = data.gparams;
    
    // create an r-tree of corners
    // identify closest corner and save the distance
    boost_pointID_rtree rtree;
    for(int t=0; t<nTerminals; ++t)
      {
	const double* X = getTerminalCoordinates(t);
	rtree.insert({boost_point(X[0],X[1]),t});
      }

    closest_corner_distance.resize(nTerminals,0.);
    for(auto& it:rtree)
      {
	// this corner
	const auto& X = it.first;
	const int& tnum = it.second;

	// nearest distinct corner
	std::vector<boost_pointID> result{};
	rtree.query(boost::geometry::index::nearest(X,1) && 
		    boost::geometry::index::satisfies([tnum](boost_pointID const& v){return (v.second!=tnum);}),
		    std::back_inserter(result));
	const int nresults = static_cast<int>(result.size()); 
	assert(nresults==1 && result[0].second!=tnum);
	const auto& Y = result[0].first;
	closest_corner_distance[tnum] = boost::geometry::distance(X,Y);
      }
    
    // done
    return;
  }

  } // namespace um2::internal
} // namespace um2
