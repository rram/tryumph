// Sriramajayam

/** \file um2_FeatureSet_orient.cpp
 * \brief Implements calculations related to computing complementary feature sizes in class FeatureSet
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet.h>

namespace um2
{
  // Build an r-tree of all sample points
  void FeatureSet::createSampleTree()
  {
    global_rtree.clear();
    const int& nFeatures = data.nFeatures;
    const auto& gparams  = data.gparams;

    // collection of all sample points with curve ids
    // this has to be done for unsplit features since sample points for split features are repeated
    std::vector<boost_pointID> sample_points{};
    for(auto& gp:gparams)
      {
	// this feature's id
	const int& curve_id = gp.curve_id;

	// samples on this feature
	auto samples = gp.sample_feature();
	
	// insert sample points 
	for(auto& pt:samples)
	  sample_points.push_back(std::make_pair(boost_point(pt[0], pt[1]),curve_id));
      }

    // Create the global_rtree
    global_rtree.insert(sample_points.begin(), sample_points.end());
    return;
  }


  // Compute exclusion radii for corners
  void FeatureSet::computeCornerRadii()
  {
    const int& nFeatures = data.nFeatures;
    const auto& feature2Terminals = data.feature2Terminals;
    const auto& terminal2Features = data.terminal2Features;
    corner_radii.resize(nFeatures);
    
    // initialize radii to be min(local medial axis distance, 1/2 distance-to-other-segment-corner)
    for(int f=0; f<nFeatures; ++f)
      {
	const std::array<double,4> corners{data.gparams[f].leftCnr[0], data.gparams[f].leftCnr[1], data.gparams[f].rightCnr[0], data.gparams[f].rightCnr[1]};
	const auto& medialAxis = data.medialAxes[f];

	// distance to segment medial axis
	corner_radii[f].first  =  medialAxis->getDistanceToMedialAxis(corners.data()+0);
	corner_radii[f].second =  medialAxis->getDistanceToMedialAxis(corners.data()+2);
	
	// distance to other corner
	double cnr_dist = 0.5*std::sqrt( (corners[0]-corners[2])*(corners[0]-corners[2]) + (corners[1]-corners[3])*(corners[1]-corners[3]) ); // note the factor of 1/2
	if(cnr_dist<corner_radii[f].first)
	  {
	    corner_radii[f].first = cnr_dist;
	  }
	if(cnr_dist<corner_radii[f].second)
	  {
	    corner_radii[f].second = cnr_dist;
	  }
      }

    // compute the distance of each corner to features it does not belong to, update corner radius if found to be smaller
    for(int f=0; f<nFeatures; ++f)
      {
	const std::array<double,4> terminalPoints{data.gparams[f].leftCnr[0], data.gparams[f].leftCnr[1], data.gparams[f].rightCnr[0], data.gparams[f].rightCnr[1]};
	for(int t=0; t<2; ++t)         // 2 terminals per feature
	  {
	    // index of this terminal
	    const int& tindx = feature2Terminals[f][t];

	    // coordinates of this terminal
	    const double* X = terminalPoints.data()+2*t;
	      
	    //  features to which this terminal belongs
	    const int& f1 = terminal2Features[tindx][0];
	    const int& f2 = terminal2Features[tindx][1];
	    assert(f1==f || f2==f);
	    const int& curve_id1 = data.gparams[f1].curve_id;
	    const int& curve_id2 = data.gparams[f2].curve_id;
	  
	    // compute the distance of X from points in the r-tree while excluding features f1 & f2
	    std::vector<boost_pointID> result{};
	    global_rtree.query( boost::geometry::index::nearest(boost_point(X[0],X[1]),1) &&
			 boost::geometry::index::satisfies([curve_id1, curve_id2](boost_pointID const& v) {return (v.second!=curve_id1 && v.second!=curve_id2);}),
			 std::back_inserter(result) );
	    const int nresults = static_cast<int>(result.size()); // 0 if we have a closed component with 2 segments.
	    assert(nresults==0 || nresults==1);
	    if(nresults==1)
	      {
		assert(result[0].second!=curve_id1 && result[0].second!=curve_id2);
	      
		const double Y[2] = {result[0].first.get<0>(), result[0].first.get<1>()};
		const double dist = 0.5*std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1])); // note the factor of 1/2
	      
		// update radius
		if(t==0) // corner #0
		  {
		    if(dist < corner_radii[f].first)
		      corner_radii[f].first = dist;
		  }
		else // corner #1
		  {
		    if(dist < corner_radii[f].second)
		      corner_radii[f].second = dist;
		  }
	      }
	  }
      }
    
    // done
    return;
  }
  

  // Compute the complement feature size
  void FeatureSet::getComplementaryFeatureSize(const double* X, const int curve_id,
					       bool& is_computed, double& distance, double* cpt) const
  {
    // index of the curve
    auto it = data.curveid2IndexMap.find(curve_id);
    assert(it!=data.curveid2IndexMap.end());
    const int findx = it->second;
    
    // is this point within the exclusion zone of the feature it belongs to?
    const std::array<double,4> corners{data.gparams[findx].leftCnr[0], data.gparams[findx].leftCnr[1], data.gparams[findx].rightCnr[0], data.gparams[findx].rightCnr[1]};
    const double& r0 = corner_radii[findx].first;
    const double& r1 = corner_radii[findx].second;

    is_computed = true;
    if( (X[0]-corners[0])*(X[0]-corners[0]) + (X[1]-corners[1])*(X[1]-corners[1]) < r0*r0 ||
	(X[0]-corners[2])*(X[0]-corners[2]) + (X[1]-corners[3])*(X[1]-corners[3]) < r1*r1 )
      is_computed = false;
    
    // don't compute distance for points in exclusion zones
    if(is_computed==false)
      return;
    
    // otherwise, compute the distance to features distinct from "curve_id"
    std::vector<boost_pointID> result{};
    global_rtree.query( boost::geometry::index::nearest(boost_point(X[0],X[1]),1) &&
		 boost::geometry::index::satisfies([curve_id](boost_pointID const& v) {return v.second!=curve_id;}),
		 std::back_inserter(result));
    assert(static_cast<int>(result.size())==1 && result[0].second!=curve_id);
    cpt[0] = result[0].first.get<0>();
    cpt[1] = result[0].first.get<1>();
    distance = std::sqrt( (X[0]-cpt[0])*(X[0]-cpt[0]) + (X[1]-cpt[1])*(X[1]-cpt[1]) ); // no factor of 1/2 used here
    
    // done
    return;
  }
  
}
