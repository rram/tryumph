// Sriramajayam

/** \file um2_FeatureSet_Algorithms_enumerate.cpp 
 *
 * \brief Implements helper functionalities to enumerate distinct
 * corners of a set of features for use by the FeatureSet and
 * SplitFeatureSet classes.
 *
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {

    // Enumerate terminal vertices of all features
    void enumerateFeatureTerminals(const std::vector<FeatureParams>& gparams,
				   const double absTol,
				   int& nTerminals,
				   std::vector<std::array<int,2>>& feature2Terminals,
				   std::vector<std::array<int,2>>& terminal2Features)
    {
      const int nFeatures = static_cast<int>(gparams.size());
      feature2Terminals.resize(nFeatures, std::array<int,2>{-1,-1});
      
      // Populate an r-tree with all terminal vertices
      boost_pointID_rtree rtree;
      int indx = 0;
      boost_point pt;
      boost_point minCorner, maxCorner;
      
      for(int n=0; n<nFeatures; ++n)
	{
	  // This feature
	  const auto& geom = gparams[n];
	  const std::array<double,4> terminalPoints{geom.leftCnr[0], geom.leftCnr[1], geom.rightCnr[0], geom.rightCnr[1]};
	  
	  // Each feature has 2 terminal points
	  // Try inserting each corner into the r-tree
	  // If a coincident point is found, set the index for the vertex
	  // Otherwise, create a nu index
	  for(int t=0; t<2; ++t)
	    {
	      const double* X = &terminalPoints[2*t];
	      minCorner = boost_point(X[0]-absTol, X[1]-absTol);
	      maxCorner = boost_point(X[0]+absTol, X[1]+absTol);
	      
	      // Query box
	      boost_box query_box(minCorner, maxCorner);

	      // Identify all vertices within this box
	      std::vector<boost_pointID> query_result{};
	      rtree.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));
	      const int nQueries = static_cast<int>(query_result.size());

	      // Sanity check: tolerance cannot be too large
	      assert(nQueries==0 || nQueries==1);
	      
	      // Query box empty? Nu terminal vertex
	      if(nQueries==0)
		{
		  pt = boost_point(X[0], X[1]);
		  rtree.insert({pt, indx});
		  feature2Terminals[n][t] = indx;
		  ++indx;
		}
	      else // Not a nu vertex
		feature2Terminals[n][t] = query_result[0].second;
	    }
	}

      // Sanity check: all terminal vertices should have been assigned
      for(auto& tv:feature2Terminals)
	{ assert(tv[0]>=0 && tv[1]>=0); }
      
      nTerminals = indx;

      // Inverse of feature2Terminals
      // terminal2Features[n][0] = curve index for which 'n' is the left vertex
      // terminal2Features[n][1] = curve index for which 'n' is the right vertex
      terminal2Features.resize(nTerminals);
      for(auto& tf:terminal2Features)
	{ tf[0] = -1;
	  tf[1] = -1; }

      for(int n=0; n<nFeatures; ++n)
	for(int i=0; i<2; ++i)
	  {
	    const int& vert = feature2Terminals[n][i];
	    assert(terminal2Features[vert][(i+1)%2]==-1); // Ensures that at most 2 features connect
	    terminal2Features[vert][(i+1)%2] = n;
	  }

      // done
      return;
    }


    // Check enumeration of terminals
    void checkTerminals(const FeatureSetDetails& fsd, const double absTol)
    {
      const auto& gparams = fsd.gparams;
      const int nFeatures = fsd.nFeatures;
      assert(static_cast<int>(gparams.size())==nFeatures);
      const auto& f2T = fsd.feature2Terminals;
      const auto& t2F = fsd.terminal2Features;
      const auto& nTerminals = fsd.nTerminals;
      assert(static_cast<int>(f2T.size())==nFeatures);
      assert(static_cast<int>(t2F.size())==nTerminals);

      for(auto& tverts:f2T)
	for(auto& p:tverts)
	  { assert(p>=0 && p<nTerminals); }
  
      // Coordinates of terminals
      std::vector<double> tcoords(2*nTerminals);
      for(int f=0; f<nFeatures; ++f)
	{
	  const std::array<double,4> terminalPoints{gparams[f].leftCnr[0], gparams[f].leftCnr[1], gparams[f].rightCnr[0], gparams[f].rightCnr[1]};
	  for(int p=0; p<2; ++p)
	    {
	      const int& indx = f2T[f][p];
	      for(int k=0; k<2; ++k)
		tcoords[2*indx+k] = terminalPoints[2*p+k];
	    }
	}

      // Check accuracy of indexing
      for(int f=0; f<nFeatures; ++f)
	{
	  const std::array<double,4> terminalPoints{gparams[f].leftCnr[0], gparams[f].leftCnr[1], gparams[f].rightCnr[0], gparams[f].rightCnr[1]};
	  for(int p=0; p<2; ++p)
	    {
	      const double* X = &tcoords[2*f2T[f][p]];
	      const double* Y = &terminalPoints[2*p];
	      double dist = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      assert(dist<absTol);
	    }
	}

      // t2F should be the inverse of f2T, and vice versa
      for(int t=0; t<nTerminals; ++t)
	{
	  // feature to the left/right of t
	  const int& lf = t2F[t][0];
	  const int& rf = t2F[t][1];
	  if(lf!=-1) { assert(f2T[lf][1]==t); }
	  if(rf!=-1) { assert(f2T[rf][0]==t); }
	}

      for(int f=0; f<nFeatures; ++f)
	{
	  // Terminals to the left and right of f
	  const int& lt = f2T[f][0];
	  const int& rt = f2T[f][1];
	  assert(lt>=0 && lt<nTerminals);
	  assert(rt>=0 && rt<nTerminals);
	  assert(t2F[lt][1]==f && t2F[rt][0]==f);
	}
      
      // done
      return;
    }
    
  } // namespace um2::internal
} // namespace um2
