// Sriramajayam

/** \file um2_FeatureMedialAxis.cpp
 *
 * \brief Implementation of the class FeatureMedialAxis
 *
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureMedialAxis.h>
#include <fstream>

namespace um2 {
  namespace internal {
    
  // Constructor
  FeatureMedialAxis::FeatureMedialAxis(const std::list<FeatureSample>& fSamples,
				       const MedialAxisParams& mparams)
  {
    validate_input(mparams);
    
    // Compute the medial axis
    sampleTree.clear();
    mSamples.clear();
    innerTree.clear();
    outerTree.clear();

    // Compute the bounding box size for the samples. Use this to scale non dimensional medial axis parameters
    double xlim[] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest()};
    double ylim[] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest()};
    for(auto& fSample:fSamples)
      {
	const auto& X = std::get<1>(fSample);
	const double& x = X[0];
	const double& y = X[1];

	if(x<xlim[0]) xlim[0] = x;
	if(x>xlim[1]) xlim[1] = x;
	  
	if(y<ylim[0]) ylim[0] = y;
	if(y>ylim[1]) ylim[1] = y;
      }
    double hx = xlim[1]-xlim[0];
    double hy = ylim[1]-ylim[0];
    const double lenscale = (hx>hy) ? hx : hy;

    // Populate the sample tree
    createSampleTree(fSamples, lenscale*mparams.exclusion_radius, mSamples, sampleTree);

    // Compute the medial axis: populates the medial balls for sample points
    computeMedialAxisTransform(lenscale*mparams.eps_radius, lenscale*mparams.maximum_radius, sampleTree, mSamples);

    // Populate the medial axis trees
    boost_point pt;
    for(auto& mSample:mSamples)
      {
	// outer tree
	pt = boost_point(mSample.outer_ball.center[0], mSample.outer_ball.center[1]);
	outerTree.insert(pt);

	// Populate the tree of inner medial ball centers
	pt = boost_point(mSample.inner_ball.center[0], mSample.inner_ball.center[1]);
	innerTree.insert(pt);
      }

    // done
    return;
  }

    
  // Populates the sample tree
  void FeatureMedialAxis::createSampleTree(const std::list<FeatureSample>& fSamples,
					   const double exclusion_radius,
					   std::list<FeatureMedialAxis::MedialSample>& msamples,
					   boost_pointID_rtree& sample_tree)
  {
    msamples.clear();
      
    // temporaries
    boost_pointID pid;
    boost_point minCorner, maxCorner;
    const int nSamples = static_cast<int>(fSamples.size());
    assert(nSamples>0);

    // insert points sequentially
    MedialSample mSample;
    for(auto& fSample:fSamples)
      {
	// transfer fSample -> mSample
	mSample.index = std::get<0>(fSample);
	for(int k=0; k<SPD; ++k)
	  {
	    mSample.Point[k]  = std::get<1>(fSample)[k];
	    mSample.Normal[k] = std::get<2>(fSample)[k];
	  }

	// This point
	const double* X = mSample.Point;
	
	// Exclusion zone around this point
	minCorner = boost_point(X[0]-exclusion_radius, X[1]-exclusion_radius);
	maxCorner = boost_point(X[0]+exclusion_radius, X[1]+exclusion_radius);
	boost_box query_box(minCorner, maxCorner);
	
	// Is this point well separated from samples already in this tree?
	std::vector<boost_pointID> query_result{};
	sample_tree.query(boost::geometry::index::within(query_box), std::back_inserter(query_result));

	// If the box is empty, include this point in the sampleTree
	if(query_result.empty()) 	
	  {
	    pid.first = boost_point(X[0], X[1]);
	    pid.second = mSample.index;
	    sample_tree.insert(pid);
	    msamples.push_back(mSample);
	  }
      }

    // done
    return;
  }
  

  // Compute the medial axis transform for points in the sample tree
  void FeatureMedialAxis::computeMedialAxisTransform(const double eps_radius, const double max_radius,
						     const boost_pointID_rtree& sample_tree,
						     std::list<MedialSample>& msamples)
  {
    // Temporaries
    double dot = 0.;
    double normPQ = 0.;
    const double signs[] = {1., -1.}; // outward, inward orientations
    double new_rad;
    boost_point center;
    double Q[2];

    // Compute the medial axis for points included in the rtree
    for(auto& mSample:msamples)
      {
	// This point
	const double* P = mSample.Point;
	const double* N = mSample.Normal;
	double rads[] = {max_radius, max_radius};
	int limiting_indx[2] = {-1,-1};
	    
	// Outer & inner radii
	for(int orientation=0; orientation<2; ++orientation)
	  {
	    double& rad = rads[orientation];
	    const double& sign = signs[orientation];
		
	    while(true)
	      {
		// Guess for the center
		for(int k=0; k<2; ++k)
		  Q[k] = P[k]+sign*rad*N[k];
		center = boost_point(Q[0], Q[1]);
		    
		// Find the closest sample point to the center
		std::vector<boost_pointID> query_result{};
		sample_tree.query(boost::geometry::index::nearest(center, 1), std::back_inserter(query_result));
		assert(static_cast<int>(query_result.size())==1);
		    
		// Closest point sample to Q
		auto& qid = query_result[0];
		limiting_indx[orientation] = qid.second;
		    
		// If Q = P, stop.
		if(qid.second==mSample.index)
		  break;
		  
		// Compute new radius
		Q[0] = qid.first.get<0>();
		Q[1] = qid.first.get<1>();
		normPQ = 0.;
		dot = 0.;
		for(int k=0; k<2; ++k)
		  {
		    normPQ += (P[k]-Q[k])*(P[k]-Q[k]);
		    dot += N[k]*(P[k]-Q[k]);
		  }
		normPQ = std::sqrt(normPQ);
		assert(std::abs(dot)>eps_radius*normPQ); 
		assert(normPQ>eps_radius);
		dot /= normPQ;
		new_rad = -0.5*sign*normPQ/dot;
		  
		// radius should be monotonically decreasing
		assert(new_rad<rad+eps_radius);
		  
		// converged?
		if(std::abs(new_rad-rad)<eps_radius)
		  break;
		  
		// otherwise, continue
		rad = new_rad;
	      }
	  }
	    
	// Assign the inner and outer medial ball radii, centers, limiting point
	mSample.outer_ball.radius = rads[0];
	mSample.inner_ball.radius = rads[1];
	for(int k=0; k<2; ++k)
	  {
	    mSample.outer_ball.center[k] = P[k]+mSample.outer_ball.radius*N[k];
	    mSample.inner_ball.center[k] = P[k]-mSample.inner_ball.radius*N[k];
	  }
	mSample.outer_ball.limiting_point_index = limiting_indx[0];
	mSample.inner_ball.limiting_point_index = limiting_indx[1];

      } // Proceed to the next sample point
    
    // done
    return;
  }


  // Get the distances of a point from the medial axes
  void FeatureMedialAxis::getDistanceToMedialAxis(const double* pt,
						  double& out_dist, double* out_X,
						  double& in_dist, double* in_X) const
  {
    boost_point bp(pt[0], pt[1]);
    std::vector<boost_point> out_result{}, in_result{};
    outerTree.query(boost::geometry::index::nearest(bp, 1), std::back_inserter(out_result));
    innerTree.query(boost::geometry::index::nearest(bp, 1), std::back_inserter(in_result));
    assert(static_cast<int>(out_result.size())==1 && static_cast<int>(in_result.size())==1);

    out_X[0] = out_result[0].get<0>();
    out_X[1] = out_result[0].get<1>();
    out_dist = boost::geometry::distance(bp, out_result[0]);
    
    in_X[0]  = in_result[0].get<0>();
    in_X[1]  = in_result[0].get<1>();
    in_dist = boost::geometry::distance(bp, in_result[0]);

    // done
    return;
  }

  // Get the distance of a point from the medial axes
  double FeatureMedialAxis::getDistanceToMedialAxis(const double* pt) const
  {
    double out_dist, in_dist;
    double out_X[2], in_X[2];
    getDistanceToMedialAxis(pt, out_dist, out_X, in_dist, in_X);
    return (out_dist<in_dist) ? out_dist : in_dist;
  }
    

  // Access the medial axis samples
  std::list<std::array<double,2>> FeatureMedialAxis::getInnerMedialAxisSamples() const
  {
    std::list<std::array<double,2>> in_centers{};
    for(auto& it:mSamples)
      {
	const double* X = it.inner_ball.center;
	in_centers.push_back({X[0],X[1]});
      }
    return in_centers;
  }

  std::list<std::array<double,2>> FeatureMedialAxis::getOuterMedialAxisSamples() const
  {
    std::list<std::array<double,2>> out_centers{};
    for(auto& it:mSamples)
      {
	const double* X = it.outer_ball.center;
	out_centers.push_back({X[0],X[1]});
      }
    return out_centers;
  }

  // Overload extraction
  std::ostream& operator << (std::ostream& out, const FeatureMedialAxis& medial)
  {
    assert(out.good());

    out<<"#X, Y, inner.centerX, inner.centerY, outer.centerX, outer.centerY";
    for(auto& mSample:medial.mSamples)
      {
	out<< std::endl
	   << mSample.Point[0] << ", " << mSample.Point[1] << ", "
	   << mSample.inner_ball.center[0] << ", " << mSample.inner_ball.center[1] << ", "
	   << mSample.outer_ball.center[0] << ", " << mSample.outer_ball.center[1] << std::flush;
      }
    
    return out;
  }

  } // namespace um2::internal
} // namespace um2
