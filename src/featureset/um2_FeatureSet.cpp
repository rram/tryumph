// Sriramajayam

/** \file um2_FeatureSet.cpp
 * \brief Implements the class FeatureSet
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet.h>
#include <cmath>

namespace um2
{
  // Constructor
  FeatureSet::FeatureSet(const std::vector<FeatureParams>& fpvec,
			 const double cornerTol,
			 const MedialAxisParams& mparams)
    :corner_tolerance(cornerTol),
     medial_axis_params(mparams),
     data(),
     corner_radii{}
  {
    validate_input(mparams);
    for(auto& fp:fpvec)
      {
	validate_input(fp);
	
	// check distance between terminals
	const double* A = fp.leftCnr.data();
	const double* B = fp.rightCnr.data();
	double dAB = std::sqrt((A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]));
	assert(dAB>2.*cornerTol);

	// append feature
	auto result = data.curveid2IndexMap.insert({fp.curve_id, data.nFeatures});
	assert(result.second==true);
	data.gparams.push_back(fp);
	++data.nFeatures;
      }
    assert(data.nFeatures==static_cast<int>(data.gparams.size()));
    
    // enumerate feature corners
    internal::enumerateFeatureTerminals(data.gparams, cornerTol, // in
				    data.nTerminals, data.feature2Terminals, data.terminal2Features); // out
    assert(data.nTerminals>0);
    internal::checkTerminals(data, cornerTol);

    // identify connected components
    internal::connectFeatures(data.gparams, data.feature2Terminals, data.terminal2Features, // in 
			  data.connectedComponents); // out
    internal::checkConnectedComponents(data);
    
    // Closed components
    internal::closedComponents(data.connectedComponents, // in
			   data.closedComponents); // out
    
    // Compute feature orientations
    computeOrientations();

    // Split features
    splitFeatures();
       
    // Compute the medial axis 
    internal::computeMedialAxes(data, mparams);
    
    // Build an r-tree of all sample points of unsplit features
    createSampleTree();
    
    // Compute local feature sizes at corners of unsplit features
    computeCornerRadii();

    // done
    return;
  }


  // Helper for generating parameters of split features
  std::pair<FeatureParams, FeatureParams> splitFeatureGenerator(const int left_id, const int right_id,
								  const FeatureParams& parent) {
    FeatureParams left, right;

    // left
    left.curve_id  = left_id;
    left.leftCnr = parent.leftCnr;
    left.rightCnr = parent.intPoint;
    left.sample_feature  = parent.sample_feature;
    left.signed_distance_function = parent.signed_distance_function;
    
    // right
    right.curve_id = right_id;
    right.leftCnr = parent.intPoint;
    right.rightCnr = parent.rightCnr;
    right.sample_feature = parent.sample_feature;
    right.signed_distance_function = parent.signed_distance_function;
	  
    return {left,right};
  }

  
  // Split features and keep track of curve ids
  void FeatureSet::splitFeatures()
  {
    // Available curve ids
    std::set<int> curve_ids{};
    for(auto& it:data.gparams)
      curve_ids.insert(it.curve_id);
    assert(static_cast<int>(curve_ids.size())==data.nFeatures);

    int id_count = *(curve_ids.rbegin())+1;

    // map from split ids -> parent id
    std::map<int,int> split_to_parent_id_map{};
    
    // Loop over features
    // If a split orientation is encountered, split it and make a note of the indices
    std::vector<FeatureParams> split_gparams{};
    std::vector<FeatureOrientation> split_orientations{};
    int nsplit_Features = 0;
    for(int f=0; f<data.nFeatures; ++f)
      if(data.orientations[f]==FeatureOrientation::Positive || data.orientations[f]==FeatureOrientation::Negative)
	{
	  split_gparams.push_back(data.gparams[f]);
	  split_orientations.push_back(data.orientations[f] );
	  auto result = split_to_parent_id_map.insert({data.gparams[f].curve_id, data.gparams[f].curve_id});
	  assert(result.second==true);
	  ++nsplit_Features;
	}
      else // split feature
	{
	  const auto& parent_param = data.gparams[f];
	  
	  // split this feature
	  auto split_params =  splitFeatureGenerator(id_count, id_count+1, parent_param);
	  validate_input(split_params.first);
	  validate_input(split_params.second);
	  split_gparams.push_back( split_params.first );
	  split_gparams.push_back( split_params.second );

	  // orientations of left and right splits
	  if(data.orientations[f]==FeatureOrientation::Split_Negative2Positive)
	    {
	      split_orientations.push_back(FeatureOrientation::Negative);
	      split_orientations.push_back(FeatureOrientation::Positive);
	    }
	  else // Split_Positive2Negative
	    {
	      split_orientations.push_back(FeatureOrientation::Positive);
	      split_orientations.push_back(FeatureOrientation::Negative);
	    }

	  auto result = split_to_parent_id_map.insert({id_count, parent_param.curve_id});
	  assert(result.second==true);
	  result = split_to_parent_id_map.insert({id_count+1, parent_param.curve_id});
	  assert(result.second==true);
	  
	  // update number of features & id counter
	  nsplit_Features += 2;
	  id_count += 2;
	}

    assert(nsplit_Features==static_cast<int>(split_orientations.size()));
    assert(nsplit_Features==static_cast<int>(split_gparams.size()));
    assert(nsplit_Features==static_cast<int>(split_to_parent_id_map.size()));
    assert(nsplit_Features>=data.nFeatures);
    
    // create split feature set
    split_fset = std::make_unique<internal::SplitFeatureSet>(split_gparams, split_to_parent_id_map,
							     split_orientations, corner_tolerance, medial_axis_params);
    
    // Sanity check
    assert(split_fset->getConnectedComponents().size()==data.connectedComponents.size());
    
    // done
    return;
  }
  
}
