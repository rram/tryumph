// Sriramajayam

/** \file um2_FeatureMedialAxis.h
 *
 * \brief Definition of the class FeatureMedialAxis
 *
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <um2_Input.h>
#include <um2_BoostAliases.h>

#include <utility>
#include <list>

namespace um2 {
  namespace internal {
    using FeatureSample = std::tuple<int, std::array<double,2>, std::array<double,2>>; //!< triplet of sample point index, Cartesian coordinates, and the normal

    /** \brief Class implementing the shrinking ball algorithm to
     * estimate the medial axis of a feature using discrete data
     * samples.
     * 
     * The class in fact computes the inner and outer medial axies, with
     * the orientation deduced from the normal field.
     */
    class FeatureMedialAxis {     
    public:
      /** \brief Constructor. Invokes the shrinking-ball algorithm.
       * \param[in] mSamples List of discrete samples of the feature. 
       * \param[in] params Algorithmic parameters.
       * \sa MedialAxisParams
       * ingroup features
       */
      FeatureMedialAxis(const std::list<FeatureSample>& mSamples,
			const MedialAxisParams& params);
    
      // Destructor
      ~FeatureMedialAxis() = default;

      // Disable copy and assignment
      FeatureMedialAxis(const FeatureMedialAxis&) = delete;
      FeatureMedialAxis operator=(const FeatureMedialAxis&) = delete;

      /** \brief Compute the distance of a point to the inner/outer medial axis.
       * 
       * \param[in] pt Cartesian coordinates of point 
       * \param[out] out_dist Distance to the outer medial axis
       * \param[out] in_dist Distance to the inner medial axis
       * \param[out] out_X Point on the outer medial axis closest to pt
       * \param[out] out_X Point on the inner medial axis closest to pt
       */
      void getDistanceToMedialAxis(const double* pt,
				   double& out_dist, double* out_X,
				   double& in_dist,  double* in_X) const;

      /** \brief Compute the distance of a point to the medial axis.
       * 
       * \param[in] pt Cartesian coordinates of point 
       * \return Closest distance between pt and points on the inner and outer medial axes.
       */
      double getDistanceToMedialAxis(const double* pt) const;

      //! \brief Access the points sampling the inner medial axis. Helpful for visualization.
      std::list<std::array<double,2>> getInnerMedialAxisSamples() const;

      //! \brief Access the points sampling the inner medial axis. Helpful for visualization.
      std::list<std::array<double,2>> getOuterMedialAxisSamples() const;
      
    private:
      /** \brief Center and radius of a medial ball. Additionally, the
       * struct records the index of the sample point on the feature
       * that determining the location and extent of the medial ball.
       */
      struct MedialBall   {
	double center[2];         //!< Center of the medial ball
	double radius;            //!< Radius of the medial ball
	int limiting_point_index; //!< index of sample point determining the medial ball
      };

      /** \brief Location, index, normal, inner medial ball and outer
       * medial ball at a sample point along a feature.
       */
      struct MedialSample   {
	double Point[2];  //!< Coordinates of the sample point
	double Normal[2]; //!< Unit outward pointing normal 
	int    index;     //!< Index of sample point as defined in the feature sample
      
	// Feature-level calculation
	MedialBall inner_ball;   //!< Computed medial ball centered along the negative normal
	MedialBall outer_ball;   //!< Computed medial ball centered along the positive normal
      };
    
      /** \brief Helper function to populate the r-tree of sampling
       * points and to initialize medial samples along a
       * feature. Invoked by the constructor.
       *
       * \param[in] fSamples Sample points along a feature
       * \param[in] exclusion_radius Absolute radius to use for separating sample points
       * \param[out] msamples At exit, the sample point info is populated
       * \param[out] sample_tree R-tree of feature samples
       */
      static void createSampleTree(const std::list<FeatureSample>& fSamples,
				   const double exclusion_radius,
				   std::list<FeatureMedialAxis::MedialSample>& msamples,
				   boost_pointID_rtree& sample_tree);
      
      /** \brief Helper function implementing the shinking ball algorithm.
       * 
       * \param[in] eps_radius Absolute value of the tolerance to use for convergence of medial ball radii in the algorithm.
       *
       * \param[in] max_radius Absolute value for the upper bound on the medial all radii.
       *
       * \param[in] sample_tree R-tree containing feature samples.
       *
       * \param[in,out] msamples At input, contains feature samples. On
       * output, augmented with medial ball calculations.
       */
      static void computeMedialAxisTransform(const double eps_radius, const double max_radius,
					     const boost_pointID_rtree& sample_tree,
					     std::list<MedialSample>& msamples);
    
    
      static constexpr int    SPD = 2;  //!< Spatial dimension
      boost_pointID_rtree   sampleTree; //!< R-trees with sample points on the feature
      std::list<MedialSample> mSamples; //!< Flattening of sample and medial axis data along the curve
      boost_point_rtree     innerTree;  //!< R-tree of inner medial ball centers
      boost_point_rtree     outerTree;  //!< R-tree of outer medial ball centers

      /** \brief Overload extraction, helpful for file
       * output/visualization.  Format: X, Y, inner.centerX,
       * inner.centerY, outer.centerX, outer.centerY.
       * 
       * \param[in,out] Output stream (e.g., file or cout)
       *
       * \param[in] medial Medial axis instance
       */
      friend std::ostream& operator << (std::ostream& out, const FeatureMedialAxis& medial);
    };

  } // namespace um2::internal
} // namespace um2

