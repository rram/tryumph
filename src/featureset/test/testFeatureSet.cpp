// Sriramajayam

#include <iostream>
#include <string>
#include <um2_utils_FeatureFactory.h>

int main()
{
  // Read the list of test cases
  std::fstream pfile;
  pfile.open((char*)"geom-data/geom-list.txt", std::ios::in);
  assert(pfile.good());
  std::vector<std::string> geom_list{};
  std::string gname;
  pfile >> gname;
  while(pfile.good())
    {
      geom_list.push_back(gname);
      pfile >> gname;
    }
  pfile.close();

  for(auto& gfile:geom_list)
    {
      std::cout<<"\nProcessing geometry: "<<gfile << std::flush;
      
      // Nonlinear solver parameters
      um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
      // Medial axis calc parameters
      um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

      // Number of samples per feature
      const int nSamples = 50;
      
      // corner tolerance
      const double cornerTol = 1.e-4;

       // Read features
       std::string fname = "geom-data/" + gfile;
       um2::utils::FeatureFactory gdata(fname, nSamples, cornerTol, mas_params, nlparams);
       
       // Access the feature set
       const auto& fset = gdata.getFeatureSet();
    }
}
