// Sriramajayam

#include <um2_FeatureMedialAxis.h>
#include <um2_utils_CubicSpline.h>
#include <fstream>

using namespace um2;
using namespace um2::internal;

int main()
{
  // Ellipse
  {
    const int ellipse_curve_id = 0;
    MedialAxisParams mParams{.exclusion_radius=1.e-3, .maximum_radius=10., .eps_radius=1.e-5};

    const double A = 10.;
    const double B = 5.;
    const double nSamples = 50;
    std::list<FeatureSample> fSamples;
    fSamples.clear();
    FeatureSample fSample;
    for(int n=0; n<nSamples; ++n)
      {
	const double theta = 2.*M_PI*static_cast<double>(n)/static_cast<double>(nSamples);
	std::get<0>(fSample) = n;
	auto& Pt = std::get<1>(fSample);
	Pt[0] = A*std::cos(theta);
	Pt[1] = B*std::sin(theta);
	auto& N = std::get<2>(fSample);
	N[0] = B*std::cos(theta)/std::sqrt(A*A+B*B);
	N[1] = A*std::sin(theta)/std::sqrt(A*A+B*B);
	fSamples.push_back(fSample);
      }
    
    FeatureMedialAxis maxis(fSamples, mParams);
    
    // Visualize
    std::fstream pfile;
    pfile.open("ellipse.csv", std::ios::out);
    pfile << maxis;
    pfile.close();
  }
  
  // Farm animals with sample points from a spline
  {
    std::fstream pfile;
    pfile.open("geom-data/farm748.txt", std::ios::in);
    assert(pfile.good());
    std::vector<double> xy{};
    double val[2];
    pfile >> val[0];
    while(pfile.good())
      {
	pfile >> val[1];
	xy.push_back(val[0]/100.);
	xy.push_back(val[1]/100.);
	pfile >> val[0];
      }
    pfile.close();
    const int nPoints = static_cast<int>(xy.size()/2);
    std::vector<double> tvec(nPoints);
    const double dt = 1./(nPoints-1);
    tvec[0] = 0.;
    for(int i=1; i<nPoints; ++i)
      tvec[i] = tvec[i-1] + dt;


    // spline
    const int spline_curve_id = 1;
    utils::RootFindingParams nlParams{.digits=5, .max_iter=20, .normTol=1.e-4};
    utils::CubicSpline spline(spline_curve_id, tvec, xy, 5000, nlParams);

    // sample points
    auto samples = spline.GetSampling();
    std::list<FeatureSample> fSamples{};
    FeatureSample fs;
    auto& fs_indx = std::get<0>(fs);
    auto& fs_pt   = std::get<1>(fs);
    auto& fs_nvec = std::get<2>(fs);
    int indx = 0;
    double sd;
    for(auto& pt:samples)
      {
	fs_indx = indx++;
	fs_pt   = pt;
	spline.GetSignedDistance(&fs_pt[0], sd, &fs_nvec[0]);
	fSamples.push_back(fs);
      }
    
    // medial axis
    MedialAxisParams mParams{.exclusion_radius=1.e-3, .maximum_radius=10., .eps_radius=1.e-5};
    FeatureMedialAxis maxis(fSamples, mParams);

    // Visualize
    pfile.open("farm-medial.csv", std::ios::out);
    pfile << maxis;
    pfile.close();
  }

  // done
}
