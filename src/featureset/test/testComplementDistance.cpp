// Sriramajayam

#include <iostream>
#include <string>
#include <um2_utils_FeatureFactory.h>

int main()
{
  // Nonlinear solver parameters
  um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=25, .normTol=1.e-4};
  
  // Medial axis calc parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // # samples
  const int nSamples = 50;

  // corner tolerance
  const double cornerTol = 1.e-4;
  
  // Read features
  std::string fname = "geom-data/201636.fset";
  um2::utils::FeatureFactory gdata(fname, nSamples, cornerTol, mas_params, nlparams);

  // access the feature set
  const auto& fset = gdata.getFeatureSet();
  
  // access features
  const int nFeatures = fset.getNumFeatures();
  const auto& gparams = fset.getFeatureParams();
  
  // Write complementary distance data to file
  std::fstream pfile;
  pfile.open("global.dat", std::ios::out);
  assert(pfile.good());
  double cpt[2];
  double distance;
  bool is_computed;
  for(auto& gp:gparams)
    {
      auto samples = gp.sample_feature();
      for(auto& it:samples)
	{
	  double X[] = {it[0], it[1]};
	  fset.getComplementaryFeatureSize(X, gp.curve_id, is_computed, distance, cpt);
	  if(is_computed)
	    pfile << X[0] <<" "<<X[1]<< std::endl << cpt[0] <<" " <<cpt[1] << std::endl << std::endl;
	  else
	    pfile << X[0] <<" "<<X[1] << std::endl << std::endl;
		
	}
    }
  pfile.close();
  
  // write corners and the exclusion radii to a file
  pfile.open("corners.dat", std::ios::out);
  assert(pfile.good());
  for(int f=0; f<nFeatures; ++f)
    {
      const auto& gp = gparams[f];
      const auto radii = fset.getCornerExclusionRadii(gp.curve_id);
      pfile << gp.leftCnr[0]  << " " << gp.leftCnr[1]  << " " << radii.first  << std::endl
	    << gp.rightCnr[0] << " " << gp.rightCnr[1] << " " << radii.second << std::endl;
    }
  pfile.close();


  // write mesh size estimate to file
  pfile.open("meshsize.dat", std::ios::out);
  assert(pfile.good());
  for(auto& gp:gparams)
    {
      auto samples = gp.sample_feature();
      for(auto& it:samples)
	{
	  double X[] = {it[0], it[1]};
	  double hval = fset.getFeatureSize(X, gp.curve_id);
	  pfile << X[0] <<" "<<X[1] <<" "<<hval<< std::endl;
	}
    }
  pfile.close();
  
  // done
}
