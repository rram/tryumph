// Sriramajayam

#include <um2_FeatureSet.h>
#include <um2_utils_LineSegment.h>
#include <cmath>
#include <iostream>
#include <random>


// Create geometries and features
void Create(um2::utils::RootFindingParams& nlparams, 
	    std::vector<std::unique_ptr<um2::utils::LineSegment>>& features,
	    const bool is_closed);
	    
	    
// Test correctness of orientation assignment
//void CheckOrientations(std::vector<std::unique_ptr<um2::LineSegment>>& features, const um2::FeatureSet& fset);

int main()
{
  // solver parameters
  um2::utils::RootFindingParams nlparams{.digits=5, .max_iter=15, .normTol=1.e-3};

  // Medial axis parameters
  um2::MedialAxisParams mas_params{.exclusion_radius=1.e-4, .maximum_radius=5., .eps_radius=1.e-3};

  // corner tolerance
  const double cornerTol = 1.e-4;
  // trials
  for(int run=0; run<100; ++run)
    {
      // Create geometries and features
      std::vector<std::unique_ptr<um2::utils::LineSegment>> features{};
      bool is_closed = (run%2==0) ? true : false;
      Create(nlparams, features, is_closed);

      // Create a feature set
      std::vector<um2::FeatureParams> gparams{};
      for(auto& it:features)
	{
	  gparams.push_back(it->GetFeatureParams());
	}
      
      um2::FeatureSet fset(gparams, cornerTol, mas_params);
      
      // Test correctness
      //CheckOrientations(features, fset);
    }

  // done
}


// Create geometries and features
void Create(um2::utils::RootFindingParams& nlparams, 
	    std::vector<std::unique_ptr<um2::utils::LineSegment>>& features,
	    const bool is_closed)
{
  features.clear();

  // Random number generators
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> numdis(3, 15);
  std::uniform_real_distribution<> ptdis(-1.,1.);

  // How many points?
  const int nPoints = numdis(gen);
  std::vector<double> coords{};
  for(int p=0; p<nPoints; ++p)
    { coords.push_back(ptdis(gen));
      coords.push_back(ptdis(gen)); }
  
  // Randomy shuffle order of segments
  std::vector<int> indxlist{};
  for(int s=0; s<nPoints-1; ++s)
    indxlist.push_back(s);
  if(is_closed) indxlist.push_back(nPoints-1);
  std::shuffle(indxlist.begin(), indxlist.end(), gen);
  const int nSegments = static_cast<int>(indxlist.size());
  
  // Create features
  for(auto& s:indxlist)
    {
      int a = s;
      int b = (s+1)%nPoints;
      features.push_back(std::make_unique<um2::utils::LineSegment>(s, &coords[2*a], &coords[2*b], 10, nlparams));
    }
 
  // done
}


/*
// Test correctness of orientation assignment
void CheckOrientations(std::vector<std::unique_ptr<um2::LineSegment>>& features,
		       const um2::FeatureSet& fset)
{
  const auto& connFeatures = fset.GetConnectedComponents();
  const auto& orientations = fset.GetFeatureOrientations();
  
  // Check connectedness
  assert(static_cast<int>(connFeatures.size())==1);
  const int nSegments = static_cast<int>(connFeatures[0].size());
  if(connFeatures[0].front()==connFeatures[0].back())
    assert(static_cast<int>(features.size()+1)==nSegments);
  else
    assert(static_cast<int>(features.size())==nSegments);
  
  std::set<int> curve_IDs{};
  for(auto& s:connFeatures[0])
    curve_IDs.insert(features[s]->GetCurveID());
  assert(curve_IDs.size()==features.size());

  // Check orientations
  for(auto& connList:connFeatures)
    {
      std::vector<int> segNums(connList.begin(), connList.end());
      assert(static_cast<int>(segNums.size())==nSegments);

      // Closed?
      if(connList.front()==connList.back())
	{ assert(orientations[connList.front()]==orientations[connList.back()]); }
	  
      // Visit each corner and check the required
      for(int s=1; s<nSegments; ++s)
	{
	  // tminus
	  double tpoints[4];
	  features[segNums[s-1]]->GetTerminalPoints(tpoints);
	  double tprev[2] = {tpoints[2]-tpoints[0], tpoints[3]-tpoints[1]};
	  double norm = std::sqrt(tprev[0]*tprev[0]+tprev[1]*tprev[1]);
	  tprev[0] /= norm; tprev[1] /= norm;

	  // tplus
	  features[segNums[s]]->GetTerminalPoints(tpoints);
	  double tnext[2] = {tpoints[2]-tpoints[0], tpoints[3]-tpoints[1]};
	  norm = std::sqrt(tnext[0]*tnext[0]+tnext[1]*tnext[1]);
	  tnext[0] /= norm; tnext[1] /= norm;

	  double angle = std::atan2(tprev[0]*tnext[1]-tprev[1]*tnext[0], tprev[0]*tnext[0]+tprev[1]*tnext[1]);

	  const auto& prev = orientations[segNums[s-1]];
	  const auto& next = orientations[segNums[s]];
	  
	  // Check
	  if(std::abs(angle)<M_PI/2.)
	    {
	      // s-1 and s should have opposite orientations
	      if(prev==um2::FeatureOrientation::Negative || prev==um2::FeatureOrientation::Split_Positive2Negative)
		assert(next==um2::FeatureOrientation::Positive || next==um2::FeatureOrientation::Split_Positive2Negative);
	      else
		assert(next==um2::FeatureOrientation::Negative || next==um2::FeatureOrientation::Split_Negative2Positive);
	    }
	  else if(angle>=M_PI/2.) 
	    {
	      // s-1 and s should both be negative at the corner
	      assert(prev==um2::FeatureOrientation::Negative || prev==um2::FeatureOrientation::Split_Positive2Negative);
	      assert(next==um2::FeatureOrientation::Negative || next==um2::FeatureOrientation::Split_Negative2Positive);
	    }
	  else // angle < -MPI/2
	    {
	      // s-1 and s should both be positive at the corner
	      assert(prev==um2::FeatureOrientation::Positive || prev==um2::FeatureOrientation::Split_Negative2Positive);
	      assert(next==um2::FeatureOrientation::Positive || next==um2::FeatureOrientation::Split_Positive2Negative);
	    }
	}
    }

  // done
  return;
}
*/
