// Sriramajayam

/** \file um2_FeatureSet_Algorithms_connect.cpp 
 *
 * \brief Implements helper functionalities to identify connected
 * connected components of features for use by the FeatureSet and
 * SplitFeatureSet classes.
 *
 * \author Ramsharan Rangarajan
 */
#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {

    // Helper functions/structures
    // Connect features at the terminal vertices
    void connectFeatures(const std::vector<FeatureParams>& gparams,
			 const std::vector<std::array<int,2>>& feature2Terminals,
			 const std::vector<std::array<int,2>>& terminal2Features,
			 std::vector<std::list<int>>& connectedComponents)
    {
      // Make connections
      connectedComponents.clear();

      const int nFeatures = static_cast<int>(gparams.size());
      std::vector<bool> is_feature_inspected(nFeatures, false);
      while(true)
	{
	  // Create a nu connected component
	  std::list<int> component{};
	  
	  // Identify an uninspected feature
	  int seed_feature = -1;
	  for(int n=0; n<nFeatures && seed_feature==-1; ++n)
	    if(is_feature_inspected[n]==false)
	      seed_feature = n;

	  // No nu component? We are done
	  if(seed_feature==-1) break;
	  
	  // Grow this component by looking right from seed_feature
	  int next_feature = seed_feature;
	  while(next_feature!=-1 && is_feature_inspected[next_feature]==false)
	    {
	      // Append this feature & note that it has been inspected
	      component.push_back( next_feature );
	      is_feature_inspected[next_feature] = true;

	      // Right terminal vertex
	      int vert = feature2Terminals[next_feature][1];

	      // this feature is to the left of 'vert'
	      // next feature is to the right of 'vert'
	      assert(terminal2Features[vert][0]==next_feature);
	      next_feature = terminal2Features[vert][1];
	    }

	  // Grow this component by looking to the left from
	  // seed_feature Note that 'seed_feature' has already been
	  // added
	  next_feature = seed_feature;
	  do 
	    {
	      is_feature_inspected[next_feature] = true;
	      
	      // Left terminal vertex 
	      int vert = feature2Terminals[next_feature][0];

	      // feature to the left of 'vert'
	      assert(terminal2Features[vert][1]==next_feature);
	      next_feature = terminal2Features[vert][0];

	      // Append this feature and note that it has been inspected
	      if(next_feature!=-1)
		component.push_front(next_feature);
	      
	    } while(next_feature!=-1 && is_feature_inspected[next_feature]==false);

	  // This connected component is complete
	  connectedComponents.push_back(component);

	  // Proceed to the next feature
	}

      // Sanity checks
      
      // Each feature should appear once
      std::vector<int> is_featured(nFeatures, 0);
      const int nComponents = static_cast<int>(connectedComponents.size());
      for(int n=0; n<nComponents; ++n)
	{
	  const auto& comp = connectedComponents[n];
	  for(auto& f:comp)
	    { ++is_featured[f];
	      assert(is_featured[f]==1 || is_featured[f]==2); // Each feature can appear exactly once or twice
	      if(is_featured[f]==2) // If appearing twice, the feature should appear at the ends of the component
		{ assert(f==comp.front() && f==comp.back()); }
	    }
	}

      // Correctness checks
      for(auto& comp:connectedComponents)
	{
	  const int first = comp.front();
	  const int last = comp.back();
	  std::vector<int> compNums(comp.begin(), comp.end());
	  const int ncomp_features = static_cast<int>(comp.size());
	  for(int i=0; i<ncomp_features-1; ++i)
	    { const int& f = compNums[i];
	      const int& g = compNums[i+1];
	      const int& vf = feature2Terminals[f][1];
	      const int& vg = feature2Terminals[g][0];
	      assert(vf==vg);
	    }
	  if(first!=last) // This is an open component
	    { const int& vf = feature2Terminals[first][0];
	      assert(terminal2Features[vf][0]==-1);
	      const int& vg = feature2Terminals[last][1];
	      assert(terminal2Features[vg][1]==-1); }
	}
      // done
      return;
    }

    
    void checkConnectedComponents(const FeatureSetDetails& fsd)
    {
      const auto& connComps = fsd.connectedComponents;
      const auto& f2Tmap    = fsd.feature2Terminals;
      const auto& t2Fmap    = fsd.terminal2Features;
      
      // each feature should appear exactly once
      std::set<int> all_features{};
      for(auto& comp:connComps)
	{
	  for(auto& c:comp)
	    all_features.insert(c);

	  std::set<int> comp_features{};
	  std::list<int> opencomp = comp;
	  if(comp.front()==comp.back())
	    opencomp.pop_back();
	  for(auto& c:comp)
	    {
	      auto it = comp_features.insert(c);
	      assert(it.second = true);
	    }
	}
      assert(static_cast<int>(all_features.size())==fsd.nFeatures);
      all_features.clear();
    
      // check connectedness
      for(auto& comp:connComps)
	{
	  std::vector<int> compvec(comp.begin(), comp.end());
	  const int nsegments = static_cast<int>(compvec.size());

	  // open components
	  if(comp.front()!=comp.back()) 
	    {
	      // first terminal of first component should have only a right feature
	      const auto& t1 = f2Tmap[comp.front()];
	      assert(t2Fmap[t1[0]][0]==-1);

	      // second terminal of the last component should have only a left feature
	      const auto& t2 = f2Tmap[comp.back()];
	      assert(t2Fmap[t2[1]][1]==-1);
	    }

	  // open & closed components
	  for(int i=0; i<nsegments-1; ++i)
	    {
	      const int& fnum = compvec[i];
	      const int& next_fnum = compvec[i+1];
	      assert(f2Tmap[fnum][1]==f2Tmap[next_fnum][0]);
	    }
	}

      // done
      return;
    }


  } // namespace um2::internal
} // namespace um2
