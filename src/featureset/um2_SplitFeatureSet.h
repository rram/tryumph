// Sriramajayam

/** \file um2_SplitFeatureSet.h
 *
 * \brief Definition of the class SplitFeatureSet
 *
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {
    /** \brief Class implementing geometric queries for a set of
     * features having self-consistent orientations as required by the
     * meshing algorithms.
     * 
     * The class differs from FeatureSet mainly in the fact that
     * features provided are assumed to have only positive or negative
     * orientations. In comparison, features provided to FeatureSet may
     * have split orientations as well.
     *
     * Instances of this class are only instantiated by objects of the
     * FeatureSet class. 
     *
     * At construction, the class:
     *
     * - enumerates the set of distinct corners in the collection of
     * features provided, 
     *
     * - identifies connected components
     * 
     * - identifies closed components
     * 
     * - assigns polarities to features that decides the side on which positive edges
     * are identified.
     *
     * - estimates the medial axes of features using an instance of the
     * FeatureMedialAxis class
     *
     * \sa FeatureSet
     * \ingroup features
     */
    class SplitFeatureSet {
    public:
      /** \brief Constructor that precomputes all geometric details
       * (enumerates corners, identifies connected components, identifies closed
       * components, and computes medial axis samplings).
       * 
       * \param[in] fp Vector of split feature parameters. See @ref
       * userguide for detailed discussion.
       *
       * \param[in] split_parent_id_map map from parent feature id to
       * the split feature id (not indices)
       *
       * \param[in] orientations Orientations of split features- should
       * be one of FeatureOrientation::Positive or
       * FeatureOrientation::Negative.
       *
       * \param[in] cornerTol Absolute tolerance to identify distinct
       * corners in the set of features.
       *
       * \param[in] mparams Algorithmic parameters for sampling medial
       * axes of features. For convenience, the same set of parameters
       * are used for all features.
       */
      SplitFeatureSet(const std::vector<FeatureParams>& fp,
		      const std::map<int,int>& split_parent_id_map,
		      const std::vector<FeatureOrientation>& orientations,
		      const double cornerTol,
		      const MedialAxisParams& mparams);

      //! \brief Destructor
      inline virtual ~SplitFeatureSet() {}

      // \brief Disable copy and assignment
      SplitFeatureSet(const SplitFeatureSet&) = delete;
      SplitFeatureSet& operator=(const SplitFeatureSet&) = delete;

      //! \brief Access the parameters of split features
      inline const std::vector<FeatureParams>& getFeatureParams() const
      { return data.gparams; }

      /** \brief Access the identifier of the parent feature
       *
       *  \param[in] split_curve_id ID of the split feature 
       *
       *\return ID of the  parent feature 
       */
      inline int getParentCurveID(const int split_curve_id) const {
	auto it =
	  curve_parent_id_map.find(split_curve_id);
	assert(it!=curve_parent_id_map.end()); return it->second; }
    
      //! \brief Access the number of split features
      inline int getNumFeatures() const
      { return data.nFeatures; }

      //! \brief Access orientations of split features
      inline const std::vector<FeatureOrientation>& getFeatureOrientations() const
      { return data.orientations; }

      /** \brief Access the list of connected components of split features
       * 
       * \return connectedComponents[i] = sequential indices of features comprising the i-th connected component.
       */
      inline const std::vector<std::list<int>>& getConnectedComponents() const
      { return data.connectedComponents; }

      /** \brief Access the list of closed components of split features
       * 
       * \return closedComponents[i] = sequential indices of features comprising the i-th closed component.
       */
      const std::vector<std::list<int>>& getClosedComponents() const
      { return data.closedComponents; }
    
      //! \brief Access the number of distinct terminals/corners of split features
      inline int getNumTerminals() const
      { return data.nTerminals; }

      //! \brief Access the coordinates of a terminal
      //! \param[in] tnum Index of the corner
      //! \return Cartesian coordinates of the corner
      const double* getTerminalCoordinates(const int tnum) const;
      
      //! \brief Acess map from features to terminals
      //! \return f2Tmap[i] = indices of corners of the feature with index i
      inline const std::vector<std::array<int,2>>& getFeaturesToTerminalsMap() const
	{	return data.feature2Terminals; }

      //! \brief Acess map from terminals to features
      //! \return t2Fmap[i] = indices of the pair of features incident at terminal with index i. Non-existent features are assigned index -1
      inline const std::vector<std::array<int,2>>& getTerminalsToFeaturesMap() const
	{ return data.terminal2Features; }

      /** \brief Compute distance of a point to the medial axis of a feature.
       *
       * This information is used to size the background mesh along
       * features.
       *
       * \param[in] X Cartesian coordinate of the quried point
       *
       * \param[in] curve_id ID of the feature to query
       */
      inline double getFeatureSize(const double* X, const int curve_id) const {
	// index of this feature
	auto it = data.curveid2IndexMap.find(curve_id);
	assert(it!=data.curveid2IndexMap.end());
	const int& findx = it->second;
	return data.medialAxes[findx]->getDistanceToMedialAxis(X);
      }

      /** \brief Computes the distance of a corner to the nearest corner
       *
       * This distance is used to estimate the mesh size in the vicinity of corners.
       *
       * \param[in] corner_num Index of the corner
       *
       * \return Distance to the corner closest to corner_num.
       */
      double getDistanceToNearestCorner(const int corner_num) const {
	assert(corner_num>=0 && corner_num<data.nTerminals);
	return closest_corner_distance[corner_num];
      }
    
    private:
      /** \brief Helper function to check orientations of features provided at construction. 
       * Invoked by the constructor.
       */
      void checkOrientations() const;

      /** \brief Helper function to compute distances between corners.
       * Invoked by the constructor.
       */
      void computeCornerDistances();
    
      const double            corner_tolerance; //!< Absolute tolerance to identify distinct corners
      const MedialAxisParams  medial_axis_params; //!< Parameters to use for computing medial axes
      internal::FeatureSetDetails data; //!< Encapsulation of data
      const std::map<int,int> curve_parent_id_map; //! Copy of the map from parent ids to split curve ids
      std::vector<double>     closest_corner_distance; //! closest_corner_distance[i] = Distance of the i-th corner to its closest neighbor
    };

  } // namespace um2::internal
} // namespace um2
