// Sriramajayam

/** \file um2_FeatureSet_Algorithms.cpp 
 *
 * \brief Collection of algorithms used by the FeatureSet and
 * SplitFeatureSet classes.  
 *
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <um2_Input.h>
#include <um2_BoostAliases.h>
#include <um2_FeatureMedialAxis.h>
#include <list>
#include <cassert>

namespace um2 {
  
  /** \brief Orientations of geometric features relative to the sign of the signed distance function
   *
   * The meshing algorithm identifies positive edges based on this orientation.
   */
  enum class FeatureOrientation {
    Unassigned, //!< Default
      Positive, //!< Positive edges are identified on the positive side 
      Negative, //!< Positive edges are identified on the negative side 
      Split_Positive2Negative, //!< The feature is split. Positive edges are identified on the positive side, followed by the negative side
      Split_Negative2Positive //!< The feature is split. Positive edges are identified on the negative side, followed by the positive side
      };


  namespace internal {
    /** \brief Encapsulation of data stored/computed by the FeatureSet
     * and SplitFeatureSet classes.  Meant for internal use.
     */
    struct FeatureSetDetails {
      //! \brief Constructor, sets members to trival state
      FeatureSetDetails(); 
      std::vector<FeatureParams> gparams; //!< Vector of feature parameters
      int nFeatures;  //!< number of features
      int nTerminals; //!< number of distinct corners
      std::map<int,int>                               curveid2IndexMap; //!< map between curve id and their index in the gparams vector
      std::vector<std::array<int,2>>                  feature2Terminals; //!< feature2Terminals[i] = indices of the two corners of feature with index i
      std::vector<std::array<int,2>>                  terminal2Features; //!< terminal2Features[i] = indices of the features indident at corner with index i. Non-existent features are set to -1
      std::vector<std::list<int>>                     connectedComponents; //!< connectedComponents[i] = sequential list of indices of features creating the i-th connected component
      std::vector<std::list<int>>                     closedComponents; //!< closedComponents[i] = sequential list of indices of features creating the i-th closed component
      std::vector<FeatureOrientation>                 orientations; //!< orientations assigned to features
      std::vector<std::unique_ptr<FeatureMedialAxis>> medialAxes; //!< medial axis of each feature
    };


    /** \brief Helper function used by FeatureSet and SplitFeatureSet
     * classes to enumerate distinct corners in a collection of features.
     * 
     * \param[in] gparams Parameters for the set of features whose corners to enumerate
     * \param[in] absTol Absolute tolerance to identify distinct corners
     * \param[out] nTerminals Number of distinct corners
     * \param[out] feature2Terminals feature2Terminals[i] = indices of corners of feature #i.
     * \param[out] terminal2Features The inverse map of feature2Terminals, terminal2Features[i] = indices of features sharing terminal #i.
     *  No-existent features are assigned -1.
     */ 
    void enumerateFeatureTerminals(const std::vector<FeatureParams>& gparams,
				   const double absTol,
				   int& nTerminals,
				   std::vector<std::array<int,2>>& feature2Terminals,
				   std::vector<std::array<int,2>>& terminal2Features);

    /** \brief Helper function used by FeatureSet and SplitFeatureSet classes to 
     * identify connected components in a given collection of features.
     * 
     * \param[in] gparams Parameters for the set of features whose corners to enumerate
     * \param[in] feature2Terminals Map from feature index to corner indices as computed with enumerateFeatureTerminals
     * \param[in] terminal2Features Map from corner index to feature indices incident at the corner as computed with enumerateFeatureTerminals
     * \param[out] connectedComponents Computed list of connected components. connectedComponents[i] = sequential indices of features comprising the i-th connected component.
     */
    void connectFeatures(const std::vector<FeatureParams>& gparams,
			 const std::vector<std::array<int,2>>& feature2Terminals,
			 const std::vector<std::array<int,2>>& terminal2Features,
			 std::vector<std::list<int>>& connectedComponents);

    /** \brief Helper function used by FeatureSet and SplitFeatureSet classes to 
     * identify connected components in a given collection of features. 
     * 
     * \param[in] connectedComponents Connected components as computed with connectFeatures.
     * \param[out] closedComponents Subset of connectedComponents consisting of closed connected components.
     */
    void closedComponents(const std::vector<std::list<int>>& connectedComponents,
			  std::vector<std::list<int>>& closedComponents);
        
    /** \brief Helper function used by FeatureSet and SplitFeatureSet
     * to compute the medial axes of features. 
     *
     * \param[in,out] splitData The medialAxis member of the instance is updated with feature-wise calculation of the medial axis.
     * \param[in] mparams Parameters to use for medial axis computations.
     */
    void computeMedialAxes(FeatureSetDetails& splitData,
			   const MedialAxisParams& mparams);
    
    /** \brief Helper function used by the FeatureSet and
     * SplitFeatureSet classes to verify correctness of corner
     * enumerations.
     *
     * \param[in] fsd Data for which to verify correctness of corner enumerations.
     * \param[in] absTol Absolute tolerance to use for checking coincidence/seperateness of corners.
     * 
     * The function asserts in case the computation of corners is incorrect.
     */
     void checkTerminals(const FeatureSetDetails& fsd, const double absTol);

      /** \brief Helper function used by the FeatureSet and
     * SplitFeatureSet classes to verify correctness in idetification of connected components.
     *
     * \param[in] fsd Data for which to verify correctness of connected components
     * 
     * The function asserts in case the data is inconsistent
     */
    void checkConnectedComponents(const FeatureSetDetails& fsd);

  } // namspace um2::internal
  
} // namespace um2
