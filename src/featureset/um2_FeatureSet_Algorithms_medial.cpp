// Sriramajayam

/** \file um2_FeatureSet_Algorithms_medial.cpp
 *
 * \brief Implements medial axis calculations for a set of features
 * for use by the FeatureSet and SplitFeatureSet classes.
 *
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {
    
    // Medial axis computation
    void computeMedialAxes(FeatureSetDetails& data,
			   const MedialAxisParams& mparams)
    {
      const int& nFeatures = data.nFeatures;
      const auto& gparams  = data.gparams;
      auto& medialAxes     = data.medialAxes;
      medialAxes.clear();

      for(int f=0; f<nFeatures; ++f)
	{
	  // sample points for this features
	  auto sample_points = gparams[f].sample_feature();

	  // generate feature samples (index, point, normal)
	  std::list<FeatureSample> fsamples{};
	  FeatureSample fs;
	  auto& fs_indx   = std::get<0>(fs);
	  auto& fs_point  = std::get<1>(fs);
	  auto& fs_normal = std::get<2>(fs);
	  double sd, dsd[2];
	  int indx = 0;
	  for(auto& pt:sample_points)
	    {
	      fs_indx  = indx++;
	      fs_point = pt;
	      gparams[f].signed_distance_function(&fs_point[0], sd, &fs_normal[0]);
	      fsamples.push_back(fs);
	    }

	  // medial axis
	  medialAxes.push_back(std::unique_ptr<FeatureMedialAxis>(new FeatureMedialAxis(fsamples, mparams)));  
	}

      // done
      return;
    }

  } // namespace um2::internal
} // namespace um2
