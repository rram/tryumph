// Sriramajayam

/** \file um2_FeatureSet_orient.cpp
 * \brief Implements the calculation of feature orientations in class FeatureSet
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet.h>

namespace um2 {

  // helper function
  void orientConnectedComponent(const std::vector<FeatureParams>& gparams,
				const std::vector<int>& segNums,
				std::vector<FeatureOrientation>& segOrientations)
  {
    const int nSegments = static_cast<int>(segNums.size());
    assert(nSegments>0);
    //assert(segNums[0]!=segNums[nSegments-1]);
    segOrientations.resize(nSegments);
    std::fill(segOrientations.begin(), segOrientations.end(), FeatureOrientation::Unassigned);

    // Arbitrarily orient seg #0
    segOrientations[0] = FeatureOrientation::Positive;
      
    // temporaries
    double nMinus[2], nPlus[2];
    double tMinus[2], tPlus[2];
    double sdval, theta;
      
    // Determine orientations for the remaining segments
    for(int s=1; s<nSegments; ++s)
      {
	// Previous segment cannot be unassigned or split
	assert(segOrientations[s-1]==FeatureOrientation::Positive ||
	       segOrientations[s-1]==FeatureOrientation::Negative);

	// Parameters for this segment
	const auto& this_gparam = gparams[segNums[s]];
	  
	// Parameters for the previous segment
	const auto& prev_gparam = gparams[segNums[s-1]];
	  
	// The corner to inspect
	const double* cnr = &this_gparam.leftCnr[0];
	  
	// Normal/tangent at the corner from the left segment
	prev_gparam.signed_distance_function(cnr, sdval, nMinus);
	tMinus[0] = nMinus[1]; tMinus[1] = -nMinus[0];
	  
	// Normal/tangent at the corner from the right segment
	this_gparam.signed_distance_function(cnr, sdval, nPlus);
	tPlus[0] = nPlus[1];   tPlus[1] = -nPlus[0];

	// Angle between the tangents in the range (-pi,pi)
	// atan2(sin=cross(tminus,tplus), cos=dot(tminus,tplus))
	theta = std::atan2(tMinus[0]*tPlus[1]-tMinus[1]*tPlus[0],
			   tMinus[0]*tPlus[0]+tMinus[1]*tPlus[1]);

	// Decide how to orient this segment
	if(std::abs(theta)<M_PI/2.) // Assign 's' the opposite orientation of 's-1'
	  {
	    if(segOrientations[s-1]==FeatureOrientation::Positive)
	      segOrientations[s] = FeatureOrientation::Negative;
	    else if(segOrientations[s-1]==FeatureOrientation::Negative)
	      segOrientations[s] = FeatureOrientation::Positive;
	  }
	else if(theta>=M_PI/2.) // s-1 and s should both be negative
	  {
	    if(s==1) // s=0 is arbitrarily set to be positive. Flip it
	      segOrientations[s-1] = FeatureOrientation::Negative; 
	    else if(segOrientations[s-1]==FeatureOrientation::Positive) // split this segment 
	      segOrientations[s-1] = FeatureOrientation::Split_Positive2Negative;

	    segOrientations[s] = FeatureOrientation::Negative;
	  }
	else if(theta<=-M_PI/2.) // s-1 and s should both be positive
	  {
	    // s = 0 is positive.
	    if(segOrientations[s-1]==FeatureOrientation::Negative) // split 
	      segOrientations[s-1] = FeatureOrientation::Split_Negative2Positive;

	    segOrientations[s] = FeatureOrientation::Positive;
	  }
      }

    // Sanity check: all orientations have been assigned
    for(auto& v:segOrientations)
      { assert(v!=FeatureOrientation::Unassigned); }

    // First and last segments cannot be split
    assert(segOrientations[0]==FeatureOrientation::Positive ||
	   segOrientations[0]==FeatureOrientation::Negative);
    assert(segOrientations[nSegments-1]==FeatureOrientation::Positive ||
	   segOrientations[nSegments-1]==FeatureOrientation::Negative);
	    
    // done
    return;
  }

  
  // Assign orientations to each feature
  void FeatureSet::computeOrientations()
  {
    // Unassign all feature orientations
    data.orientations.resize(data.gparams.size(), FeatureOrientation::Unassigned);
      
    // Assign orientations to features one connected component at a time
    for(auto& segIndices:data.connectedComponents)
      {
	// Feature indices of segments in this component
	std::vector<int> segNums(segIndices.begin(), segIndices.end()); // copy to a vector for easy access

	// Orientations of segments indexed relative to segNum
	const int nSegments = static_cast<int>(segNums.size());
	std::vector<FeatureOrientation> segOrientations(nSegments);
	  
	// Orient this component
	orientConnectedComponent(data.gparams, segNums, segOrientations);

	// If it is closed, reconcile first and last segments
	if(segIndices.front()==segIndices.back() &&
	   segOrientations[0]!=segOrientations[nSegments-1])
	  {
	    if(segOrientations[0]==FeatureOrientation::Positive) // (-,+)
	      segOrientations[0] = FeatureOrientation::Split_Negative2Positive;
	    else // (+,-)
	      segOrientations[0] = FeatureOrientation::Split_Positive2Negative;
	      
	    segOrientations[nSegments-1] = segOrientations[0];
	  }
	  
	// Set orientations in the master array
	for(int s=0; s<nSegments; ++s)
	  data.orientations[segNums[s]] = segOrientations[s];
      }
      
    // Check that all features have been assigned orientations
    for(auto& v:data.orientations)
      { assert(v!=FeatureOrientation::Unassigned); }
      
    // done
    return;
  }

}
