// Sriramajayam

/** \file um2_FeatureSet.h
 * \brief Define the class FeatureSet
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <um2_SplitFeatureSet.h>

namespace um2 {

  /** \brief The FeatureSet class implements all geometry-related
   * computations required by the meshing algorithm.
   *
   * At construction, the class:
   *
   * - enumerates the set of distinct corners in the collection of
   * features provided, 
   *
   * - identifies connected components
   * 
   * - identifies closed components
   * 
   * - assigns polarities to features that decides the side on which positive edges
   * are identified.
   *
   * - estimates the medial axes of features using an instance of the
   * FeatureMedialAxis class
   *
   * \sa MeshManager::triangulate
   *
   * \ingroup features
   */
  class FeatureSet {
  public:
    /** \brief Constructor that precomputes all geometric details
     * (enumerates corners, identifies connected components, identifies closed
     * components, assigned polarities to features, and computes medial axis samplings).
     * 
     * \param[in] fp Vector of feature parameters. See @ref userguide
     * for detailed discussion.  
     *
     * \param[in] cornerTol Absolute tolerance to identify distinct
     * corners in the set of features.
     *
     * \param[in] mparams Algorithmic parameters for sampling medial
     * axes of features. For convenience, the same set of parameters
     * are used for all features.
     */
    FeatureSet(const std::vector<FeatureParams>& fp,
	       const double cornerTol,
	       const MedialAxisParams& mparams);

    //! Destructor
    inline virtual ~FeatureSet() {}
    
    // Disable copy and assignment
    FeatureSet(const FeatureSet&) = delete;
    FeatureSet& operator=(const FeatureSet&) = delete;

    //! Access the tolerance used to distinguish distinct corners
    inline double getCornerTolerance() const {
      return corner_tolerance; }

    //! Access the set of medial axis parameters provided at construction
    MedialAxisParams getMedialAxisParams() const {
      return medial_axis_params; }

    //! Access the set of feature parameters provided at construction
    const auto& getFeatureParams() const
    { return data.gparams; }
    
    //! Access the number of features
    inline int getNumFeatures() const
    { return data.nFeatures; }

    /** \brief Computes the distance of a point in the plane of the
     * geometry to the medial axis of a feature. Expected to be used
     * to estimate the local mesh size to set for the background mesh
     * in the vicinity of X.
     * 
     * \param[in] X Cartesian coordinates of point 
     * \param[in] curve_id Identifier of the feature to be queried
     * \return Distance of X to the medial axis of feature #curve_id
     * \sa getComplementaryFeatureSize
     */
    inline double getFeatureSize(const double* X, const int curve_id) const
    {
      // index of this feature
      auto it = data.curveid2IndexMap.find(curve_id);
      assert(it!=data.curveid2IndexMap.end());
      const int& findx = it->second;
      return data.medialAxes[findx]->getDistanceToMedialAxis(X);
    }

    /** \brief Estimates the distance of a point from the medial axis
     * of the entire feature set, while excluding (i) a specified
     * feature, and (ii) neighborhoods around corners.
     *
     * The getFeatureSize method helps size the mesh based on a single
     * feature. This method, on the other hand, enables mesh sizing
     * while considering the complementary set of features.  The
     * method returns the distance of a given point to the medial axis
     * of the geometry while not including a specified feature.
     *
     * A caveat in the computation is that in the vicinity of corners
     * in the geometry, the medial axis sampling can get arbitrarily
     * close to the geometry itself. Hence, the calculation is
     * implemented only for points not lying too close to corners.
     *
     * \sa getFeatureSize
     * 
     * \param[in] X Cartesian coordinates of point at which to query
     *
     * \param[in] curve_id Feature to exclude in the medial axis
     * sampling 
     *
     * \param[out] is_computed True if the feature size was computed,
     * which is the case when X is farther from every corner by the
     * radius of the cut locus. False otherwise
     *
     * \param[out] distance Computed distance, meaningful only if
     * is_computed is returned as true
     *
     * \param[out] cpt Identified closest point on the medial axis,
     * meaningful only of is_computed is returned as true.
     *
     */
    void getComplementaryFeatureSize(const double* X, const int curve_id,
				     bool& is_computed, double& distance, double* cpt) const;

    /** \brief Access feature orientations
     * \sa FeatureOrientation
     */
    inline const std::vector<FeatureOrientation>& getFeatureOrientations() const
    { return data.orientations; }
    
    //! \brief Access the split feature set
    inline const internal::SplitFeatureSet& getSplitFeatureSet() const
    { return *split_fset; }
   
    //! \brief Access the exclusion radii around a feature's pair of corners
    inline std::pair<double,double> getCornerExclusionRadii(const int curve_id) const
    {
      auto it = data.curveid2IndexMap.find(curve_id);
      assert(it!=data.curveid2IndexMap.end());
      return corner_radii[it->second];
    }

  private:
    //! \brief Helper function to compute feature orientations, invoked in the constructor
    void computeOrientations();

    //! \brief Helper function to split features and track split curve ids, invoked in the constructor
    void splitFeatures();

    //! \brief Helper function to build an r-tree of all sample points of all features, invoked in the constructor
    void createSampleTree();
				 
    //! \brief Helper function to compute exclusion radii around corners for all features, invoked in the constructor
    void computeCornerRadii();
					    
    const double corner_tolerance; //!< tolerance separating distinct corners, provided at construction
    const MedialAxisParams medial_axis_params; //!< Parameters for sampling medial axis, provided at construction
    internal::FeatureSetDetails data; //!< Encapsulation of internal data 
    std::unique_ptr<internal::SplitFeatureSet> split_fset; //!< Split feature set
    std::vector<std::pair<double,double>> corner_radii; //!< Cut locus radii computed featurewise.
    boost_pointID_rtree global_rtree; //!< boost r-tree containing all feature samples, used for closest point searches
  };
} // namespace um2

