// Sriramajayam

/** \file um2_FeatureSet_Algorithms_closed.cpp 
 *
 * \brief Implements the function um2::internal::closedComponents to
 * identify closed connected components of features for use by the
 * FeatureSet and SplitFeatureSet classes.
 *
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {

    // Helper functions/structures
    // Identify closed connected components
    void closedComponents(const std::vector<std::list<int>>& connComps,
			  std::vector<std::list<int>>& closedComponents)
    {
      // Identify closed connected components
      closedComponents.clear();
      for(auto& comp:connComps)
	{
	  // Closed if first component == last component
	  if(comp.front()==comp.back())
	    {
	      // Remove the last repeated feature
	      std::list<int> compNums = comp;
	      compNums.pop_back();

	      // Note this as a closed connected component
	      closedComponents.push_back(compNums);
	    }
	}

      // done
      return;
    }

  } // namespace um2::internal
} // namespace um2
 
