// Sriramajayam

/** \file um2_FeatureSet_Algorithms.cpp
 * 
 * \brief Implements trivial constructor of the class internal::FeatureSetDetails.
 * 
 * \author Ramsharan Rangarajan
 */

#include <um2_FeatureSet_Algorithms.h>

namespace um2 {
  namespace internal {
    FeatureSetDetails::FeatureSetDetails()
      :gparams{},
       nFeatures(0),
       nTerminals(0),
       curveid2IndexMap{},
       feature2Terminals{},
       terminal2Features{},
       connectedComponents{},
       closedComponents{},
       orientations{},
       medialAxes{}
    {}
    
  } // namespace um2::internal
} // namespace um2
