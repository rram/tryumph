// Sriramajayam

#include "demo_CircularArc.h"
#include <cassert>
#include <iostream>

namespace demo {
  
  // Constructor
  CircularArc::CircularArc(int id, int nsamples,
			   std::pair<double,double> C, double R,
			   std::pair<double,double> angle)
    :curve_id(id), nSamples(nsamples),
     center{C.first, C.second}, radius(R),
     theta0(angle.first), theta1(angle.second),
     P0{center[0]+radius*std::cos(theta0), center[1]+radius*std::sin(theta0)},
     P1{center[0]+radius*std::cos(theta1), center[1]+radius*std::sin(theta1)} {

       // wrap angle
       while(theta0>=2.*M_PI) {
	 theta0 -= 2.*M_PI;
	 theta1 -= 2.*M_PI;
       }
     
       assert(0.<=theta0 && theta1-theta0>=0. && theta1-theta0<2.*M_PI);
     }

  // feature params
  um2::FeatureParams CircularArc::generateFeatureParams() {

    um2::FeatureParams params;
  
    // curve id
    params.curve_id = curve_id;

    // signed distance -> std::function<void(const double* X, double& sd, double* dsd)> 
    params.signed_distance_function  = std::bind(&CircularArc::SignedDistance, this,
						 std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);             
      
    // sampling -> std::function<std::list<std::pair<double,double>> ()>
    params.sample_feature = std::bind(&CircularArc::Sampling, this);
	  
    // end points
    for(int k=0; k<2; ++k) {
      params.leftCnr[k]  = P0[k];
      params.rightCnr[k] = P1[k];
    }

    // midpoint
    params.intPoint[0] = center[0]+radius*std::cos(0.5*(theta0+theta1));
    params.intPoint[1] = center[1]+radius*std::sin(0.5*(theta0+theta1));
  
    // done
    um2::validate_input(params);
    return params;
  }

  // sampling function
  std::list<std::array<double,2>> CircularArc::Sampling() const {
  
    std::list<std::array<double,2>> samples{};
    const double dtheta = (theta1-theta0)/static_cast<double>(nSamples);
    for(int i=0; i<=nSamples; ++i) {
      double theta = theta0 + i*dtheta;
      samples.push_back({center[0]+radius*std::cos(theta), center[1]+radius*std::sin(theta)});
    }
    return std::move(samples);
  }


  // signed distance function
  void CircularArc::SignedDistance(const double* X, double& sd, double* dsd) {
  
    // angular coordinate of the closest point projection on the circle in the range [0,2pi)
    double angle = std::atan2(X[1]-center[1], X[0]-center[0]);
    if(angle<0.) {
      angle += 2.*M_PI;
    }

    // angular coordinate of the closest point on the circular arc
    if((theta1<=2.*M_PI && (angle<theta0         || angle>theta1)) ||
       (theta1>2.*M_PI  && (angle>theta1-2.*M_PI && angle<theta0))) {
      double d1 = (X[0]-P0[0])*(X[0]-P0[0]) +  (X[1]-P0[1])*(X[1]-P0[1]);
      double d2 = (X[0]-P1[0])*(X[0]-P1[0]) +  (X[1]-P1[1])*(X[1]-P1[1]);
      angle = (d1<d2) ? theta0 : theta1;
    }
  
    // closest point on the arc
    double cpt[] = {center[0]+radius*std::cos(angle), center[1]+radius*std::sin(angle)};
  
    // normal
    const double nvec[] = {-std::cos(angle), -std::sin(angle)};

    // signed distance
    sd = (X[0]-cpt[0])*nvec[0] + (X[1]-cpt[1])*nvec[1];
  
    // Gradient
    if(dsd!=nullptr) {
      dsd[0] = nvec[0];
      dsd[1] = nvec[1];
    }

    // done
    return;
  }

}
