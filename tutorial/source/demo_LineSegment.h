// Sriramajayam

#pragma once

#include <um2_Input.h>
#include <cmath>

namespace demo {
  
  //! Encapsulate data for a line segment
  struct LineSegmentData {
    int curve_id;
    int nSamples;
    std::array<double,2> A, B;
  };

  //! sampling function
  std::list<std::array<double,2>> line_segment_sampling(const LineSegmentData& data);

  //! signed distance function
  void line_segment_signed_distance(const double* X, double& sd, double* dsd,
				    const LineSegmentData& data);

  //! generate feature parameters
  um2::FeatureParams generateLineSegmentFeatureParams(const LineSegmentData &data);

}
