// Sriramajayam

#pragma once

#include <um2_Input.h>
#include <utility>
#include <cmath>

namespace demo {
  
  class CircularArc
  {
  public:
    //! Constructor
    CircularArc(int id, int nsamples,
		std::pair<double,double> C, double R,
		std::pair<double,double> angle);
  
    //! Destructor
    ~CircularArc() = default;

    //! Disable copy and assignment
    CircularArc(const CircularArc&) = delete;
    CircularArc operator=(const CircularArc&) = delete;

    // Register feature params
    um2::FeatureParams generateFeatureParams();

  private:
  
    //! sampling function
    std::list<std::array<double,2>> Sampling() const;
    
    //! signed distance function
    void SignedDistance(const double* X, double& sd, double* dsd);

    const int curve_id;
    const int nSamples;
    const double center[2];
    const double radius;
    double theta0, theta1;
    const double P0[2], P1[2];
  };
}
