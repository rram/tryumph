// Sriramajayam

#include "demo_LineSegment.h"
#include <iostream>

namespace demo {
  
  // sampling function
  std::list<std::array<double,2>> line_segment_sampling(const LineSegmentData& data) {

    std::list<std::array<double,2>> sampling{};
    std::array<double,2> pt;
    for(int i=0; i<=data.nSamples; ++i) {
      const double lambda = static_cast<double>(i)/static_cast<double>(data.nSamples);
      pt[0] = (1.-lambda)*data.A[0] + lambda*data.B[0];
      pt[1] = (1.-lambda)*data.A[1] + lambda*data.B[1];
      sampling.push_back(pt);
    }
    return std::move(sampling);
  }


 
  // signed distance function
  void line_segment_signed_distance(const double* X, double& sd, double* dsd,
				    const LineSegmentData &data)
  {
    const auto& A = data.A;
    const auto& B = data.B;
  
    // coordinate of the closest point: lambda
    // r = A + lambda(B-A)
    double lAB2 = (B[0]-A[0])*(B[0]-A[0]) + (B[1]-A[1])*(B[1]-A[1]);
    double dot  = (X[0]-A[0])*(B[0]-A[0]) + (X[1]-A[1])*(B[1]-A[1]);
    double lambda = dot/lAB2;

    // closest point = end point?
    if(lambda<0.) {
      lambda = 0.;
    }
    else if(lambda>1.) {
      lambda = 1.;
    }

    // normal
    double lAB = std::sqrt(lAB2);
    const double nvec[2] = {-(B[1]-A[1])/lAB, (B[0]-A[0])/lAB};

    // closest point
    const double cpt[2] = {A[0]+lambda*(B[0]-A[0]), A[1]+lambda*(B[1]-A[1])};

    // signed distance
    sd = (X[0]-cpt[0])*nvec[0] + (X[1]-cpt[1])*nvec[1];

    // gradient
    if(dsd!=nullptr) {
      dsd[0] = nvec[0];
      dsd[1] = nvec[1];
    }

    // done
    return;
  }


  //! generate feature parameters
  um2::FeatureParams generateLineSegmentFeatureParams(const LineSegmentData &data) {

    um2::FeatureParams params;
    params.curve_id = data.curve_id;
    for(int k=0; k<2; ++k) {
      params.leftCnr[k]  = data.A[k];
      params.rightCnr[k] = data.B[k];
    }

    for(int k=0; k<2; ++k) {
      params.intPoint[k] = 0.5*(data.A[k]+data.B[k]);
    }

    // sampling -> std::function<std::list<std::pair<double,double>> ()>
    params.sample_feature = std::bind(&line_segment_sampling, data);
  
    // signed distance -> std::function<void(const double* X, double& sd, double* dsd)> 
    params.signed_distance_function  = std::bind(&line_segment_signed_distance,
						 std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, data);
      
    // done
    um2::validate_input(params);
    return params;
  }
}
