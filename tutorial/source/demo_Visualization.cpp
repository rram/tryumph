// Sriramajayam

#include "demo_Visualization.h"

namespace demo {
  
  void plotFeaturesVTK(const std::vector<um2::FeatureParams>& params,
		       const std::string filename) {
  
    assert(std::string(std::filesystem::path(filename).extension())==".vtk");
  
    std::fstream file;
    file.open(filename, std::ios::out);
    assert(file.good() && file.is_open());

    // Headers
    file << "# vtk DataFile Version 3.0" << std::endl;
    file << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
    file << "ASCII" << std::endl;
    file << "DATASET POLYDATA" << std::endl;

    // feature samples
    std::vector<std::list<std::array<double,2>>> samples{};
    int nPoints = 0;
    for(auto& fp:params) {
      samples.emplace_back(fp.sample_feature());
      nPoints += static_cast<int>(samples.back().size());
    }

    // nodes
    file << "POINTS " << nPoints << " double" << std::endl;
    for(auto& s:samples) {
      for(auto& p:s) {
	file << p[0] << " " << p[1] << " " << 0. << std::endl;
      }
    }

    // each line = samples on a feature
    // cell size = total number of integers in the list
    const int nFeatures = static_cast<int>(params.size());
    const int cell_size = nPoints + nFeatures;
    file << "LINES " << nFeatures << " " << cell_size << std::endl;
    int pcount = 0;
    for(auto& s:samples) {
      const int nsamples = static_cast<int>(s.size());
      file << nsamples << " ";
      for(int i=0; i<nsamples; ++i, pcount++) {
	file << pcount << " ";
      }
      file << std::endl;
    }

    // colors for each feature
    file << "CELL_DATA " << nFeatures << std::endl
	 << "SCALARS color int 1" << std::endl
	 << "LOOKUP_TABLE default" << std::endl;
    for(auto& fp:params) {
      file << fp.curve_id << std::endl;
    }
  
    // done
    file.close();
  }
}
