// Sriramajayam

#pragma once

#include <um2_Input.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <cassert>

namespace demo {
  void plotFeaturesVTK(const std::vector<um2::FeatureParams>& params,
			 const std::string filename);

  template<typename MeshType>
    void plotMeshVTK(const MeshType& mesh, const std::string filename)
    {
      assert(std::string(std::filesystem::path(filename).extension())==".vtk");
      std::fstream out;
      out.open(filename, std::ios::out);
      assert(out.good());
  
      // Headers
      out << "# vtk DataFile Version 3.0" << std::endl;
      out << "Polygon mesh " << std::filesystem::path(filename).stem() << std::endl;
      out << "ASCII" << std::endl;
      out << "DATASET POLYDATA" << std::endl;
  
      // nodes
      const int nNodes = mesh.n_nodes();
      out << "POINTS " << nNodes << " double" << std::endl;
      for(int i=0; i<nNodes; ++i) {
	const double* X = mesh.coordinates(i);
	out << X[0] << " " << X[1] << " " << 0. << std::endl;
      }

      // cells
      const int nCells = mesh.n_elements();
      int cell_size = (3+1)*nCells;
      out << "POLYGONS " << nCells << " " << cell_size << std::endl;
      for(int e=0; e<nCells; ++e) {
	const int* conn = mesh.connectivity(e);
	out << 3 << " " << conn[0] << " " << conn[1] << " " << conn[2] << std::endl;
      }

      // done
      out.close();
      return;
    }
}
