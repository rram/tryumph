// Sriramajayam

#include <TryUMph>
#include <TryUMph_utils>
#include "source/demo_CircularArc.h"
#include "source/demo_LineSegment.h"
#include "source/demo_Visualization.h"

#include <memory>

void createSmileyFace(const double theta, 
			std::vector<std::unique_ptr<demo::CircularArc>>& arcs,
			std::vector<demo::LineSegmentData>& segs);

int main() {
  
  // Medial axis calc parameters
  um2::MedialAxisParams medial_axis_params{.exclusion_radius=1.e-4, .maximum_radius=2.0, .eps_radius=1.e-3};

  // corner tolerance
  const double corner_tol = 1.e-3;
  
  // Meshing parameters
  um2::MeshingParams meshing_params{
    .nonlocal_refinement=false,
    .refinement_factor=1.,
      .num_proj_steps = 5,
      .num_dvr_int_relax_iters = 10,
      .num_dvr_int_relax_dist = 2,
      .num_dvr_bd_relax_iters = 3,
      .num_dvr_bd_relax_samples = 10,
      .min_quality = 0.01,
      .max_num_trys = 8};

  // Create the mesh manager
  const um2::Box box{.center={0.,0.}, .size=4.0};
  um2::MeshManager<um2::TriangleMesh> mesh_manager(box);
  um2::utils::Final_Try_Visualization_Callback<um2::TriangleMesh> cb("./");
  mesh_manager.setCallback(cb);

  for(int riter=0; riter<15; ++ riter)
    {
      // geometry
      const double angle = static_cast<double>(riter)*M_PI/15.;
      std::vector<std::unique_ptr<demo::CircularArc>> arcs{};
      std::vector<demo::LineSegmentData> segs{};
      createSmileyFace(angle, arcs, segs);
      
      // populate feature parameters
      std::vector<um2::FeatureParams> feature_params{};
      for(auto& arc:arcs) {
	feature_params.emplace_back(arc->generateFeatureParams());
      }
      for(auto& seg:segs) {
	feature_params.emplace_back(generateLineSegmentFeatureParams(seg));
      }

      // visualize features
      demo::plotFeaturesVTK(feature_params, "smiley-"+std::to_string(riter)+".vtk");

      
      // Feature set
      um2::FeatureSet feature_set(feature_params, corner_tol, medial_axis_params);
      
      // Mesh
      const int case_id = riter;
      um2::RunInfo run_info;
      if(case_id==0) {
	run_info = mesh_manager.triangulate(case_id, feature_set, meshing_params);
      }
      else {
	run_info = mesh_manager.triangulate(case_id-1, case_id, feature_set, meshing_params);
      }
      assert(run_info.success==true);
      
      // visualize the background mesh and the working mesh
      const auto& BG = mesh_manager.getUniversalMesh(case_id);
      const auto& WM = mesh_manager.getWorkingMesh(case_id);
      //demo::plotMeshVTK(BG, "bgmesh-"+std::to_string(riter)+".vtk");
      //demo::plotMeshVTK(WM, "conforming-"+std::to_string(riter)+".vtk");
    }
  // done
}


void createSmileyFace(const double theta, 
			std::vector<std::unique_ptr<demo::CircularArc>>& arcs,
			std::vector<demo::LineSegmentData>& segs) {
  arcs.clear();
  segs.clear();
  
  const int    nSamples = 25;
  const double R_face   = 1.25;
  const double R_eye    = 0.75;
  const double R_smile  = 0.8625;
  const double r_eye    = 0.3;
  const double y_eye    = 0.15;
  const double alpha    = std::atan(y_eye/r_eye);
  const double rhat     = std::sqrt(r_eye*r_eye + y_eye*y_eye);
  
  // coordinates
  const double C_right_eye[]     = {R_eye*std::cos(M_PI/3.)+0.5*r_eye, R_eye*std::sin(M_PI/3.)-r_eye};
  const double C_right_eye_low[] = {R_eye*std::cos(M_PI/3.)+0.5*r_eye, R_eye*std::sin(M_PI/3.)-r_eye+y_eye};

  const double C_left_eye[]      = {R_eye*std::cos(2.*M_PI/3.)-0.5*r_eye, R_eye*std::sin(2.*M_PI/3.)-r_eye};
  const double C_left_eye_low[]  = {R_eye*std::cos(2.*M_PI/3.)-0.5*r_eye, R_eye*std::sin(2.*M_PI/3.)-r_eye+y_eye};

  const double nose_verts[3][2] = {{0,0.15},
				   {0.65*std::cos(M_PI+0.75*M_PI/3.),    0.65*std::sin(M_PI+0.75*M_PI/3.)},
				   {0.65*std::cos(2.*M_PI-0.75*M_PI/3.), 0.65*std::sin(2.*M_PI-0.75*M_PI/3.)}};

  
  // face (unaffected by rotation)
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(0, nSamples, {0.,0.}, R_face, {0.,M_PI})));
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(1, nSamples, {0.,0.}, R_face, {M_PI,2.*M_PI})));

  // rotation matrix
  const double Rot[2][2] = {{std::cos(theta), -std::sin(theta)}, {std::sin(theta), std::cos(theta)}};
  
  double c_right_eye[2]     = {0.,0.};
  double c_right_eye_low[2] = {0.,0.};
  double c_left_eye[2]      = {0.,0.};
  double c_left_eye_low[2]  = {0.,0.};
  std::vector<std::array<double,2>> rot_nose_verts = {{0.,0.},{0.,0.},{0.,0.}};
  for(int i=0; i<2; ++i) {
    for(int j=0; j<2; ++j) {
      c_right_eye[i]     += Rot[i][j]*C_right_eye[j];
      c_right_eye_low[i] += Rot[i][j]*C_right_eye_low[j];
      c_left_eye[i]      += Rot[i][j]*C_left_eye[j];
      c_left_eye_low[i]  += Rot[i][j]*C_left_eye_low[j];
      for(int a=0; a<3; ++a) {
	rot_nose_verts[a][i] += Rot[i][j]*nose_verts[a][j];
      }
    }
  }
    
  // right eye
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(2, nSamples, {c_right_eye[0],    c_right_eye[1]},     r_eye, {0.+theta,M_PI+theta})));   
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(3, nSamples, {c_right_eye_low[0],c_right_eye_low[1]}, rhat,  {M_PI+alpha+theta,2.*M_PI-alpha+theta})));

  // left eye
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(4, nSamples, {c_left_eye[0],    c_left_eye[1]},     r_eye, {0.+theta,M_PI+theta})));  
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(5, nSamples, {c_left_eye_low[0],c_left_eye_low[1]}, rhat,  {M_PI+alpha+theta,2.*M_PI-alpha+theta})));
  
  // smile
  arcs.emplace_back(std::unique_ptr<demo::CircularArc>(new demo::CircularArc(6, nSamples, {0.,0.}, R_smile, {M_PI+M_PI/4.+theta,2.*M_PI-M_PI/4.+theta})));

  // corners of the nose
  segs.emplace_back(demo::LineSegmentData{.curve_id=7,.nSamples=nSamples,.A=rot_nose_verts[0],.B=rot_nose_verts[1]});
  segs.emplace_back(demo::LineSegmentData{.curve_id=8,.nSamples=nSamples,.A=rot_nose_verts[1],.B=rot_nose_verts[2]});
  segs.emplace_back(demo::LineSegmentData{.curve_id=9,.nSamples=nSamples,.A=rot_nose_verts[2],.B=rot_nose_verts[0]});

  // done
  return;
}
